#!/usr/bin/perl

my $libdir;
BEGIN {
 #find path to Physics-OPC-x.x.x/lib directory containing the OPC perl modules.
 use Cwd 'cwd';                      # to retrieve current working dir
 use File::Spec qw/splitdir catdir/; # to split or combine directory names
 #first check if environment variable is set
 $libdir = File::Spec->catdir($ENV{OPC_HOME}, 'lib');
 unless (-d $libdir) {
   #not found, now check in path to directory from which script is run 
   my @mydirs =  File::Spec->splitdir( Cwd->cwd() );
   while (@mydirs) {
     $libdir = File::Spec->catdir (@mydirs, $OPC_VERSiON, 'lib');
     last if -d $libdir;
     pop @mydirs;
   }
 }
}

use lib "$libdir";
use Physics::OPC;
use strict;

$VERBOSE = 0;

my $npoints = 351;         # number of grid points (square grid)
my $nslices = 800;	       # number of slices
my $dgrid = 3e-3;          # size of grid
my $mesh = 0.38e-3;  	     # size of mesh
my $grid = $npoints*$mesh; # width of grid (-grid/2 to grid/2)

my $cl = 299792458;        # speed of light in vacuum
my $pi = 3.14159265359;    # number pi
my $wavelength = 500e-9;   # radiation wavelength
my $trms=10.0e-14;         # rms duration of pulse

my %units = ( bw => "nm",
	            lambda_avg => "nm",
	            sx => "m",
	            sy => "m",
	            sr => "m",
	            xc => "m",
	            yc => "m",
	            w  => "m");
$units{tot} = $nslices > 1 ? "J" : "W";

$mesh = 2*$dgrid/($npoints-1);  # recalculate distance between meshpoints

# initialise the optical field (pulse is initialised at the postiion of the waist
print "Creating input field ...\n";
$field = field
	mode_type => 'hermite',
	mx => 0,
	my => 0,
	p0 => 1.0e6,
	w0 => 1e-3,
	z0 => 0.0,
	tprofile => 'gaussian',
	trms => $trms,
	rad_offset => 0.0e-12,
	lambda => $wavelength,
	npoints => $npoints,
	mesh => $mesh,
	nslices => $nslices;
	
# set_param $field export_wisdom => "single";

# comment out the next line if you want to run on a single core
$USE_MPI{optics} ="/opt/intel/openmpi/bin/mpiexec -n 6";

# in the following we calculate the properties of the pulse,
# transform to the frequency domain and determine the properties again
# then we calculatute the time-bandwidth product and compare it with
# the theoretical value of 0.5 for a Gaussian Transform limited pulse

# example of direct calculation of rms duration of the optical field
# as derived from the P(t) data.

print "Calculating rms pulse duration from P(t) ...\n";
my @power = total($field);

my $tav=0.0;
my $t2 =0.0;
my $tot=0.0;
foreach my $i (1..$nslices) {
	my $t = ($i-1)*$wavelength/$cl;
	$tot += $power[$i-1];
	$tav += $power[$i-1]*$t;
	$t2 += $power[$i-1]*$t**2;
}

my %mystat = statistics($field);
print "initial energy = $mystat{tot}\n";

$tav = $tav/$tot;
my $sdev = $t2/$tot - $tav**2;
my $sigma_t = sqrt($sdev);

# transform to frequency domain
fourier($field);

my $optics = optics("stat var=t1
                     fresnel z=10
                     stat var=t2
                     ");

# retrieve frequency spacing between the slices
my $dfreq = get_param $field "dfreq";

# example of how to retrieve the spectrum
# my @variable=spectrum($field,file,slice,x,y)
#
# $field contains the optical field 
# file   name of an output file to contain the data
# slice  if equal to zero, output all frequencies, 
#        otherwise only a specific frequency
# x,y    (optional) if specified determine spectrum at 
#        given location in grid (point or line) 
#
print "Calculating on-axis spectral width ...\n";
my @spectrum=spectrum($field,undef,0,0.0,0.0);

# calculate rms width in m, use first frequency as the
# spectral power density is equally spaced in frequency
my $fav=0.0;
my $f2 =0.0;
my $tot=0.0;
foreach my $i (1..$nslices) {
	my $f = $cl/$wavelength + $dfreq*($i-$nslices/2);
	$tot += $spectrum[$i-1];
	$fav += $spectrum[$i-1]*$f;
	$f2  += $spectrum[$i-1]*$f**2;
}
$fav = $fav/$tot;
my $sdev = $f2/$tot - $fav**2;
my $sigma_f = sqrt($sdev);
# convert to bandwidth in m
my $bw_f = $sigma_f*$wavelength**2/$cl;

print "bw on-axis = $bw_f\n";

# determine optical pulse properties using complete pulse
%mystat = statistics($field);  

foreach my $key (sort {lc $a cmp lc $b} keys %mystat) {
  print "$key \t: $mystat{$key}  $units{$key}\n" if defined $mystat{$key};
}

run $optics;

foreach my $key (sort {lc $a cmp lc $b} keys %{$optics_data{stat_t1}{data}}) {
  print "$key \t: $optics_data{stat_t1}{data}{$key}  $units{$key}\n" if defined $optics_data{stat_t1}{data}{$key};
}



#spectral width in angular frequency using complete pulse
my $bw_rms_omega = 2*$pi*$cl*$mystat{bw}/($wavelength**2);

print "center of pulse (time) = ",sprintf("%12.6e",$tav)," s\n";
print "rms duration (time)    = ",sprintf("%12.6e",$sigma_t)," s\n";
print "input rms duration)    = ",sprintf("%12.6e",$trms)," s\n";
print "rms bandwidth (omega)  = ",sprintf("%12.6e",$bw_rms_omega)," Hz\n";
print "time-bandwidth product = ",sprintf("%.6f",$trms*$bw_rms_omega),"\n";
  
my %bw = bandwidth($field, undef);

for my $key (sort {$a <=> $b} keys %bw) {
  print sprintf("%12.3e \t%12.3e \n",$key,$bw{$key}); 
}
