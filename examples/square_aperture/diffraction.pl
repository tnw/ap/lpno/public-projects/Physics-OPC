#!/usr/bin/perl

my $libdir;

BEGIN {
 use Cwd 'cwd';             # to retrieve current working dir
 use File::Spec qw/splitdir catdir/; # to split or combine directory names
 my @mydirs =  File::Spec->splitdir( Cwd->cwd() );
 while (@mydirs) {
   $libdir = File::Spec->catdir (@mydirs, $OPC_VERSiON, 'lib');
   last if -d $libdir;
   pop @mydirs;
 }
}

use lib "$libdir";

use Physics::OPC;
$VERBOSE = 0;

my $width = 0.005;	# width of aperture

my $npoints = 270;	# number of grid points, choose a product of 2,3,5 and 7.
$npoints = 3*3*5*7;
my $grid = 0.10;      # initial guess of grid size (-dgrid/2 to dgrid/2)
my $mesh =  $grid/($npoints-1); # distance betweeen grid points

# adjust mesh such that a whole number of points with associated mesh area falls
# within the aperture

my $nap = int($width/$mesh);
$mesh = $width/$nap;
$grid = ($npoints-1)*$mesh;

$field = field
	mode_type	=> 'plane',
	mx      => 0,
	my      => 0,
	w0			=> 2e-2,
	z0      => 1.0,
	trms    => 100e-15,
	tprofile => 'gaussian',
	p0	    => 1,
	rad_offset => 0.0,
	lambda	=> 4E-5,	
	mesh_x	=> $mesh,
	mesh_y	=> $mesh,
	npoints_x	=> $npoints,
	npoints_y	=> $npoints,
	nslices => 1;
	
copy $field => "gaussian_y.dfl";
set_param $field field_next=>"gaussian_y.dfl";
	

$optics = optics "
#  spectral z=1
#	fresnel z=1
	dump var=ba
	square	d=0.005
	dump var=aa
#	square	d=0.005 xoff=0.0025
#	tilt	xr=0.001
	fresnel	z=2
#	fresnel A=1 D=1 M=1 B=2
#	spectral z=2
" ;

unlink 'diffraction.cross.txt';
unlink 'diffraction.dfl';

run   $optics;

print "Performing analysis on fiel(s) ...\n";

cross $field '>diffraction_x.cross.txt';
move  $field => 'diffraction.dfl';

my @tot = total $field;
print "Total diffracted power in Ex is $_\n" for @tot;

