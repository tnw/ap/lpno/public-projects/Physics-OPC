#!/usr/bin/perl

# Library headers
my $libdir;

BEGIN {
 use Cwd 'cwd';             # to retrieve current working dir
 use File::Spec qw/splitdir catdir/; # to split or combine directory names
 my @mydirs =  File::Spec->splitdir( Cwd->cwd() );
 while (@mydirs) {
   $libdir = File::Spec->catdir (@mydirs, $OPC_VERSiON, 'lib');
   last if -d $libdir;
   pop @mydirs;
 }
}

use lib "$libdir";
use Physics::OPC;
$VERBOSE = 0;

$var1 = mask {type=>'zernike', n=>2, m=>2, R=>0.0045, A=>-20},
             {type=>'zernike', n=>2, m=>0, R=>0.0045, A=>-10};
my $npoints = 630;   # number of grid points
my $grid = 0.016;     # width of grid (-grid/2 to grid/2)
my $mesh = $grid/($npoints-1);

$field = field
    mode_type => 'plane',
    p0 => 1,
    lambda => 1.0e-6,
    npoints_x => $npoints,
    npoints_y => $npoints,
    mesh_x => $mesh,
    mesh_y => $mesh,
    nslices => 1;

$optics = optics "
    diaphragm r=0.0045
    fresnel z=0.5
    dump var=input
    opld var=var1
    fresnel z=1.55";
    
run $optics;
move $field => "focus.dfl";
move $input => "input.dfl";

# remove temporary files
unlink glob "op* var1*";
