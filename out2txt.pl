#!/usr/bin/perl

# Script to extract columns from Genesis ".out" files

die "Usage: $0 FILE COLUMN_NAME\n" unless @ARGV == 2;

$file = shift @ARGV;
$name = shift @ARGV;

open IN, $file or die $!;

$state = 0;
$col = undef;
while (<IN>) {
	if ($state == 0) {
		$state = 1 if /^\*+\s+output:\s+slice\s+(\d+)/;
		warn "slice $1\n" if $state == 1;
	}
	elsif ($state == 1) {
		@cols = grep /\w/, split /\s+/, $_;
		($col) = grep { $cols[$_] eq $name } 0 .. $#cols;
		$state = 2 if defined $col
	}
	elsif ($state == 2) {
		if (not /^\s*\d/) {
			$state = 0;
			print "\n";
			next;
		}

		@cols = grep /\d/, split /\s+/, $_;
		print $cols[$col], "\n";
	}
}

close IN
