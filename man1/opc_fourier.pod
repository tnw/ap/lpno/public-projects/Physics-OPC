=pod

=head1 NAME

opc_fourier - Frequency dependent optical propagation

=head1 DESCRIPTION

This program propagates each spectral component of an optical field
to obtain correct propagation of short pulses and allow for dispersive
media

=head1 COPYRIGHT

Copyright (C) 2006 J.G. Karssenberg, P.J.M. van der Slot, I.V.Volokhine

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; version 2 of the
License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

=cut

