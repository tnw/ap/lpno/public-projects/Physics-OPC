=pod

=head1 NAME

opc_utils - Utilities for the Optical Propagation Code

=head1 DESCRIPTION

This program contains a number of utilities useful for
the optical propagation code.

B<See the OPC manual for more information.>

=head2 Input

The program asks for a config file. This file should contain a
namelist called "optics". The program then ask for a command
to execute. The command may require additional input parameters. 

=head2 Output

Depending on the command executed, the program generates one or
more files. 

=head1 COPYRIGHT

Copyright (C) 2007 J.G. Karssenberg, P.J.M. van der Slot

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; version 2 of the
License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

=cut

