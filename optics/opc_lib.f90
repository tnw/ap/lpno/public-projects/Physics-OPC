MODULE opc_lib

! ##################################################################
! Copyright (C) 2006-2014 J.G. Karssenberg, P.J.M. van der Slot,
! I.V.Volokhine
!
! This program is free software; you can redistribute it and/or
! modify it under the terms of the GNU General Public License as
! published by the Free Software Foundation; version 2 of the
! License.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! ##################################################################

CONTAINS

  SUBROUTINE read_dfl(filename, irec, var)
  ! ==================================================================
  ! Read binary fieldfile and store it in var 
  ! ------------------------------------------------------------------

  USE opc_globals
#ifdef MPI
    USE mpi
#endif
  IMPLICIT NONE ! be strict      

  CHARACTER (len=100), INTENT(IN)              :: filename
  INTEGER, INTENT(IN)                          :: irec
  INTEGER                                      :: ioff_x, iend_x, ioff_y, iend_y
  COMPLEX (kind=rk16), dimension(:,:), POINTER :: var
  INTEGER                                      :: IFH
#ifdef MPI
  INTEGER (kind=MPI_OFFSET_KIND)               :: offset
  INTEGER                                      :: amode, mpi_err
  INTEGER                                      :: istatus(MPI_STATUS_SIZE)
#else
  INTEGER                                      :: recsize, ix, iy, ierr
#endif
     
  CALL field_offset(ioff_x, iend_x, ioff_y, iend_y)
#ifdef MPI
  amode = MPI_MODE_RDONLY
  CALL MPI_FILE_OPEN(MPI_COMM_SELF, trim(filename), amode, MPI_INFO_NULL, IFH, mpi_err)
  IF (mpi_err /= MPI_SUCCESS) THEN
    PRINT *,'opc_lib.f90::read_dfl -> Error opening file ', trim(filename), ' for input, IO-status = ',mpi_err
    CALL MPI_FINALIZE(mpi_err)
    STOP 
  END IF
  offset = (irec-1) * IO_recsize
  CALL MPI_FILE_SET_VIEW(IFH, offset, MPI_DOUBLE_COMPLEX, IO_filetype, 'native',  MPI_INFO_NULL, mpi_err)
  CALL MPI_FILE_READ(IFH,var(ioff_x:iend_x,ioff_y:iend_y),(iend_x-ioff_x+1)*(iend_y-ioff_y+1), & 
       MPI_DOUBLE_COMPLEX, istatus, mpi_err)
  IF (mpi_err /= MPI_SUCCESS) THEN
    PRINT *,'opc_lib.f90::read_dfl -> error reading file ', trim(filename), ' IO-status = ',mpi_err
    CALL MPI_FILE_CLOSE(IFH,mpi_err)
    CALL MPI_FINALIZE(mpi_err)
    STOP
  END IF
  CALL MPI_FILE_CLOSE(IFH, mpi_err)
#else
  recsize = Nx*Ny*16
  OPEN( unit=FHID, file=trim(filename), recl=recsize, access='direct', status='old', iostat=ierr )
  !OPEN( unit=FHID, file=trim(filename), recl=recsize, access='direct', status='old')
  IF (ierr /= 0) call opc_abort("opc_lib.f90::read_dfl","Error opening file " // trim(filename))
  read( unit=FHID, rec=irec, iostat=ierr ) ((var(ix,iy), ix=ioff_x, iend_x), iy=ioff_y, iend_y)
  IF (ierr /= 0) call opc_abort('opc_lib.f90::read_dfl','error reading file ' // trim(filename))
  CLOSE(FHID)
#endif
 ! PRINT *,'returning from read_dfl, max value is ',maxval(abs(var))
  RETURN
  END SUBROUTINE read_dfl ! subroutine read_dfl


  SUBROUTINE write_dfl(filename, irec, var)
  ! ==================================================================
  ! Write binary fieldfile with data from var
  ! ------------------------------------------------------------------

  USE opc_globals
#ifdef MPI
  USE mpi
#endif
  IMPLICIT NONE ! be strict
      
  CHARACTER (len=100), INTENT(IN)              :: filename
  INTEGER, INTENT(IN)                          :: irec
  INTEGER                                      :: ioff_x, iend_x, ioff_y, iend_y
  COMPLEX (kind=rk16), dimension(:,:), POINTER :: var
  INTEGER                                      :: OFH
#ifdef MPI
  INTEGER (kind=MPI_OFFSET_KIND)               :: offset
  INTEGER                                      :: amode, mpi_err
  INTEGER                                      :: istatus(MPI_STATUS_SIZE)
#else
  INTEGER                                      :: recsize, ix, iy, ierr
#endif
     
  CALL field_offset(ioff_x, iend_x, ioff_y, iend_y)

#ifdef MPI
  amode = IOR(MPI_MODE_CREATE,MPI_MODE_WRONLY)
  CALL MPI_FILE_OPEN(MPI_COMM_SELF, trim(filename), amode, MPI_INFO_NULL, OFH, mpi_err)
  IF (mpi_err == MPI_SUCCESS) THEN
    offset = (irec-1) * IO_recsize 
    CALL MPI_FILE_SET_VIEW(OFH, offset, MPI_DOUBLE_COMPLEX, IO_filetype, 'native',  MPI_INFO_NULL, mpi_err)
    CALL MPI_FILE_WRITE(OFH,var(ioff_x:iend_x,ioff_y:iend_y),(iend_x-ioff_x+1)*(iend_y-ioff_y+1), &
         MPI_DOUBLE_COMPLEX, istatus, mpi_err)
    IF (mpi_err /= MPI_SUCCESS) THEN
      PRINT *,'opc_lib.f90::write_dfl -> error writing to filename ', trim(filename), ' IO-status = ', mpi_err
      CALL MPI_FILE_CLOSE(OFH, mpi_err)
      CALL MPI_FINALIZE(mpi_err)
      STOP
    END IF
    CALL MPI_FILE_CLOSE(OFH, mpi_err)
  ELSE
    PRINT *,'opc_lib.f90::write_dfl -> Error opening file ', trim(filename), ' for output, IO-status = ',mpi_err
    CALL MPI_FINALIZE(mpi_err)
    STOP
  END IF
#else
  recsize = Nx*Ny*16
  OPEN( unit=FHOD, file=trim(filename), recl=recsize, access='direct', status='unknown', iostat=ierr )
  ! OPEN( unit=FHOD, file=trim(filename), recl=recsize, access='direct', status='unknown')
  IF (ierr /= 0) THEN
    PRINT *,'opc_lib.f90::write_dfl -> Error opening file ', trim(filename), ' for output, IO-status = ',ierr
    STOP
  END IF
  WRITE( unit=FHOD, rec=irec, iostat=ierr ) ((var(ix,iy), ix=ioff_x, iend_x), iy=ioff_y, iend_y)
  CLOSE(FHOD)
  IF (ierr /= 0) THEN
    PRINT *,'opc_lib.f90::write_dfl -> error writing to filename ', trim(filename), ' IO-status = ', ierr
    STOP
  END IF
#endif   
  RETURN
  END SUBROUTINE write_dfl ! subroutine write_dfl



  SUBROUTINE read_opld(filename, ngrid, var)
  ! ==================================================================
  ! Read binary optical path length difference file and store it 
  ! in var
  ! ------------------------------------------------------------------

  USE opc_globals
#ifdef MPI
  USE mpi
#endif
  IMPLICIT NONE ! be strict
      
  CHARACTER(len=100), INTENT(IN)              :: filename
  INTEGER, INTENT(IN)                         :: ngrid
  INTEGER                                     :: ioff_x, iend_x, ioff_y, iend_y
  TYPE(opld_grid), dimension(:), POINTER      :: var
  INTEGER                                     :: PFH
#ifdef MPI
  INTEGER (kind=MPI_OFFSET_KIND)              :: offset
  INTEGER                                     :: mpi_err, amode
  INTEGER                                     :: istatus(MPI_STATUS_SIZE)
#else
  INTEGER                                     :: irec, recsize, ix, iy, ierr
#endif

  CALL field_offset(ioff_x, iend_x, ioff_y, iend_y)

#ifdef MPI
  amode = MPI_MODE_RDONLY
  CALL MPI_FILE_OPEN(MPI_COMM_SELF, trim(filename), amode, MPI_INFO_NULL, PFH, mpi_err)
  IF (mpi_err == MPI_SUCCESS) THEN
    offset = 0
    CALL MPI_FILE_SET_VIEW(PFH, offset, MPI_DOUBLE_PRECISION, MPI_DOUBLE_PRECISION, 'native',  MPI_INFO_NULL, mpi_err)
    CALL MPI_FILE_READ(PFH,var(ngrid) % r(ioff_x:iend_x,ioff_y:iend_y),(iend_x-ioff_x+1)*(iend_y-ioff_y+1), &
         MPI_DOUBLE_PRECISION, istatus, mpi_err)
    IF (mpi_err /= MPI_SUCCESS) THEN
      PRINT *,'opc_lib.f90::read_opld -> error reading file ', trim(filename), ' IO-status = ',mpi_err
      CALL MPI_FILE_CLOSE(PFH,mpi_err)
      CALL MPI_FINALIZE(mpi_err)
      STOP
    END IF
    CALL MPI_FILE_CLOSE(PFH,mpi_err)
  ELSE
    PRINT *,'opc_lib.f90::read_opld -> Error opening file ', trim(filename), ' for input, IO-status = ',mpi_err
    CALL MPI_FINALIZE(mpi_err)
    STOP 
  END IF
#else
  recsize=Nx*Ny*8 ! record size in binary files
  irec = 1

  OPEN( unit=FHIO, file=trim(filename), recl=recsize, access='direct', status='old', iostat=ierr )
  IF (ierr == 0) THEN
    read( unit=FHIO, rec=irec, iostat=ierr ) ((var(ngrid) % r(ix,iy), ix=ioff_x, iend_x), iy=ioff_y, iend_y)
    CLOSE( unit=FHIO )
    IF (ierr /=0) THEN
      PRINT *,'opc_lib.f90::read_opld -> error reading from ', trim(filename), ' IO-status = ',ierr
      STOP
    END IF
  ELSE
    PRINT *,'opc_lib.f90::read_opld -> error opening ', trim(filename), ' IO-status = ',ierr
    STOP
  END IF
#endif
  RETURN
  END SUBROUTINE read_opld ! subroutine read_opld



  SUBROUTINE write_opld(filename, ngrid, var)
  ! ==================================================================
  ! Write binary optical path length difference file with data 
  ! from var
  ! ------------------------------------------------------------------

  USE opc_globals
#ifdef MPI
  USE mpi
#endif
  IMPLICIT NONE ! be strict
      
  CHARACTER(len=100),INTENT(IN)              :: filename
  INTEGER,INTENT(IN)                         :: ngrid
  TYPE(opld_grid), dimension(:), POINTER     :: var
  INTEGER                                    :: ioff_x, iend_x, ioff_y, iend_y
  INTEGER                                    :: PFH
#ifdef MPI
  INTEGER (kind=MPI_OFFSET_KIND)             :: offset
  INTEGER                                    :: mpi_err, amode
  INTEGER                                    :: istatus(MPI_STATUS_SIZE)
#else
  INTEGER                                    :: irec, recsize, ix, iy, ierr
#endif
      
  CALL field_offset(ioff_x, iend_x, ioff_y, iend_y)

#ifdef MPI
  amode = IOR(MPI_MODE_CREATE,MPI_MODE_WRONLY)
  CALL MPI_FILE_OPEN(MPI_COMM_SELF, trim(filename), amode, MPI_INFO_NULL, PFH, mpi_err)
  IF (mpi_error == MPI_SUCCESS) THEN
    offset = 0
    CALL MPI_FILE_SET_VIEW(PFH, offset, MPI_DOUBLE_PRECISION, MPI_DOUBLE_PRECISION, 'native',  MPI_INFO_NULL, mpi_err)
    CALL MPI_FILE_WRITE(PFH,var(ngrid) % r(ioff_x:iend_x,ioff_y:iend_y),(iend_x-ioff_x+1)*(iend_y-ioff_y+1), &
         MPI_DOUBLE_PRECISION, istatus, mpi_err)
    IF (mpi_err /= MPI_SUCCESS) THEN
      PRINT *,'opc_lib.f90::write_opld -> error writing to filename ', trim(filename), ' IO-status = ', mpi_err
      CALL MPI_FILE_CLOSE(PFH, mpi_err)
      CALL MPI_FINALIZE(mpi_err)
      STOP
    END IF
    CALL MPI_FILE_CLOSE(PFH,mpi_err)
  ELSE
    PRINT *,'opc_lib.f90::write_opld -> Error opening file ', trim(filename), ' for output, IO-status = ',mpi_err
    CALL MPI_FINALIZE(mpi_err)
    STOP
  END IF
#else
  recsize=Nx*Ny*8 ! record size in binary files
  irec = 1

  OPEN( unit=FHOO, file=trim(filename), recl=recsize, access='direct', status='unknown', iostat=ierr )
  IF (ierr == 0) THEN
    WRITE( unit=FHOO, rec=irec, iostat=ierr ) ((var(ngrid) % r(ix,iy), ix=ioff_x, iend_x), iy=ioff_y, iend_y)
    CLOSE( unit=FHOO )
    IF (ierr /= 0) THEN 
      PRINT *,'opc_lib.f90::write_opld -> error writing ', trim(filename), ' IO-status = ', ierr
      STOP
    END IF
  ELSE 
    PRINT *,'opc_lib.f90::write_opld -> error opening ', trim(filename), ' IO-status = ', ierr
    STOP
  END IF
#endif
  RETURN
  END SUBROUTINE write_opld ! subroutine write_opld
  
  
  FUNCTION read_parameter_int(paramfile, var, dummy)
  ! returns one parameter from a .param file that is associated with a .dfl 
  ! file. This function is part of an group of overloaded functions to provide
  ! return values of different type. Currently, integer, real and string are
  ! supported. This function may not access opc_globals to avoid modifying 
  ! global variables.
  use kinds, only : rk16
  IMPLICIT NONE ! be strict
  
  CHARACTER(len=*), INTENT (IN)  :: paramfile
  CHARACTER(len=*), INTENT (IN)  :: var
  INTEGER                        :: read_parameter_int
  INTEGER                        :: dummy
! variables in the namelist
  CHARACTER(len=100)             :: infile, scriptfile, outfile, field_next
  REAL(KIND=rk16)                :: lambda, dfreq, mesh_x, mesh_y, mesh, zsep,&
                                    Mx, My
  INTEGER                        :: npoints, npoints_x, npoints_y, Nx, Ny,    &
                                    nslices, nxcross, nycross, nstat, ntotal, &
                                    nfluence, nspectrum 
! other variables 
  INTEGER                        :: ierr 
  INTEGER, PARAMETER             :: FHIC = 33
  namelist /optics/                                                         &
    ! Files
    infile, scriptfile, outfile, field_next,                                  &
    ! Field parameters
    lambda, dfreq, npoints, npoints_x, npoints_y, Nx, Ny, mesh_x,             &
    mesh, mesh_y, nslices, zsep,                                              &
    ! Arrays sizes for output to perl script
    nxcross, nycross, nstat, ntotal, nfluence, nspectrum,                     &
    ! Unused parameters
    Mx, My       
  ! open param file   
  OPEN(unit=FHIC,file=trim(paramfile),status='old', iostat=ierr)
  IF (ierr == 0) THEN
  	! set integer variables to zero 
  	npoints=0
  	npoints_x=0
  	npoints_y=0
  	Nx=0
  	Ny=0
  	nslices=0
    READ(unit=FHIC,nml=optics,iostat=ierr)
    IF (ierr /=0 ) THEN
      print *,ierr
      STOP 'opc_lib.f90::read_parameter_int -> Error while reading param file ' 
    END IF 
    SELECT CASE (trim(var))
      CASE('npoints') 
        read_parameter_int = npoints
      CASE('npoints_x') 
        read_parameter_int = npoints_x
      CASE('npoints_y') 
        read_parameter_int = npoints_y
      CASE('Nx') 
        read_parameter_int = Nx
      CASE('Ny') 
        read_parameter_int = Ny
      CASE('nslices') 
        read_parameter_int = nslices
      CASE DEFAULT 
        read_parameter_int = 0
    END SELECT     
    CLOSE(unit=FHIC)
  ELSE
    STOP 'opc_lib.f90::read_parameter_int -> Error, could not open param file '
  END IF
  RETURN                                                           
  END FUNCTION  read_parameter_int


  FUNCTION read_parameter_real(paramfile, var, dummy)
  ! returns one parameter from a .param file that is associated with a .dfl 
  ! file. This function is part of an group of overloaded functions to provide
  ! return values of different type. Currently, integer, real and string are
  ! supported. This function may not access opc_globals to avoid modifying 
  ! global variables.
  use kinds, only : rk16
  IMPLICIT NONE ! be strict
  
  CHARACTER(len=*), INTENT (IN)  :: paramfile
  CHARACTER(len=*), INTENT (IN)  :: var
  REAL(KIND=rk16)                :: read_parameter_real
  REAL(KIND=rk16)                :: dummy
! variables in the namelist
  CHARACTER(len=100)             :: infile, scriptfile, outfile, field_next
  REAL(KIND=rk16)                :: lambda, dfreq, mesh_x, mesh_y, mesh, zsep,&
                                    Mx, My
  INTEGER                        :: npoints, npoints_x, npoints_y, Nx, Ny,    &
                                    nslices, nxcross, nycross, nstat, ntotal, &
                                    nfluence, nspectrum 
! other variables 
  INTEGER                        :: ierr 
  INTEGER, PARAMETER             :: FHIC = 33
  namelist /optics/                                                         &
    ! Files
    infile, scriptfile, outfile, field_next,                                  &
    ! Field parameters
    lambda, dfreq, npoints, npoints_x, npoints_y, Nx, Ny, mesh_x,             &
    mesh, mesh_y, nslices, zsep,                                              &
    ! Arrays sizes for output to perl script
    nxcross, nycross, nstat, ntotal, nfluence, nspectrum,                     &
    ! Unused parameters
    Mx, My       
  ! open param file   
  OPEN(unit=FHIC,file=trim(paramfile),status='old', iostat=ierr)
  IF (ierr == 0) THEN
  	! set integer variables to zero 
  	lambda=0.0d0
  	dfreq=0.0d0
  	mesh_x=0.0d0
  	mesh_y=0.0d0
  	mesh=0.0d0
  	zsep=0.0d0
  	Mx=0.0d0
  	My=0.0d0
    READ(unit=FHIC,nml=optics,iostat=ierr)
    IF (ierr /=0 ) THEN
      print *,ierr
      STOP 'opc_lib.f90::read_parameter_real -> Error while reading param file ' 
    END IF 
    SELECT CASE (trim(var))
      CASE('lambda') 
        read_parameter_real = lambda
      CASE('dfreq')  
        read_parameter_real = dfreq
      CASE('mesh_x') 
        read_parameter_real = mesh_x
      CASE('mesh_y') 
        read_parameter_real = mesh_y
      CASE('mesh')   
        read_parameter_real = mesh
      CASE('zsep')   
        read_parameter_real = zsep
      CASE('Mx')     
        read_parameter_real = Mx
      CASE('My')     
        read_parameter_real = My
      CASE DEFAULT 
        read_parameter_real = 0.0d0
    END SELECT     
    CLOSE(unit=FHIC)
  ELSE
    STOP 'opc_lib.f90::read_parameter_real -> Error, could not open param file '
  END IF
  RETURN                                                           
  END FUNCTION  read_parameter_real


  FUNCTION read_parameter_string(paramfile, var, dummy)
  ! returns one parameter from a .param file that is associated with a .dfl 
  ! file. This function is part of an group of overloaded functions to provide
  ! return values of different type. Currently, integer, real and string are
  ! supported. This function may not access opc_globals to avoid modifying 
  ! global variables.
  use kinds, only : rk16
  IMPLICIT NONE ! be strict
  
  CHARACTER(len=*), INTENT (IN)  :: paramfile
  CHARACTER(len=*), INTENT (IN)  :: var
  CHARACTER(len=100)             :: read_parameter_string
  CHARACTER(len=100)             :: dummy
! variables in the namelist
  CHARACTER(len=100)             :: infile, scriptfile, outfile, field_next
  REAL(KIND=rk16)                :: lambda, dfreq, mesh_x, mesh_y, mesh, zsep,&
                                    Mx, My
  INTEGER                        :: npoints, npoints_x, npoints_y, Nx, Ny,    &
                                    nslices, nxcross, nycross, nstat, ntotal, &
                                    nfluence, nspectrum 
! other variables 
  INTEGER                        :: ierr 
  INTEGER, PARAMETER             :: FHIC = 33
  
  namelist /optics/                                                         &
    ! Files
    infile, scriptfile, outfile, field_next,                                  &
    ! Field parameters
    lambda, dfreq, npoints, npoints_x, npoints_y, Nx, Ny, mesh_x,             &
    mesh, mesh_y, nslices, zsep,                                              &
    ! Arrays sizes for output to perl script
    nxcross, nycross, nstat, ntotal, nfluence, nspectrum,                     &
    ! Unused parameters
    Mx, My       
  ! open param file   
  OPEN(unit=FHIC,file=trim(paramfile),status='old', iostat=ierr)
  IF (ierr == 0) THEN
  	! set string variables to zero length string 
    field_next=''
    READ(unit=FHIC,nml=optics,iostat=ierr)
    IF (ierr /=0 ) THEN
      print *,ierr
      STOP 'opc_lib.f90::read_parameter_string -> Error while reading param file ' 
    END IF 
    SELECT CASE (trim(var))
      CASE('field_next') 
        read_parameter_string = trim(field_next)
      CASE DEFAULT 
        read_parameter_string = ''
    END SELECT    
     
    CLOSE(unit=FHIC)
  ELSE
    STOP 'opc_lib.f90::read_parameter_string -> Error, could not open param file '
  END IF
  RETURN                                                           
  END FUNCTION  read_parameter_string


  SUBROUTINE write_param(paramfile)
  ! writes the .param file associated with a .dfl file. Mostly not needed. However, 
  ! when propagating several .dfl files in a single execution of opc_optics, may 
  ! require writing the .param file (e.g. different polarisations or harmonics
  ! writes only parameters following new format.
  use opc_globals
  IMPLICIT NONE ! be strict
  
  CHARACTER(len=*), INTENT (IN)  :: paramfile
  INTEGER                        :: npoints_x, npoints_y, ierr 
  REAL(KIND=rk16)                :: mesh
  NAMELIST /optics/                                                      &
    ! Files
    field_next,                                                           &
    ! Field parameters
    lambda, dfreq, npoints_x, npoints_y, mesh_x, mesh_y, nslices, zsep, &
    ! Unused parameters
    Mx, My      
  ! assign values to nonglobal variables
   npoints_x=Nx
   npoints_y=Ny
  ! open param file   
  OPEN(unit=FHIC,file=paramfile,status='replace', iostat=ierr)
  IF (ierr == 0) THEN
    WRITE(unit=FHIC,nml=optics,iostat=ierr)
    IF (ierr /=0 ) THEN
      print *,ierr
      STOP 'opc_lib.f90::write_param -> Error while writing to param file '
    END IF      
    CLOSE(unit=FHIC)
  ELSE
    STOP 'opc_lib.f90::write_param -> Error, could not write param file '
  END IF
  RETURN                                                           
  END SUBROUTINE  write_param  
  
  
  SUBROUTINE opc_init(conffile)
  ! reads the init file generated by PERL to initialise the fortran propagation code
  ! Starting from version 0.7.5, mesh -> mesh_x, mesh_x and npoints -> npoints_x and npoints_y
  ! To be compatible with older version, the variables mesh and npoints are still available.
  !
    USE opc_globals   
    USE fftw3
#ifdef MPI
    USE mpi
#endif
    IMPLICIT NONE ! be strict
 
    INTEGER              :: npoints, npoints_x, npoints_y, ierr, mpi_rank, mpi_size
    LOGICAL              :: perl_output ! signals to write to internal file to pass data to perl script
    REAL(KIND=rk16)      :: mesh
    CHARACTER(len=100)   :: conffile
#ifdef MPI
    INTEGER                        :: mpi_err
    INTEGER(KIND=MPI_ADDRESS_KIND) :: lb
#endif
 
    namelist /optics/                                                         &
    ! Files
    infile, scriptfile, outfile, field_next,                                  &
    ! Field parameters
    lambda, dfreq, npoints, npoints_x, npoints_y, Nx, Ny, mesh_x,             &
    mesh, mesh_y, nslices, zsep,                                              &
    ! Arrays sizes for output to perl script
    nxcross, nycross, nstat, ntotal, nfluence, nspectrum,                     &
    ! Unused parameters
    Mx, My,                                                                   &
    ! save wisdom files
    export_wisdom  
                                                                
    NULLIFY(res_xcross, xcross_name,xcross_fname)
    ! determine rank 
#ifdef MPI
    call MPI_COMM_RANK(MPI_COMM_WORLD, mpi_rank, mpi_err)
    call MPI_COMM_SIZE(MPI_COMM_WORLD, mpi_size, mpi_err)
#else
    mpi_rank = 0
    mpi_size = 1
#endif 
    ! initialise variables
    npoints = 0
    npoints_x = 0
    npoints_y = 0
 	  Nx = 0
    Ny = 0
    nxcross = 0
    nycross = 0
    nstat = 0
    ntotal = 0
    nfluence = 0
    nspectrum = 0
    perl_output = .FALSE.
    nslices = 1
    lambda = zero
    dfreq = zero
    mesh = 0
    mesh_x = zero
    mesh_y = zero
    zsep = zero
    field_next = 'none'
    export_wisdom = 'single'! options are none (no wisdom files are written), single (only one file is written),
                            ! all (each subprocess writes and reads its own wisdom file)
    IF (mpi_rank == 0) THEN ! open, read and close config file, only be master process
      OPEN(unit=FHIC,file=conffile,status='old', iostat=ierr)
      IF (ierr /= 0) CALL opc_abort('opc_lib.f90::opc_init', 'Error opening config file'//trim(conffile))
      READ(unit=FHIC,nml=optics,iostat=ierr)
      IF (ierr /=0 ) CALL opc_abort('opc_lib.f90::opc_init', 'Error while reading config file'//trim(conffile) )
      CLOSE(unit=FHIC)
    END IF
#ifdef MPI
    IF (mpi_size > 1) THEN ! running on multiple cores, send data to other processes
    	CALL mpi_bcast(infile,100,mpi_character,0,mpi_comm_world,mpi_err)
    	CALL mpi_bcast(scriptfile,100,mpi_character,0,mpi_comm_world,mpi_err)
    	CALL mpi_bcast(outfile,100,mpi_character,0,mpi_comm_world,mpi_err)
    	CALL mpi_bcast(field_next,100,mpi_character,0,mpi_comm_world,mpi_err)
    	CALL mpi_bcast(lambda,1,mpi_double_precision,0,mpi_comm_world,mpi_err)
    	CALL mpi_bcast(dfreq,1,mpi_double_precision,0,mpi_comm_world,mpi_err)
    	CALL mpi_bcast(npoints,1,mpi_integer,0,mpi_comm_world,mpi_err)
    	CALL mpi_bcast(npoints_x,1,mpi_integer,0,mpi_comm_world,mpi_err)
    	CALL mpi_bcast(npoints_y,1,mpi_integer,0,mpi_comm_world,mpi_err)
    	CALL mpi_bcast(Nx,1,mpi_integer,0,mpi_comm_world,mpi_err)
    	CALL mpi_bcast(Ny,1,mpi_integer,0,mpi_comm_world,mpi_err)
    	CALL mpi_bcast(mesh,1,mpi_double_precision,0,mpi_comm_world,mpi_err)
    	CALL mpi_bcast(mesh_x,1,mpi_double_precision,0,mpi_comm_world,mpi_err)
    	CALL mpi_bcast(mesh_y,1,mpi_double_precision,0,mpi_comm_world,mpi_err)
    	CALL mpi_bcast(nslices,1,mpi_integer,0,mpi_comm_world,mpi_err)
    	CALL mpi_bcast(zsep,1,mpi_double_precision,0,mpi_comm_world,mpi_err)
    	CALL mpi_bcast(nxcross,1,mpi_integer,0,mpi_comm_world,mpi_err)
    	CALL mpi_bcast(nycross,1,mpi_integer,0,mpi_comm_world,mpi_err)
    	CALL mpi_bcast(nstat,1,mpi_integer,0,mpi_comm_world,mpi_err)
    	CALL mpi_bcast(ntotal,1,mpi_integer,0,mpi_comm_world,mpi_err)
    	CALL mpi_bcast(nfluence,1,mpi_integer,0,mpi_comm_world,mpi_err)
    	CALL mpi_bcast(nspectrum,1,mpi_integer,0,mpi_comm_world,mpi_err)
    	CALL mpi_bcast(Mx,1,mpi_double_precision,0,mpi_comm_world,mpi_err)
    	CALL mpi_bcast(My,1,mpi_double_precision,0,mpi_comm_world,mpi_err)
    	CALL mpi_bcast(export_wisdom,10,mpi_character,0,mpi_comm_world,mpi_err)
    END IF
#endif
    ! write(*,nml=optics) ! debug: print parameters
    IF (npoints > 0) THEN ! older format config file, square mesh
      npoints_x=npoints
      npoints_y=npoints
    END IF
    IF (npoints_y == 0) THEN ! square mesh
    	Nx = npoints_x
    	Ny = npoints_x
    ELSE
    	Nx = npoints_x
    	Ny = npoints_y
    END IF
    IF ( mesh_x == zero) THEN ! older format used, check if mesh is specified
      IF ( mesh > 0) THEN ! older format config file, square mesh
        mesh_x = mesh
        mesh_y = mesh
      ELSE
      	CALL opc_abort('opc_lib.f90::opc_init', 'Missing grid size')
      END IF
    END IF
    IF ( mesh_y == zero ) THEN
      mesh_y = mesh_x ! mesh sizes are assumed to be equal
    END IF
    IF ( My == zero ) THEN ! same magnification assumed for both directions
    	My = Mx
    END IF
    IF ((Nx == 0) .AND. (Ny == 0)) CALL opc_abort('opc_lib.f90::opc_init', 'Missing number of grid points')
    
    k_wave = TwoPi/lambda
    Mxtot  = 1 
    Mytot  = 1
    Mxbuf = 1
    Mybuf = 1
    IF (nslices == 1) THEN ! separation in time between slices
      dt = zero
    ELSE
      dt = zsep*lambda / cl
    END IF
    
    ! allocate memory

    p_grid = fftw_alloc_complex(int(Nx*Ny, C_SIZE_T))
    call c_f_pointer(p_grid, grid, [Nx, Ny])        
    grid = dcmplx(zero,zero)    
    p_buffer = fftw_alloc_complex(int(Nx*Ny, C_SIZE_T))
    call c_f_pointer(p_buffer, buffer, [Nx, Ny])
    buffer = dcmplx(zero,zero)
    include_Ey = .FALSE.
    if (field_next /= 'none') then
      include_Ey = .TRUE.
      ! allocate memory second polarisation.  
      p_gridy = fftw_alloc_complex(int(Nx*Ny, C_SIZE_T))
      call c_f_pointer(p_gridy, gridy, [Nx, Ny])        
      gridy = dcmplx(zero,zero)    
      p_buffery = fftw_alloc_complex(int(Nx*Ny, C_SIZE_T))
      call c_f_pointer(p_buffery, buffery, [Nx, Ny])
      buffery = dcmplx(zero,zero)
    END IF
    ALLOCATE(xaxis(Nx), stat=ierr)
    IF (ierr /=0) call opc_abort("opc_lib.f90::opc_init", "Not enough memory to allocate Xaxis")
    xaxis = zero
    ALLOCATE(yaxis(Ny), stat=ierr)
    IF (ierr /=0) call opc_abort("opc_lib.f90::opc_init", "Not enough memory to allocate Yaxis")
    yaxis = zero
    
    IF (nxcross > 0) THEN ! output cross-section in x-direction requested by user
    	ALLOCATE(res_xcross(Nx,nslices,nxcross), stat=ierr)
      IF (ierr /=0) call opc_abort("opc_lib.f90::opc_init", "Not enough memory to allocate res_xcross")
      res_xcross(:,:,:) = zero
      ALLOCATE(xcross_name(nxcross), stat=ierr)
      IF (ierr /=0) call opc_abort("opc_lib.f90::opc_init", "Not enough memory to allocate xcross_name")
      xcross_name(1:nxcross) = 'no_perl_output'
	    ALLOCATE(xcross_fname(nxcross), stat=ierr)
      IF (ierr /=0) call opc_abort("opc_lib.f90::opc_init", "Not enough memory to allocate xcross_fname")
      xcross_fname(1:nxcross) = 'no_output_file'
    END IF      
    
    IF (nycross > 0) THEN ! output cross-section in y-direction requested by user
    	ALLOCATE(res_ycross(Ny,nslices,nycross), stat=ierr)
      IF (ierr /=0) call opc_abort("opc_lib.f90::opc_init", "Not enough memory to allocate res_ycross")
      res_ycross(:,:,:) = zero
      ALLOCATE(ycross_name(nycross), stat=ierr)
      IF (ierr /=0) call opc_abort("opc_lib.f90::opc_init", "Not enough memory to allocate ycross_name")
      ycross_name(1:nycross) = 'no_perl_output'
      ALLOCATE(ycross_fname(nycross), stat=ierr)
      IF (ierr /=0) call opc_abort("opc_lib.f90::opc_init", "Not enough memory to allocate ycross_fname")
      ycross_fname(1:nycross) = 'no_output_file'  
    END IF
    
    IF (ntotal > 0) THEN ! output total requested by user
    	ALLOCATE(res_total(nslices,ntotal), stat=ierr)
      IF (ierr /=0) call opc_abort("opc_lib.f90::opc_init", "Not enough memory to allocate res_total")
      res_total(:,:) = zero
      ALLOCATE(total_name(ntotal), stat=ierr)
      IF (ierr /=0) call opc_abort("opc_lib.f90::opc_init", "Not enough memory to allocate total_name")
      total_name(1:ntotal) = 'no_perl_output'
      ALLOCATE(total_fname(ntotal), stat=ierr)                                                
      IF (ierr /=0) call opc_abort("opc_lib.f90::opc_init", "Not enough memory to allocate total_fname") 
      total_fname(1:ntotal) = 'no_output_file'
    END IF
    
    IF (nstat > 0) THEN ! output stat requested by user
    	ALLOCATE(res_stat(9,nstat), stat=ierr)
      IF (ierr /=0) call opc_abort("opc_lib.f90::opc_init", "Not enough memory to allocate res_stat")
      res_stat(:,:) = zero
      ALLOCATE(res_stat_temp(8,nslices,nstat), stat=ierr)
      IF (ierr /=0) call opc_abort("opc_lib.f90::opc_init", "Not enough memory to allocate res_stat_temp")
      res_stat_temp = zero
      ALLOCATE(stat_name(nstat), stat=ierr)
      IF (ierr /=0) call opc_abort("opc_lib.f90::opc_init", "Not enough memory to allocate stat_name")
      stat_name(1:nstat) = 'no_perl_output'
      ALLOCATE(stat_fname(nstat), stat=ierr)
      IF (ierr /=0) call opc_abort("opc_lib.f90::opc_init", "Not enough memory to allocate stat_fname")
      stat_fname(1:nstat) = 'no_output_file'
    END IF
    
    IF (nfluence > 0) THEN ! output fluence requested by user
    	ALLOCATE(res_fluence(Nx,Ny,nfluence), stat=ierr)
      IF (ierr /=0) call opc_abort("opc_lib.f90::opc_init", "Not enough memory to allocate res_fluence")
      res_fluence(:,:,:) = zero
      ALLOCATE(fluence_name(nfluence), stat=ierr)
      IF (ierr /=0) call opc_abort("opc_lib.f90::opc_init", "Not enough memory to allocate fluence_name")
      fluence_name(1:nfluence) = 'no_perl_output'
      ALLOCATE(fluence_fname(nfluence), stat=ierr)                                                
      IF (ierr /=0) call opc_abort("opc_lib.f90::opc_init", "Not enough memory to allocate fluence_fname") 
      fluence_fname(1:nfluence) = 'no_output_file'
    END IF
    
    IF (nspectrum > 0) THEN ! output total requested by user
    	ALLOCATE(res_spectrum(nslices,nspectrum), stat=ierr)
      IF (ierr /=0) call opc_abort("opc_lib.f90::opc_init", "Not enough memory to allocate res_spectrum")
      res_spectrum(:,:) = zero
      ALLOCATE(spectrum_name(nspectrum), stat=ierr)
      IF (ierr /=0) call opc_abort("opc_lib.f90::opc_init", "Not enough memory to allocate spectrum_name")
      spectrum_name(1:nspectrum) = 'no_perl_output'
      ALLOCATE(spectrum_fname(nspectrum), stat=ierr)                                                
      IF (ierr /=0) call opc_abort("opc_lib.f90::opc_init", "Not enough memory to allocate spectrum_fname") 
      spectrum_fname(1:nspectrum) = 'no_output_file'
    END IF
    
    IF (nxcross+nycross+ntotal+nstat+nfluence+nspectrum > 0) THEN ! some output is requested by user
      ! arrays to store local mesh size at the locations of the diagnostics commands
      ALLOCATE(res_xmesh(nxcross+nycross+ntotal+nstat+nfluence+nspectrum), stat=ierr)
      IF (ierr /=0) call opc_abort("opc_lib.f90::opc_init", "Not enough memory to allocate res_xmesh")
      res_xmesh(:) = zero
      ALLOCATE(res_ymesh(nxcross+nycross+ntotal+nstat+nfluence+nspectrum), stat=ierr)
      IF (ierr /=0) call opc_abort("opc_lib.f90::opc_init", "Not enough memory to allocate res_xmesh")
      res_ymesh(:) = zero
    END IF  

    CALL fillaxis()
    
#ifdef MPI
    ! Size of the data read or writen to file, used to determine the offset in the file
    ! IO_filetype is used to set file view for complex field data (grid)
    CALL MPI_TYPE_CONTIGUOUS(Nx*Ny,MPI_DOUBLE_COMPLEX,IO_filetype,mpi_err)
    CALL MPI_TYPE_COMMIT(IO_filetype,mpi_err)
    ! determine size of IO_filetype
    CALL MPI_TYPE_GET_EXTENT(IO_filetype,lb,IO_recsize,mpi_err)
#endif
    RETURN
  END SUBROUTINE opc_init ! subroutine opc_init
      
      
  SUBROUTINE fillaxis

    USE opc_globals
    IMPLICIT NONE ! be strict

    INTEGER          :: i, offset
    REAL (kind=rk16) :: C
    
    ! x-axis
    IF (mod(Nx,2) == 0) THEN  ! even
       offset = Nx/2
    ELSE ! odd
      offset = (Nx+1)/2
    END IF
    ! i = 1 => i - Nx/2 = -Nx/2+1
    ! i = Nx/2 => i - Nx/2 = 0
    ! i = Nx => i - Nx/2 = Nx/2
    ! Thus center is correctly shifted left by half a grid point
 
    i=0 ! avoid i is uninitialised warning  
    C = mesh_x * Mxtot
    xaxis = zero
    xaxis = C * (/ (i - offset, i = 1, Nx) /)
    
    ! y-axis
    IF (mod(Ny,2) == 0) THEN  ! even
       offset = Ny/2
    ELSE ! odd
      offset = (Ny+1)/2
    END IF
    
    C = mesh_y * Mytot
    yaxis = zero
    yaxis = C * (/ (i - offset, i = 1, Ny) /)
    
    RETURN
  END SUBROUTINE fillaxis ! subroutine axis
  
  
  SUBROUTINE opc_close
    USE opc_globals  
#ifdef MPI
    USE mpi
#endif 
    IMPLICIT NONE ! be strict
    INTEGER :: ierr
#ifdef MPI
    INTEGER :: mpi_err
#endif
    LOGICAL :: file_exists

    ! deallocate memory
    if (c_associated(p_grid))   call fftw_free( p_grid )
    if (c_associated(p_buffer)) call fftw_free( p_buffer )
    DEALLOCATE(xaxis, Stat=ierr)
    IF (ierr /=0) call opc_abort("opc_lib.f90::opc_close", "Could not free memory allocated to xaxis")
    NULLIFY(xaxis)
    DEALLOCATE(yaxis, Stat=ierr)
    IF (ierr /=0) call opc_abort("opc_lib.f90::opc_close", "Could not free memory allocated to yaxis") 
    NULLIFY(yaxis)
    IF (ASSOCIATED(res_xcross)) THEN
      DEALLOCATE(res_xcross, Stat=ierr)
      IF (ierr /=0) call opc_abort("opc_lib.f90::opc_close", "Could not free memory allocated to res_xcross")
      NULLIFY(res_xcross)
      DEALLOCATE(xcross_name, Stat=ierr)
      IF (ierr /=0) call opc_abort("opc_lib.f90::opc_close", "Could not free memory allocated to xcross_name")
      NULLIFY(xcross_name)
      DEALLOCATE(xcross_fname, Stat=ierr)
      IF (ierr /=0) call opc_abort("opc_lib.f90::opc_close", "Could not free memory allocated to xcross_fname")
      NULLIFY(xcross_fname)
    END IF
    IF (ASSOCIATED(res_ycross)) THEN
      DEALLOCATE(res_ycross, Stat=ierr)
      IF (ierr /=0) call opc_abort("opc_lib.f90::opc_close", "Could not free memory allocated to res_ycross")
      NULLIFY(res_ycross)
      DEALLOCATE(ycross_name, Stat=ierr)
      IF (ierr /=0) call opc_abort("opc_lib.f90::opc_close", "Could not free memory allocated to ycross_name")
      NULLIFY(ycross_name)
      DEALLOCATE(ycross_fname, Stat=ierr)
      IF (ierr /=0) call opc_abort("opc_lib.f90::opc_close", "Could not free memory allocated to ycross_fname")
      NULLIFY(ycross_fname)
    END IF
    IF (ASSOCIATED(res_total)) THEN
      DEALLOCATE(res_total, Stat=ierr)
      IF (ierr /=0) call opc_abort("opc_lib.f90::opc_close", "Could not free memory allocated to res_total")
      NULLIFY(res_total)
      DEALLOCATE(total_name, Stat=ierr)
      IF (ierr /=0) call opc_abort("opc_lib.f90::opc_close", "Could not free memory allocated to total_name")
      NULLIFY(total_name)
      DEALLOCATE(total_fname, Stat=ierr)
      IF (ierr /=0) call opc_abort("opc_lib.f90::opc_close", "Could not free memory allocated to total_fname")
      NULLIFY(total_fname)
    END IF
    IF (ASSOCIATED(res_stat)) THEN
      DEALLOCATE(res_stat, Stat=ierr)
      IF (ierr /=0) call opc_abort("opc_lib.f90::opc_close", "Could not free memory allocated to res_stat")
      NULLIFY(res_stat)
      DEALLOCATE(stat_name, Stat=ierr)
      IF (ierr /=0) call opc_abort("opc_lib.f90::opc_close", "Could not free memory allocated to stat_name")
      NULLIFY(stat_name)
      DEALLOCATE(stat_fname, Stat=ierr)
      IF (ierr /=0) call opc_abort("opc_lib.f90::opc_close", "Could not free memory allocated to stat_fname")
      NULLIFY(stat_fname)  
    END IF
    IF (ASSOCIATED(res_spectrum)) THEN
      DEALLOCATE(res_spectrum, Stat=ierr)
      IF (ierr /=0) call opc_abort("opc_lib.f90::opc_close", "Could not free memory allocated to res_spectrum")
      NULLIFY(res_spectrum)
      DEALLOCATE(spectrum_name, Stat=ierr)
      IF (ierr /=0) call opc_abort("opc_lib.f90::opc_close", "Could not free memory allocated to spectrum_name")
      NULLIFY(spectrum_name)
      DEALLOCATE(spectrum_fname, Stat=ierr)
      IF (ierr /=0) call opc_abort("opc_lib.f90::opc_close", "Could not free memory allocated to spectrum_fname")
      NULLIFY(spectrum_fname)
    END IF
    IF (ASSOCIATED(res_fluence)) THEN
      DEALLOCATE(res_fluence, Stat=ierr)
      IF (ierr /=0) call opc_abort("opc_lib.f90::opc_close", "Could not free memory allocated to res_fluence")
      NULLIFY(res_spectrum)
      DEALLOCATE(fluence_name, Stat=ierr)
      IF (ierr /=0) call opc_abort("opc_lib.f90::opc_close", "Could not free memory allocated to fluence_name")
      NULLIFY(fluence_name)
      DEALLOCATE(fluence_fname, Stat=ierr)
      IF (ierr /=0) call opc_abort("opc_lib.f90::opc_close", "Could not free memory allocated to fluence_fname")
      NULLIFY(fluence_fname)
    END IF
    IF (ASSOCIATED(res_xmesh)) THEN
      DEALLOCATE(res_xmesh, Stat=ierr)
      IF (ierr /=0) call opc_abort("opc_lib.f90::opc_close", "Could not free memory allocated to res_xmesh")
      NULLIFY(res_xmesh)
    END IF
    IF (ASSOCIATED(res_ymesh)) THEN
      DEALLOCATE(res_ymesh, Stat=ierr)
      IF (ierr /=0) call opc_abort("opc_lib.f90::opc_close", "Could not free memory allocated to res_ymesh")
      NULLIFY(res_ymesh)
    END IF
    
    RETURN
  END SUBROUTINE opc_close ! subroutine opc_close



  SUBROUTINE field_offset(ioff_x, iend_x, ioff_y, iend_y)
  ! ==================================================================
  ! - Method to calculate difference between nout and N
  ! Currently no difference any more between nout and N 
  ! due to the use of FFTW which handles arbitrary grid
  ! sizes
  ! ------------------------------------------------------------------
     USE opc_globals
     IMPLICIT NONE ! be strict
  
     INTEGER,INTENT(OUT) :: ioff_x, iend_x, ioff_y, iend_y

     ! 'N' should always be even (since FFTW not required any more)
     ! 'nout' can be even or odd and should be less than 'N'

     ioff_x = 1
     iend_x = Nx
     
     ioff_y = 1
     iend_y = Ny
     
     !print *,'Read Nx: ', Nx, ' range: ', ioff_x, iend_x
     !print *,'Read Ny: ', Ny, ' range: ', ioff_y, iend_y
     
     RETURN
    
  END SUBROUTINE field_offset



  SUBROUTINE shift_fft(NZx,NZy)
  ! ==================================================================
  ! Routine reorganizes spatial frequencies to have the zero frequency
  ! in the center of the grid. 
  ! Nx(y) odd : -Nx(y)/2, ..., -1, 0, 1, ... Nx(y)/2 times dkx(y)
  ! Nx(y) even: -Nx(y)/2, ..., -1, 0, 1, ... Nx(y)/2 -1 time dkx(y)
  ! 2D transform:  A B
  !                C D
  ! where A, B, C, D are the quadrants with the low frequencies in the
  ! corner. Swith A and D & B and C to get the zero frequency in the 
  ! center.
  ! On return NZx, NZy are the indices of the grid point for zero freq.
  ! The routine inv_shift_fft performs the inverse operation and
  ! prepares the data for the inverse Fourier transform
  ! ==================================================================
    USE opc_globals
    IMPLICIT NONE ! be strict
    
    INTEGER, INTENT(INOUT)          :: NZx, NZy
    INTEGER                         :: ix, iy, ierr
    COMPLEX(kind=rk16), allocatable :: temp(:)
    
    if (mod(Nx,2) == 0) then ! Nx even
    	NZx = Nx/2  
    	allocate(temp(NZx+1),stat=ierr)
      if (ierr/=0) call opc_abort('opc_lib.f90::shift_fft','Error, allocating memory for temp array failed')
      temp = dcmplx(zero,zero)	
      do iy=1,Ny
    	  temp = grid(1:NZx+1,iy)
    	  grid(1:NZx-1,iy) = grid(NZx+2:Nx,iy)
    	  grid(NZx:Nx,iy) = temp
    	  if (include_Ey) then
    	  	temp = gridy(1:NZx+1,iy)
    	    gridy(1:NZx-1,iy) = gridy(NZx+2:Nx,iy)
    	    gridy(NZx:Nx,iy) = temp
    	  end if
      end do
    else                     ! Nx odd
      NZx = Nx/2+1
      allocate(temp(NZx),stat=ierr)
      if (ierr/=0) call opc_abort('opc_lib.f90::shift_fft','Error, allocating memory for temp array failed')
      temp = dcmplx(zero,zero)	
      do iy=1,Ny
    	  temp = grid(1:NZx,iy)
    	  grid(1:NZx-1,iy) = grid(NZx+1:Nx,iy)
    	  grid(NZx:Nx,iy) = temp
    	  if (include_Ey) then
    	  	temp = gridy(1:NZx,iy)
    	    gridy(1:NZx-1,iy) = gridy(NZx+1:Nx,iy)
    	    gridy(NZx:Nx,iy) = temp
    	  end if
      end do
    end if 
    deallocate(temp) 
    if (mod(Ny,2) == 0) then ! Ny even
    	NZy = Ny/2
    	allocate(temp(NZy+1),stat=ierr)
      if (ierr/=0) call opc_abort('opc_lib.f90::shift_fft','Error, allocating memory for temp array failed')
      temp = dcmplx(zero,zero)	
      do ix=1,Nx
    	  temp = grid(ix,1:NZy+1)
    	  grid(ix,1:NZy-1) = grid(ix,NZy+2:Ny)
    	  grid(ix,NZy:Ny) = temp
    	  if (include_Ey) then
    	  	temp = gridy(ix,1:NZy+1)
    	    gridy(ix,1:NZy-1) = gridy(ix,NZy+2:Ny)
    	    gridy(ix,NZy:Ny) = temp
    	  end if
      end do
    else                     ! Ny odd
      NZy = Ny/2+1
      allocate(temp(NZy),stat=ierr)
      if (ierr/=0) call opc_abort('opc_lib.f90::shift_fft','Error, allocating memory for temp array failed')
      temp = dcmplx(zero,zero)	
      do ix=1,Nx
    	  temp = grid(ix,1:NZy)
    	  grid(ix,1:NZy-1) = grid(ix,NZy+1:Ny)
    	  grid(ix,NZy:Ny) = temp
    	  if (include_Ey) then
    	  	temp = gridy(ix,1:NZy)
    	    gridy(ix,1:NZy-1) = gridy(ix,NZy+1:Ny)
    	    gridy(ix,NZy:Ny) = temp
    	  end if
      end do
    end if 
    deallocate(temp)
  END SUBROUTINE shift_fft 



  SUBROUTINE inv_shift_fft(NZx, NZy)
  ! ==================================================================
  ! Routine reorganizes spatial frequencies for calculating inverse
  ! Fourier transform. See shift_fft for details
  ! ==================================================================
    USE opc_globals
    IMPLICIT NONE
    
    INTEGER, INTENT(IN)             :: NZx, NZy
    INTEGER                         :: ix, iy, ierr
    COMPLEX(kind=rk16), allocatable :: temp(:)
    
    allocate(temp(NZx-1),stat=ierr)
    if (ierr/=0) call opc_abort('opc_lib.f90::shift_fft','Error, allocating memory for temp array failed')
!    temp = dcmplx(zero,zero)
    
    do iy=1,Ny
    	temp = grid(1:NZx-1,iy)
    	grid(1:NZx,iy) = grid(NZx:Nx,iy)
    	if (mod(Nx,2) == 0) then
    	  grid(NZx+2:Nx,iy) = temp
    	else 
    	  grid(Nzx+1:Nx,iy) = temp
    	end if 
    	if (include_Ey) then
    		temp = gridy(1:NZx-1,iy)
    	  gridy(1:NZx,iy) = gridy(NZx:Nx,iy)
    	  if (mod(Nx,2) == 0) then
    	  	gridy(NZx+2:Nx,iy) = temp
    	  else
    	  	gridy(Nzx+1:Nx,iy) = temp
    	  end if
    	end if
    end do
    deallocate(temp)
    allocate(temp(NZy-1),stat=ierr)
    if (ierr/=0) call opc_abort('opc_lib.f90::shift_fft','Error, allocating memory for temp array failed')
    do ix=1,Nx
    	temp = grid(ix,1:NZy-1)
    	grid(ix,1:NZy) = grid(ix,NZy:Ny)
    	if (Mod(Ny,2) == 0) then
    	  grid(ix,NZy+2:Ny) = temp
    	else
    	  grid(ix,Nzy+1:Nx) = temp
    	end if
    	if (include_Ey) then
    		temp = gridy(ix,1:NZy-1)
    	  gridy(ix,1:NZy) = gridy(ix,NZy:Ny)
    	  if (Mod(Ny,2) == 0) then
    	    gridy(ix,NZy+2:Ny) = temp
    	  else
    	    gridy(ix,Nzy+1:Ny) = temp
    	  end if
    	end if
    end do
    deallocate(temp)
  END SUBROUTINE inv_shift_fft 



  SUBROUTINE total(is, res_1d)
  ! ==================================================================
  ! Routine that prints the total power for each slice
  ! ------------------------------------------------------------------

    USE opc_globals
    IMPLICIT NONE ! be strict

    INTEGER, INTENT(IN)                          :: is
    REAL(kind=rk16), dimension(:), INTENT(INOUT) :: res_1d
 
    res_1d(is) = sum(abs(grid(1:Nx,1:Ny))**2)
 
    RETURN
    
  END SUBROUTINE total

 
 
  SUBROUTINE spectrum(is, res_1d, x, y)
  ! ==================================================================
  ! Routine that prints the spectral power density for each slice
  ! ------------------------------------------------------------------

    USE opc_globals
    IMPLICIT NONE ! be strict

    INTEGER, INTENT(IN)                          :: is
    REAL(kind=rk16), dimension(:), INTENT(INOUT) :: res_1d
    REAL(kind=rk16), INTENT(IN), OPTIONAL        :: x, y
    INTEGER, dimension(1)                        :: xa, ya
    INTEGER                                      :: ix, iy
    
    if (present(x)) then
      xa = maxloc(xaxis(:), MASK=xaxis(:)<=x)
      ix = xa(1)
    end if
    if (present(y)) then
      ya = maxloc(yaxis(:), MASK=yaxis(:)<=y)
      iy = ya(1)
    end if
    
    if (present(x)) then
      if (present(y)) then
        res_1d(is) = abs(grid(ix,iy))**2
      else
        res_1d(is) = sum(abs(grid(ix,1:Ny))**2)
      end if
    else
      if (present(y)) then
        res_1d(is) = sum(abs(grid(1:Nx,iy))**2)
      else
        res_1d(is) = sum(abs(grid(1:Nx,1:Ny))**2)
      end if
    end if 
    RETURN
    
  END SUBROUTINE spectrum
  
  
  
  SUBROUTINE fluence(is, res_2d)
  ! ==================================================================
  ! Routine that adds the contribution of slice is to the fluence
  ! ------------------------------------------------------------------

    USE opc_globals
    IMPLICIT NONE ! be strict

    INTEGER, INTENT(IN)                            :: is
    REAL(kind=rk16), dimension(:,:), INTENT(INOUT) :: res_2d
    REAL(kind=rk16)                                :: perarea
 
    perarea = one/(Mxtot*mesh_x*Mytot*mesh_y)
    res_2d = res_2d + perarea*abs(grid)**2
 
    RETURN
    
  END SUBROUTINE fluence
     
      
      
  SUBROUTINE  maximum(is, res_1d)
  ! ==================================================================
  ! Routine that prints the maximum intensity for each slice
  ! ------------------------------------------------------------------
     
     USE opc_globals
     IMPLICIT NONE ! be strict
     
     INTEGER, INTENT(IN)                          :: is
     REAL(kind=rk16), dimension(:), INTENT(INOUT) :: res_1d
     REAL (kind=rk16)                             :: perarea
     
     perarea = one/(Mxtot*mesh_x*Mytot*mesh_y)
     res_1d(is) = perarea * maxval(abs(grid))**2 ! I = |E|**2
     
     RETURN
    
  END SUBROUTINE maximum

      
      
  SUBROUTINE xcross(is, res_2d)
  ! ==================================================================
  ! Routine that prints a cross section of each slice
  ! ------------------------------------------------------------------
     
     USE opc_globals
     IMPLICIT NONE ! be strict
          
     INTEGER, INTENT(IN)                            :: is
     REAL(kind=rk16), dimension(:,:), INTENT(INOUT) :: res_2d
     INTEGER                                        :: iy, ioff_x, iend_x, ioff_y, iend_y
     REAL (kind=rk16)                               :: perarea
         
     CALL field_offset(ioff_x, iend_x, ioff_y, iend_y)
         
     iy=Ny/2 ! along y=0 line

     perarea = one/(Mxtot*mesh_x*Mytot*mesh_y)
     res_2d(1:iend_x-ioff_x+1,is) = perarea * abs(grid(ioff_x:iend_x,iy))**2 ! I = |E|**2
     ! divide by (mesh**2) because value stored at grid points is amplitude
     ! times square root of area associated with grid point.
        
     RETURN 
    
  END SUBROUTINE xcross ! subroutine xcross

      
      
  SUBROUTINE ycross(is, res_2d)
  ! ==================================================================
  ! Routine that prints a cross section of each slice
  ! ------------------------------------------------------------------

     USE opc_globals
     IMPLICIT NONE ! be strict
 
     INTEGER, INTENT(IN)                            :: is
     REAL(kind=rk16), dimension(:,:), INTENT(INOUT) :: res_2d
     INTEGER                                        :: ix, ioff_x, iend_x, ioff_y, iend_y
     REAL (kind=rk16)                               :: perarea

     CALL field_offset(ioff_x, iend_x, ioff_y, iend_y)
 
     ix=Nx/2 ! along line x=0
     perarea = one/(Mxtot*mesh_x*Mytot*mesh_y)

     res_2d(1:iend_y-ioff_y+1,is) = perarea * abs(grid(ix,ioff_y:iend_y))**2 ! I = |E|**2
     ! divide by (mesh**2) because value stored at grid points is amplitude
     ! times square root of area associated with grid point.
 
     RETURN 
     
  END SUBROUTINE ycross ! subroutine ycross

   
   
  SUBROUTINE statss(is, res_2d)
	! ==================================================================
	! Routine that calculates several statistical parameters for the
	! field distribution:
	! tot    = total steady state power or total energy within pulse
	! xc     = center of mass in x-direction
	! yc     = center of mass in y-direction
	! sr     = standard deviation in r
	! w      = half width of the beam (assuming a Gaussian distribution)
	! sx     = standard deviation in x-direction
	! sy     = standard deviation in y-direction
	! 
	! in case the file is in the spectral domain, this routine also
	! determines the average wavelength and rms bandwidth
	!
	! lambda_avg = intensity weighted average wavelength
	! bw     = rms bandwidth (intensity weighted)
	! 
	! Here the contribution of each slice is determined, the caller
	! needs to collect all data and calculated the above quantities
	! when all mpi processes have finished
	! ------------------------------------------------------------------
	
	 use opc_globals
	 implicit none ! be strict
	   
	 INTEGER, INTENT(IN)                            :: is   
	 INTEGER                                        :: ix, iy
	 REAL(kind=rk16)                                :: sum2, ttot, xs, ys
	 REAL(kind=rk16)                                :: my_freq
	 REAL(kind=rk16), dimension(:,:), INTENT(INOUT) :: res_2d
	 
	 sum2=zero
	 ttot = zero
	 res_2d(1:6,is)=zero
     do iy = 1, Ny
	     do ix = 1, Nx
	       sum2 = abs(grid(ix,iy))**2
	       ttot = ttot + sum2
	       xs = xaxis(ix)
	       ys = yaxis(iy)
	       res_2d(2,is) = res_2d(2,is) + sum2*xs
	       res_2d(3,is) = res_2d(3,is) + sum2*ys
	       res_2d(4,is) = res_2d(4,is) + sum2*xs*xs
	       res_2d(5,is) = res_2d(5,is) + sum2*ys*ys
	       res_2d(6,is) = res_2d(6,is) + sum2*(xs*xs+ys*ys)
	     end do
	 end do
	 if (spectral_domain) then
	   ! spectrum is equidistant in frequency, not in wavelenngth. So adjust 
	   ! for spacing between wavelength samples
	   my_freq = cl*k_wave/TwoPi
	   res_2d(7,is) = res_2d(7,is) + ttot*my_freq
	   res_2d(8,is) = res_2d(8,is) + ttot*my_freq**2
	 end if
	 res_2d(1,is) = res_2d(1,is) + ttot
	 return
	end subroutine statss
  

  
  SUBROUTINE phase_P(is, res_1d, x, y)
  ! ==================================================================
  ! Routine that prints the slowly varying phase of the 
  ! optical field in point (x,y) 
  ! currently nearest neighbour implemented, in future we should use
  ! interpolation to better estimated the value at the point of 
  ! interest
  ! ------------------------------------------------------------------
     USE opc_globals
     IMPLICIT NONE ! be strict
 
     INTEGER, INTENT(IN)                            :: is
     REAL(kind=rk16), INTENT(IN)                    :: x, y
     REAL(kind=rk16), dimension(:), INTENT(INOUT)   :: res_1d
     INTEGER                                        :: ix, iy, ioff_x, iend_x, ioff_y, iend_y

     CALL field_offset(ioff_x, iend_x, ioff_y, iend_y)  

     ix = Nx/2 + INT(x/(Mxtot*mesh_x))
     ix = MIN(ix, Nx)   ! make sure that point remains in grid
     ix = MAX(ix, 1)
     iy = Ny/2 + INT(y/(Mytot*mesh_y))
     iy = MIN(iy, Ny)   ! make sure that point remains in grid
     iy = MAX(iy, 1)
     
     res_1d(is) = atan2( aimag(grid(ix, iy)), dble(grid(ix,iy)) )
     
     return
  END SUBROUTINE phase_P ! end subroutine phase
  
  
  SUBROUTINE phase_1D_x(is, res_2d) 
     ! ==================================================================
     ! Routine that prints the phase along "x" cross-section
     ! ------------------------------------------------------------------

     USE opc_globals
     IMPLICIT NONE ! be strict
 
     INTEGER, INTENT(IN)                            :: is
     REAL(kind=rk16), dimension(:,:), INTENT(INOUT) :: res_2d
     INTEGER                                        :: i, ioff_x, iend_x, ioff_y, iend_y

     CALL field_offset(ioff_x, iend_x, ioff_y, iend_y)

   	 i=Ny/2 ! along line y=0
     res_2d(1:iend_x-ioff_x+1,is) = atan2(aimag(grid(1:iend_x-ioff_x+1, i)), dble(grid(1:iend_x-ioff_x+1, i)))

     RETURN      
  END SUBROUTINE phase_1D_x ! subroutine phase_1D_x


  SUBROUTINE phase_1D_y(is, res_2d) 
     ! ==================================================================
     ! Routine that prints the phase along "y" cross-section
     ! ------------------------------------------------------------------

     USE opc_globals
     IMPLICIT NONE ! be strict
 
     INTEGER, INTENT(IN)                            :: is
     REAL(kind=rk16), dimension(:,:), INTENT(INOUT) :: res_2d
     INTEGER                                        :: i, ioff_x, iend_x, ioff_y, iend_y

     CALL field_offset(ioff_x, iend_x, ioff_y, iend_y)

   	 i=Nx/2 ! along line x=0
   	 res_2d(1:iend_y-ioff_y+1,is) = atan2(aimag(grid(i, 1:iend_y-ioff_y+1)), dble(grid(i, 1:iend_y-ioff_y+1)))

     RETURN 
  END SUBROUTINE phase_1D_y ! subroutine phase_1D_y
  
  
  subroutine fourier(in_file, in_memory)
     ! ==================================================================
     ! Routine that transforms a dfl file form time to spectral domain 
     ! and back, where the direction is determined by the extension of the
     ! input file name (.dfl -> .sfl and .sfl -> .dfl).
     ! Since multiple time slices are required, this function is only 
     ! implemented for MPI and requires all slices to be present. So 
     ! only acts on a file.
     ! 
     ! in_file   : name of file to transform
     ! in_memory : boolean to indicate if we can read whole file in memory
     ! ------------------------------------------------------------------
      use, intrinsic :: iso_c_binding
      use opc_globals
      use opc_interface
      use fftw3
#ifdef MPI
      use mpi
#endif
      implicit none ! be strict
      
      interface
        integer(C_INT) function strlen(s) bind(C, name='strlen')
         import
         type(C_PTR), value :: s
        end function strlen
        subroutine free(p) bind(C, name='free')
         import
         type(C_PTR), value :: p
        end subroutine free
      end interface
      
      character(len=100), INTENT (IN)            :: in_file
      logical, INTENT (IN)                       :: in_memory
      character(len=100)                         :: out_file, opc_wisdom_filename
      logical                                    :: to_freq_domain, opc_wisdom_exist
      integer                                    :: i, j, n, ierr, IFH, OFH, iprogress, ix, iy, is
      integer                                    :: recsize, nFFT, nFFT_extra
      complex (kind=rk16), dimension(:,:),   pointer :: data_in
      complex (kind=rk16), dimension(:,:,:), pointer :: data_memory
      real (kind=rk16)                           :: norm
      type(C_PTR)                                :: p_data_in, p_data_memory, p      
      integer(C_INT)                             :: ret    ! for FFTW wisdom import/export
      character(C_CHAR), pointer                 :: s_c(:)
      integer(C_SIZE_T)                          :: slen_c
      character,dimension(:),allocatable         :: s_f
      integer                                    :: slen_f
#ifdef MPI
      integer (kind=MPI_OFFSET_KIND)             :: offset
      integer                                    :: mpi_err, amode, IO_column
      integer                                    :: istatus(MPI_STATUS_SIZE)
#endif
      integer                                    :: mpi_rank, mpi_size
      
      if (nslices == 1) &
        call opc_abort('opc_lib.f90::fourier', 'Error called for a single slice, cannot transform to spectral domain')
      
#ifdef MPI
      call MPI_COMM_RANK(MPI_COMM_WORLD, mpi_rank, mpi_err) ! rank and number of processes
      call MPI_COMM_SIZE(MPI_COMM_WORLD, mpi_size, mpi_err)
#else
      call opc_abort('opc_lib.f90::fourier', 'Error, OPC will only calculate spectrum running in MPI mode')
      mpi_rank=0 ! default value for NON-MPI execution on a single node   ! for future use when spectrum is implemented for single process (no mpi).
      mpi_size=1 ! default value for NON-MPI execution on a single node
#endif 
     ! determine direction of Fourier transform
     i=index(in_file,'.dfl', .TRUE.)  ! find the last occurance of '.dfl' in infile
     if (i > 0) then 
     	  to_freq_domain = .TRUE.          ! corresponding to FFTW_FORWARD
     	  out_file=replace_string(in_file, '.dfl', '.sfl')
     else
     	  i=index(in_file,'.sfl', .TRUE.)  ! find the last occurance of '.sfl' in infile
        if (i > 0) then 
        	to_freq_domain = .FALSE.       ! corresponding to FFTW_BACKWARD
        	out_file=replace_string(in_file, '.sfl', '.dfl') 
        else 
        	call opc_abort('opc_lib.f90::fourier', 'Error, no valid file specified (unknown extension)')
        end if
     end if
     if (mpi_rank == 0) then
       if (to_freq_domain) then
     	   print *, 'Applying Fourier transform from temporal to frequency domain'
       else
         print *, 'Applying Fourier transform from frequency to temporal domain' 
       end if  
     end if   
     
     if (in_memory) then
       ! enough memory available to read complete file into memory
       ! allocated memory
       ! the master node reads the whole file (and reserves memory) and then divides tha appropriate data over
       ! the various nodes.
       if (mpi_rank == 0) then
         p_data_memory = fftw_alloc_complex(int(Nx*Ny*nslices, C_SIZE_T))
         call c_f_pointer(p_data_memory, data_memory, [Nx,Ny,nslices])        
         data_memory = dcmplx(zero,zero)
         ! read data
         recsize = 16*Nx*Ny;
         open( unit=FHID, file=trim(in_file), recl=recsize, access='direct', status='old', iostat=ierr )
         if (ierr /= 0) call opc_abort('opc_lib.f90::fourier','Error opening file ' // trim(in_file))
         do is=1,nslices
           read( unit=FHID, rec=is, iostat=ierr ) ((data_memory(ix,iy,is), ix=1,Nx), iy=1,Ny)
           if (ierr /= 0) call opc_abort('opc_lib.f90::fourier','error reading file ' // trim(in_file))
         end do
         close(FHID)
         print *,'Input file has been read'
       end if
       ! determine number of FFTs per node
       nFFT = INT((Nx*Ny)/mpi_size)
       nFFT_extra = MODULO(Nx*Ny,nFFT)  ! remaining FFTs, will be distributed over part of the nodes
       ! allocate memory to receive data for FFTs
       p_data_in = fftw_alloc_complex(int((nFFT+1)*nslices, C_SIZE_T))
       call c_f_pointer(p_data_in, data_in, [nslices,nFFT+1])
       data_in=dcmplx(zero,zero) 
     else
       !! allocate memory to contain data
       ! not enough memory available on system to read in complete data, so use slow disk IO to retrieve the
       ! correct data.
       !
       p_data_in = fftw_alloc_complex(int(nslices, C_SIZE_T))
       call c_f_pointer(p_data_in, data_in, [nslices,1])        
       data_in =  dcmplx(zero,zero)
     end if
     !
     !! plan FFT for data points. Use wisdom if available
     !! check if wisdom already has been created before 
     if ((trim(export_wisdom) == 'all').OR.(mpi_size == 1)) then
       write(opc_wisdom_filename,FMT='(A19,I3.3,"_",I5.5,"x",I5.5)') 'opc_wisdom_spectrum',mpi_rank,Nx,Ny
       inquire(FILE=trim(opc_wisdom_filename),EXIST=opc_wisdom_exist)
       if (opc_wisdom_exist) then
         ! restore plans from previous runs using the same grid size
         ret = fftw_import_wisdom_from_filename(C_CHAR_''//trim( opc_wisdom_filename) // C_NULL_CHAR)
         if (ret == 0) call  opc_abort('opc_optics.f90::main', 'error importing wisdom from file')
       end if
     else if ((trim(export_wisdom) == 'single').AND.(mpi_size>1)) then
 	     if (mpi_rank == 0) then  ! read wisdom file and broadcast to other subprocesses
 	 	     write(opc_wisdom_filename,FMT='(A19,I3.3,"_",I5.5,"x",I5.5)') 'opc_wisdom_spectral',mpi_rank,Nx,Ny
         inquire(FILE=trim(opc_wisdom_filename),EXIST=opc_wisdom_exist)
         if (opc_wisdom_exist) then
           ! restore plans from previous runs using the same grid size
           ret = fftw_import_wisdom_from_filename(C_CHAR_''//trim( opc_wisdom_filename) // C_NULL_CHAR)
           if (ret == 0) call  opc_abort('opc_optics.f90::main', 'error importing wisdom from file')
         end if
         p = fftw_export_wisdom_to_string()
         if (.not. c_associated(p)) call opc_abort('opc_optics.f90::main', 'error converting wisdom to string')
         slen_c = strlen(p)+1
         call c_f_pointer(p, s_c, [slen_c])
         slen_f = slen_c
         allocate(s_f(slen_f),stat=ierr)
         s_f=s_c
#ifdef MPI
         call mpi_bcast(slen_f,1,mpi_integer,0,mpi_comm_world,mpi_err)
         call mpi_bcast(s_f,slen_f,mpi_character,0,mpi_comm_world,mpi_err) 
         call free(p)     
#endif       
       end if
#ifdef MPI
       if (mpi_rank > 0) then  ! receive wisdom broadcasted by subprocess with rank 0
   	     call mpi_bcast(slen_f,1,mpi_integer,0,mpi_comm_world,mpi_err)
   	     allocate(s_f(slen_f),stat=ierr)
   	     call mpi_bcast(s_f, slen_f, mpi_character,0,mpi_comm_world,mpi_err)
   	     ret = fftw_import_wisdom_from_string(C_CHAR_''//s_f)
   	     deallocate(s_f)
       end if
#endif
     end if
     write(opc_wisdom_filename,FMT='(A19,I4.4,A1,I6.6)') 'opc_wisdom_spectrum_', mpi_rank,'-',nslices
     inquire(FILE=trim(opc_wisdom_filename),EXIST=opc_wisdom_exist)
     if (opc_wisdom_exist) then
       ! restore plans from previous runs using the same number of slices
       ret = fftw_import_wisdom_from_filename(C_CHAR_''//trim( opc_wisdom_filename) // C_NULL_CHAR)
       if (ret .eq. 0) call opc_abort('Opc_lib.f90::fourier', 'Error importing wisdom from file '//opc_wisdom_filename)
     end if
     if (to_freq_domain) then
   	   if (mpi_rank == 0) print *, 'Planning forward Fourier transform'
   	   plan_spectrum  = fftw_plan_dft_1d ( nslices, data_in(1:nslices,1), data_in(1:nslices,1), FFTW_FORWARD, FFTW_MEASURE )
     else
   	   if (mpi_rank == 0) print *, 'Planning backward Fourier transform'
   	   plan_spectrum = fftw_plan_dft_1d ( nslices, data_in(1:nslices,1), data_in(1:nslices,1), FFTW_BACKWARD, FFTW_MEASURE )
     end if
     !! write wisdom to disc if it does not exists
     if (.NOT.opc_wisdom_exist) then
       ! no previous plans exists for this grid size, check if we need to store plan to file for later use
       if (trim(export_wisdom) == 'single') then
   	     if (mpi_rank == 0) then
           ret = fftw_export_wisdom_to_filename(C_CHAR_'' // trim(opc_wisdom_filename) // C_NULL_CHAR)
           if (ret == 0) call opc_abort('opc_optics.f90::main', 'error exporting wisdom to file')
         end if
       end if
       if (trim(export_wisdom) == 'all') then
   	     ret = fftw_export_wisdom_to_filename(C_CHAR_'' // trim(opc_wisdom_filename) // C_NULL_CHAR)
         if (ret == 0) call opc_abort('opc_optics.f90::main', 'error exporting wisdom to file')
       end if   
     end if
     
     if (in_memory) then
     	 ! ! distribute data over nodes, first part of data_memory is done by master node, so start with
       ! offset
       n=nFFT+1                      ! number of FFTs done by mater node
       if (nFFT_extra == 0)  n=nFFT  ! no extra FFT required
       iy = int(n/Nx)+1  ! determine offset for sending data to other nodes
       ix = MODULO(n,Nx)
       do i = 1, mpi_size-1 
       	 n = nFFT                    ! number of FFTs done be other nodes
         if (i< nFFT_extra) n=nFFT+1 ! extra FFT required
         if ((mpi_rank == 0)) then
           do j = 1, n  !! set data in right order for FFT
             ix = ix + 1
             if (ix > Nx) then
               ix = 1
               iy = iy+1
               if (iy > Ny) call opc_abort('opc_lib.f90::fourier','error running out of array boundaries')
             end if   
             data_in(1:nslices,j) = data_memory(ix,iy,1:nslices)
           end do
           !! submit data to other node
           if (i > 0) then ! if i = master node, then we do not need to send and receive
#ifdef MPI
             call MPI_SEND(data_in, n*nslices, MPI_DOUBLE_COMPLEX, i, 78, MPI_COMM_WORLD, mpi_err)
#endif
             if (mpi_size < 10) print *,'Master node has send data to node ',i
           end if
         end if
           
         if ((mpi_rank == i) .and. (i>0) ) then ! each rank > 0 only receives the data it needs
#ifdef MPI
           call MPI_RECV(data_in, n*nslices, MPI_DOUBLE_COMPLEX, 0, 78, MPI_COMM_WORLD, istatus, mpi_err)

#endif
         end if
       end do
       ! now fill data_in with data for master node
       if (mpi_rank==0) then
       	 n=nFFT                                  ! number of FFTs to be done by the node
         if (mpi_rank < nFFT_extra) n=nFFT+1     ! one FFT extra for this node

         ix=0
         iy=1
         do j = 1, n
           ix=ix+1
           if (ix > Nx) then
             ix = 1
             iy = iy+1
             if (iy > Ny) call opc_abort('opc_lib.f90::fourier','error running out of array boundaries')
           end if   
           data_in(1:nslices,j) = data_memory(ix,iy,1:nslices)
         end do
       end if
       ! perform the FFTs
       n = nFFT
       if (mpi_rank < nFFT_extra) n = nFFT+1
       if (to_freq_domain) then
         norm = dt
       else 
         norm = one/((nslices-1)*dt)  ! dfreq
       end if
       iprogress = n/10
       do j=1, n
         !! perform FFT
         if ((modulo(j, iprogress) == 0).and.(mpi_rank==0)) write(*, "(A,I3,A)") 'Master node - progress is ', 10*j/iprogress, ' %'
          call fftw_execute_dft ( plan_spectrum, data_in(1:nslices,j), data_in(1:nslices,j) ) 
       end do
       !! normalise 
       data_in = norm * data_in 
       !! collect data on the master node but first store data on master node in data_memory       
       if (mpi_rank == 0) then
         print *, 'Collecting data on master node'
         ix = 0 ! initialise counters
         iy = 1
         do j = 1, n ! store data_in from master node to data_memory before collecting data from other nodes
           ix = ix + 1
           if (ix > Nx) then
             ix = 1
             iy = iy+1
             if (iy > Ny) call opc_abort('opc_lib.f90::fourier','error running out of array boundaries')
           end if   
           data_memory(ix,iy,1:nslices) = data_in(1:nslices,j)
         end do
       end if
       ! now collect data from other nodes and store in data_memory via data_in
       do i = 1, mpi_size-1 ! do not send/receive from to master node
       	 n = nFFT
         if (i < nFFT_extra) n = nFFT+1
         if (mpi_rank == i) then
#ifdef MPI
           call MPI_SEND(data_in, n*nslices, MPI_DOUBLE_COMPLEX, 0, i, MPI_COMM_WORLD, mpi_err)
#endif
         end if
         if (mpi_rank == 0) then
#ifdef MPI
           call MPI_RECV(data_in, n*nslices, MPI_DOUBLE_COMPLEX, i, i, MPI_COMM_WORLD, istatus, mpi_err)
           if (mpi_size < 10) print *, 'Master node received data from node ', i
#endif     
           do j = 1, n  !! set data in right order for FFT
             ix = ix + 1
             if (ix > Nx) then
               ix = 1
               iy = iy+1
               if (iy > Ny) call opc_abort('opc_lib.f90::fourier','error running out of array boundaries')
             end if   
             data_memory(ix,iy,1:nslices) = data_in(1:nslices,j)
           end do  
         end if
       end do 
       ! now write the data back to file
       if (mpi_rank==0) then
         print *,'Writing data back to file'
         recsize = 16*Nx*Ny;
         open( unit=FHOD, file=trim(out_file), recl=recsize, access='direct', status='replace', iostat=ierr )
         if (ierr /= 0) call opc_abort('opc_lib.f90::fourier','Error opening file ' // trim(out_file))
         do is=1,nslices
            write( unit=FHOD, rec=is, iostat=ierr ) ((data_memory(ix,iy,is), ix=1,Nx), iy=1,Ny)
           if (ierr /= 0) call opc_abort('opc_lib.f90::fourier','error writing file ' // trim(out_file))
         end do  
         close(FHOD)
       end if
       if (mpi_rank == 0) call fftw_free(p_data_memory) ! free up memory
     else         
       ! create file view for accessing the data column wise
       !! commit new data layout
#ifdef MPI
       call MPI_TYPE_VECTOR(nslices, 1, Nx*Ny, MPI_DOUBLE_COMPLEX, IO_column, mpi_err)
       if (mpi_err /= MPI_SUCCESS) call opc_abort ('opc_lib.f90::fourier','Error creating type for column file view')
       call MPI_TYPE_COMMIT(IO_column, mpi_err)
       if (mpi_err /= MPI_SUCCESS) call opc_abort ('opc_lib.f90::fourier','Error committing vector type for column view')
       !! open data file to read data
       amode = MPI_MODE_RDONLY
       call MPI_FILE_OPEN(MPI_COMM_SELF, trim(in_file), amode, MPI_INFO_NULL, IFH, mpi_err)
       if (mpi_err /= MPI_SUCCESS) call opc_abort ('opc_lib.f90::fourier','Error opening file ' // trim(in_file) // ' for reading')
       !! open data file to write data
       amode = IOR(MPI_MODE_CREATE,MPI_MODE_WRONLY)
       call MPI_FILE_OPEN(MPI_COMM_SELF, trim(out_file), amode, MPI_INFO_NULL, OFH, mpi_err)                 
       if (mpi_err /= MPI_SUCCESS) call opc_abort ('opc_lib.f90::fourier','Error opening file ' // trim(out_file) // ' for writing')
#endif   
      
       !! loop over grid points
       if (mpi_rank == 0) print *, 'Start reading and Fourier transforming data ...'
       if (to_freq_domain) then
         norm = dt
       else 
         norm = one/((nslices-1)*dt)  ! dfreq
       end if
 	     iprogress = Nx*Ny/20
       do i=1+mpi_rank,Nx*Ny,mpi_size
       	  if (modulo(i, iprogress) == 0) write(*, "(A,I3,A)") 'Progress is ', 5*i/iprogress, ' %'
#ifdef MPI
          !! read data
          offset = (i-1) * sizeof(data_in(1,1)) 
          call MPI_FILE_SET_VIEW(IFH, offset, MPI_DOUBLE_COMPLEX, IO_column, 'native',  MPI_INFO_NULL, mpi_err)
          call MPI_FILE_READ_ALL(IFH, data_in(:,1), nslices, MPI_DOUBLE_COMPLEX, istatus, mpi_err)
          if (mpi_err /= MPI_SUCCESS) call opc_abort('opc_lib.f90::fourier', 'Error reading from filename ' // trim(in_file))
#endif
          !! perform FFT
          call fftw_execute_dft ( plan_spectrum, data_in(:,1), data_in(:,1) ) 
          !! normalise 
          data_in = norm * data_in
#ifdef MPI
          !! write data 
          call MPI_FILE_SET_VIEW(OFH, offset, MPI_DOUBLE_COMPLEX, IO_column, 'native',  MPI_INFO_NULL, mpi_err)
          call MPI_FILE_WRITE_ALL(OFH, data_in(:,1), nslices, MPI_DOUBLE_COMPLEX, istatus, mpi_err)
          if (mpi_err /= MPI_SUCCESS) call opc_abort('opc_lib.f90::fourier', 'Error writing to filename ' // trim(out_file))        
#endif
       end do
#ifdef MPI
       call MPI_FILE_CLOSE(IFH, mpi_err)
       if (mpi_err /= MPI_SUCCESS) call opc_abort('opc_lib.f90::fourier', 'Error closing file ' // trim(in_file))        
       call MPI_FILE_CLOSE(OFH, mpi_err)
       if (mpi_err /= MPI_SUCCESS) call opc_abort('opc_lib.f90::fourier', 'Error closing file ' // trim(out_file))        
#endif
     end if
  end subroutine fourier

  
  
  Function replace_string(string, substring1, substring2)
     ! =====================================================================
     ! replace substring1 by substring2 in string
     ! =====================================================================
     character(len=100), intent (IN) :: string
     character(len=*), intent (IN)   :: substring1, substring2
     character(len=100)              :: replace_string
     
     i1 = index(string, substring1, .TRUE.)  ! .TRUE. returns the last occurance in the string
     l1 = len_trim(substring1)
     l2 = len_trim(substring2)
     l  = len_trim(string)
     if ( (l-l1+l2)>len(string) ) &
       call opc_abort('opc_lib.f90::replace', 'total lengt of final string is larger than size of string')
     replace_string = string(:i1-1) // trim(substring2) // trim(string(i1+l1:))
     return
  end function replace_string
  
  
  subroutine opc_abort (caller, message)
     ! ==================================================================
     ! Routine that prints prints error message produced by caller
     ! ------------------------------------------------------------------
     use opc_globals
#ifdef MPI
     use mpi
#endif
     implicit none ! be strict
      
     character (len=*), INTENT(IN)                :: message
     character (len=*), INTENT(IN)                :: caller
     integer                                      :: mpi_rank, mpi_size
 
#ifdef MPI
     integer                                      :: mpi_err
#else
     integer                                      :: recsize, ix, iy, ierr
#endif

#ifdef MPI 
!initialize MPI
     call MPI_COMM_RANK(MPI_COMM_WORLD, mpi_rank, mpi_err)
     call MPI_COMM_SIZE(MPI_COMM_WORLD, mpi_size, mpi_err) 
     call MPI_FINALIZE(mpi_err) 
#else
     mpi_rank = 0 ! default value for single node execution 
     mpi_size = 1 ! default value for single node execution
#endif
     if (mpi_rank == 0) then
     	 print *, trim(caller) // " -> " // trim(message)
     end if
  	 stop
  end subroutine opc_abort
  
  subroutine check_file (in_file, fstat, fpos)
     ! ==================================================================
     ! Routine that checks status of file and need to replace or append
     ! ------------------------------------------------------------------
     character(len=*), intent (INOUT)     ::  in_file, fstat, fpos
     integer                              ::  file_stat
     logical                              ::  fexist
     file_stat = 0
     do while (in_file(1:1) == '>')
       file_stat = file_stat + 1
       in_file = in_file(2:len(in_file))
     end do
     if (file_stat <= 1) then
       fstat = 'REPLACE'
       fpos = 'REWIND'
     end if
     if (file_stat >= 2) then
       INQUIRE (file=trim(in_file), EXIST=fexist) ! check if file exist, open new if not	
       if (fexist) then
         fstat = 'OLD'
     	 else
     	 	 fstat = 'REPLACE'
     	 end if
       fpos = 'APPEND'
   	 end if
  end subroutine check_file



  subroutine write_header(iunit, cmd, direction, offset, meshx, meshy) 
 ! ==================================================================
 ! write header for diagnostic files requested by users
 ! ------------------------------------------------------------------
  use opc_globals
  implicit none
 
  integer, intent(in)                   :: iunit
  character (len=*), intent(in)         :: cmd
  character (len=*), intent(in)         :: direction
  real (kind=rk16), intent(in)          :: offset
  real (kind=rk16), intent(in)          :: meshx
  real (kind=rk16), intent(in),optional :: meshy
  
  
  write(unit=iunit,fmt="('# direction: ',A)") direction
  write(unit=iunit,fmt="('# lambda:    ',E13.6)") lambda
  write(unit=iunit,fmt="('# mesh_x:    ',E13.6)") meshx
  if (present(meshy)) write(unit=iunit,fmt="('# mesh_y:    ',E13.6)") meshy
  write(unit=iunit,fmt="('# npoints_x: ',I5)") Nx
  write(unit=iunit,fmt="('# npoints_y: ',I5)") Ny
  write(unit=iunit,fmt="('# nslices:   ',I5)") nslices
  write(unit=iunit,fmt="('# type:      ',A)") trim(cmd)
  write(unit=iunit,fmt="('# zsep:      ',F12.4)") zsep

 end subroutine write_header

END MODULE

