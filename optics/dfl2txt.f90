
! ##################################################################
! Copyright (C) 2006 J.G. Karssenberg, P.J.M. van der Slot,
! I.V.Volokhine
!
! This program is free software; you can redistribute it and/or
! modify it under the terms of the GNU General Public License as
! published by the Free Software Foundation; version 2 of the
! License.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! ##################################################################


program dfl2txt
! ==================================================================
! Program to print human readable values (intensity of phase) from
! the complix electric field from a binary dfl file
! ------------------------------------------------------------------
      
 use opc_globals
 use opc_lib
 implicit none ! be strict
      
 integer            :: iarg, irec, ix, iy, ioff_x, iend_x, ioff_y, iend_y, is, imax 
 integer, INTRINSIC :: iargc
 integer            :: recsize, ierr, slen, ifirst, ilast, j
 real (kind=rk16)   :: perarea
 character(len=100) :: fieldfile, conffile, argument
 character(len=40)  :: fmt2D
 character(len=5)   :: output_mode
 
 
! check number of command line arguments
 iarg = iargc()
 if (iarg < 5) then
   ! right number of arguments specified, argument is config file
   call getarg(1, conffile)
   call getarg(2, fieldfile)
   call getarg(3, argument)
   read(argument, '(i4)') irec 
   output_mode = 'i'
   if (iarg == 4) then
  	 call getarg(4, output_mode)
   end if
 else
   stop "dfl2txt.f90::main -> Error, wrong number of command line arguments"
 end if 

 if (.NOT.(trim(output_mode) == 'i' .OR. output_mode == 'p') ) then
 	 stop "dfl2txt.f90::main -> Error, unknown output mode requested"
 end if
 
 spectral_domain= .FALSE.
 slen = len(trim(fieldfile))
 if (fieldfile(slen-2:slen)=='sdl') spectral_domain = .TRUE.
 ! when an FFT is preformed on a .dfl file, the spectral components are ordered
 ! as follows, positive frequency in first half of array and the negative frequencies
 ! in the second half of the array (0, f1, f2, ..., fn, -fn, -fn-1, ,,, -f1)
 ! We want to return the data in the correct order when we convert to ascii
 ! The variable spectral_domain signals if we are dealing with spectral data (.TRUE.)
 ! or temporal data (.FALSE.)
 
 call opc_init(conffile)
 
 call field_offset(ioff_x, iend_x, ioff_y, iend_y)
   
! If irec > 0 we only output one slice
! else we loop over all slice as defined by ns

 if (irec > 0) then
   imax=irec
 else
   irec=1
   imax=nslices
 end if
 
 write (unit=fmt2D, fmt=10) Nx,Ny ! compile format
 perarea = one/(mesh_x*mesh_y)
 
 if (trim(output_mode) == 'i') then  ! intensity is requested
   if (spectral_domain) then
   	 if (imax == irec) then
   	 	 call read_dfl(fieldfile, irec, grid)
       write (unit=*,fmt=fmt2D)  & 
         ( ( perarea * abs(grid(ix,iy))**2, ix=ioff_x, iend_x, 1), iy=ioff_y, iend_y, 1)
         ! multiply by 'perarea' because value stored at grid points is amplitude
         ! times square root of area associated with grid point.
   	 else
   	   ifirst = imax/2+1 ! first second half = negative frequencies, then first half = positive frequencies
   	   ilast  = imax
   	   do j=1,2 ! loop first over negative and then over positive frequencies
   	   	 do is=ifirst,ilast           
           call read_dfl(fieldfile, is, grid)
           write (unit=*,fmt=fmt2D)  & 
             ( ( perarea * abs(grid(ix,iy))**2, ix=ioff_x, iend_x, 1), iy=ioff_y, iend_y, 1)
             ! multiply by 'perarea' because value stored at grid points is amplitude
             ! times square root of area associated with grid point.
         end do
         ifirst = 1  
   	   	 ilast  = imax/2
       end do
     end if
   else	
     do is=irec,imax ! loop one or more slices      
       call read_dfl(fieldfile, is, grid)
       write (unit=*,fmt=fmt2D)  & 
         ( ( perarea * abs(grid(ix,iy))**2, ix=ioff_x, iend_x, 1), iy=ioff_y, iend_y, 1)
         ! multiply by 'perarea' because value stored at grid points is amplitude
         ! times square root of area associated with grid point.
     end do
   end if
 else if (trim(output_mode) == 'p') then ! phase is requested
 if (spectral_domain) then
   	 if (imax == irec) then
   	 	 call read_dfl(fieldfile, irec, grid)
       write (unit=*, fmt=fmt2D) &
         	( ( atan2(aimag(grid(ix, iy)), dble(grid(ix, iy))), ix=ioff_x, iend_x, 1), iy=ioff_y, iend_y, 1)
   	 else
   	   ifirst = imax/2+1 ! first second half = negative frequencies, then first half = positive frequencies
   	   ilast  = imax
   	   do j=1,2 ! loop first over negative and then over positive frequencies
   	   	 do is=ifirst,ilast ! 
           call read_dfl(fieldfile, is, grid)
           write (unit=*, fmt=fmt2D) &
           	  ( ( atan2(aimag(grid(ix, iy)), dble(grid(ix, iy))), ix=ioff_x, iend_x, 1), iy=ioff_y, iend_y, 1)      
         end do
         ifirst = 1  
   	   	 ilast  = imax/2
       end do
     end if
   else	
     do is=irec,imax ! loop one or more slices      
       call read_dfl(fieldfile, is, grid)
       write (unit=*, fmt=fmt2D) &
       	  ( ( atan2(aimag(grid(ix, iy)), dble(grid(ix, iy))), ix=ioff_x, iend_x, 1), iy=ioff_y, iend_y, 1)
     end do
   end if	
 end if

 10 format ('(',I10,'(',I10,'(E16.9, /), /))')

end ! program dfl2txt
