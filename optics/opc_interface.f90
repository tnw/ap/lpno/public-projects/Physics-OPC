 MODULE opc_interface

! ##################################################################
! Copyright (C) 2009 P.J.M. van der Slot
!
! This program is free software; you can redistribute it and/or
! modify it under the terms of the GNU General Public License as
! published by the Free Software Foundation; version 2 of the
! License.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
!
! ##################################################################

 interface

   subroutine write_header(iunit, cmd, direction, offset, mesh)
     use opc_globals
     use kinds, only : rk16
     implicit none
 
     integer, intent(in)              :: iunit
     character (len=*), intent(in)    :: cmd
     character (len=*), intent(in)    :: direction
     real (kind=rk16), intent(in)     :: offset
     real (kind=rk16), intent(in)     :: mesh
   end subroutine write_header

 end interface

end module opc_interface
