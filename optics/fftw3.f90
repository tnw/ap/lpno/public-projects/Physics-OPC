 module FFTW3
   !! module to include in Fortran programs to use FFTW3
   !! Fortran compilers must support iso_c_binding
   use, intrinsic :: iso_c_binding
   include 'fftw3.f03'
 end module