program gaussian
! ==================================================================
! Program to create gaussian.dfl
! ------------------------------------------------------------------
     
 use opc_globals
 use opc_lib
 use opc_math
 implicit none   ! be strict

 integer            :: j, ix, iy, iarg, lx, ly, is, ncentre
 integer, INTRINSIC :: iargc
 real (kind=rk16)   :: w0, z0, zR, r2, I0, p0, xp, yp, ghx, ghy, Psi_z, Rz, area
 real (kind=rk16)   :: trms, wz, wz2, const_r, z_slice, tslice, rad_offset, tfactor
 complex (kind=rk16):: q, q0, const_c
 character(len=100) :: conffile, modetype, tprofile, filename
 character(len=100) :: arg
 integer            :: irec
 real (kind=rk16), dimension(:), pointer :: hx,hy,lr
 logical            :: debug = .false.
 real (kind=rk16), allocatable  :: roots(:), zerror(:) 
 complex (kind=rk16):: Bnm
 real (kind=rk16)   :: rootBJP, power, const, kappa
 complex (kind=rk16):: Ex1, Ex2, Ey1, Ey2    ! Calculated fields at x,y for mode in, im
 
 debug=.false.
 ! check number of command line arguments
 iarg = iargc()
 if (iarg == 11) then
   ! right number of arguments specified, argument is config file
   call getarg(1, conffile)
   call getarg(2, outfile)
   call getarg(3, arg)
   read(arg,'(D13.6)') w0     ! waist 
   call getarg(4, arg)
   read(arg, '(D13.6)') z0    ! position of waist
   call getarg(5, arg)
   read(arg,'(D13.6)') p0     ! (peak) intensity in the optical pulse (at location of waist)
   call getarg(6, arg) 
   read(arg,'(I4)') lx        ! first mode number
   call getarg(7, arg) 
   read(arg,'(I4)') ly        ! second mode number
   call getarg(8, arg)
   read(arg,'(D13.6)') trms   ! rms_duration in time of intensity profile I(t)
   call getarg(9, tprofile)   ! temporal profile: 'gaussian, parabolic, sech2
   call getarg(10, arg) 
   read(arg,'(D14.6)') rad_offset ! offset of radiatin field with respect to center of time window
   call getarg(11, modetype)   ! mode type 'laguerre', 'hermite' of 'plane'
   if (debug) then
   	 print *,'dfl_gauss: conffile   = ', trim(conffile)
   	 print *,'           w0         = ', w0
   	 print *,'           z0         = ', z0
   	 print *,'           p0         = ', p0
   	 print *,'           lx         = ', lx
   	 print *,'           ly         = ', ly
   	 print *,'           trms       = ', trms
   	 print *,'           tprofile   = ', trim(tprofile)
   	 print *,'           rad_offset = ', rad_offset
   	 print *,'           mode_type  = ', trim(modetype)
   end if
 else
   print *,"Dfl_gauss.f90 -> Error, wrong number of command line arguments"
 end if

 call opc_init(conffile)  
 
 zR = pi*w0**2/lambda
 area = mesh_x*mesh_y

 if (trim(modetype) == 'hermite') then
   ! initialise for Hermite-Gaussian modes
   allocate( hx(0:lx) )
   call Hermite_Coeff(lx, hx)
   allocate( hy(0:ly) )
   call Hermite_Coeff(ly, hy)
 else if (trim(modetype) == 'laguere') then
   ! initialise for Laguerre-Gaussian modes
   allocate( lr(0:lx) )
   call Laguerre_Coeff(lx, lr)
 else if (trim(modetype) == 'cylindrical') then
   ! lx -> n
   ! ly -> m
   ! w0 -> Rwg
   allocate( roots(ly) )
   allocate( zerror(ly) )
   call rootJP(lx,ly,roots,zerror)
   rootBJP=roots(ly)
   ! to determine mode amplitude, first set it to one and calculate power to renormalise
   Bnm = cmplx(one, zero)
   kappa = rootBJP/w0   ! transverse wavenumber, w0 = Rwg
   power = TE_Power(lx,ly,Bnm,rootBJP,w0)
   Bnm = Bnm*sqrt(p0/Power)
 end if

 ! values at grid points are the complex amplitude times the square root
 ! of the area associated with the grid point (same as in .dfl file)
 ! the magnitude of the complex amplitude squared is equal to the local
 ! intensity at the grid point. 
 
 ! determine the slice number corresponding to the center of the pulse
 ! rad_offset is the offset of the pulse with respect to the center of time
 ! window
 ncentre = floor(nslices/two + half - cl*rad_offset/(lambda*zsep))

 do is=1,nslices      
   ! calculated the temporal profile factor for the slice (middle of pulse = 1)
   ! note, the profile is for the intensity, so for the field we require the sqrt of the
   ! profile.
   tslice = (is-ncentre)*zsep*lambda/cl  ! time of slice relative to centre of pulse
   if (trim(tprofile) == 'gaussian') then
     tfactor = exp(-((tslice/trms)**2)/four)
   else if (trim(tprofile) == 'parabolic') then
     if ((tslice < -sqrt(5.0d0)*trms) .OR. (tslice > sqrt(5.0d0)*trms)) then
       tfactor = zero
     else
       tfactor = sqrt( (one- ((tslice/trms)**2)/5.0d0) )
     end if
   else if (trim(tprofile) == 'sech2') then
     ! factor = sech(t/tau); Tfwhm = 2*tau * ln( sqrt(2)+1 ); Trms = pi*tau/( 2*sqrt(3) )
     tfactor = one/cosh(tslice*pi /( two*sqrt(three)*trms ))
   else
     tfactor = one ! default, spatial profile is constant in time
   end if
   if (trim(modetype) == 'plane') then
     ! fille grid with  plane wave
     I0 = sqrt(p0*area)          ! Power is assumed to be over an area of 1 square meter
     grid = tfactor*dcmplx(I0, zero) 
   else if (trim(modetype) == 'hermite') then
     ! intensity
     ! determine distance of slice to position of waist
     z_slice = z0 - (is - ncentre)*lambda*zsep
     wz = w0 * dsqrt(1 + (z_slice/zR)**2)      ! width of beam
     wz2 = wz**2
     if (abs(z_slice)<1e-10) then
     	 Rz = 1d99                               ! set to large value (should be infinite)
     else 
       Rz = z_slice + zR**2/z_slice            ! Radius of curvature
     end if
     Psi_z = atan(z_slice/zR)                  ! Guoy phase shift
     const_r=one
     do j=1,lx
       const_r=const_r*j  ! lx!
     end do
     do j=1,ly
       const_r=const_r*j  ! lx!ly!
     end do
     const_c = dsqrt(p0*area)*dsqrt(one/(const_r * 2**lx * 2**ly))*w0/wz
     const_c = tfactor * const_c * exp(dcmplx(zero,one)*(lx+ly+1)*Psi_z)
     ! fill grid with gausian shape
     do iy=1, Ny
       do ix=1, Nx
         xp = xaxis(ix)         ! grid coordinates
         yp = yaxis(iy)
         r2 = xp**2 + yp**2     ! radial position
         xp = dsqrt(two)*xp/wz  ! rescale for argument Hermite polynomial
         yp = dsqrt(two)*yp/wz  
         ghx = zero
         ghy = zero
         do j = 0, lx
           ghx = ghx + hx(j)*xp**j
         end do
         do j = 0, ly
           ghy = ghy + hy(j)*yp**j
         end do
         grid(ix, iy) = const_c * ghx * ghy * exp(-half*dcmplx(zero,one)*k_wave*r2/Rz - r2/wz2) 
       end do
     end do
   else if (trim(modetype) == 'laguerre') then
   else if (trim(modetype) == 'cylindrical') then
     grid = cmplx(zero,zero)
     gridy = cmplx(zero,zero)
     const = sqrt(area*cl*eps0/two)
     include_Ey = .TRUE.
     do iy = 1, Ny
       do ix = 1, Nx
         if ( (xaxis(ix)**2 + yaxis(iy)**2) > w0**2 ) cycle  ! w0 = Rwg
         call Enm(lx, ly, xaxis(ix), yaxis(iy), kappa, Ex1, Ey1, Ex2, Ey2)
         Ex1 = const*Bnm*Ex1
         Ey1 = const*Bnm*Ey1
         grid(ix,iy)  = grid(ix,iy)  + Ex1
         gridy(ix,iy) = gridy(ix,iy) + Ey1
       end do ! loop over y-coordinates
     end do  ! loop over x-coordinates  
   end if 

   call write_dfl(outfile, is, grid)
   if ( include_Ey )then
     filename = replace_string(outfile, '.dfl','_y.dfl')
     call write_dfl(filename, is, gridy)
   end if
   if (debug) print *,is,maxval(abs(grid))

 end do

end ! program gaussian
