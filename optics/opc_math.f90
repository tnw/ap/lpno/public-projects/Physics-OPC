MODULE opc_math

CONTAINS

  SUBROUTINE Hermite_Coeff(n,A)
    USE kinds
    INTEGER (kind=ik4), INTENT(IN)                         :: n
    REAL (kind=rk16), DIMENSION(:), pointer, INTENT(INOUT) :: A
    REAL (kind=rk16), DIMENSION(:,:), pointer              :: B
    INTEGER (kind=ik4)                                     :: i,j
        
    IF (n == 0) THEN
      A(0) = 1.0d0
      RETURN
    ELSE IF (n == 1) THEN
      A(0) = 0.0d0
      A(1) = 2.0d0
      RETURN
    END IF
    ! initialise
    allocate (B(0:n, 0:n))
    B=0.d0
    B(0,0)=1.0d0
    B(1,0)=0.0d0
    B(1,1)=2.0d0
    DO i = 2, n
      B(i,0)=-2.0d0*(i-1)*B(i-2,0)
      DO j = 1, i
        !Basic recursion relation
        B(i,j)=2.0d0*B(i-1,j-1) - 2.0d0*(i-1)*B(i-2,j)
      END DO
    END DO
 
    DO i = 0, n
      A(i)=B(n, i)
    END DO
    
    deallocate(B)
    
    RETURN

  END SUBROUTINE Hermite_Coeff


  SUBROUTINE Laguerre_Coeff(n,A)
  !***************************************************
  !* Laguerre polynomial coefficients evaluation by  *
  !*'* means of recursion relation. The order of the *
  !* polynomial is n. The coefficients are returned  *
  !*'* in A(i).                                      *
  !***************************************************
    USE kinds
    INTEGER (kind=ik4), INTENT(IN)                :: n
    REAL (kind=rk16), DIMENSION(0:n), INTENT(OUT) :: A
    REAL (kind=rk16), DIMENSION(0:n, 0:n)         :: B
    INTEGER (kind=ik4)                            :: i,j
    REAL (kind=rk16)                              :: fact
    
    !Initialise
    B=0.0d0
    B(0,0)=1.0d0
    B(1,0)=1.0d0
    B(1,1)=-1.d0
    IF (n == 0) THEN
      A(0) = 1.0d0
      RETURN
    ELSE IF (n == 1) THEN
      A(0) = 1.0d0
      A(1) = -1.0d0
      RETURN
    END IF

    fact = 1.0d0  ! calculate factorial n!
    DO i = 2, n
      fact = fact*i
      B(i,0)=(2*i-1)*B(i-1,0) - (i-1)*(i-1)*B(i-2,0)
      DO j = 1, i
        !Basic recursion relation
        B(i,j)=(2*i-1)*B(i-1,j) - B(i-1,j-1) - (i-1)*(i-1)*B(i-2,j)
      END DO
    END DO

    DO i = 0, n
      A(i)=B(n,i)/fact
    END DO

    RETURN

  END SUBROUTINE Laguerre_Coeff


  Subroutine Enm(in, im, x, y, kappa, Ex1, Ey1, Ex2, Ey2)
  ! returns the normalised (amplitude=1) field components of a TE mode in 
  ! a cylindrical waveguide
  ! inputs
  ! in, im  : mode numbers (integers, n=0,1,...  and m=1,2,3,....
  ! x,y     : coordinates for location field components
  ! kappa   : transverse wavenumber
  ! outputs
  ! Ex1, Ey1 : field components for x-polarisation 
  ! Ex2, Ey2 : field components for y-polarisation (mutually orthogonal)
  ! 
  ! author : PJM van der Slot
  ! date   : 23 july 2013
  !          initial version
    ! ==================================================================
    ! Calculates Ex and Ey at location x, y in a cylindrical waveguide
    ! for mode in,im, for an amplitude of 1!
    ! ------------------------------------------------------------------
  
    use opc_globals
    implicit none
    integer, intent (IN)             :: in, im   ! mode numbers      
    real(kind=rk16), intent (IN)     :: x, y     ! coordinates in the grid
    real(kind=rk16), intent (IN)     :: kappa    ! transverse wavenumber of mode
    complex(kind=rk16), intent (OUT) :: Ex1, Ey1, Ex2, Ey2 ! x, y components of the mode field amplitude
    real(kind=rk16)                  :: r, phi   ! polar coordinates
    real(kind=rk16)                  :: k0Z0     ! product of vacuum wavenumber and vacuum impedance
    real(kind=rk16)                  :: kappar   ! product of kappa times r
    real(kind=rk16)                  :: snp, cnp, sp, cp ! sin(cos) of in*phi, sin(cos) of phi
    real(kind=rk16)                  :: dfin     ! float of IN
    real(kind=rk16)                  :: jnmkr, djnmkr ! bessel_JN(kappa*r) and derivative.
  
    k0Z0 = (TwoPi*VacImp/lambda)
    r = sqrt(x**2 + y**2)
    if (include_Ey) then
      if (abs(r) <= 1.0D-10) then
        if (in == 1) then
          Ex1 = cmplx(zero, -half*k0Z0*kappa)
          Ex2 = cmplx(zero, zero)
          Ey1 = cmplx(zero, zero)
          Ey2 = cmplx(zero, half*k0Z0*kappa)
        else
          Ex1 = cmplx(zero, zero)
          Ex2 = cmplx(zero, zero)
          Ey1 = cmplx(zero, zero)
          Ey2 = cmplx(zero, zero)
        end if
      else
        phi = atan2(y,x)
        kappar = kappa*r
        dfin = dfloat(in)
        jnmkr = bessel_JN(in,kappar)/kappar
        djnmkr = besselJP(in,kappar)
        cnp = cos(dfin*phi)
        cp = cos(phi)
        snp = sin(dfin*phi)
        sp = sin(phi)
        Ex1 = k0Z0*kappa*cmplx(zero, -dfin*jnmkr*cnp*cp - djnmkr*snp*sp)
        Ey1 = k0Z0*kappa*cmplx(zero, -dfin*jnmkr*cnp*sp + djnmkr*snp*cp)
        Ex2 = k0Z0*kappa*cmplx(zero, +dfin*jnmkr*snp*cp - djnmkr*cnp*sp)
        Ey2 = k0Z0*kappa*cmplx(zero, +dfin*jnmkr*snp*sp + djnmkr*cnp*cp)
      end if  
    else ! no need for Ey
       if (abs(r) <= 1.0D-10) then
        if (in == 1) then
          Ex1 = cmplx(zero, -half*k0Z0*kappa)
          Ex2 = cmplx(zero, zero)
        else
          Ex1 = cmplx(zero, zero)
          Ex2 = cmplx(zero, zero)
        end if
      else
        phi = atan2(y,x)
        kappar = kappa*r
        dfin = dfloat(in)
        jnmkr = bessel_JN(in,kappar)/kappar
        djnmkr = besselJP(in,kappar)
        cnp = cos(dfin*phi)
        cp = cos(phi)
        snp = sin(dfin*phi)
        sp = sin(phi)
        Ex1 = k0Z0*kappa*cmplx(zero, -dfin*jnmkr*cnp*cp - djnmkr*snp*sp)
        Ex2 = k0Z0*kappa*cmplx(zero,  dfin*jnmkr*snp*cp - djnmkr*cnp*sp)
      end if 
      Ey1 = cmplx(zero,zero)
      Ey2 = cmplx(zero,zero)
    end if ! do we need to calculate Ey?
    return     
  end subroutine Enm
  
  
  function TE_Power(n,m,Amplitude,kappar,rw)
  ! returns the power of a TE mode in a cylindrical waveguide
  ! n, m     : mode numbers (integers, n=0,1,...  and m=1,2,3,....
  ! Amplitude: complex amplitude of mode
  ! rw       : radius waveguide
  ! kappar   : product of transverse wavenumber and rw
  ! 
  ! author : PJM van der Slot
  ! date   : 23 july 2013
  !          initial version
    use opc_globals
    implicit none
    
    integer, intent (IN)            :: n, m     ! mode numbers
    complex(kind=rk16), intent (IN) :: Amplitude
    real(kind=rk16), intent (IN)    :: kappar   ! product transverse wavenumber and radius waveguide
    real(kind=rk16), intent (IN)    :: rw       ! waveguide radius
    real(kind=rk16)                 :: TE_Power
    ! local variables
    real(kind=rk16)                 :: k0Z0, beta, fn    
    
    k0Z0 = (TwoPi*VacImp/lambda)
    beta = sqrt((twopi/lambda)**2 - (kappar/rw)**2)
    fn = float(n)
    TE_Power = half*k0Z0*beta*pi*(kappar**2-fn**2)*(abs(Amplitude)*bessel(n,kappar))**2
    if (n > 0) TE_Power = half*TE_Power
    return
  end function TE_Power
  
  
  function TM_Power(n,m,Amplitude,kappar,rw)
  ! returns the power of a TM mode in a cylindrical waveguide
  ! n, m     : mode numbers (integers, n=0,1,...  and m=1,2,3,....
  ! Amplitude: complex amplitude of mode
  ! rw       : radius waveguide
  ! kappar   : product of transverse wavenumber and rw
  ! 
  ! author : PJM van der Slot
  ! date   : 23 july 2013
  !          initial version
    use opc_globals
    implicit none
    
    integer, intent (IN)            :: n, m     ! mode numbers
    complex(kind=rk16), intent (IN) :: Amplitude
    real(kind=rk16), intent (IN)    :: kappar   ! product transverse wavenumber and radius waveguide
    real(kind=rk16), intent (IN)    :: rw       ! waveguide radius
    real(kind=rk16)                 :: TM_Power
    ! local variables
    real(kind=rk16)                 :: k0Y0, beta, fn    
    k0Y0 = TwoPi/(VacImp*lambda)
    beta = sqrt((twopi/lambda)**2 - (kappar/rw)**2)
    
    fn = float(n)
    TM_Power = k0Y0*beta*pi*abs(Amplitude)**2*(kappar**2-fn**2)*besselJP(n,kappar)**2
    if (n == 0) then
      TM_Power = TM_Power/two
    else
      TM_power = TM_power/four
    end if
    return
  end function TM_Power
  	
    
  SUBROUTINE FCS ( t, c, s )
    USE kinds
    IMPLICIT none
    !*******************************************************************************
    ! subroutine FCS evaluates the real Fresnel Cosine (C) and Sine (S)
    ! integrals
    ! 
    !  Parameters:
    !
    !    Input, T, double precision
    !
    !    Output, C, S, double precision
    !
    ! subroutine is taken from NSWC library of mathematics subroutines
    !
    !*******************************************************************************
    REAL (kind=rk16), INTENT(IN) :: t
    REAL (kind=rk16), INTENT(OUT) :: c,s
    
    REAL (kind=rk16) :: a(6)
    REAL (kind=rk16) :: ad(6)
    REAL (kind=rk16) :: an(6)
    REAL (kind=rk16) :: b(6)
    REAL (kind=rk16) :: bd(6)
    REAL (kind=rk16) :: bn(6)
    REAL (kind=rk16) :: cd(5)
    REAL (kind=rk16) :: cn(5)
    REAL (kind=rk16) :: cp(13)
    REAL (kind=rk16) :: ct
    REAL (kind=rk16) :: dd(5)
    REAL (kind=rk16) :: dn(5)
    REAL (kind=rk16) :: fp(7)
    REAL (kind=rk16) :: gp(7)
    INTEGER :: i, imax, l, m
    REAL (kind=rk16) :: p(6)
    REAL (kind=rk16) :: pd(6)
    REAL (kind=rk16) :: pn(6)
    REAL (kind=rk16) :: q(6)
    REAL (kind=rk16) :: qd(6)
    REAL (kind=rk16) :: qn(6)
    REAL (kind=rk16) :: sp(13)
    REAL (kind=rk16) :: st
    REAL (kind=rk16) :: f, fd, fn, g, gd, gn, r, x, xx, y
    REAL (kind=rk16) :: pix, pixx, n, sy, cy
    
    REAL (kind=rk16), PARAMETER :: Pi=3.14159265358979323846264338327950288419716939937510D+00
  !
    DATA a(1)/-.119278241233760D-05/,  a(2)/.540730666359417D-04/, &
         a(3)/-.160488306381990D-02/,  a(4)/.281855008757077D-01/, &
         a(5)/-.246740110027210D+00/,  a(6)/.100000000000000D+01/
    DATA b(1)/-.155653074871090D-06/,  b(2)/.844415353045065D-05/, &
         b(3)/-.312116934326082D-03/,  b(4)/.724478420395276D-02/, &
         b(5)/-.922805853580325D-01/,  b(6)/.523598775598300D+00/
  !
    DATA cp(1) /.114739945188034D-20/,  cp(2) /-.384444827287950D-18/, &
         cp(3) /.832125729394275D-16/,  cp(4) /-.142979507360076D-13/, &
         cp(5) /.198954961821465D-11/,  cp(6) /-.220226545457144D-09/, &
         cp(7) /.188434924092257D-07/,  cp(8) /-.120009722914157D-05/, &
         cp(9) /.540741337442140D-04/,  cp(10)/-.160488313553028D-02/, &
         cp(11)/.281855008777956D-01/,  cp(12)/-.246740110027196D+00/, &
         cp(13)/.999999999999996D+00/
    DATA sp(1) /.705700784853927D-22/,  sp(2) /-.252757991492418D-19/, &
         sp(3) /.594117488940008D-17/,  sp(4) /-.112161631555448D-14/, &
         sp(5) /.173332189994074D-12/,  sp(6) /-.215742302078015D-10/, &
         sp(7) /.210821173208116D-08/,  sp(8) /-.156471443116560D-06/, &
         sp(9) /.844427287845253D-05/,  sp(10)/-.312116942346186D-03/, &
         sp(11)/.724478420418951D-02/,  sp(12)/-.922805853580323D-01/, &
         sp(13)/.523598775598300D+00/
  !
    DATA pn(1)/.318309816100920D+00/,  pn(2)/.134919391391516D+02/, &
         pn(3)/.158258097490377D+03/,  pn(4)/.598796451682535D+03/, &
         pn(5)/.632369782194966D+03/,  pn(6)/.967985390141920D+02/
    DATA pd(1)/.100000000000000D+01/,  pd(2)/.426900960480796D+02/, &
         pd(3)/.509085485682426D+03/,  pd(4)/.200034664144742D+04/, &
         pd(5)/.231910140792937D+04/,  pd(6)/.486678558201084D+03/
    DATA qn(1)/.101320876178478D+00/,  qn(2)/.490534697099052D+01/, &
         qn(3)/.652095157811808D+02/,  qn(4)/.274183825747887D+03/, &
         qn(5)/.305040725009211D+03/,  qn(6)/.364566615872326D+02/
    DATA qd(1)/.100000000000000D+01/,  qd(2)/.499330024470621D+02/, &
         qd(3)/.709854097670206D+03/,  qd(4)/.343470762861172D+04/, &
         qd(5)/.522213879312684D+04/,  qd(6)/.168801831831851D+04/
  !
    DATA an(1)/.318309885869756D+00/,  an(2)/.254179177393500D+02/, &
         an(3)/.575003792540838D+03/,  an(4)/.426673405867140D+04/, &
         an(5)/.891831887923938D+04/,  an(6)/.267955736537967D+04/
    DATA ad(1)/.100000000000000D+01/,  ad(2)/.801567066285184D+02/, &
         ad(3)/.182971463354850D+04/,  ad(4)/.138848884373420D+05/, &
         ad(5)/.309228411873207D+05/,  ad(6)/.120421274105856D+05/
    DATA bn(1)/.101321181932417D+00/,  bn(2)/.925021984290547D+01/, &
         bn(3)/.240932023056602D+03/,  bn(4)/.206079616836437D+04/, &
         bn(5)/.484901973010149D+04/,  bn(6)/.130680669688315D+04/
    DATA bd(1)/.100000000000000D+01/,  bd(2)/.928158182389149D+02/, &
         bd(3)/.250926840439955D+04/,  bd(4)/.233924458152954D+05/, &
         bd(5)/.685638896406835D+05/,  bd(6)/.418593101455019D+05/
  !
    DATA cn(1)/.318309886182000D+00/,  cn(2)/.299191968327887D+02/, &
         cn(3)/.691428839605668D+03/,  cn(4)/.394539800974744D+04/, &
         cn(5)/.290314254767015D+04/
    DATA cd(1)/.100000000000000D+01/,  cd(2)/.942978925136851D+02/, &
         cd(3)/.219977296283666D+04/,  cd(4)/.129726479671006D+05/, &
         cd(5)/.114991427758165D+05/
    DATA dn(1)/.101321183630876D+00/,  dn(2)/.110988033615242D+02/, &
         dn(3)/.306282306497228D+03/,  dn(4)/.213130259794164D+04/, &
         dn(5)/.171270676541694D+04/
    DATA dd(1)/.100000000000000D+01/,  dd(2)/.111060616085627D+03/, &
         dd(3)/.318197586347414D+04/,  dd(4)/.249342095714049D+05/, &
         dd(5)/.359241903823488D+05/
  !
    DATA fp(1)/.449763389301234D+05/,  fp(2)/-.188763642051836D+04/, &
         fp(3)/.669261097103246D+02/,  fp(4)/-.343966606879114D+01/, &
         fp(5)/.343112896133346D+00/,  fp(6)/-.967546019461500D-01/, &
         fp(7)/.318309886183465D+00/
    DATA gp(1)/.316642183365360D+06/,  gp(2)/-.120618995106638D+05/, &
         gp(3)/.359164749179351D+03/,  gp(4)/-.142252603258172D+02/, &
         gp(5)/.982934118445454D+00/,  gp(6)/-.153989722912325D+00/, &
         gp(7)/.101321183639714D+00/
  !
    DATA p(1)/-654729075.0/,  p(2)/2027025.0/,  p(3)/-10395.0/, &
         p(4)/105.0/,  p(5)/-3.0/,  p(6)/1.0/
    DATA q(1)/-13749310575.0/,  q(2)/34459425.0/,  q(3)/-135135.0/, &
         q(4)/945.0/,  q(5)/-15.0/,  q(6)/1.0/
  !
    imax = HUGE ( imax )
    x = ABS(t)
    IF (x > 4.0D0) GO TO 50
    xx = x*x
    y = xx*xx
  !
  ! evaluation of c(x) and s(x) for x < 1.65
  ! where x = ABS(t)
  !
    IF (x > 0.6D0) GO TO 10
    ct = ((((a(1)*y + a(2))*y + a(3))*y + a(4))*y + a(5))*y + a(6)
    st = ((((b(1)*y + b(2))*y + b(3))*y + b(4))*y + b(5))*y + b(6)
    c = t*ct
    s = t*xx*st
    RETURN
  !
 10 IF (x >= 1.65D0) GO TO 20
    ct = cp(1)
    st = sp(1)
    DO i = 2,13
       ct = cp(i) + ct*y
       st = sp(i) + st*y
    END DO
    c = t*ct
    s = t*xx*st
    RETURN
  !
  ! evaluation of the auxiliary functions f(x) and g(x)
  ! for x >= 1.65
  !
 20 IF (x >= 2.0D0) GO TO 30
    fn = ((((pn(1)*y + pn(2))*y + pn(3))*y + pn(4))*y + pn(5))*y &
                     + pn(6)
    fd = ((((pd(1)*y + pd(2))*y + pd(3))*y + pd(4))*y + pd(5))*y &
                     + pd(6)
    gn = ((((qn(1)*y + qn(2))*y + qn(3))*y + qn(4))*y + qn(5))*y &
                     + qn(6)
    gd = ((((qd(1)*y + qd(2))*y + qd(3))*y + qd(4))*y + qd(5))*y &
                     + qd(6)
    f = fn/(x*fd)
    g = gn/(x*xx*gd)
    y = 0.5D0*xx
    GO TO 80
  !
 30 IF (x >= 3.0D0) GO TO 40
    fn = ((((an(1)*y + an(2))*y + an(3))*y + an(4))*y + an(5))*y &
                     + an(6)
    fd = ((((ad(1)*y + ad(2))*y + ad(3))*y + ad(4))*y + ad(5))*y &
                     + ad(6)
    gn = ((((bn(1)*y + bn(2))*y + bn(3))*y + bn(4))*y + bn(5))*y &
                     + bn(6)
    gd = ((((bd(1)*y + bd(2))*y + bd(3))*y + bd(4))*y + bd(5))*y &
                     + bd(6)
    f = fn/(x*fd)
    g = gn/(x*xx*gd)
    GO TO 70
  !
 40 fn = (((cn(1)*y + cn(2))*y + cn(3))*y + cn(4))*y + cn(5)
    fd = (((cd(1)*y + cd(2))*y + cd(3))*y + cd(4))*y + cd(5)
    gn = (((dn(1)*y + dn(2))*y + dn(3))*y + dn(4))*y + dn(5)
    gd = (((dd(1)*y + dd(2))*y + dd(3))*y + dd(4))*y + dd(5)
    f = fn/(x*fd)
    g = gn/(x*xx*gd)
    GO TO 70
  !
 50 IF (x >= 6.0) GO TO 60
    xx = x*x
    y = 1.0D0/(xx*xx)
    f = (((((fp(1)*y + fp(2))*y + fp(3))*y + fp(4))*y + fp(5))*y &
                     + fp(6))*y + fp(7)
    g = (((((gp(1)*y + gp(2))*y + gp(3))*y + gp(4))*y + gp(5))*y &
                     + gp(6))*y + gp(7)
    f = f/x
    g = g/(x*xx)
    GO TO 70
  
 60 IF (x >= real(imax)) GO TO 100
    pix = pi * x
    pixx = pix * x
    y = 1.0D0 / pixx
    y = y*y
    f = ((((p(1)*y + p(2))*y + p(3))*y + p(4))*y + p(5))*y + p(6)
    g = ((((q(1)*y + q(2))*y + q(3))*y + q(4))*y + q(5))*y + q(6)
    f = f / pix
    g = g / (pix*pixx)
  !
  ! evaluation of sin(0.5*pi*x*x) and cos(0.5*pi*x*x)
  ! the results are stored in sy and cy
  !
 70 m = INT(x)
    l = mod(m,2)
    n = REAL(m - l)
    y = x - m
    r = x - n
  !
    y = y*n
    m = INT(y)
    y = y - m
    IF (mod(m,2) /= 0) y = (y - 0.5D0) - 0.5D0
    y = y + 0.5D0*r*r
  !
 80 sy = sin1(y)
    cy = cos1(y)
  !
  ! termination
  !
 90 c = 0.5 + (f*sy - g*cy)
    s = 0.5 - (f*cy + g*sy)
    IF (t >= 0.0) RETURN
       c = - c
       s = - s
       RETURN
  !
100 IF (t < 0.0) GO TO 110
       c = 0.5
       s = 0.5
       RETURN
110 c = -0.5
    s = -0.5
    RETURN
    
  END SUBROUTINE FCS
  
  
  
  FUNCTION sin1(x)
    USE kinds
    IMPLICIT NONE
  !
  !*******************************************************************************
  !
  ! SIN1: evaluation  of sin(x*pi)
  !
    REAL (kind=rk16) :: sin1
    REAL (kind=rk16), INTENT(IN) :: x
    REAL (kind=rk16) :: a0, a1, a2, a3, a4, a5, a6, b1, b2, b3, b4, b5, b6
    INTEGER :: imax, n
    REAL (kind=rk16) :: a, t
  !
    DATA a0 /.314159265358979D+01/, a1 /-.516771278004995D+01/, &
         a2 /.255016403987327D+01/, a3 /-.599264528932149D+00/, &
         a4 /.821458689493251D-01/, a5 /-.737001831310553D-02/, &
         a6 /.461514425296398D-03/
    DATA b1 /-.493480220054460D+01/, b2 /.405871212639605D+01/, &
         b3 /-.133526276691575D+01/, b4 /.235330543508553D+00/, &
         b5 /-.258048861575714D-01/, b6 /.190653140279462D-02/
  !
    imax = HUGE ( imax )
    a = ABS(x)
  
    IF ( a >= REAL ( imax ) ) THEN
      sin1 = 0.0D0
      RETURN
    END IF
  
    n = INT(a)
    a = a - REAL(n)
    IF (a > 0.75D0) GO TO 20
    IF (a < 0.25D0) GO TO 21
  !
  ! 0.25 <= a <= 0.75
  !
    a = 0.25D0 + (0.25D0 - a)
    t = a*a
    sin1 = ((((((b6*t + b5)*t + b4)*t + b3)*t + b2)*t &
                      + b1)*t + 0.5D0) + 0.5D0
    GO TO 30
  !
  ! a < 0.25  or  a > 0.75
  !
 20 a = 0.25D0 + (0.75D0 - a)
 21 t = a*a
    sin1 = ((((((a6*t + a5)*t + a4)*t + a3)*t + a2)*t &
                      + a1)*t + a0)*a
  !
  ! termination
  !
 30 IF (x < 0.0D0) sin1 = - sin1
    IF (mod(n,2) /= 0) sin1 = - sin1
    RETURN
  END FUNCTION sin1
  
 
 
  FUNCTION cos1(x)
  !
  !*******************************************************************************
  !
  ! COS1: evaluation  of cos(x*pi)
  !
    USE kinds
    IMPLICIT NONE 
    
    REAL (kind=rk16) :: cos1
    REAL (kind=rk16), INTENT(IN) :: x
    REAL (kind=rk16) :: a0, a1, a2, a3, a4, a5, a6, b1, b2, b3, b4, b5, b6
    INTEGER :: imax, n
    REAL (kind=rk16) :: a, t
    
  !
    DATA a0 /.314159265358979D+01/, a1 /-.516771278004995D+01/, &
         a2 /.255016403987327D+01/, a3 /-.599264528932149D+00/, &
         a4 /.821458689493251D-01/, a5 /-.737001831310553D-02/, &
         a6 /.461514425296398D-03/
    DATA b1 /-.493480220054460D+01/, b2 /.405871212639605D+01/, &
         b3 /-.133526276691575D+01/, b4 /.235330543508553D+00/, &
         b5 /-.258048861575714D-01/, b6 /.190653140279462D-02/
  !
    imax = HUGE ( imax )
  
    a = ABS(x)
  
    IF ( a >= REAL(imax) ) THEN
      cos1 = 1.0D0
      RETURN
    END IF
  
    n = INT(a)
    a = a - REAL(n)
    IF (a > 0.75D0) GO TO 20
    IF (a < 0.25D0) GO TO 21
  !
  ! 0.25 <= a <= 0.75
  !
    a = 0.25D0 + (0.25D0 - a)
    t = a*a
    cos1 = ((((((a6*t + a5)*t + a4)*t + a3)*t + a2)*t &
                      + a1)*t + a0)*a
    GO TO 30
  !
  ! a < 0.25  or  a > 0.75
  !
 20 a = 0.25D0 + (0.75D0 - a)
    n = n - 1
 21 t = a*a
    cos1 = ((((((b6*t + b5)*t + b4)*t + b3)*t + b2)*t &
                      + b1)*t + 0.5D0) + 0.5D0
  !
  ! termination
  !
 30 IF (mod(n,2) /= 0) cos1 = - cos1
    RETURN
  END FUNCTION cos1
  
  
  
  SUBROUTINE ROOTJ(N,NK,JZERO,ZERROR)
! ----------------------------------------------------------------------
!     CALCULATE THE FIRST NK ZEROES OF BESSEL FUNCTION J(N,X)
!
!     INPUTS:
!       N    ORDER OF FUNCTION J (INTEGER >= 0)                  I*4
!       NK   NUMBER OF FIRST ZEROES  (INTEGER > 0)               I*4
!     OUTPUTS:
!       JZERO(NK)  TABLE OF FIRST ZEROES (ABCISSAS)              R*8  
!       ZERROR(NK) TABLE OF values BESSELJ(JZERO)                R*8
!
!     REFERENCE :
!     ABRAMOWITZ M. & STEGUN IRENE A.
!     HANDBOOK OF MATHEMATICAL FUNCTIONS
! ----------------------------------------------------------------------
  use kinds, only : rk16 
  implicit none
  real (kind=rk16) :: ZEROJ,B0,B1,B2,B3,B5,B7,T0,T1,T3,T5,T7,PI,FN,FK
  real (kind=rk16) :: C1,C2,C3,C4,C5,F1,F2,F3,TOL,ERRJ
  real (kind=rk16), INTENT (OUT)   :: JZERO(NK)
  real (kind=rk16), INTENT (OUT)   :: ZERROR(NK)
  real (kind=rk16)                 :: ZERR
  INTEGER, INTENT (IN) :: N, NK
  INTEGER              :: NITMX, K
  
  DATA PI/3.14159265358979D0/,TOL/1.D-12/,NITMX/20/
  DATA C1,C2,C3,C4,C5 /1.8557571D0,1.033150D0,0.00397D0,0.0908D0,0.043D0/
  FN = FLOAT(N)

! first zero
  IF (N.EQ.0) THEN
     ! estimate of zero       
     ZEROJ = C1+C2-C3-C4+C5
     ! refine zero
     CALL  SECANT(BESSEL,N,NITMX,TOL,ZEROJ,ZERR)   
	 ZERROR(1)=ZERR
     JZERO(1)=ZEROJ
  ELSE
     F1 = FN**(1.D0/3.D0)
     F2 = F1*F1*FN
     F3 = F1*FN*FN
     ZEROJ = FN+C1*F1+(C2/F1)-(C3/FN)-(C4/F2)+(C5/F3)
     CALL SECANT(BESSEL,N,NITMX,TOL,ZEROJ,ZERR)
     ZERROR(1)=ZERR
     JZERO(1)=ZEROJ
  ENDIF
  T0 = 4.D0*FN*FN
  T1 = T0-1.D0
  T3 = 4.D0*T1*(7.D0*T0-31.D0)
  T5 = 32.D0*T1*((83.D0*T0-982.D0)*T0+3779.D0)
  T7 = 64.D0*T1*(((6949.D0*T0-153855.D0)*T0 + 1585743.D0)*T0 - 6277237.D0)
! other zeros
  DO K = 2,NK
     JZERO(K) = 0.D0
     FK = FLOAT(K)
! Mac Mahon's series for k >> N
     B0 = (FK+.5D0*FN-.25D0)*PI
     B1 = 8.D0*B0
     B2 = B1*B1
     B3 = 3.D0*B1*B2
     B5 = 5.D0*B3*B2
     B7 = 7.D0*B5*B2
     ZEROJ = B0-(T1/B1)-(T3/B3)-(T5/B5)-(T7/B7)
! improve solution using secant method
     IF (ZERR.GT.TOL) CALL SECANT(BESSEL, N,NITMX,TOL,ZEROJ,ZERR)
     JZERO(K)=ZEROJ
     ZERROR(K)=ZERR
  ENDDO   
  RETURN
  END SUBROUTINE ROOTJ
  
  
  
  SUBROUTINE ROOTJP(N,NK,JPZERO, ZERROR)
!--------------------------------------------------------------------
!     CALCULATE THE FIRT NK ZEROS OF THE DERIVATIVE OF BESSEL FUNCTION
!     OF ORDER N, J(N,X) 
!--------------------------------------------------------------------
!
!     INPUTS:
!       N    ORDER OF FUNCTION J (INTEGER >= 0)                  I*4
!       NK   NUMBER OF FIRST ZEROES  (INTEGER > 0)               I*4
!     OUTPUTS:
!       JPZERO(NK)  TABLE OF FIRST ZEROES (ABCISSAS)             R*8  
!       ZERROR(NK)  values of BESSELJP(JPZERO)                   R*8
!
!     REFERENCE :
!     ABRAMOWITZ M. & STEGUN IRENE A.
!     HANDBOOK OF MATHEMATICAL FUNCTIONS!     
!---------------------------------------------------------------------
  use kinds, only : rk16
  implicit none
  INTEGER, INTENT (IN) :: N, NK
  REAL(kind=rk16), INTENT (OUT) :: JPZERO(NK), ZERROR(NK)
  
  REAL (kind=rk16) :: B0,B1,B2,B3,B5,B7,T0,T1,T3,T5,T7
  REAL (kind=rk16) :: C1,C2,C3,C4,F1,F2,F3,P,DP,P0,P1,Q0,Q1
  REAL (kind=rk16) :: TOL   ! desired accuracy for obtaining roots
  INTEGER          :: NITMX ! maximum number of evaluation to improve zero's
  REAL (kind=rk16) :: PI, ZEROJP, ZERR, FN, FK
  INTEGER          :: K
  DATA TOL/1.D-7/,NITMX/15/
  DATA PI/3.14159265358979D0/,TOL/1.D-12/,NITMX/30/
  DATA C1,C2,C3,C4 /0.8086165D0,0.072490D0,.05097D0,.0094D0/
      
  FN = FLOAT(N)
  
! first zero
  
  IF (N.EQ.0) THEN
    ZEROJP = 3.832  ! first zero is 0.0 but we do not include this 
  ELSE  
! TCHEBYCHEV'S SERIES FOR K <= N
    F1 = FN**(1.D0/3.D0)
    F2 = F1*F1*FN
    ZEROJP = FN+C1*F1+(C2/F1)-(C3/FN)+(C4/F2)
  ENDIF
  CALL SECANT(BESSELJP,N,NITMX,TOL,ZEROJP,ZERR) 
  JPZERO(1)=ZEROJP
  ZERROR(1)=ZERR

!  IF (N <= 20) THEN 
!    ZEROJP = 0.961587 + 1.07703 * FN 
!  ELSE 
!    ZEROJP = FN + 0.80861 * FN ** 0.33333 + 0.07249 / FN ** 0.33333 
!  END IF 
!  IF (N == 0) ZEROJP= 3.8317
!  CALL SECANT(BESSELJP,N,NITMX,TOL,ZEROJP,ZERR) 
!  JPZERO(1)=ZEROJP
!  ZERROR(1)=ZERR
  
! rest of the roots

! Mac Mahon's series for K >> N

  T0 = 4.D0*FN*FN
  T1 = T0+3.D0
  T3 = 4.D0*((7.D0*T0+82.D0)*T0-9.D0)
  T5 = 32.D0*(((83.D0*T0+2075.D0)*T0-3039.D0)*T0+3537.D0)
  T7 = 64.D0*((((6949.D0*T0+296492.D0)*T0-1248002.D0)*T0+7414380.D0)*T0-5853627.D0)

  DO K=2, NK
    IF (N == 0) THEN 
      FK = FLOAT(K+1) ! first root is excluded, shift by one
    ELSE 
      FK = FLOAT(K)
    END IF
    B0 = (FK+.5D0*FN-.75D0)*PI
    B1 = 8.D0*B0
    B2 = B1*B1
    B3 = 3.D0*B1*B2
    B5 = 5.D0*B3*B2
    B7 = 7.D0*B5*B2
    ZEROJP = B0-(T1/B1)-(T3/B3)-(T5/B5)-(T7/B7)
!   DO K=2, NK
!    FK = FLOAT(K)
!    ZEROJP= ZEROJP + 3.1416 + (0.4955 + 0.0915*FN - 0.000435*FN**2) / FK
! improve solution using secant method
    CALL SECANT(BESSELJP, N, NITMX, TOL, ZEROJP, ZERR)
    JPZERO(K)=ZEROJP
    ZERROR(K)=ZERR
  ENDDO   
  RETURN
  END SUBROUTINE ROOTJP

  
  SUBROUTINE SECANT(F,N,NITMX,TOL,ZEROJ,ZERROR)
  use kinds, only : rk16
  implicit none
  REAL (kind=rk16), INTENT(INOUT) :: ZEROJ
  REAL (kind=rk16), INTENT(OUT)   :: ZERROR
  REAL (kind=rk16), INTENT(IN)    :: TOL
  INTEGER, INTENT (IN)            :: N, NITMX
  REAL (kind=rk16)                :: P0,P1,Q0,Q1,DP,P,C(2)
  INTEGER                         :: IT, NTRY
  INTERFACE
    FUNCTION F(N,X) RESULT (FOUT)
      use kinds, only : rk16
      REAL (kind=rk16) :: FOUT
      REAL (kind=rk16), INTENT (IN) :: X
      INTEGER, INTENT (IN) :: N
    END FUNCTION f
  END INTERFACE
   DATA C/0.95D0,0.999D0/
   NTRY=1
     
 5 P0 = C(NTRY)*ZEROJ

   P1 = ZEROJ
   Q0 = F(N,P0)
   Q1 = F(N,P1)

   DO IT = 1,NITMX
     IF(Q1.EQ.Q0) GO TO 15
     P = P1-Q1*(P1-P0)/(Q1-Q0)
     DP = P-P1
     IF (IT.EQ.1) GO TO 10
     IF (ABS(DP).LT.TOL) GO TO 20
  
 10  P0 = P1
     Q0 = Q1
     P1 = P
     Q1 = F(N,P1)
   ENDDO
 15 NTRY=NTRY+1
    IF(NTRY.LE.2) GO TO 5
 20 ZEROJ = P
    ZERROR = F(N,ZEROJ)
  RETURN
  END SUBROUTINE SECANT



 FUNCTION BESSEL(N,X)
 ! ----------------------------------------------------------------------
 !  FIRST KIND BESSEL FUNCTION OF ORDER N, FOR REAL X
 !
 !  I  VARIABLE DIMENSION/TYPE  DESCRIPTION  (INPUTS)
 !        N       I*4           ORDER OF FUNCTION                             .
 !        X       R*8           ABSCISSA OF FUNCTION BESSELJP(N,X)             .   
 !
 !  O  VARIABLE,DIMENSION/TYPE  DESCRIPTION  (OUTPUT)  
 !
 !      BESSELJ   R*8           FUNCTION EVALUATION AT X                        .
 ! ----------------------------------------------------------------------
   use kinds, only : rk16
   IMPLICIT NONE
   REAL (KIND=RK16) :: BESSEL
   REAL (KIND=RK16), INTENT (IN) :: X
   INTEGER, INTENT (IN) :: N
 !
   BESSEL = BESSEL_JN(N,X) 
 END FUNCTION BESSEL



 FUNCTION BESSELJP (N,X)
 ! ----------------------------------------------------------------------
 !  FIRST DERIVATIVE OF FIRST KIND BESSEL FUNCTION OF ORDER N, FOR REAL X
 !
 !  I  VARIABLE DIMENSION/TYPE  DESCRIPTION  (INPUTS)
 !        N       I*4           ORDER OF FUNCTION                             .
 !        X       R*8           ABSCISSA OF FUNCTION BESSELJP(N,X)             .   
 !
 !  O  VARIABLE,DIMENSION/TYPE  DESCRIPTION  (OUTPUT)  
 !
 !      BESSELJP  R*8           FUNCTION EVALUATION AT X                        .
 ! ----------------------------------------------------------------------
 use kinds, only : rk16
 implicit none
 REAL (kind=rk16), INTENT (IN) ::  X
 INTEGER, INTENT (IN)          ::  N
 REAL (kind=rk16)              ::  BESSELJP
 

 IF (N.EQ.0) THEN
     BESSELJP = -1.0D0*BESSEL_JN(1,X)
 ELSE
     BESSELJP=0.5D0*(BESSEL_JN(N-1,X)- BESSEL_JN(N+1,X))
 ENDIF
 RETURN
 END FUNCTION BESSELJP


END MODULE

