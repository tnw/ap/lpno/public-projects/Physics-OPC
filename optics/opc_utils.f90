
! ##################################################################
! Copyright (C) 2007 J.G. Karssenberg, P.J.M. van der Slot
!
! This program is free software; you can redistribute it and/or
! modify it under the terms of the GNU General Public License as
! published by the Free Software Foundation; version 2 of the
! License.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! ##################################################################
!
!
program opc_util
! ==================================================================
! This program contails several utilities:
!   - zernike : calculates aberrations using Zernike polynomials
!               writes aberrations to an .opd file
! ------------------------------------------------------------------

 use opc_globals
 use opc_lib
 implicit none ! be strict

 integer            :: int1, int2, iarg
 real (kind=rk16)   :: dbl1, dbl2, dbl3, dbl4
 character(len=100) :: conffile, command, chr1, argument, filename1, filename2, filename3

 ! check number of command line arguments
 iarg = iargc()
 if (iarg > 2) then
   call getarg(1, conffile)
   call getarg(2, command)
 else
   print *,"opc_utils.f90::main -> Error, wrong number of command line arguments"
 end if
 call opc_init(conffile)

 ! dispatch command

 if (command == 'zernike') then
 ! print *, "executing command opld::zernike"
   if (iarg == 9) then
     call getarg(3, argument)
     read(argument,'(i4)') int1
     call getarg(4, argument)
     read(argument,'(i4)') int2
     call getarg(5, argument)
     read(argument,'(e14.6)') dbl1
     call getarg(6, argument)
     read(argument,'(e14.6)') dbl2
     call getarg(7, chr1)
     call getarg(8, argument)
     read(argument,'(e14.6)') dbl3
     call getarg(9, argument)
     read(argument,'(e14.6)') dbl4
   else
     print *,"dfl_utils.f90::main -> Error, command=zernike: wrong number of command line arguments"
   end if
   call zernike(int1, int2, dbl1, dbl2, chr1, dbl3, dbl4)
 else if (command == 'opc_add') then
   if(iarg == 5) then
     call getarg(3, filename1)
     call getarg(4, filename2)
     call getarg(4, filename3)
     call opc_add(filename1, filename2, filename3)
   else
     print *,"dfl_utils.f90::main -> Error, command=add: wrong number of command line arguments"
   end if
 else if (command == 'opc_substract') then
   if(iarg == 5) then
     call getarg(3, filename1)
     call getarg(4, filename2)
     call getarg(5, filename3)
     call opc_substract(filename1, filename2, filename3)
   else
     print *,"dfl_utils.f90::main -> Error, command=substract: wrong number of command line arguments"
   end if
 else
   print *,'opc_utils.f90::main -> Error, unknown command: ', command
 end if ! dispatch command

end ! program opc_utils



subroutine zernike(nz, mz, Rz, Az, filename, xoff, yoff)
! ==================================================================
! Subroutine that calculates the aberation given by a certain order
! n, m of the Zernike polynomal. The radial coordinate is scaled by
! R and A is the amplitude of the aberation. The aberation is
! written as an optical path length difference to file
! ------------------------------------------------------------------

 use opc_globals
 use opc_lib
 implicit none ! be strict

 integer, intent(IN)             :: nz, mz
 real (kind=rk16), intent(IN)    :: Rz, Az
 character(len=100), intent(IN)  :: filename
 real (kind=rk16), intent(IN)    :: xoff, yoff
 integer                         :: ix, iy, ngrid, ierr
 character(len=100)              :: fname
 real (kind=rk16)                :: rho, phi, dphase
 real (kind=rk16), external      :: zer

! check if we need to add abberations to an existing file or
! create a new file
  ngrid = 1
  fname = filename
  allocate(ogrid(ngrid), stat=ierr)
  if (ierr /= 0) stop "opc_utils.f90::zernike -> Error allocating memory for array ogrid"
  allocate(ogrid(ngrid) % r(Nx,Ny), stat=ierr)
  if (ierr /= 0) stop "opc_utils.f90::zernike -> Error allocating memory for array ogrid % r"
  ogrid(ngrid) % r = zero
  if (fname(1:1) == '+') then ! add to existing mask
    do while (fname(1:1) == '+')
      fname(1:len(fname)-1) = fname( 2 : len_trim(fname))
    end do
    call read_opld(fname, ngrid, ogrid)
  end if

  do iy = 1, Ny
    do ix = 1, Nx
      rho = sqrt( (xaxis(ix)-xoff)**2 + (yaxis(iy)-yoff)**2 ) / Rz
      phi = atan2( (yaxis(iy)-yoff), (xaxis(ix)-xoff) )

      dphase = Az*zer(nz, mz, rho, phi) ! phase change due to aberation
      ogrid(ngrid) % r(ix,iy) = ogrid(ngrid) % r(ix,iy) + dphase/k_wave ! convert to optical pathlength difference
    end do ! ix
  end do ! iy

  call write_opld(fname, ngrid, ogrid)

  deallocate(ogrid(ngrid) % r, stat=ierr)
  if (ierr /= 0) stop "opc_utils.f90::zernike -> Error deallocating memory associated with ogrid % r"
  deallocate(ogrid, stat=ierr)
  if (ierr /= 0) stop "opc_utils.f90::zernike -> Error deallocating memory associated with ogrid"

end ! subroutine zernike


real (kind=kind(1.0D0)) recursive function factorial(nn) result (res)
! ==================================================================
! Recursive function that calculates the factorial of n
! ------------------------------------------------------------------
 use opc_globals
 implicit none ! be strict
 integer, intent(IN) :: nn
 if (nn < 0) then
   print *,"OPC - Utils -> ERROR: Factorial n<0"
   stop
 end if
 if (nn == 0) then
   res = 1
 else
   res=nn*factorial(nn-1)
 end if
end ! function factorial


real (kind=kind(1.0D0)) function zer(nn,mm,rho,phi)
! ==================================================================
! Function that calculates the Zernike polynominals
!     +-m
!    R    (rho) x ( cos(m phi), m>0 or sin(m phi), m<0 )
!     n
! See Born and Wolf, p465, 6th edition.
! ------------------------------------------------------------------
 use opc_globals
 implicit none ! be strict
 integer, intent(IN)           :: nn, mm
 real (kind=rk16), intent(IN)  :: rho,phi
 integer                       :: ind, int_sign, mmm, nms1, nms2, ncheck, s
 real (kind=rk16)              :: ss, pp
 real (kind=rk16), external    :: factorial

! check input
 if (nn < 0) then
   print *, "OPC - Utils -> Zernike: n must be > 0; |m| must be less or equal than n."
   print *,"         If n is odd then m must be odd."
   print *,"         If n is even then m must be even."
   stop
 end if

 ind = 0
 do ncheck = nn, -nn, -2
   if (ncheck == mm) ind = 1
 end do
 if (ind == 0) then
   print *, "OPC - Utils -> Zernike: n must be > 0; |m| must be less or equal than n."
   print *,"         If n is odd then m must be odd."
   print *,"         If n is even then m must be even."
   stop
 end if

 mmm = int(abs(mm))
 ss = 0
 int_sign = 1

 do s=0, int((nn-mmm)/2)
   if ((nn-2*s) /= 0) then
     pp = rho**(nn-2*s)
   else
     pp = 1.0d0
   end if
   pp = pp * factorial(nn-s)*int_sign
   nms1 = ((nn+mmm)/2) - s
   nms2 = ((nn-mmm)/2) - s
   pp = pp / ( factorial(s)*factorial(nms1)*factorial(nms2) )
   ss = ss + pp
   int_sign = -int_sign
 end do
 if (mm >= 0) then
   zer = ss*cos(mm*phi)      ! arbitrary choice of reference angle
 else
   zer = ss*sin(mm*phi)      ! rotated by 90 degrees
 end if
end function ! function zernike


subroutine opc_add(filename1, filename2, filename3)
! ==================================================================
! Function that adds two optical fields. Data stored in
! files filename1 and filename2, result is returned in
! filename3. If filename3 equals filename1, the field in
! filenam1 is overwriten (filename3=filanme1+filename2
! ------------------------------------------------------------------
 use opc_globals
 use opc_lib
#ifdef MPI
 use mpi
#endif
 implicit none ! be strict
 character(len=100) :: filename1, filename2, filename3

end subroutine opc_add

subroutine opc_substract(filename1, filename2, filename3)
! ==================================================================
! Function that subtracts two optical fields. Data stored in
! files filename1 and filename2, result is returned in
! filename3. If filename3 equals filename1, the field in
! filenam1 is overwriten (filename3=filanme1+filename2
! ------------------------------------------------------------------
 use opc_globals
 use opc_lib
#ifdef MPI
 use mpi
#endif

 implicit none ! be strict
 character(len=100) :: filename1, filename2, filename3

end subroutine opc_substract
