! ##################################################################
! Copyright (C) 2007 J.G. Karssenberg, P.J.M. van der Slot,
! 
! This program is free software; you can redistribute it and/or
! modify it under the terms of the GNU General Public License as
! published by the Free Software Foundation; version 2 of the
! License.
! 
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! ##################################################################
      
program convert_opld
! ==================================================================
! Program to convert a binary OPLD file to ASCII format
! ------------------------------------------------------------------
 use kinds, only:rk16
 implicit none ! be strict
 
 integer                                       :: ix, iy, recsize, ios
 integer                                       :: Nx, Ny
 character(len=100)                            :: filein, fileout
      
 real (kind=rk16), allocatable, dimension(:,:) :: grid
      
 print *, 'Converting binary OPLD file to ASCII'
      
 write(*,'(A)', advance="no", err=20, iostat=ios) 'Enter filename to convert : '
 read(*,*) filein
      
 write(*,'(A)', advance="no", err=20, iostat=ios) 'Enter filename for output : '
 read(*,*) fileout


 write(*,'(A)', advance="no", err=20, iostat=ios) 'Enter number of grid points in x-direction : '
 read(*,*) Nx
 
 write(*,'(A)', advance="no", err=20, iostat=ios) 'Enter number of grid points in y-direction : '
 read(*,*) Ny
 
      
 allocate(grid(Nx,Ny))

 recsize=Nx*Ny*8 ! record size in binary files

 open( unit=43, file=filein, recl=recsize, access='direct', status='unknown', err=10, iostat=ios )
 read( unit=43, rec=1, err=20, iostat=ios ) ((grid(ix,iy), ix=1, Nx), iy=1, Ny)
 close( unit=43 )
      
 open(unit=43,file=fileout,status='unknown',err=30,iostat=ios)

 do iy=1,Ny
   do ix=1,Nx
     write(43,*, err=40, iostat=ios) ix, iy, grid(ix,iy)
   end do ! ix
 end do ! iy

      
 stop "Normal end of program convert_opld"
10 print *,'error opening ', trim(filein),' IO status is ',ios
 stop
20 print *,'error writing ', trim(filein),' IO status is ',ios
 stop
30 print *,'error opening ', trim(fileout),' IO status is ',ios
 stop
40 print *,'error writing ', trim(fileout),' IO status is ',ios
end ! program convert_opld
