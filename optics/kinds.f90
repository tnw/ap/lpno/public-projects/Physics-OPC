module kinds 
! this module contains kind definitions for variables
! it also determines the integer size required to 
! contain a pointer (necessary for interfacing with FFTW)

! for ifort we use INT_PTR_KIND() function to be compatible
! with older ifort compilers. For gfortran we use
! ISO_C_BINDING module and require version 4.3 or higher
#ifdef GFORTRAN
#ifndef PTR32
#ifndef PTR64
   use ISO_C_BINDING, only:C_INTPTR_T
#endif
#endif
#endif
   
   implicit none

   INTEGER*4          :: it4
   INTEGER*8          :: it8
#ifdef GFORTRAN
#ifdef PTR32
   INTEGER, PARAMETER :: ipk = 4
#elif PTR64
   INTEGER, PARAMETER :: ipk = 8
#else
   INTEGER, PARAMETER :: ipk  = C_INTPTR_T
#endif
#endif
#ifdef IFORT
   INTEGER, PARAMETER :: ipk  = INT_PTR_KIND()
#endif
   INTEGER, PARAMETER :: ik4  = kind(it4)
   INTEGER, PARAMETER :: ik8  = kind(it8)
   INTEGER, PARAMETER :: rk8  = kind(1.0)
   INTEGER, PARAMETER :: rk16 = kind(1.0D0)
   INTEGER, PARAMETER :: ck16 = kind(cmplx(1.0,1.0))
   INTEGER, PARAMETER :: ck32 = kind(dcmplx(1.0D0,1.0D0))
end module kinds

