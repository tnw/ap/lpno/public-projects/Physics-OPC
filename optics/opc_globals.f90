MODULE opc_globals
! ##################################################################
! Copyright (C) 2006-2014 J.G. Karssenberg, P.J.M. van der Slot,
! I.V.Volokhine
!
! This program is free software; you can redistribute it and/or
! modify it under the terms of the GNU General Public License as
! published by the Free Software Foundation; version 2 of the
! License.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! ##################################################################
!
!
! ==================================================================
! This file defines common type declarations we need in a lot
! of subroutines in the program
! ------------------------------------------------------------------
#ifdef MPI
 use mpi,   only: MPI_ADDRESS_KIND
#endif
 use kinds, only : rk16, ipk
 use FFTW3
 
 implicit none ! be strict

 type opld_grid
   real (kind=rk16), dimension(:,:), pointer :: r
 end type
   
 character(len=50), parameter :: opc_version='0.8.1'

 real (kind=rk16), parameter :: zero=0d0,                                &
                                half=0.5d0,                              &
                                one=1.0d0,                               &
                                two=2.0d0,                               &
                                three=3.0d0,                             &
                                four=4.0d0,                              &
                                eight=8.0d0,                             &
                                nine=9.0d0,                              &
                                sixteen=16.0d0,                          &
                                Pi=3.14159265358979323846264338327950288419716939937510D+00, &
                                TwoPi=two*Pi,                            &
                                cl = 299792458d0,                        & ! speed of light in vacuum
                                mu0 = 4.0D-7*Pi,                         &
                                eps0 = one/(mu0*cl*cl),                  &
                                h_planck = 6.62607004d-34,               &
                                ec = 1.60217662d-19,                     &
                                keV = ec*1000d0,                         &
                                VacImp = mu0*cl                            ! vacuum impedance
                                
 integer, parameter :: nogrid_max=100   ! hardcoded max number of masks
 integer, parameter :: cmd_length=256    ! hardcoded max length of lines in script file
 integer, parameter :: FHID=41,     &	  ! file handle reading dfl
                       FHOD=42,     &   ! file handle writing dfl
                       FHIO=43,     &   ! file handle reading opld
                       FHOO=44,     &   ! file handle writing opld
                       FHDO=50,     &   ! file handle writing diagnostic output
                       FHPO=51,     &   ! file handle writing output to perl script
                       FHIP=90,     &   ! file handle reading param
                       FHOP=91,     &   ! file handle writing param  
                       FHIT=94,     &   ! file handle reading txt
                       FHOT=95,     &   ! file handle writing txt
                       FHIC=96,     &   ! file handle reading config
                       FHOC=97,     &   ! file hangle writing config  
                       FHIS=99,     &   ! file handle reading script
                       FHOW=80,     &   ! file handle writing mode coefficients waveguide
                       FHOE=81,     &   ! file handle writing mode energy waveguide
                       FHW=101          ! file handle for reading/writing opc_wisdom files
 
!common variables
 integer                                          :: Nx, Ny, nout_x, nout_y, nslices, nogrid
 real (kind=rk16), dimension(:), pointer          :: xaxis, yaxis
 real (kind=rk16)                                 :: mesh_x, mesh_y, lambda, dfreq, k_wave
 real (kind=rk16)                                 :: Mxtot, Mytot, Mxbuf,Mybuf, Pav, dt
 complex (kind=rk16), dimension(:,:), pointer     :: grid, buffer, gridy, buffery
 type (C_PTR)                                     :: p_grid, p_buffer, p_gridy, p_buffery
 complex(C_DOUBLE_COMPLEX), dimension(:), pointer :: fresnel_ux, fresnel_uy, &
                                                     fresnel_vx, fresnel_vy, fresnel_w
 type(C_PTR)                                      :: p_fresnel_ux, p_fresnel_uy, &
                                                     p_fresnel_vx, p_fresnel_vy, p_fresnel_w
 type (opld_grid), dimension(:), pointer          :: ogrid
 real (kind=rk16)                                 :: tot, xc, yc, sr, sx, sy, lambda_avg, bw
 real (kind=rk16)                                 :: zsep, Mx, My
 logical                                          :: init_opld, init_waveguide, include_Ey, spectral_domain
 character(len=10)                                :: export_wisdom
! variables and arrays to store intermediate diagnostic data
 integer                                          :: nxcross, nycross, nstat, ntotal, nfluence, nspectrum
 real(kind=rk16), dimension(:,:,:), pointer       :: res_xcross, res_ycross, res_stat_temp, res_fluence
 real(kind=rk16), dimension(:,:), pointer         :: res_stat, res_total, res_spectrum
! store local mesh size at positions where diagnostics are requested.
 real(kind=rk16), dimension(:), pointer           :: res_xmesh, res_ymesh
! names for the variables in the perl script      
 character(len=32), dimension(:), pointer         :: xcross_name, ycross_name, stat_name, total_name, spectrum_name
 character(len=100), dimension(:), pointer        :: xcross_fname, ycross_fname, stat_fname, total_fname, spectrum_fname
 character(len=100), dimension(:), pointer        :: fluence_name, fluence_fname
 character(len=cmd_length), dimension(:), pointer :: prop_cmd 

 type(C_PTR) :: plan_forward_spectral_x
 type(C_PTR) :: plan_backward_spectral_x
 type(C_PTR) :: plan_forward_spectral_y
 type(C_PTR) :: plan_backward_spectral_y
 type(C_PTR) :: plan_forward_fresnel_ux
 type(C_PTR) :: plan_backward_fresnel_ux
 type(C_PTR) :: plan_forward_fresnel_vx
 type(C_PTR) :: plan_forward_fresnel_uy
 type(C_PTR) :: plan_backward_fresnel_uy
 type(C_PTR) :: plan_forward_fresnel_vy
 type(C_PTR) :: plan_spectrum
 
 character(len=100)                           :: infile, outfile, scriptfile, opldfile
 character(len=100)                           :: field_next
 character(len=100), dimension(:), pointer    :: ogrid_name
#ifdef MPI
 integer                                      :: IO_filetype
 INTEGER(KIND=MPI_ADDRESS_KIND)               :: IO_recsize
#endif


! grid     = field matrix (Nx, Ny)  x-polarisation
! gridy    = field matrix (Nx, Ny)  y-ppolarisation
! ogrid    = optical path length difference matrix (Nx, Ny)
! nout     = grid size genesis (# points)
! mesh_x   = mesh size in x_direction (rect. grid possible)
! mesh_y   = mesh size in y_direction
! nslices  = number of slices (for time dependent sim)
! Nx       = grid points optics in x-direction (# points >= ncar+1, rect. grid)
! Ny       = grid points optics in y-direction
! axis     = map of index number to real axis value
! lambda   = base wave length - remains constant
! dlambda  = delta wave length - zero for normal field
!            else the lambda is incremented for each slice
! dt       = separation in time between two succesive slices,
!            zero for steady state
! k_wave   = wave vector - is updated per slice if dlambda is used
! Pav      = average power associated with optical pulse 
!          => Energy per pulse * rep rate (nslice > 1)
!          => Power in slice * duty cycle (nslices=1)
! tot      = field values summed over complete pulse
! xc,yc    = center of mass position of field
! sx,sy,sr = size (standard deviation) in x, y and radial direction
! lambda_avg = intensity weighted average wavelength in spectrum
! bw       = rms bandwidth of the spectrum (intensity weigthed)
! buffer   = space allocated to copy backup of grid
! Mxtot    = total magnification of the mesh size in x-direction
! Mytot    = total magnification of the mesh size in y-direction
! Mxbuf    = copy of Mxtot for 'buffer'
! Mybuf    = copy of Mytot for 'buffer'
! init_opld= initialise subroutine only once in case of MPI
! init_waveguide = initialisation subroutine once per propagation path.
! include_Ey = track second polariation along optical path
! spectrum_domain = tracks if we work in the spectral or temporal domain
! zsep     = seperation in wavelength between slices
! M        = local magnification
! infile   = input file name
! field_next = name of next field file to process (e.g. second polarisation)
! outfile    = output file name
! scriptfile = name of script file containing commands to be executed
! opldfile = opld file name
! plan_forward_*  = holds executation plan for forward FFT using FFTW
! plan_backward_* = holds executation plan for backward FFT using FFTW
! export_wisdom = variable that determines if we write wisdom files for all subproccess (all), 
!                 a single wisdom file (single) or no wisdom files (none)
!
! vim: filetype=fortran

END MODULE opc_globals
