
! ##################################################################
! Copyright (C) 2006-2014 J.G. Karssenberg, P.J.M. van der Slot,
! I.V.Volokhine
!
! This program is free software; you can redistribute it and/or
! modify it under the terms of the GNU General Public License as
! published by the Free Software Foundation; version 2 of the
! License.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! ##################################################################

program dfl_utils
! ==================================================================
! Program that performs various actions on .dfl files
! ------------------------------------------------------------------

 use opc_globals
 use opc_lib
 use opc_interface
#ifdef MPI
 use mpi
#endif
 implicit none ! be strict

 integer            :: iarg, is, irec, idrec, imax, nadd, ierr, nout, slen
 real (kind=rk16)   :: zshift, xp, yp
 character(len=100) :: conffile, ifile, command, argument, infile2, outfile1, outfile2
 character(len=132) :: fname, temp
 character(len=25)  :: fmt1D, direction
 character          :: mode
 logical            :: print_1d, print_2d, fexist
 real (kind=rk16), dimension(:),   allocatable :: res_1d
 real (kind=rk16), dimension(:,:), allocatable :: res_2d
#ifdef MPI
 integer            :: mpi_err
 ! additional arrays for communicating between nodes
 real (kind=rk16), dimension(:),   allocatable :: res_1da
 real (kind=rk16), dimension(:,:), allocatable :: res_2da
#endif
 integer            :: mpi_rank, mpi_size
 ! mpi_rank and mpi_size are always defined and set to 0 and 1 respectively
 ! so make coding for MPI and NON-MPI executing easier

#ifdef MPI
 !initialize MPI
 call MPI_INIT(mpi_err)
#endif

 ! check number of command line arguments
 iarg = iargc()
 if (iarg > 3) then
   call getarg(1, conffile)
   call getarg(2, ifile)
   call getarg(3, argument)
   read(argument,'(i6)') irec
   call getarg(4, command)
 else
  call opc_abort("dfl_utils.f90::main", "Error, wrong number of command line arguments, should be at least 4")
 end if

! set defaults for "infile", "outfile" and "scriptfile"
 infile = ' '
 outfile = ' '
 scriptfile = ' '

 call opc_init(conffile)
! set correct input file
 infile = ifile

! determine if we work in time or frequency domain
 spectral_domain = .FALSE. ! work in the time domain
 slen = len(trim(infile))
 if (infile(slen-2:slen)=='sfl') spectral_domain = .TRUE. ! work in the spectral domain
      ! when an FFT is preformed on a .dfl file, the spectral components are ordered
      ! as follows, positive frequency in first half of array and the negative frequencies
      ! in the second half of the array (0, f1, f2, ..., fn, -fn, -fn-1, ,,, -f1)
      ! We want to return the data in the correct order when we convert to ascii

! If irec > 0 we only output one slice else we loop over all slices as defined by range for ns
 if (irec.gt.0) then
  imax=irec
 else
  irec=1
  imax=nslices
 end if

! dispatch command
 if (command == 'stat') then  !commands on whole file
    call stats()
 else if (command.eq.'bandwidth') then
 	  call bandwidth()
 else if (command.eq.'shift') then
    if (iarg == 6) then
       call getarg(5, argument)
       read (argument, '(e13.6)') zshift
       call getarg(6, argument)
       read (argument, '(i4)') nadd
   else
     call opc_abort("dfl_utils.f90::main", "Error, wrong number of command line arguments for command stat")
   end if
   call shift(zshift, nadd, conffile, infile)
 else if (command == 'fourier') then
   call getarg(5, argument)
   if (argument == 'true') then
     call fourier(infile, .TRUE.) ! extension of file determines direction of FFT (dfl -> sfl and sfl -> dfl)
   else
     call fourier(infile, .FALSE.)
   end if
 else if (command == 'stokes') then
 	 if (iarg == 7) then
 	 	  call getarg(5, argument)    ! number of slices to include in average
 	 	  read (argument,'(i6)') idrec
 	 	  call getarg(6, infile2)  ! file containing y-polarization
 	 	  call getarg(7, outfile1) ! file containing Stokes vector
 	 else
 	    call opc_abort("dfl_utils.f90::main", "Error, wrong number of command line arguments for command stokes")
 	 end if
 	 call stokes(infile, infile2, irec, idrec, outfile1)
 else if (command == 'fluence') then
   if (iarg == 5) then
     call getarg(5, outfile1)
   else
     call opc_abort("dfl_utils.f90::main", "Error, wrong number of command line arguments for command fluence")
   end if
   call get_fluence(infile, outfile1)
 else
   ! allocate memory for results
   if (command == 'total' .OR. command == 'spectrum' .OR. command == 'maximum' .OR. command == 'phase_P') then
     xp=-9.99d99  ! set default values for optional parameters
   	 yp=-9.99d99
   	 if (command == 'total' .OR. command == 'maximum') then
       if (iarg ==5) then
         call getarg(5, outfile1) ! filename specified, write to file
       else
         outfile1 = "*" ! no name for output file, use standard output
       end if
     else if (command == 'phase_P') then ! command = phase_P, read in point where phase must be evaluated
     	 call getarg(5, argument)
     	 read (argument, '(e13.6)') xp
     	 call getarg(6, argument)
     	 read (argument, '(e13.6)') yp
     	 if (iarg == 7) then ! get filename for output
         call getarg(7, outfile1)
       else
         outfile1 = "*" ! no filename specified, output to standard output
       end if
     else if (command == 'spectrum') then
       if (iarg == 4) then
         outfile1 = "*" ! no name for output file, use standard output
       else if (iarg == 5) then
       	 outfile1 = "*"
         call getarg(5, argument)
         if (trim(argument) /= 'undef') outfile1=trim(argument) ! filename specified, write to file
       else if (iarg == 7) then
       	 outfile1 = "*"
       	 call getarg(5, argument)
       	 if (trim(argument) /= 'undef') outfile1=trim(argument) ! filename specified, write to file
         call getarg(6, argument)
         if (trim(argument) /= '-') read (argument, '(e13.6)') xp
         call getarg(7, argument)
         if (trim(argument) /= '-') read (argument, '(e13.6)') yp
       else if (iarg > 7)  then
         call opc_abort("dfl_utils.f90::main", "Error, wrong number of command line arguments for command spectrum")
       end if
     end if
     allocate(res_1d(imax-irec+1), stat=ierr)
     if (ierr /=0) call opc_abort("dfl_utils.f90::main", "Not enough memory to allocate res_1d")
     res_1d = zero
#ifdef MPI
     ! memory for additional array required for communcation between nodes, MPI_REDUCE
     ! requires different buffers for SENT and RECEIVE variables
     allocate(res_1da(imax-irec+1), stat=ierr)
     if (ierr /=0) call opc_abort("dfl_utils_mpi.f90::main", "Not enough memory to allocate res_1da")
     res_1da = zero
#endif
   else if (command == 'xcross' .OR. &
   	        command == 'ycross' .OR. &
   	        command == 'phase_1D_x' .OR. &
   	        command == 'phase_1D_y') then
     if (iarg == 5) then ! get filename for output
        call getarg(5, outfile1)
     else
        outfile1 = "*" ! no filename specified, output to standard output
     end if
     if (command == 'xcross' .OR. command == 'phase_1D_x') then
     	  nout = Nx
     else
     	  nout = Ny
     end if
     allocate(res_2d(nout,imax-irec+1), stat=ierr)  ! profile along x-direction
     if (ierr /=0) call opc_abort("dfl_utils.f90::main","Not enough memory to allocate res_2d")
     res_2d = zero
#ifdef MPI
     ! memory for additional array required for communcation between nodes, MPI_REDUCE
     ! requires different buffers for SENT and RECEIVE variables
     allocate(res_2da(nout,imax-irec+1), stat=ierr)
     if (ierr /=0) call opc_abort("dfl_utils_mpi.f90::main","Not enough memory to allocate res_2da")
     res_2da = zero
#endif
   else if (command == 'tot_xcross' .OR. command == 'tot_ycross') then
     if (iarg == 6) then ! get filenames for output
       call getarg(5, outfile1)  ! first filename is for 1d data (total)
       call getarg(6, outfile2)  ! second filename is for 2d data (x- or ycross)
     else
       call opc_abort("dfl_utils.f90::main","Error, wrong number of command line arguments")
     end if
     allocate(res_1d(imax-irec+1), stat=ierr)
     if (ierr /=0) call opc_abort("dfl_utils.f90::main","Not enough memory to allocate res_1d")
     res_1d = zero
     if (command == 'tot_xcross') then  ! profile in x-direction
     	  nout = Nx
     else  ! profile in y-direction
        nout = Ny
     end if
     allocate(res_2d(nout,imax-irec+1), stat=ierr)
     if (ierr /=0) call opc_abort("dfl_utils.f90::main","Not enough memory to allocate res_2d")
     res_2d = zero
#ifdef MPI
     ! memory for additional array required for communcation between nodes, MPI_REDUCE
     ! requires different buffers for SENT and RECEIVE variables
     allocate(res_1da(imax-irec+1), stat=ierr)
     if (ierr /=0) call opc_abort("dfl_utils_mpi.f90::main","Not enough memory to allocate res_1da")
     res_1da = zero
     allocate(res_2da(nout,imax-irec+1), stat=ierr)
     if (ierr /=0) call opc_abort("dfl_utils_mpi.f90::main","Not enough memory to allocate res_2da")
     res_2da = zero
#endif
   else
     call opc_abort('dfl_utils.f90::main','ERROR: unknown command: ' // trim(command))
   end if ! initialisation per command

#ifdef MPI
   call MPI_COMM_RANK(MPI_COMM_WORLD, mpi_rank, mpi_err)
   call MPI_COMM_SIZE(MPI_COMM_WORLD, mpi_size, mpi_err)
#else
   mpi_rank = 0 ! default value for single node execution
   mpi_size = 1 ! default value for single node execution
#endif

   print_1d = .FALSE.
   print_2d = .FALSE.

   do is=irec+mpi_rank,imax,mpi_size ! loop for both single and multi node execution
     call read_dfl(infile, is, grid) ! read slice
     ! dispatch command
     if      (command == 'total') then
             call total(is, res_1d)
             print_1d = .TRUE.
     else if (command == 'spectrum') then
             if (xp < -1d99) then  ! no x-location specified
               if (yp < -1d99) then  ! no y-location specified
                 call spectrum(is, res_1d) ! use full grid
               else
                 call spectrum(is, res_1d, y=yp) ! use specific y-location
               end if
             else
               if (yp < -1d99) then
                 call spectrum(is, res_1d, x=xp) ! use specific x-location
               else
                 call spectrum(is, res_1d, x=xp, y=yp) ! use specific grid point
               end if
             end if
             print_1d = .TRUE.
     else if (command == 'maximum') then
             call maximum(is, res_1d)
             print_1d = .TRUE.
     else if (command == 'xcross') then
             call xcross(is, res_2d)
             print_2d = .TRUE.
     else if (command == 'ycross') then
             call ycross(is, res_2d)
             print_2d = .TRUE.
     else if (command == 'tot_xcross') then
             call total(is, res_1d)
             call xcross(is, res_2d)
             print_1d = .TRUE.
             print_2d = .TRUE.
     else if (command == 'tot_ycross') then
             call total(is, res_1d)
             call ycross(is, res_2d)
             print_1d = .TRUE.
             print_2d = .TRUE.
     else if (command == 'phase_P') then
     	       call phase_P(is, res_1d, xp, yp)
     	       print_1d = .TRUE.
     else if (command == 'phase_1D_x') then
     	       call phase_1D_x(is, res_2d)
     	       print_2d = .TRUE.
     else if (command == 'phase_1D_y') then
     	       call phase_1D_y(is, res_2d)
     	       print_2d = .TRUE.
     else
       print *,'dfl_utils.f90::main -> ERROR: unknown command: ', trim(command)
       stop
     end if ! commands per slice
   end do
#ifdef MPI
   ! wait till all processes have finished (synchronisation)
   call MPI_BARRIER(MPI_COMM_WORLD,mpi_err)
#endif
   if (print_1d) then
#ifdef MPI
     ! add all the arrays of each process to fill the gaps in array of process with rank == 0
     call MPI_REDUCE(res_1d,res_1da,imax-irec+1,MPI_DOUBLE_PRECISION,MPI_SUM,0,MPI_COMM_WORLD,mpi_err)
     if (mpi_rank == 0) res_1d = res_1da ! copy back so that NON-MPI and MPI code can be the same onward
#endif
     if (mpi_rank==0) then
       write (unit=fmt1D, fmt="('(',I10,'(E16.9, /))')") imax-irec+1 ! compile output format
       if (outfile1(1:1) == '+') then
         outfile1(1:len(outfile1)-1) = outfile1(2 : len_trim(outfile1))
         INQUIRE (file=outfile1, EXIST=fexist) ! check if file exist, open new if not
         if (fexist) then
           open (unit=FHOT,file=outfile1, status="OLD", position="APPEND")
         else
           open (unit=FHOT,file=outfile1, status="REPLACE")
         end if
       else if (outfile1(1:1) /= '*') then
         open (unit=FHOT,file=outfile1, status="REPLACE")
       end if
       if (outfile1(1:1) == "*") then
       	 if (spectral_domain) then
       	 	 write (unit=*,fmt=fmt1D) res_1d((nslices+1)/2+1:nslices), res_1d(1:(nslices+1)/2)
         else
           write (unit=*,fmt=fmt1D) res_1d
         end if
       else
         if (spectral_domain) then
       	 	 write (unit=FHOT,fmt=fmt1D) res_1d((nslices+1)/2+1:nslices), res_1d(1:(nslices+1)/2)
         else
           write (unit=FHOT,fmt=fmt1D) res_1d
         end if
         close (unit=FHOT)
       end if
     end if
   end if
   if (print_2d) then
#ifdef MPI
     ! add all the arrays of each process to fill the gaps in array of process with rank == 0
     ! nout has value of Nx or Ny depending on profile along x- or y-direction
     call MPI_REDUCE(res_2d,res_2da,(imax-irec+1)*nout,MPI_DOUBLE_PRECISION,MPI_SUM,0,MPI_COMM_WORLD,mpi_err)
     if (mpi_rank == 0) res_2d = res_2da ! copy back so that NON-MPI and MPI code can be the same onward
#endif
     if (mpi_rank==0) then
       write (unit=fmt1D, fmt="('(',I10,'(E16.9, /))')") nout ! compile output format
       if (print_1d) then
         fname = outfile2 ! both 1d and 2d output requested, thus two files to write
       else
         fname = outfile1 ! only 2d output requested, thus only one file to write
       end if
       if (fname(1:1) == '+') then ! check if we need to overwrite or append data
         fname(1:len(fname)-1) = fname(2 : len_trim(fname))
         INQUIRE (file=fname, EXIST=fexist) ! check if file exist, open new if not
         if (fexist) then
           open (unit=FHOT,file=fname, status="OLD", position="APPEND")
         else
           open (unit=FHOT,file=fname, status="REPLACE")
         end if
       else if (fname(1:1) /= '*') then
         open (unit=FHOT,file=fname, status="REPLACE")
       end if
       if (outfile1(1:1) == "*") then
       	 if (spectral_domain) then
       	 	 write (unit=*,fmt=fmt1D) res_2d(:,(nslices+1)/2+1:nslices), res_2d(:,1:(nslices+1)/2)
         else
           write (unit=*,fmt=fmt1D) res_2d(:,1:nslices)
         end if
       else
         if (spectral_domain) then
       	 	 write (unit=FHOT,fmt=fmt1D) res_2d(:,(nslices+1)/2+1:nslices), res_2d(:,1:(nslices+1)/2)
         else
           write (unit=FHOT,fmt=fmt1D) res_2d(:,1:nslices)
         end if
         close (unit=FHOT)
       end if
     end if
   end if
   ! release memory
   if (allocated(res_1d)) then
     deallocate(res_1d,STAT=ierr)
     if (ierr /=0) call opc_abort("dfl_utils.f90::main","Problem with deallocating res_1d")
   end if
   if (allocated(res_2d)) then
     deallocate(res_2d,STAT=ierr)
     if (ierr /=0) call opc_abort("dfl_utils.f90::main","Problem with deallocating res_2d")
   end if
 end if !commands on whole file
#ifdef MPI
! finalize MPI
 call MPI_FINALIZE(mpi_err)
#endif
end ! program diagnostics

subroutine stokes(x_pol, y_pol, irec, idrec, txtfile)
! ==================================================================
! Routine that calculates the Stokes vector from the Ex and Ey field
! components.
! Ex = Ex0 cos(kz-wt + phi_x)
! Ey = Ey0 sin(kz-wt + phi_y)
! Let phi = phi_y - phi_x.
! S0 = <Ex>^2 + <Ey>^2
! S1 = <Ex>^2 - <Ey>^2
! S2 = <2ExEy cos(phi)> = 2Re(ExEy*)
! S3 = <2ExEy sin(phi)> = -2Im(ExEy*)
!
! x_pol : name of file containing x polarization
! y_pol : name of file containing y polarization
! irec  : slice for which to determine Stokes vector, if 0 average
!         over all slices
! idrec : number of slices to average over if irec <> 0
! outfile : file containing the Stokes vector [S0, S1, S2, S3]
! ------------------------------------------------------------------
 use opc_globals
 use opc_lib
 use opc_interface
#ifdef MPI
 use mpi
#endif
 implicit none ! be strict

 character(len=100), INTENT(IN)       :: x_pol, y_pol, txtfile
 integer, INTENT(IN)                  :: irec, idrec
 integer                              :: ix, iy, ifirst, ilast, is, strlen, ipos, ierr, OFH
 real (kind=rk16)                     :: r1, r2, S0, S1, S2, S3, perarea
 complex (kind=rk16)                  :: z1
 character(len=3), dimension(4)       :: stokes_comp
 character(len=32)                    :: fmt1D
 character(len=103)                   :: temp_filename
 real (kind=rk16), dimension(:,:,:), allocatable :: dat_stokes, temp_2d
#ifdef MPI
 integer                              :: mpi_err, istatus(MPI_STATUS_SIZE)
 integer (kind=MPI_OFFSET_KIND)       :: offset
 integer                              :: IO_num_as_string
#endif
 integer                              :: mpi_rank, mpi_size, nslices_y
 ! mpi_rank and mpi_size are always defined and set to 0 and 1 respectively for NON-MPI execution of the
 ! code to allow easier coding of MPI and NON-MPI execution.

!reserve memory for y-component
 p_gridy = fftw_alloc_complex(int(Nx*Ny, C_SIZE_T))
 call c_f_pointer(p_gridy, gridy, [Nx, Ny])
 gridy = dcmplx(zero,zero)
! reserve memory to contain Stokes components
 allocate(dat_stokes(Nx,Ny,4), stat=ierr)
 if (ierr /=0) call opc_abort("dfl_utils_mpi.f90::stokes","Not enough memory to allocate array dat_stokes")
 dat_stokes=zero
#ifdef MPI
 call MPI_COMM_RANK(MPI_COMM_WORLD, mpi_rank, mpi_err)
 call MPI_COMM_SIZE(MPI_COMM_WORLD, mpi_size, mpi_err)
#else
 mpi_rank=0 ! default value for NON-MPI execution on a single node
 mpi_size=1 ! default value for NON-MPI execution on a single node
#endif

! first determine the number of slices in the other polarization direction
 if (mpi_rank==0) then
   temp_filename=replace_string(y_pol,'.dfl','.param')
   nslices_y = read_parameter_int(temp_filename,'nslices',nslices_y)
 end if
#ifdef MPI
 if (mpi_size>1) call mpi_bcast(nslices_y,1,MPI_integer,0,MPI_COMM_WORLD,mpi_err)
#endif
 if (nslices_y ==0) call opc_abort("dfl_utils_mpi.f90::stokes","Error obtaining number of slices for y-polarization")
 if (ifirst /= ilast) ilast = min(nslices,nslices_y)  ! files may be of unequal size
 perarea = one/(Mxtot*mesh_x*Mytot*mesh_y)
 do is = ifirst+mpi_rank, ilast, mpi_size
   call read_dfl(x_pol, is, grid)
   call read_dfl(y_pol, is, gridy)
   do iy=1,Ny
    	do ix=1,Nx
    		r1 = perarea*abs(grid(ix,iy))**2
    		r2 = perarea*abs(gridy(ix,iy))**2
    		S0 = r1 + r2
    		S1 = r1 - r2
    		z1 = grid(ix,iy)*conjg(gridy(ix,iy))*perarea
    		S2 = two*real(z1)
    		S3 = -two*imag(z1)
    		dat_stokes(ix,iy,1) = dat_stokes(ix,iy,1) + S0
    		dat_stokes(ix,iy,2) = dat_stokes(ix,iy,2) + S1
    		dat_stokes(ix,iy,3) = dat_stokes(ix,iy,3) + S2
    		dat_stokes(ix,iy,4) = dat_stokes(ix,iy,4) + S3
     end do ! loop over x
   end do ! loop over y
 end do ! loop over slices
#ifdef MPI
 if ((ifirst /= ilast).AND.(mpi_size > 1)) then
   ! collect data on master node by adding all the results of the nodes
   ! allocate memory to receive data
   allocate(temp_2d(Nx,Ny,4), stat=ierr)
   if (ierr /=0) call opc_abort("dfl_utils_mpi.f90::stokes","Not enough memory to allocate array temp_2d")
   temp_2d=zero
   call MPI_REDUCE(dat_stokes,temp_2d,Nx*Ny*4,MPI_DOUBLE_PRECISION,MPI_SUM,0,MPI_COMM_WORLD,mpi_err)
   if (ierr /=0) call opc_abort('dfl_utils_mpi.f90::stokes', "Error mpi_reduce for stokes data")
   ! assign back to original array
   if (mpi_rank == 0) dat_stokes(:,:,:) = temp_2d(:,:,:)
   deallocate(temp_2d, stat=ierr)
   if (ierr/=0)call opc_abort('dfl_utils_mpi.f90::stokes',  "Error deallocating memory associated with temp_2d")
 end if ! mpi stuff when running more than one process
#endif
 ! write data to file, only from master node
 ! normalise Stokes components with Fluence (S0)
 if (mpi_rank == 0) then
 	 do is = 2,4
 	 	where (dat_stokes(:,:,1) /= 0)
 	   dat_stokes(:,:,is) = dat_stokes(:,:,is)/(dat_stokes(:,:,1))
 	  else where
 	   dat_stokes(:,:,is) = dat_stokes(:,:,is)/(ilast-ifirst+1)
 	  end where
 	 end do
 	 ! now set the proper value for the Fluence (S0)
 	 if (ilast >ifirst) dat_stokes(:,:,1) = dat_stokes(:,:,1)*zsep*lambda/cl
 	 stokes_comp = ['_S0','_S1','_S2','_S3']
 	 ! format string
 	 write (unit=fmt1D, fmt="('(',I10,'(E17.9, 2X))')") Nx ! compile output format
 	 ! adjust filename to include Stokes component names
 	 ipos = index(txtfile,'.',back=.TRUE.) ! rightmost position of "." in the string
 	 strlen = len(trim(txtfile))
 	 do is=1,4
 	 	 temp_filename=txtfile(1:ipos-1)//stokes_comp(is)//trim(txtfile(ipos:strlen))
     open(unit=FHOD, file=trim(temp_filename), status='unknown', iostat=ierr)
     if (ierr /= 0) call opc_abort('dfl_utils.f90::stokes','Error opening text file '//trim(temp_filename)//' for output')
     do iy=1,Ny
       write(unit=FHOD, fmt=fmt1D, iostat=ierr) (dat_stokes(ix,iy,is), ix=1,Nx)
     end do ! loop over iy
     close(FHOD)
     if (ierr /= 0) call opc_abort('dfl_utils.f90::stokes','Error writing data to file')
   end do ! loop over Stokes components
 end if
 return
 end subroutine stokes


 subroutine stats()
! ==================================================================
! Routine that calculates several statistical parameters for the
! field distribution:
! tot    = total steady state power or total energy within pulse
! xc     = center of mass in x-direction
! yc     = center of mass in y-direction
! sr     = standard deviation in r
! w      = half width of the beam (assuming a Gaussian distribution)
! sx     = standard deviation in x-direction
! sy     = standard deviation in y-direction
! lambda_avg = intensity weighted average wavelength of spectrum
! bw     = rms bandwidth of spectrum
!
! lambda and bw are only determined when we work in the frequency domain
!
! The values are printed and stored in variables in opc_globals.mod
! ------------------------------------------------------------------

 use opc_globals
 use opc_lib
 use opc_interface
#ifdef MPI
 use mpi
#endif
 implicit none ! be strict

 integer                                      :: ix, iy, ioff_x, iend_x, ioff_y, iend_y, is
 real (kind=rk16)                             :: sum2, sumx, sumy, sumr, sumx2, sumy2, sumr2, x, y
 real (kind=rk16)                             :: ttot, sum_freq, sum_freq2, my_freq, my_lambda, freq_avg, freq_dev
 complex (kind=rk16), dimension(:,:), pointer :: gridt
 integer                                      :: FH, recsize, ierr
#ifdef MPI
 integer                                      :: mpi_err
 real (kind=rk16)                             :: result ! required for communicating between nodes
#endif
 integer                                      :: mpi_rank, mpi_size
 ! mpi_rank and mpi_size are always defined and set to 0 and 1 respectively for NON-MPI execution of the
 ! code to allow easier coding of MPI and NON-MPI execution.

#ifdef MPI
 call MPI_COMM_RANK(MPI_COMM_WORLD, mpi_rank, mpi_err)
 call MPI_COMM_SIZE(MPI_COMM_WORLD, mpi_size, mpi_err)
#else
 mpi_rank=0 ! default value for NON-MPI execution on a single node
 mpi_size=1 ! default value for NON-MPI execution on a single node
#endif

 tot = zero
 xc = zero
 yc = zero
 sr = zero
 sx = zero
 sy = zero
 lambda_avg = zero
 bw = zero
 sumx = zero
 sumy = zero
 sumx2 = zero
 sumy2 = zero
 sumr2 = zero
 sum_freq = zero
 sum_freq2 = zero

 allocate(gridt(Nx,Ny), stat=ierr)
 if (ierr /=0) call opc_abort("dfl_utils_mpi.f90::stat","Not enough memory to allocate array gridt")
 call field_offset(ioff_x, iend_x, ioff_y, iend_y)
 do is = 1+mpi_rank, nslices, mpi_size
   call read_dfl(infile, is, gridt)
   ttot = zero
   do iy = ioff_y, iend_y
     do ix = ioff_x, iend_x
       sum2 = abs(gridt(ix,iy))**2
       ttot = ttot + sum2
       x = xaxis(ix)
       y = yaxis(iy)
       sumx = sumx + sum2*x
       sumy = sumy + sum2*y
       sumx2 = sumx2 + sum2*x*x
       sumy2 = sumy2 + sum2*y*y
       sumr2 = sumr2 + sum2*(x*x+y*y)
     end do
   end do
   if (spectral_domain) then  ! integrate over frequency as this is equidistant
     if (is <= (nslices+1)/2) then  ! order of frequencies for the slices is fc, fc+df, ...., fc+m*df, fc-m*df, ..., fc-df
       my_freq = cl/lambda + (is-1)*dfreq
     else
       my_freq = cl/lambda - (nslices-is+1)*dfreq
     end if
     sum_freq  = sum_freq  + my_freq*ttot
     sum_freq2 = sum_freq2 + my_freq**2*ttot
   end if
   tot = tot + ttot
 end do


#ifdef MPI
 ! synchronise
 call MPI_BARRIER(MPI_COMM_WORLD,mpi_err)
 result = zero
 call MPI_REDUCE(tot,result,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,MPI_COMM_WORLD,mpi_err)
 if (mpi_rank == 0) tot = result
 result = zero
 call MPI_REDUCE(sumx,result,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,MPI_COMM_WORLD,mpi_err)
 if (mpi_rank == 0) sumx = result
 result = 0
 call MPI_REDUCE(sumy,result,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,MPI_COMM_WORLD,mpi_err)
 if (mpi_rank == 0) sumy = result
 result = 0
 call MPI_REDUCE(sumx2,result,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,MPI_COMM_WORLD,mpi_err)
 if (mpi_rank == 0) sumx2 = result
 result = 0
 call MPI_REDUCE(sumy2,result,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,MPI_COMM_WORLD,mpi_err)
 if (mpi_rank == 0) sumy2 = result
 result = 0
 call MPI_REDUCE(sumr2,result,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,MPI_COMM_WORLD,mpi_err)
 if (mpi_rank == 0) sumr2 = result
 if (spectral_domain) then
   result = 0
   call MPI_REDUCE(sum_freq,result,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,MPI_COMM_WORLD,mpi_err)
   if (mpi_rank == 0) sum_freq = result
   result = 0
   call MPI_REDUCE(sum_freq2,result,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,MPI_COMM_WORLD,mpi_err)
   if (mpi_rank == 0) sum_freq2 = result
   result = 0
 end if
#endif
 if (mpi_rank == 0) then
   if (abs(tot)>tiny(tot)) then
     xc = sumx/tot
     yc = sumy/tot
     !calculate moments of the field distribution
     sx = sqrt(sumx2/tot - xc**2)
     sy = sqrt(sumy2/tot - yc**2)
     sr = sqrt(sumr2/tot - (xc**2+yc**2))
     if (spectral_domain) then
       freq_avg = sum_freq/tot
       freq_dev = sqrt(sum_freq2/tot - freq_avg**2)
       lambda_avg = cl/freq_avg
       bw = cl*freq_dev/freq_avg**2
     end if
     if (nslices.gt.1) then
       if (spectral_domain) then
         tot = tot*dfreq ! energy in pulse (from file in frequency domain)
       else
         tot = tot*dt ! energy in pulse (from file in time domain)
       end if
     end if
     !use w(z) definition of Siegman to report the half width of the beam w0 is the waist
     !at the position of the focus
     if (spectral_domain) then
       write(*,'(9(E16.9,2X))') tot, xc, yc, sx, sy, sr, sqrt(two)*sr, lambda_avg, bw
     else
       write(*,'(7(E16.9,2X))') tot, xc, yc, sx, sy, sr, sqrt(two)*sr
     end if
   else
     if (spectral_domain) then
       write(*,'(9(E16.9,2X))') 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
     else
       write(*,'(7(E16.9,2X))') 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
     end if
   end if
 end if

 deallocate(gridt, stat=ierr)
 if (ierr /=0) call opc_abort("dfl_utils.f90::stat","Error deallocating memory associated with array gridt")

end ! subroutine stats


subroutine bandwidth()
! ==================================================================
! Routine that calculates the rms bandwidth of the field contained
! in a circular area with increasing radius. The area is centered
! at the center of the optical pulse
!
! Routine only works when in the spectral domain
!
! The values are printed and returned to calling perl script in an
! array
! ------------------------------------------------------------------

 use opc_globals
 use opc_lib
 use opc_interface
#ifdef MPI
 use mpi
#endif
 implicit none ! be strict

 integer                                      :: ix, iy, ioff_x, iend_x, ioff_y, iend_y, is, ir, nr
 real (kind=rk16)                             :: sum2, sumx, sumy, x, y, dr, r2, rmax2, temp1, temp2
 real (kind=rk16)                             :: ttot, my_freq, my_lambda, freq_avg, freq_dev
 real (kind=rk16), dimension(:), pointer      :: sum_tot, sum_freq, sum_freq2
 complex (kind=rk16), dimension(:,:), pointer :: gridt
 integer                                      :: FH, recsize, ierr
#ifdef MPI
 integer                                      :: mpi_err
 real (kind=rk16)                             :: result ! required for communicating between nodes
 real (kind=rk16), dimension(:), pointer      :: mpi_rarray ! idem
#endif
 integer                                      :: mpi_rank, mpi_size
 ! mpi_rank and mpi_size are always defined and set to 0 and 1 respectively for NON-MPI execution of the
 ! code to allow easier coding of MPI and NON-MPI execution.

 if (.NOT.spectral_domain) return  ! nothing to do, input is not in the spectral domain.

#ifdef MPI
 call MPI_COMM_RANK(MPI_COMM_WORLD, mpi_rank, mpi_err)
 call MPI_COMM_SIZE(MPI_COMM_WORLD, mpi_size, mpi_err)
#else
 mpi_rank=0 ! default value for NON-MPI execution on a single node
 mpi_size=1 ! default value for NON-MPI execution on a single node
#endif

! first determine centre of optical pulse
 ttot = zero
 xc = zero
 yc = zero
 sumx = zero
 sumy = zero

 allocate(gridt(Nx,Ny), stat=ierr)
 if (ierr /=0) call opc_abort("dfl_utils_mpi.f90::stat","Not enough memory to allocate array gridt")
 call field_offset(ioff_x, iend_x, ioff_y, iend_y)
 do is = 1+mpi_rank, nslices, mpi_size
   call read_dfl(infile, is, gridt)
   do iy = ioff_y, iend_y
     do ix = ioff_x, iend_x
       sum2 = abs(gridt(ix,iy))**2
       ttot = ttot + sum2
       x = xaxis(ix)
       y = yaxis(iy)
       sumx = sumx + sum2*x
       sumy = sumy + sum2*y
     end do ! loop over x
   end do ! loop over y
 end do ! loop over slices

#ifdef MPI
 ! synchronise
 call MPI_BARRIER(MPI_COMM_WORLD,mpi_err)
 result = zero
 call MPI_REDUCE(ttot,result,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,MPI_COMM_WORLD,mpi_err)
 if (mpi_rank == 0) ttot = result
 result = zero
 call MPI_REDUCE(sumx,result,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,MPI_COMM_WORLD,mpi_err)
 if (mpi_rank == 0) sumx = result
 result = 0
 call MPI_REDUCE(sumy,result,1,MPI_DOUBLE_PRECISION,MPI_SUM,0,MPI_COMM_WORLD,mpi_err)
#endif
 if (mpi_rank == 0) then
   if (abs(tot)>tiny(ttot)) then
     xc = sumx/ttot
     yc = sumy/ttot
   else
     xc=zero
     yc=zero
   end if
 end if
! broadcast center of optical pulse to other processes
#ifdef MPI
 if (mpi_size > 1) then
   call MPI_BCAST(xc,1,mpi_double_precision,0,mpi_comm_world,mpi_err)
   call MPI_BCAST(yc,1,mpi_double_precision,0,mpi_comm_world,mpi_err)
 end if
#endif
! now calculate spectral bandwidth as a function of the radius of an aperture that
! clips the optical pulse
 temp1=min(abs(xc-xaxis(1)),abs(xaxis(Nx)-xc))
 temp2=min(abs(yc-yaxis(1)),abs(yaxis(Ny)-yc))
 rmax2=temp1**2 + temp2**2 ! square of maximum size of aperture that still fits inside the grid
 dr = min(xaxis(2)-xaxis(1), yaxis(2)-yaxis(1))
 nr = int(sqrt(rmax2)/dr)  ! number of points in radial direction
! allocate arrays to store values as a function of r (reduce amount of IO)
 allocate(sum_tot(nr), stat=ierr)
 if (ierr /=0) call opc_abort("dfl_utils_mpi.f90::bandwidth","Not enough memory to allocate array sum_tot")
 sum_tot = zero
 allocate(sum_freq(nr), stat=ierr)
 if (ierr /=0) call opc_abort("dfl_utils_mpi.f90::bandwidth","Not enough memory to allocate array sum_freq")
 sum_freq = zero
 allocate(sum_freq2(nr), stat=ierr)
 if (ierr /=0) call opc_abort("dfl_utils_mpi.f90::bandwidth","Not enough memory to allocate array sum_freq2")
 sum_freq2 = zero

 call field_offset(ioff_x, iend_x, ioff_y, iend_y)
 do is = 1+mpi_rank, nslices, mpi_size
   call read_dfl(infile, is, gridt)
   do ir=1,nr
   	 r2 = (ir*dr)**2
     ttot = zero
     do iy=1,Ny
     	 do ix=1,Nx
     	 	 if (((xaxis(ix)-xc)**2+(yaxis(iy)-yc)**2) < r2) ttot=ttot + abs(gridt(ix,iy)**2)
     	 end do
     end do
     if (is <= (nslices+1)/2) then  ! order of frequencies for the slices is fc, fc+df, ...., fc+m*df, fc-m*df, ..., fc-df
       my_freq = cl/lambda + (is-1)*dfreq
     else
       my_freq = cl/lambda - (nslices-is+1)*dfreq
     end if
     sum_freq(ir)  = sum_freq(ir)  + my_freq*ttot
     sum_freq2(ir) = sum_freq2(ir) + my_freq**2*ttot
     sum_tot(ir) = sum_tot(ir) + ttot
   end do
 end do

#ifdef MPI
 ! synchronise
 allocate(mpi_rarray(nr), stat=ierr)
 if (ierr /=0) call opc_abort("dfl_utils_mpi.f90::bandwidth","Not enough memory to allocate array mpi_rarray")
 mpi_rarray = zero
 call MPI_BARRIER(MPI_COMM_WORLD,mpi_err)
 result = zero
 call MPI_REDUCE(sum_tot,mpi_rarray,nr,MPI_DOUBLE_PRECISION,MPI_SUM,0,MPI_COMM_WORLD,mpi_err)
 if (mpi_rank == 0) sum_tot = mpi_rarray
 mpi_rarray = zero
 call MPI_REDUCE(sum_freq,mpi_rarray,nr,MPI_DOUBLE_PRECISION,MPI_SUM,0,MPI_COMM_WORLD,mpi_err)
 if (mpi_rank == 0) sum_freq = mpi_rarray
 mpi_rarray = 0
 call MPI_REDUCE(sum_freq2,mpi_rarray,nr,MPI_DOUBLE_PRECISION,MPI_SUM,0,MPI_COMM_WORLD,mpi_err)
 if (mpi_rank == 0) sum_freq2 = mpi_rarray
 deallocate(mpi_rarray, stat=ierr)
 if (ierr /=0) call opc_abort("dfl_utils.f90::bandwidth","Error deallocating memory associated with array mpi_rarray")
#endif

 if (mpi_rank == 0) then
 	 do ir=1,nr
     if (abs(sum_tot(ir))>tiny(sum_tot(ir))) then
       freq_avg = sum_freq(ir)/sum_tot(ir)
       freq_dev = sqrt(sum_freq2(ir)/sum_tot(ir) - freq_avg**2)
       lambda_avg = cl/freq_avg
       bw = cl*freq_dev/freq_avg**2
       write(*,'(9(E16.9,2X))') ir*dr, lambda_avg, bw
     else
       write(*,'(9(E16.9,2X))') ir*dr, 0.0, 0.0
     end if
   end do
 end if

 deallocate(gridt, stat=ierr)
 if (ierr /=0) call opc_abort("dfl_utils.f90::bandwidth","Error deallocating memory associated with array gridt")
 deallocate(sum_tot, stat=ierr)
 if (ierr /=0) call opc_abort("dfl_utils.f90::bandwidth","Error deallocating memory associated with array sum_tot")
 deallocate(sum_freq, stat=ierr)
 if (ierr /=0) call opc_abort("dfl_utils.f90::bandwidth","Error deallocating memory associated with array sum_freq")
 deallocate(sum_freq2, stat=ierr)
 if (ierr /=0) call opc_abort("dfl_utils.f90::bandwidth","Error deallocating memory associated with array sum_freq2")

end ! subroutine bandwidth


subroutine shift(zshift, nadd, conffile, filename)
! ==================================================================
! Routine that adds nadd slices and shifts the slices by an amount
! zshift in the time window. zshift is specified in meters.
! ------------------------------------------------------------------

 use opc_globals
 use opc_interface
 use opc_lib
#ifdef MPI
 use mpi
#endif
 implicit none ! be strict

 real (kind=rk16), INTENT(IN)   :: zshift
 character(len=100), INTENT(IN) :: conffile, filename
 integer, INTENT(IN)            :: nadd
 real (kind=rk16)               :: deltaz, a, b, tzshift
 integer                        :: ix, iy, is, is1, is2, nshift, npoints, ierr
 integer                        :: ns_start, ns_end, ns_step
 complex (kind=rk16), dimension(:,:), pointer :: grid1, grid2
#ifdef MPI
 integer                        :: mpi_err
#endif
 integer                        :: mpi_rank
 ! mpi_rank is always defined and defaults to 0 for NON-MPI execution on a single node

#ifdef MPI
 call MPI_COMM_RANK(MPI_COMM_WORLD, mpi_rank, mpi_err)
#else
 mpi_rank=0 ! default value for NON-MPI execution on a single node
#endif

 if (mpi_rank > 0) return ! only node with rank 0 should execute this function

 allocate (grid1(Nx,Ny), stat=ierr)
 if (ierr /= 0) call opc_abort("dfl_utils.f90::shift", "Error allocating memory for array grid1")
 allocate (grid2(Nx,Ny), stat=ierr)
 if (ierr /= 0) call opc_abort("dfl_utils.f90::shift", "Error allocating memory for array grid2")

 ! set all grids to zero field
  grid1 = dcmplx(zero,zero)
  grid2 = dcmplx(zero,zero)

! add slices if required
 if (nadd .lt. 0) then
! add nadd slices with zero field to tail of time window
   do is=nslices + abs(nadd),1,-1
     if ((is + nadd) .gt. 0) then
       call read_dfl(filename, is + nadd, grid1)
       call write_dfl(filename, is, grid1)
     else
       call write_dfl(filename, is, grid2)
     end if
   end do
 elseif (nadd .gt. 0) then
! add nadd slices with zero field at head of time window
   do is=1,nadd
     call write_dfl(filename, nslices+is, grid2)
   end do ! is
 end if
 nslices = nslices + abs(nadd)
 if (nadd .ne. 0) then
! update .param file
   open(unit=FHOP,file=trim(conffile),iostat=ierr,status='unknown')
   write(unit=FHOP,FMT=*) " &optics"
   write(unit=FHOP,FMT=*) "   nslices = ", nslices
   write(unit=FHOP,FMT=*) "   npoints_x = ", Nx
   write(unit=FHOP,FMT=*) "   npoints_y = ", Ny
   write(unit=FHOP,FMT=*) "   lambda = ", lambda
   write(unit=FHOP,FMT=*) "   mesh_x = ", mesh_x
   write(unit=FHOP,FMT=*) "   mesh_y = ", mesh_y
   write(unit=FHOP,FMT=*) "   zsep = ", zsep
   write(unit=FHOP,FMT=*) "   scriptfile = '"//trim(scriptfile)//"'"
   write(unit=FHOP,FMT=*) "   infile = '"//trim(infile)//"'"
   write(unit=FHOP,FMT=*) "   outfile = '"//trim(outfile)//"'"
   write(unit=FHOP,FMT=*) "   Mx = ", Mx
   write(unit=FHOP,FMT=*) "   My = ", My
   write(unit=FHOP,FMT=*) " /"
   close(unit=FHOP)
 end if
 if (ierr == 0 ) then
   ! apply shift if required
   if (zshift .eq. zero) return
   tzshift = zshift/lambda    ! convert into units of lambda
   nshift = int(tzshift/zsep) ! whole number of slices to shift
   deltaz = tzshift -nshift * zsep  ! remainder of shift within a slice
   if (tzshift .lt. 0.0d0) then     ! shift towards tail
     ! work from tail to head
     ns_start = 1
     ns_end = nslices
     ns_step = 1
   else                             ! shift towards head
     ! work from head to tail
     ns_start = nslices
     ns_end = 1
     ns_step = -1
   end if
   b = abs(deltaz)/zsep
   a = 1-b
   do is=ns_start,ns_end,ns_step
     is1 = is + ns_step*abs(nshift)
     is2 = is1 + ns_step
     if ( (ns_end-is1)*ns_step .ge. 1 ) then
       call read_dfl(filename, is1, grid1)
       call read_dfl(filename, is2, grid2)
       do iy=1,Ny
         do ix=1,Nx
           grid1(ix,iy)=a*grid1(ix,iy)+b*grid2(ix,iy)
         end do ! ix
       end do ! iy
     elseif ( (ns_end-is1)*ns_step .eq. 0 ) then
       call read_dfl(filename, is1, grid1)
       do iy=1,Ny
         do ix=1,Nx
           grid1(ix,iy)=a*grid1(ix,iy)
         end do ! ix
       end do ! iy
     else
       do iy=1,Ny
         do ix=1,Nx
           grid1(ix,iy)=dcmplx(0.0d0, 0.0d0)
         end do ! ix
       end do ! iy
     end if
     call write_dfl(filename, is, grid1)
   end do ! is
 else
   print *,'dfl_utils.f90::shift -> Error, could not open config file '//trim(conffile)
   print *,'dfl_utils.f90::shift -> ierr =',ierr
 end if

 deallocate (grid1, stat=ierr)
 if (ierr /= 0) call opc_abort("dfl_utils.f90::shift","Error deallocating memory associated with array grid1")
 deallocate (grid2, stat=ierr)
 if (ierr /= 0) call opc_abort("dfl_utils.f90::shift","Error deallocating memory associated with array grid2")
 return

end ! subroutine shift

subroutine get_fluence(file_in, file_out)
! ==================================================================
! This subroutine sums the intensity for each grid point over all
! the slices
! ------------------------------------------------------------------

 use opc_globals
 use opc_lib
#ifdef MPI
 use mpi
#endif
 implicit none ! be strict

 character(len=100), intent(in)                :: file_in, file_out
 character(len=100)                            :: ofile
 character(len=17)                             :: fmt1D, fstat, fpos
 integer                                       :: is, ix, iy, ierr
 real(kind=rk16)                               :: perarea
 real (kind=rk16), dimension(:,:), allocatable :: d  ! contains the fluence data
#ifdef MPI
 integer                                       :: mpi_err
 real (kind=rk16), dimension(:,:), allocatable :: t ! required for communicating between nodes
#endif
 integer                             :: mpi_rank, mpi_size
 ! mpi_rank and mpi_size are always defined and set to 0 and 1 respectively for NON-MPI execution of the
 ! code to allow easier coding of MPI and NON-MPI execution.

#ifdef MPI
 call MPI_COMM_RANK(MPI_COMM_WORLD, mpi_rank, mpi_err)
 call MPI_COMM_SIZE(MPI_COMM_WORLD, mpi_size, mpi_err)
#else
 mpi_rank=0 ! default value for NON-MPI execution on a single node
 mpi_size=1 ! default value for NON-MPI execution on a single node
#endif
 ofile = file_out ! allow to modify filename
 ! allocate memory for reading data from file and to receive result
 allocate(grid(Nx,Ny), stat=ierr)
 if (ierr /= 0) call opc_abort('dfl_utils.f90::get_fluence', "Error allocating memory for grid")
 grid=zero
 allocate(d(Nx,Ny), stat=ierr)
 if (ierr /= 0) call opc_abort('dfl_utils.f90::get_fluence', "Error allocating memory for d")
 d=zero

 do is=1+mpi_rank,nslices,mpi_size
   call read_dfl(file_in, is, grid)
   call fluence(is, d)
 end do
#ifdef MPI
 ! collect all data if we are running under mpi
 print *,'process ',mpi_rank,' waiting for synchronisation'
 if (mpi_size > 1) then
 	 allocate(t(Nx,Ny), stat=ierr) ! temporary array to receive data from MPI_REDUCE operation
   if (ierr /= 0) call opc_abort('dfl_utils.f90::fluence', "Error allocating memory for t")
   t=zero
   call MPI_BARRIER(MPI_COMM_WORLD,mpi_err)
   call MPI_REDUCE(d,t,Nx*Ny,MPI_DOUBLE_PRECISION,MPI_SUM,0,MPI_COMM_WORLD,mpi_err)
   if (mpi_err /= 0) call opc_abort("dfl_utils::fluence","error merging data from processes")
   if (mpi_rank == 0) d=t ! set proper data for rank=0 when running in mpi mode
   deallocate(t, stat=ierr)
   if (ierr /= 0) call opc_abort('dfl_utils.f90::get_fluence', "Error deallocating memory for t")
 end if
#endif
 if (mpi_rank == 0) then
   if (nslices > 0) then
     ! scale the data to J/m^2
     if (spectral_domain) then
       d = dfreq * d
     else
       d = zsep * lambda * d / cl ! sum over slices multiplied by dT between two slices
     end if
   end if
   ! now write data to file
   call check_file(ofile, fstat, fpos)
   open(unit=FHDO, file=trim(ofile), status=fstat, position=fpos, iostat=ierr)
   if (trim(fstat) == 'REPLACE') then
     call write_header(FHDO, 'fluence', '', zero, mesh_x, mesh_y)
   else
     write(FHDO,'( )')
   end if
   if (ierr /= 0) call opc_abort('dfl_utils.f90::get_fluence', 'error opening filename '// trim(ofile))
   write (unit=fmt1D, fmt='(A1,I5,A11)') '(', Nx,'(E16.9, /))' ! compile output format
   do iy=1,Ny
     write(unit=FHDO, fmt=fmt1D, iostat=ierr ) d(:,iy)
   end do
   close(FHDO)
   if (ierr /= 0) call opc_abort('dfl_utils.f90::get_fluence', 'error writing to filename '// trim(ofile))
   deallocate(d, stat=ierr)
   if (ierr /= 0) call opc_abort('dfl_utils.f90::get_fluence', 'error deallocating d')
   deallocate(grid, stat=ierr)
   if (ierr /= 0) call opc_abort('dfl_utils.f90::get_fluence', 'error deallocating grid')
 end if

 end subroutine get_fluence
