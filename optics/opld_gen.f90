! ##################################################################
! Copyright (C) 2007 J.G. Karssenberg, P.J.M. van der Slot,
! 
! This program is free software; you can redistribute it and/or
! modify it under the terms of the GNU General Public License as
! published by the Free Software Foundation; version 2 of the
! License.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! ##################################################################

program opld_gen
! ==================================================================
! Sample program to generate an optical path length difference
! (opld) file that can be used in combination with OPC. This sample
! generates the opld create by a lens with focal distance f on
! a grid consisting of 255 by 255 points.
! ------------------------------------------------------------------

 use opc_globals
 use opc_lib
 implicit none ! e strict

 
 integer, parameter :: npoints = 255,                                    &
                       nhalf = npoints/2 + 1    
 integer            :: ix,iy, recsize, ierr
 real (kind=rk16)    :: f, const
 character(len=15)  :: filename
 
 allocate (ogrid(1), stat=ierr)
 if (ierr /= 0) stop "opld_gen.f90::main -> Error, not enough memory to allocate array ogrid"
 allocate (ogrid(1) % r(npoints,npoints), stat=ierr)
 if (ierr /= 0) stop "opld_gen.f90::main -> Error, not enough memory to allocate array ogrid % r"

 mesh_x = 78.74d-6
 mesh_y = mesh_x
 f = 3.0d0
 const = -0.5d0 * mesh_x*mesh_y / f
      
 do iy=1,npoints
   do ix=1,npoints
     ogrid(1) % r(ix,iy)= const*( (ix-nhalf)**2 + (iy-nhalf)**2 )
   end do
 end do

 recsize=npoints*npoints*8 ! record size in binary files
 filename = 'lens.opd'

 open( unit=FHOO, file=filename, recl=recsize, access='direct', status='unknown', iostat=ierr )
 if (ierr == 0) then
   write( unit=FHOO, rec=1, iostat=ierr ) ((ogrid(1) % r(ix,iy), ix=1, npoints), iy=1, npoints)
   if (ierr /= 0) print *,'opld_gen.f90::main -> error writing ', trim(filename), ' IO-status = ', ierr
   close( unit=FHOO )
 else
   if (ierr /= 0) print *,'opld_gen.f90::main -> error opening ', trim(filename), ' IO-status = ', ierr
 end if
 
 deallocate(ogrid(1) % r, stat=ierr)
 if (ierr /= 0) stop "opld_gen.f90::main -> Error deallocatng memory associated with array ogrid % r"
 deallocate (ogrid, stat=ierr)
 if (ierr /= 0) stop "opld_gen.f90::main -> Error deallocatng memory associated with array ogrid"
 
end ! program opld_gen
