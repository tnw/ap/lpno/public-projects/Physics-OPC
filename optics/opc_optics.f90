! ##################################################################
! Copyright (C) 2006-2014 J.G. Karssenberg, P.J.M. van der Slot,
! I.V.Volokhine
!
! This program is free software; you can redistribute it and/or
! modify it under the terms of the GNU General Public License as
! published by the Free Software Foundation; version 2 of the
! License.
!
! This program is distributed in the hope that it will be useful,
! but WITHOUT ANY WARRANTY; without even the implied warranty of
! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
! GNU General Public License for more details.
! ##################################################################


program optics
! ==================================================================
! Program to perform transformations of the electric field
! as produced by the genesis 1.3 simulation.
! Transformations and their parameters can be specified in a
! script file.
! ------------------------------------------------------------------
 use, intrinsic :: iso_c_binding
 use opc_globals
 use fftw3
 use opc_lib
#ifdef MPI
 use mpi
#endif
 
 implicit none  
 
 interface
   integer(C_INT) function strlen(s) bind(C, name='strlen')
     import
     type(C_PTR), value :: s
   end function strlen
   subroutine free(p) bind(C, name='free')
     import
     type(C_PTR), value :: p
   end subroutine free
 end interface  
 
 integer              :: is, iarg, ierr, isuccess, i, ix, iy, it, recsize, &
                         file_stat, slen, ioerr, nlines
 real (kind=rk16)     :: my_freq, my_lambda
 character(len=100)   :: conffile, opc_wisdom_filename, fstat, fpos, fmt1D
 logical              :: opc_wisdom_exist, for2perl
 ! for FFTW wisdom import/export
 integer(C_INT)       :: ret
 character(C_CHAR), pointer :: s_c(:)
 character,dimension(:),allocatable :: s_f
 integer(C_SIZE_T)    :: slen_c
 type (C_PTR)         :: p
 character(len=1)     :: cdir
 character(len=10)    :: ccmd
 character(len=cmd_length), dimension(:), pointer :: cmds
 character(len=256)   :: command
#ifdef MPI
 integer              :: mpi_err
 real(kind=rk16), dimension(:), pointer   :: temp_1d
 real(kind=rk16), dimension(:,:), pointer :: temp_2d
#endif
 ! mpi_rank and mpi_size are defined by default and set to 0 and 1
 ! respectively to make coding of MPI and NON-MPI execution easier
 integer              :: mpi_rank, mpi_size

! check number of command line arguments
 iarg = iargc()
 if (iarg == 1) then
   ! right number of arguments specified, argument is config file
   call getarg(iarg, conffile)
 else
   print *,"OPC_optics.f90::main -> Error, wrong number of command line arguments"
   stop
end if 

! initialisation
! MPI stuff
#ifdef MPI
 call MPI_INIT(mpi_err)
 call MPI_COMM_RANK(MPI_COMM_WORLD, mpi_rank, mpi_err)
 call MPI_COMM_SIZE(MPI_COMM_WORLD, mpi_size, mpi_err)
#else
 mpi_rank=0  ! set default for NON-MPI execution
 mpi_size=1  ! set default for NON-MPI execution
#endif 
 if (mpi_rank == 0) then
   print *,'---        OPC version ',trim(opc_version),'        ---'
   print *,'---  Planning FFTs for propagation  ---'
 end if
 call opc_init(conffile) ! read conffile
! determine if we work in time or frequency domain
 spectral_domain = .FALSE. ! work in the time domain
 slen = len(trim(infile))
 if (infile(slen-2:slen)=='sfl') spectral_domain = .TRUE. ! work in the spectral domain
      ! when an FFT is preformed on a .dfl file, the spectral components are ordered
      ! as follows, positive frequency in first half of array and the negative frequencies
      ! in the second half of the array (0, f1, f2, ..., fn, -fn, -fn-1, ,,, -f1)
      ! We want to return the data in the correct order when we convert to ascii

 ! set FFTW plan variables to null pointer to test if a plan is assigned
 plan_forward_fresnel_ux  = C_NULL_PTR
 plan_forward_fresnel_uy  = C_NULL_PTR
 plan_backward_fresnel_ux = C_NULL_PTR
 plan_backward_fresnel_uy = C_NULL_PTR
 plan_forward_fresnel_vx  = C_NULL_PTR
 plan_forward_fresnel_vy  = C_NULL_PTR
 ! plan the FFT required for the spectral propagation using in place replacement 
 ! We can not do this in subroutine spectral as planning the FFT destroys the content of 
 ! the grid. For the FRESNEL propagation, arrays used for the FFT are created dynamically
 ! so planning for these appear in subroutines FRESNEL and FRESNELM
 ! First check if appropriate plan already exists
 if ((trim(export_wisdom) == 'all').OR.(mpi_size == 1)) then
   write(opc_wisdom_filename,FMT='(A19,I3.3,"_",I5.5,"x",I5.5)') 'opc_wisdom_spectral',mpi_rank,Nx,Ny
   inquire(FILE=trim(opc_wisdom_filename),EXIST=opc_wisdom_exist)
   if (opc_wisdom_exist) then
     ! restore plans from previous runs using the same grid size
     ret = fftw_import_wisdom_from_filename(C_CHAR_''//trim( opc_wisdom_filename) // C_NULL_CHAR)
     if (ret == 0) call  opc_abort('opc_optics.f90::main', 'error importing wisdom from file')
   end if
 else if ((trim(export_wisdom) == 'single').AND.(mpi_size>1)) then
 	 if (mpi_rank == 0) then  ! read wisdom file and broadcast to other subprocesses
 	 	 write(opc_wisdom_filename,FMT='(A19,I3.3,"_",I5.5,"x",I5.5)') 'opc_wisdom_spectral',mpi_rank,Nx,Ny
     inquire(FILE=trim(opc_wisdom_filename),EXIST=opc_wisdom_exist)
     if (opc_wisdom_exist) then
       ! restore plans from previous runs using the same grid size
       ret = fftw_import_wisdom_from_filename(C_CHAR_''//trim( opc_wisdom_filename) // C_NULL_CHAR)
       if (ret == 0) call  opc_abort('opc_optics.f90::main', 'error importing wisdom from file')
     end if
     p = fftw_export_wisdom_to_string()
     if (.not. c_associated(p)) call opc_abort('opc_optics.f90::main', 'error converting wisdom to string')
     slen_c = strlen(p)+1
     call c_f_pointer(p, s_c, [slen_c])
     slen = slen_c    ! convert to fortran integer type
     allocate(s_f(slen),stat=ierr)
     s_f=s_c
#ifdef MPI
     call mpi_bcast(slen,1,mpi_integer,0,mpi_comm_world,mpi_err)
     call mpi_bcast(s_f, slen,mpi_character,0,mpi_comm_world,mpi_err) 
     call free(p)     
     deallocate(s_f)
#endif       
   end if
#ifdef MPI
   if (mpi_rank > 0) then  ! receive wisdom broadcasted by subprocess with rank 0
   	 call mpi_bcast(slen,1,mpi_integer,0,mpi_comm_world,mpi_err)
   	 allocate(s_f(slen),stat=ierr)
   	 call mpi_bcast(s_f, slen, mpi_character,0,mpi_comm_world,mpi_err)
   	 ret = fftw_import_wisdom_from_string(C_CHAR_''//s_f)
   	 deallocate(s_f)
   end if
#endif
 end if
 ! plan FFT's
 plan_forward_spectral_x  = fftw_plan_dft_2d ( Ny, Nx, grid, grid, FFTW_FORWARD, FFTW_MEASURE )
 plan_backward_spectral_x = fftw_plan_dft_2d ( Ny, Nx, grid, grid, FFTW_BACKWARD, FFTW_MEASURE )
 if (include_Ey) then
   plan_forward_spectral_y  = fftw_plan_dft_2d (Ny, Nx, gridy, gridy, FFTW_FORWARD, FFTW_MEASURE )
   plan_backward_spectral_y = fftw_plan_dft_2d (Ny, Nx, gridy, gridy, FFTW_BACKWARD, FFTW_MEASURE )
 end if 
 if (.NOT.opc_wisdom_exist) then
   ! no previous plans exists for this grid size, check if we need to store plan to file for later use
   if (trim(export_wisdom) == 'single') then
   	 if (mpi_rank == 0) then
       ret = fftw_export_wisdom_to_filename(C_CHAR_'' // trim(opc_wisdom_filename) // C_NULL_CHAR)
       if (ret == 0) call opc_abort('opc_optics.f90::main', 'error exporting wisdom to file')
     end if
   end if
   if (trim(export_wisdom) == 'all') then
   	 ret = fftw_export_wisdom_to_filename(C_CHAR_'' // trim(opc_wisdom_filename) // C_NULL_CHAR)
     if (ret == 0) call opc_abort('opc_optics.f90::main', 'error exporting wisdom to file')
   end if   
 end if

 if (mpi_rank == 0) then
   print *,'--- Start propagating optical field ---'
 ! read and store command file
   allocate(cmds(999),stat=ierr)
   if (ierr/=0) call opc_abort('opc_optics.f90::main',  "Error allocating memory associated with cmds")   
   open( unit=FHIS, file=scriptfile, status='old', iostat=ioerr)
   if (ioerr/=0) call opc_abort('opc_optics.f90::main', 'Error while opening file '//trim(scriptfile))
   nlines = 0
   do while ( ioerr == 0 )
     read( unit=FHIS, fmt='(A)', iostat=ioerr) command
     if (ioerr /= 0 ) CYCLE 
     nlines = nlines+1
     if (nlines > 999) call opc_abort('opc_optics.f90::main','error, insufficient memory to contain propagation commands')
     cmds(nlines)=command 
   end do
   close(FHIS) 
 end if
#ifdef MPI
 if (mpi_size>1) CALL mpi_bcast(nlines,1,mpi_integer,0,mpi_comm_world,mpi_err)
#endif
 allocate(prop_cmd(nlines),stat=ierr)
 if (ierr/=0) call opc_abort('opc_optics.f90::main',  "Error allocating memory associated with prop_cmd")
 if (mpi_rank == 0) then ! populate prop_cmd
   prop_cmd = cmds(1:nlines)
   deallocate(cmds,stat=ierr)
   if (ierr/=0) call opc_abort('opc_optics.f90::main',  "Error deallocating memory associated with cmds")
 end if
#ifdef MPI
 if (mpi_size > 1) then ! allocate memory and broadcast propagation commands
   CALL mpi_bcast(prop_cmd,cmd_length*nlines,mpi_character,0,mpi_comm_world,mpi_err)
 end if
#endif
 init_opld = .TRUE.
 init_waveguide = .TRUE. 
 nogrid = 0
 
! loop over slices
 do is=1+mpi_rank,nslices,mpi_size !mpi_rank=0 and mpi_size=1 for NON-MPI execution
   Mxtot=1
   Mytot=1
   call fillaxis()  ! reset axis to initial value in case net magnification during propagtion <> 1
   if (is <= (nslices+1)/2) then  ! order of frequencies for the slices is fc, fc+df, ...., fc+mdf, fc-m*df, ..., fc-df
     my_freq = cl/lambda + (is-1)*dfreq
   else 
     my_freq = cl/lambda - (nslices -is+1)*dfreq
   end if
   my_lambda = cl/my_freq
   k_wave = TwoPi * my_freq / cl
   write(*,'("Processing slice ", i5, " (lambda: ", E12.3,")")') is, my_lambda
   call do_slice(is)
 end do ! loop over slices
#ifdef MPI
! synchronize processors before finalizing MPI
   print *,'process=',mpi_rank,' finished, waiting for synchronisation'
   call MPI_BARRIER(MPI_COMM_WORLD,mpi_err)
! Check if data needs to be collected and writen to file
   if (nfluence>0) then 
   	 if ((nslices>1).AND.(mpi_size>1)) then  ! running more than one slice on multiple cores  
       ! allocate memory to receive results
   	   allocate(temp_2d(Nx,Ny), stat=ierr)
   	   if (ierr /= 0) call opc_abort('opc_optics.f90::main', "Error allocating memory for temp_2d")
   	   do i = 1, nfluence
         temp_2d=zero
   	     call MPI_REDUCE(res_fluence(1:Nx,1:Ny,i),temp_2d,Nx*Ny,MPI_DOUBLE_PRECISION,MPI_SUM,0,MPI_COMM_WORLD,mpi_err)
   	     if (ierr /=0) call opc_abort('opc_optics.f90::main;', "Error mpi_reduce for fluence data")
         if (mpi_rank == 0) res_fluence(1:Nx,1:Ny,i) = temp_2d(1:Nx,1:Ny)
       end do
       deallocate(temp_2d, stat=ierr)
       if (ierr/=0)call opc_abort('opc_optics.f90::main',  "Error deallocating memory associated with temp_2d")
     end if
   end if
! Now collect all data on the master node to write to file and perl script if required
   if (nxcross > 0 ) then
     ! allocate memory to receive results
     allocate(temp_2d(Nx,nslices), stat=ierr)
     if (ierr /= 0) call opc_abort('opc_optics.f90::main', "Error allocating memory for temp_2d")
     do i = 1, nxcross
       temp_2d=zero
       call MPI_REDUCE(res_xcross(1:Nx,1:nslices,i),temp_2d,nslices*Nx,MPI_DOUBLE_PRECISION,MPI_SUM,0,MPI_COMM_WORLD,mpi_err)
       if (ierr /=0) call opc_abort('opc_optics.f90::main;', "Error mpi_reduce for xcross data")
       if (mpi_rank == 0) res_xcross(1:Nx,1:nslices,i) = temp_2d(1:Nx,1:nslices)
     end do  
     deallocate(temp_2d, stat=ierr)
     if (ierr/=0)call opc_abort('opc_optics.f90::main',  "Error deallocating memory associated with temp_2d")
   end if 
   if (nycross > 0 ) then
     ! allocate memory to receive results
     allocate(temp_2d(Ny,nslices), stat=ierr)
     if (ierr /= 0) call opc_abort('opc_optics.f90::main', "Error allocating memory for temp_2d")
     do i = 1, nycross
       temp_2d=zero
       call MPI_REDUCE(res_ycross(1:Ny,1:nslices,i),temp_2d,nslices*Ny,MPI_DOUBLE_PRECISION,MPI_SUM,0,MPI_COMM_WORLD,mpi_err)
       if (ierr /=0) call opc_abort('opc_optics.f90::main;', "Error mpi_reduce for ycross data")
       if (mpi_rank == 0) res_ycross(1:Ny,1:nslices,i) = temp_2d(1:Ny,1:nslices)
     end do  
     deallocate(temp_2d, stat=ierr)
     if (ierr/=0) call opc_abort('opc_optics.f90::main', "Error deallocating memory associated with temp_2d")
   end if
   if (ntotal > 0 ) then
     ! allocate memory to receive results
     allocate(temp_1d(nslices), stat=ierr)
     if (ierr /= 0) call opc_abort('opc_optics.f90::main', "Error allocating memory for temp_1d")
     do i = 1, ntotal
       temp_1d=zero
       call MPI_REDUCE(res_total(1:nslices,i),temp_1d,nslices,MPI_DOUBLE_PRECISION,MPI_SUM,0,MPI_COMM_WORLD,mpi_err)
       if (mpi_rank == 0) res_total(1:nslices,i) = temp_1d(1:nslices)
     end do  
     deallocate(temp_1d, stat=ierr)
     if (ierr/=0) call opc_abort('opc_optics.f90::main', "Error deallocating memory associated with temp_1d")
   end if
   if (nspectrum > 0 ) then
     ! allocate memory to receive results
     allocate(temp_1d(nslices), stat=ierr)
     if (ierr /= 0) call opc_abort('opc_optics.f90::main', "Error allocating memory for temp_1d")
     do i = 1, nspectrum
       temp_1d=zero
       call MPI_REDUCE(res_spectrum(1:nslices,i),temp_1d,nslices,MPI_DOUBLE_PRECISION,MPI_SUM,0,MPI_COMM_WORLD,mpi_err)
       if (ierr /=0) call opc_abort('opc_optics.f90::main;', "Error mpi_reduce for spectrum data")
       if (mpi_rank == 0) res_spectrum(1:nslices,i) = temp_1d(1:nslices)
     end do  
     deallocate(temp_1d, stat=ierr)
     if (ierr/=0) call opc_abort('opc_optics.f90::main', "Error deallocating memory associated with temp_1d")
   end if
   if (nstat > 0 ) then
     ! allocate memory to receive results
     allocate(temp_2d(8,nslices), stat=ierr)
     if (ierr /= 0) call opc_abort('opc_optics.f90::main', "Error allocating memory for temp_2d")     
     do i = 1, nstat
       temp_2d=zero
       call MPI_REDUCE(res_stat_temp(:,:,i),temp_2d,nslices*8,MPI_DOUBLE_PRECISION,MPI_SUM,0,MPI_COMM_WORLD,mpi_err)
       if (ierr /=0) call opc_abort('opc_optics.f90::main;', "Error mpi_reduce for statistical data")
       if (mpi_rank == 0)  res_stat_temp(:,:,i) = temp_2d
     end do  
     deallocate(temp_2d, stat=ierr)
     if (ierr/=0) call opc_abort('opc_optics.f90::main', "Error deallocating memory associated with temp_2d")
   end if
   call MPI_FINALIZE(mpi_err)
#endif
 if (mpi_rank == 0) then
   print *,'--- End propagating optical field ---'
   ! now write data to file if requested by user
   for2perl = .FALSE. 
   if (nxcross > 0 ) then
     do i = 1, nxcross
   	   if (trim(xcross_fname(i)) /= 'no_output_file') then 
     	   ! write to file specified by user
     	   call check_file(xcross_fname(i), fstat, fpos)
     	   
     	   open(unit=FHDO, file=trim(xcross_fname(i)), status=fstat, position=fpos)
         if (trim(fstat) == 'REPLACE') then
     	     call write_header(FHDO, 'cross', 'x', zero, res_xmesh(i))
     	   else
     	     write(FHDO,'( )')
     	   end if
     	   write (unit=fmt1D, fmt='("(",I10,"(E16.9, /))")') Nx ! compile output format    	  
     	   do is = 1, nslices
     	     ! write output to file of users choice
     	     write(FHDO,fmt=fmt1D) (res_xcross(it, is, i), it=1,Nx)
     	   end do
     	   close(FHDO)
       end if
       if (trim(xcross_name(i)) /= 'no_perl_output') then
         if (.not.for2perl) then
           ! for2perl output request but file not yet opened
           open(unit=FHPO, file='for2perl.tmp', status='REPLACE')
           for2perl = .TRUE. ! indicates that file is open
         end if
     	   ! write output to file read by perl script 
     	   write(FHPO,'(A30,3(I10,2X),2(E13.6,2X))') 'xcross ' // trim(xcross_name(i)), nslices, Nx, 0, res_xmesh(i), zero
     	   write (unit=fmt1D, fmt='("(",I10,"(E16.9))")')  Nx ! compile output format   
     	   do is = 1, nslices
     	     write(FHPO,fmt=fmt1D) ( res_xcross(it,is,i),it=1,Nx )   	 		
     	   end do
  	   end if
     end do
   end if
   if (nycross > 0 ) then
   	 do i = 1, nycross
   	   if (trim(ycross_fname(i)) /= 'no_output_file') then 
         ! write to file specified by user
         call check_file(ycross_fname(i), fstat, fpos)
     	   
         open(unit=FHDO, file=trim(ycross_fname(i)), status=fstat, position=fpos)
         if (trim(fstat) == 'REPLACE') then
     	     call write_header(FHDO, 'cross', 'y', zero, res_ymesh(nxcross+i))
     	   else
     	     write(FHDO,'( )')
     	   end if
     	   write (unit=fmt1D, fmt='("(",I10,"(E16.9, /))")') Ny ! compile output format     	 	  
     	   do is = 1, nslices
     	     ! write output to file of users choice
     	     write(FHDO,fmt=fmt1D) (res_ycross(it, is, i), it=1,Ny)
     	   end do
     	   close(FHDO)
       end if
       if (trim(ycross_name(i)) /= 'no_perl_output') then
         if (.not.for2perl) then
           ! for2perl output request but file not yet opened
           open(unit=FHPO, file='for2perl.tmp', status='REPLACE')
           for2perl = .TRUE. ! indicates that file is open
         end if
     	   ! write output to file read by perl script 
     	   write(FHPO,'(A30,3(I10,2X),2(E13.6,2X))') 'ycross ' // trim(ycross_name(i)), nslices, 0, Ny, zero, res_ymesh(nxcross+i)
     	   write (unit=fmt1D, fmt='("(",I10,"(E16.9))")')  Ny ! compile output format   
     	   do is = 1, nslices
     	     write(FHPO,fmt=fmt1D) ( res_ycross(it,is,i),it=1,Ny )   	 		
       	 end do
       end if     	 	
   	 end do
   end if
   if (ntotal > 0 ) then
   	 do i = 1, ntotal
   	   if (trim(total_fname(i)) /= 'no_output_file') then 
         ! write to file specified by user
         call check_file(total_fname(i), fstat, fpos)
     	   
         open(unit=FHDO, file=trim(total_fname(i)), status=fstat, position=fpos)
         if (trim(fstat) == 'REPLACE') then
     	     call write_header(FHDO, 'total', ' ', zero, zero)
     	   
     	   else
     	     write(FHDO,'( )')
     	   end if
     	   do is = 1, nslices
    	     ! write output to file of users choice
     	     write(FHDO,fmt='(E16.9)') (res_total(is,i) )
     	   end do
     	   close(FHDO)
       end if
       if (trim(total_name(i)) /= 'no_perl_output') then
         if (.not.for2perl) then
           ! for2perl output request but file not yet opened
           open(unit=FHPO, file='for2perl.tmp', status='REPLACE')
           for2perl = .TRUE. ! indicates that file is open
         end if	! write output to file read by perl script 
     	   write(FHPO,'(A30,3(I10,2X),2(E13.6,2X))') 'total ' // trim(total_name(i)), nslices, 1, 0, zero, zero
     	   do is = 1, nslices
     	     write(FHPO,fmt='(E16.9)') ( res_total(is,i) )   	 		
     	   end do
       end if 
   	 end do ! loop over output variable for total command
   end if ! end output ntotal
   if (nspectrum > 0 ) then ! spectral power density requested by user
   	 do i = 1, nspectrum
   	   if (trim(spectrum_fname(i)) /= 'no_output_file') then 
         ! write to file specified by user
         call check_file(spectrum_fname(i), fstat, fpos)
         open(unit=FHDO, file=trim(spectrum_fname(i)), status=fstat, position=fpos)
         if (trim(fstat) == 'REPLACE') then
     	     call write_header(FHDO,'spectrum', ' ', zero, zero)
     	   else
     	     write(FHDO,'( )')
     	   end if
     	   do is = (nslices+1)/2+1, nslices ! correct for order in which spectral components are stored in array due to FFT
    	     ! write output to file of users choice
     	     write(FHDO,fmt='(E16.9)') (res_spectrum(is,i) )
     	   end do
     	   do is = 1, (nslices+1)/2
    	     ! write output to file of users choice
     	     write(FHDO,fmt='(E16.9)') (res_spectrum(is,i) )
     	   end do
     	   close(FHDO)
       end if
       if (trim(spectrum_name(i)) /= 'no_perl_output') then
         if (.not.for2perl) then
           ! for2perl output request but file not yet opened
           open(unit=FHPO, file='for2perl.tmp', status='REPLACE')
           for2perl = .TRUE. ! indicates that file is open
         end if	! write output to file read by perl script 
     	   write(FHPO,'(A30,3(I10,2X),2(E13.6,2X))') 'spectrum ' // trim(spectrum_name(i)), nslices, 1, 0, zero, zero
     	   do is = (nslices+1)/2+1, nslices ! correct for order in which spectral components are stored in array due to FFT
     	     write(FHPO,fmt='(E16.9)') ( res_spectrum(is,i) )   	 		
     	   end do
     	   do is = 1, (nslices+1)/2
     	     write(FHPO,fmt='(E16.9)') ( res_spectrum(is,i) )   	 		
     	   end do
       end if 
   	 end do ! loop over output variable for spectral command
   end if ! end output nspectral
   if (nfluence>0) then ! fluence requested by user
     ! scale the data to J/m^2 if nslices > 1
     if (nslices>1) then
       if (spectral_domain) then
         res_fluence = dfreq * res_fluence
       else
         res_fluence = zsep * lambda * res_fluence / cl ! sum over slices multiplied by dt between two slices
       end if
     end if
     ! now write data to file
     do i = 1, nfluence
   	   if (trim(fluence_fname(i)) /= 'no_output_file') then 
         ! write to file specified by user
         call check_file(fluence_fname(i), fstat, fpos)
         open(unit=FHDO, file=trim(fluence_fname(i)), status=fstat, position=fpos)
         if (trim(fstat) == 'REPLACE') then
     	     call write_header(FHDO,'fluence', ' ', zero, &
     	          res_xmesh(nxcross+nycross+ntotal+nstat+i), res_ymesh(nxcross+nycross+ntotal+nstat+i) )
     	   else
     	     write(FHDO,'( )')
     	   end if
     	   write (unit=fmt1D, fmt='("(",I5,"(E16.9, /))")') Nx ! compile output format 
         do iy=1,Ny 
           write(unit=FHDO, fmt=fmt1D, iostat=ierr ) res_fluence(:,iy,i)
         end do
     	   close(FHDO)
       end if
       if (trim(fluence_name(i)) /= 'no_perl_output') then
         if (.not.for2perl) then
           ! for2perl output request but file not yet opened
           open(unit=FHPO, file='for2perl.tmp', status='REPLACE')
           for2perl = .TRUE. ! indicates that file is open
         end if	! write output to file read by perl script 
     	   write(FHPO,'(A30,3(I10,2X),2(E13.6,2X))') 'fluence ' // trim(fluence_name(i)), 1, Nx, Ny, &
     	               res_xmesh(nxcross+nycross+ntotal+nstat+i), res_ymesh(nxcross+nycross+ntotal+nstat+i)
     	   write (unit=fmt1D, fmt='("(",I5,"(E16.9, /))")') Nx ! compile output format 
         do iy=1,Ny 
           write(unit=FHPO, fmt=fmt1D, iostat=ierr ) res_fluence(:,iy,i)
         end do
       end if 
   	 end do ! loop over output variable for fluence command
   end if ! end output nfluence
   if (nstat > 0 ) then
     do i = 1, size(stat_fname)
       ! first sum over all slices to obtain the summed values
       res_stat(1,i) = sum(res_stat_temp(1,:,i))
       res_stat(2,i) = sum(res_stat_temp(2,:,i))/res_stat(1,i)
       res_stat(3,i) = sum(res_stat_temp(3,:,i))/res_stat(1,i)
       res_stat(4,i) = sqrt(sum(res_stat_temp(4,:,i))/res_stat(1,i) - res_stat(2,i)**2)
       res_stat(5,i) = sqrt(sum(res_stat_temp(5,:,i))/res_stat(1,i) - res_stat(3,i)**2)
       res_stat(6,i) = sqrt(sum(res_stat_temp(6,:,i))/res_stat(1,i) - res_stat(2,i)**2 - res_stat(3,i)**2)
       res_stat(7,i) = sqrt(two)*res_stat(6,i)
       res_stat(8,i) = sum(res_stat_temp(7,:,i))/res_stat(1,i)
       res_stat(9,i) = sqrt(sum(res_stat_temp(8,:,i))/res_stat(1,i) - res_stat(8,i)**2)
       ! convert from power to energy in case of multiple slices
       if (nslices > 1) then
         if (spectral_domain) then
           res_stat(1,i) = res_stat(1,i)/((nslices-1)*dt)    ! multiply by df
           res_stat(9,i) = cl*res_stat(9,i)/res_stat(8,i)**2 ! change from delta_f to delta_lambda
           res_stat(8,i) = cl/res_stat(8,i)                  ! change from f_av to lambda_av
         else
           res_stat(1,i) = res_stat(1,i)*dt                ! multiply by dt
         end if
       end if
	   ! now determine if we need to write to file
   	   if (stat_fname(i) /= 'no_output_file') then 
         ! write to file specified by user
         call check_file(stat_fname(i), fstat, fpos)
      	 
         open(unit=FHDO, file=trim(stat_fname(i)), status=fstat, position=fpos)
      	 
         if (trim(fstat) == 'REPLACE') then
      	   
           call write_header(FHDO,'stat', ' ', zero, zero)
     	   
         else
     	     write(FHDO,'( )')
     	   end if
     	   ! write output to file of users choice
     	   if (spectral_domain) then
     	     write(FHDO,fmt='(9(E15.6))') (res_stat(is, i), is=1,9)
     	   else
     	     write(FHDO,fmt='(7(E15.6))') (res_stat(is, i), is=1,7)
     	   end if
     	   close(FHDO)
       end if
       if (stat_name(i) /= 'no_perl_output') then
         if (.not.for2perl) then
           ! for2perl output request but file not yet opened
           open(unit=FHPO, file='for2perl.tmp', status='REPLACE')
           for2perl = .TRUE. ! indicates that file is open
         end if
     	   ! write output to file read by perl script 
     	   write(FHPO,'(A30,3(I10,2X),2(E13.6,2X))') 'stat ' // stat_name(i), 1, 0, 0, zero, zero
     	   if (spectral_domain) then
     	     write(FHPO,fmt='(9(E15.6, 1X))') (res_stat(is, i), is=1,9)
     	   else
     	     write(FHPO,fmt='(7(E15.6, 1X))') (res_stat(is, i), is=1,7)
     	   end if
       end if  ! perl output
   	 
     end do ! loop over variables to output
   end if ! end output nstat
   if (for2perl) close(FHPO) 
 end if ! end writing data to file if requested by user
 
! destroy FFT plans
 call fftw_destroy_plan ( plan_forward_spectral_x )
 call fftw_destroy_plan ( plan_backward_spectral_x )
 if (include_Ey) then
   call fftw_destroy_plan ( plan_forward_spectral_y )
   call fftw_destroy_plan ( plan_backward_spectral_y )
 end if
 if (c_associated ( plan_forward_fresnel_ux )) then
   call fftw_destroy_plan ( plan_forward_fresnel_ux )
   call fftw_destroy_plan ( plan_forward_fresnel_uy )
   call fftw_destroy_plan ( plan_backward_fresnel_ux )
   call fftw_destroy_plan ( plan_backward_fresnel_uy )
   call fftw_destroy_plan ( plan_forward_fresnel_vx )
   call fftw_destroy_plan ( plan_forward_fresnel_vy )
 end if

 if (c_associated(p_fresnel_ux)) call fftw_free(p_fresnel_ux)
 if (c_associated(p_fresnel_uy)) call fftw_free(p_fresnel_uy)
 if (c_associated(p_fresnel_vx)) call fftw_free(p_fresnel_vx)
 if (c_associated(p_fresnel_vy)) call fftw_free(p_fresnel_vy) 
 if (c_associated(p_fresnel_w))  call fftw_free(p_fresnel_w)
 call opc_close()
end ! program optics



subroutine do_slice(is)
! ==================================================================
! Method to process one slice. Main program function.
! ------------------------------------------------------------------

 use opc_globals
 use opc_interface
 use opc_lib
 implicit none

 integer, intent(IN) :: is
 integer             :: int1, int2, icmd, i_xcross, i_ycross, i_tot, i_stat
 integer             :: i_spectrum, i_fluence, nlines
 real (kind=rk16)    :: dbl1, dbl2, dbl3, dbl4, dbl5, dbl6, dbl7, dbl8, dbl9, dbl10, dbl11
 complex(kind=rk16)  :: c1, c2, c3, c4
 character(len=32)   :: command, chr2
 character(len=100)  :: chr1, filename
    
! Read fieldfile
 grid = dcmplx(zero,zero)
 call read_dfl(infile, is, grid)
 ! check if a second field file with perpendicular polarisation is present
 if (include_Ey) call read_dfl(field_next, is, gridy)  ! read data from file
 
! dispatch commands
! See manual for call signatures used here
 
! initialize counters for diagnostics commands
 i_xcross = 0
 i_ycross = 0
 i_tot = 0
 i_stat = 0
 i_spectrum = 0
 i_fluence = 0
 
! determine the number of lines in prop_cmd
 nlines = size(prop_cmd)
 icmd = 1
 do while ( icmd <= nlines )
!   print *,is,' - executing ',trim(prop_cmd(icmd))
   select case ( prop_cmd(icmd) )
   case ('diaphragm')
   	 icmd = icmd+1
     read(prop_cmd(icmd),*,end=40) dbl1, dbl2, dbl3, int1, chr1, dbl4, int2
     call diaphragm(dbl1, dbl2, dbl3, int1, chr1, dbl4, int2)
!     print *,'diaphragm:',maxval(abs(grid))
   case ('rectangle')
   	 icmd=icmd+1
     read(prop_cmd(icmd),*,end=40) dbl1, dbl2, dbl3, dbl4, int1
     call rectangle(dbl1, dbl2, dbl3, dbl4, int1)
!     print *,'rectangle:',maxval(abs(grid))
   case ('opld')
   	 icmd=icmd+1
     read(prop_cmd(icmd),*,end=40) opldfile, dbl1, dbl2
     call opld(dbl1, dbl2)
!     print *,'opld:',maxval(abs(grid))
   case ('lens')
   	 icmd=icmd+1
     read(prop_cmd(icmd),*,end=40) dbl1, dbl2, dbl3
     call lens(dbl1, dbl2, dbl3)
!     print *,'lens:',maxval(abs(grid))
   case ('xmirror')
      icmd=icmd+1
   	  read(prop_cmd(icmd),*,end=40) chr1, dbl1, dbl2, dbl3, dbl4, dbl5, dbl6, dbl7, dbl8, dbl9
   	  c1 = dcmplx(dbl4, dbl5)
   	  c2 = dcmplx(dbl6, dbl7)
   	  c3 = dcmplx(dbl8, dbl9)
   	  call xmirror(trim(chr1), dbl1, dbl2, dbl3, c1, c2, c3, c4)
!			print *,'xmirror:',maxval(abs(grid))
   case ('tilt')
   	 icmd=icmd+1
     read(prop_cmd(icmd),fmt=*,end=40) dbl1, dbl2, dbl3, dbl4
     call tilt(dbl1, dbl2, dbl3, dbl4)
!     print *,'tilt:',maxval(abs(grid))
   case ('multiply')
   	 icmd=icmd+1
     read(prop_cmd(icmd),fmt=*,end=40) dbl1, dbl2
     call multiply(dbl1, dbl2)
!     print *,'multiply:',maxval(abs(grid))
   case ('spectral')
   	 icmd=icmd+1
     read(prop_cmd(icmd),fmt=*,end=40) dbl1
     call spectral(dbl1)
!     print *,'spectral:',maxval(abs(grid))
   case ('fresnel')
   	 icmd=icmd+1
     read(prop_cmd(icmd),fmt=*,end=40) dbl1
     call fresnel(dbl1)
!     print *,'fresnel:',maxval(abs(grid))
   case ('fresnelm')
   	 icmd=icmd+1
     read(prop_cmd(icmd),fmt=*,end=40) dbl1, dbl2, dbl3, dbl4, dbl5, dbl6, dbl7, dbl8, dbl9, dbl10
     call fresnelm(dbl1, dbl2, dbl3, dbl4, dbl5, dbl6, dbl7, dbl8, dbl9, dbl10)
!     print *,'fresnelm:',maxval(abs(grid))
   case ('fork')
   	 icmd=icmd+1
     read(prop_cmd(icmd),fmt=*,end=40) int1
     call fork(int1)
!     print *,'fork:',maxval(abs(grid))
   case ('end_fork')
   	 icmd=icmd+1
     read(prop_cmd(icmd),fmt=*,end=40) int1
     call end_fork(int1)
!     print *,'end_fork:',maxval(abs(grid))
   case ('xcross')
 	   i_xcross = i_xcross + 1
 	   res_xmesh(i_xcross) = mesh_x*Mxtot                       ! store local mesh size
 	   res_ymesh(i_xcross) = mesh_y*Mytot
 	   icmd=icmd+1
   	 read(prop_cmd(icmd),fmt=*, end=40) xcross_name(i_xcross), dbl1, xcross_fname(i_xcross)   	
   	 call xcross(is, res_xcross(:,:,i_xcross))
   case ('ycross')
 	   i_ycross = i_ycross + 1
 	   res_xmesh(nxcross+i_ycross) = mesh_x*Mxtot               ! store local mesh size
 	   res_ymesh(nxcross+i_ycross) = mesh_y*Mytot
 	   icmd=icmd+1
   	 read(prop_cmd(icmd),fmt=*, end=40) ycross_name(i_ycross), dbl1, ycross_fname(i_ycross)
   	 call ycross(is, res_ycross(:,:,i_ycross))
   case ('total')  
   	 i_tot = i_tot + 1
 	   res_xmesh(nxcross+nycross+i_tot) = mesh_x*Mxtot          ! store local mesh size
 	   res_ymesh(nxcross+nycross+i_tot) = mesh_y*Mytot
 	   icmd=icmd+1
   	 read(prop_cmd(icmd),fmt=*, end=40) total_name(i_tot), total_fname(i_tot)
   	 call total(is, res_total(:,i_tot))
   case ('stat')
   	 i_stat = i_stat + 1
 	   res_xmesh(nxcross+nycross+ntotal+i_stat) = mesh_x*Mxtot  ! store local mesh size
 	   res_ymesh(nxcross+nycross+ntotal+i_stat) = mesh_y*Mytot
 	   icmd=icmd+1
   	 read(prop_cmd(icmd),fmt=*, end=40) stat_name(i_stat), stat_fname(i_stat)
   	 call statss(is, res_stat_temp(1:6,1:nslices, i_stat))
   case ('dump')
   	 icmd=icmd+1
     read(prop_cmd(icmd),fmt=*,end=40) int1
     call dump(is, int1)
!     print *,'dump:',maxval(abs(grid))
   case ('waveguide')
   	 icmd=icmd+1
     read(prop_cmd(icmd),fmt=*,end=40) chr1, int1, int2, dbl1, dbl2, dbl3, dbl4, dbl5, dbl6
     call waveguide(is, chr1, int1, int2, dbl1, dbl2, dbl3, dbl4, dbl5, dbl6)
!     print *,'waveguide:',maxval(abs(grid))
   case ('fluence')
     i_fluence = i_fluence+1
     res_xmesh(nxcross+nycross+ntotal+nstat+i_fluence) = mesh_x*Mxtot  ! store local mesh size
 	   res_ymesh(nxcross+nycross+ntotal+nstat+i_fluence) = mesh_y*Mytot
 	   icmd=icmd+1
     read(prop_cmd(icmd),fmt=*, end=40) fluence_name(i_fluence), fluence_fname(i_fluence)
     call fluence(is,res_fluence(:,:,i_fluence))
   case ('spectrum')  
   	 i_spectrum = i_spectrum + 1
 	   res_xmesh(nxcross+nycross+ntotal+nstat+nfluence+i_spectrum) = mesh_x*Mxtot ! store local mesh size
 	   res_ymesh(nxcross+nycross+ntotal+nstat+nfluence+i_spectrum) = mesh_y*Mytot
 	   icmd=icmd+1
   	 read(prop_cmd(icmd),fmt=*, end=40) spectrum_name(i_spectrum), spectrum_fname(i_spectrum)
   	 call spectrum(is, res_spectrum(:,i_spectrum))
   case default
     print *,'OPC_optics.f90::do_slice -> ERROR: unknown command: ', command
     return
   end select
   icmd=icmd+1 ! process next command
 end do ! process commands
 
 ! Write output fieldfile
 call write_dfl(outfile, is, grid)
 if ( include_Ey )then
   filename = replace_string(outfile, '.dfl','_y.dfl')
   call write_dfl(filename, is, gridy)
 end if
 
 return
40 print *,'OPC_optics.f90::do_slice -> syntax error in script'
      
end ! subroutine slice



subroutine fork(id)
! ==================================================================
! Routine that copies the current grid to a buffer.
! This can be used to calculate two paths in one simulation.
! ------------------------------------------------------------------
 use opc_globals
 implicit none
 
 integer :: id, iy
      
 id=id ! No "variable unused" warning

 ! buffer = grid, use this hack to avoid segmentation fault when
 ! using ifort with large grid sizes
 do iy=1,Ny
   buffer(:,iy) = grid(:,iy)
 end do
 if (include_Ey) then
   do iy=1,Ny
     buffery(:,iy) = gridy(:,iy)
   end do
 end if
 Mxbuf = Mxtot  ! magnification is the same for both X- and Y-polarisation
 Mybuf = Mytot

end ! subrouting fork



subroutine end_fork(id)
! ==================================================================
! Routine that ends a fork. It restores the field from before the
! fork statement.
! ------------------------------------------------------------------

 use opc_globals
 use opc_lib
 implicit none

 integer :: id, iy
 
 id=id; ! No "variable unused" warning
 
 ! grid = buffer, use this hack to avoid segmentation fault when
 ! using ifort with large grid sizes
 do iy=1,Ny
   grid(:,iy) = buffer(:,iy)
 end do
 if (include_Ey) then
   do iy=1,Ny
     gridy(:,iy) = buffery(:,iy)
   end do
 end if

 ! re-calculate the axis
 Mxtot = Mxbuf
 Mytot = Mybuf
 call fillaxis()

end ! subrouting end_fork
      

      
subroutine dump(is, id)
! ==================================================================
! Routine that dumps the current grid to a file.
! ------------------------------------------------------------------

 use opc_globals
 use opc_interface
 use opc_lib
 implicit none
 
 integer, intent(IN) :: is, id
 character(len=100)  :: dumpfile
 character(len=2)    :: cid
      
! Write output fieldfile
 write(cid, '(I2.2)') id
 dumpfile = 'optics.dump' // cid //'.dfl'
! print *, 'dumpfile: ', dumpfile
 call write_dfl(dumpfile, is, grid)
 if (include_Ey) then
   dumpfile = 'optics.dump' // cid //'_y.dfl'
   call write_dfl(dumpfile, is, gridy)
 end if
!  
end ! subrouting dump
      
      
      
subroutine multiply(r, i)
! ================================================================== 
! Multiply the field with a constant
! used to represent losses on inperfect reflection
! ------------------------------------------------------------------ 

 use opc_globals
 implicit none
     
 real (kind=rk16), intent(IN) :: r, i
 complex (kind=rk16)          :: c
      
 c = dcmplx(r, i)
      
 grid = grid * c
 if (include_Ey) gridy = gridy * c

end ! subroutine multiply
      

      
subroutine diaphragm(xoff, yoff, r, inv, apodization, dr, nosc)
! ================================================================== 
! Set field outside diaphragm opening to zero 
! Forks automaticly by copying blocked field to buffer
! ------------------------------------------------------------------ 

 use opc_globals
 implicit none
      
 integer, intent(IN)           :: inv, nosc
 integer                       :: iy, ix, np 
 real (kind=rk16), intent(IN)  :: xoff, yoff, r, dr
 real (kind=rk16)              :: p2, rl2, ru2, rc2, angle, t, base, sigma, x, mesh
 character(len=10), intent(IN) :: apodization

 rc2 = zero ! avoid rc2 used unintialised warning

 mesh = MIN(mesh_x, mesh_y)
 if ( (apodization /= 'straight') .and. (dr <= 2*mesh) ) then
   print *,"OPC_optics.f90::diaphragm: warning, apodization amplitude is less or " &
     // "equal to twice the grid spacing (dr=",dr,")"
 end if  ! warning if too little grid points are present to sample
         ! the apodization function 
   
 do iy=1,Ny
   do ix=1, Nx
     p2 = (xaxis(ix) - xoff)**2 + (yaxis(iy) - yoff)**2
     if ( apodization == 'straight' ) then
       rc2=r*r
     else
       rl2 = (r-dr)**2 ! lower bound of apodization function
       ru2 = (r+dr)**2 ! upper bound of apodization function
       if ( p2 < rl2 ) then
         rc2=rl2
       else if ( p2 > ru2 ) then
         rc2=ru2
       else ! evaluation of apodization function required
         angle = atan2(yaxis(iy), xaxis(ix))
         select case (apodization)
         case ( 'sine' )
           rc2 = ( r + dr*sin( nosc*angle ) )**2
         case ( 'sine2' )
           rc2 = ( r + 2*dr*( sin( nosc*angle/2.0 )**2 - 0.5 ) )**2
         case ( 'bartlett' )
           t = 2*nosc*angle/Pi
           np = floor( (1 + t)/2 )
           rc2 = ( r + dr*(t - 2*np)*(-1)**np )**2
         case ( 'gaussian' )
           base = 2*Pi*r / nosc
           sigma = base / 4
           x = modulo(angle*r, base) - base/2 
           rc2 = ( r - 2.0*dr*(exp(-(x**2/(2*sigma**2)))- 0.5) )**2
         case default
           print *,"OPC_optics.f90::diaphragm: error, unkown apodization" &
             // " function in command 'diaphragm'"
           return
         end select
       end if
     end if
     if ( (inv == 1).neqv.(p2 > rc2) ) then ! NEQV is an exclusive OR
       buffer(ix, iy) = grid(ix, iy)
       grid(ix, iy) =  dcmplx(zero, zero)
     else
       buffer(ix, iy) = dcmplx(zero, zero)
     end if
     if (include_Ey) then
       if ( (inv == 1).neqv.(p2 > rc2) ) then ! NEQV is an exclusive OR
         buffery(ix, iy) = gridy(ix, iy)
         gridy(ix, iy) =  dcmplx(zero, zero)
       else
         buffery(ix, iy) = dcmplx(zero, zero)
       end if
     end if
   end do
 end do

 Mxbuf = Mxtot
 Mybuf = Mytot

end ! subroutine diaphragm

      

subroutine rectangle(xoff, yoff, xd, yd, inv)
! ================================================================== 
! Rectangular aperture
! Forks automaticly by copying blocked field to buffer
! ------------------------------------------------------------------

 use opc_globals
 implicit none
 
 integer, intent(IN)          :: inv     
 integer                      :: iy, ix
 real (kind=rk16), intent(IN) :: xoff, yoff, xd, yd
 real (kind=rk16)             :: xd2, yd2, xdt, ydt
 
 xdt = xd
 ydt = yd

 if (xd == zero) xdt = three*xaxis(Nx)
 if (yd == zero) ydt = three*yaxis(Ny)
 
 xd2 = half * xdt
 yd2 = half * ydt

 do iy=1,Ny
   do ix=1,Nx
     if ( (inv == 1) .neqv. &
        ( (abs(xaxis(ix)-xoff) > xd2) .or. (abs(yaxis(iy)-yoff) > yd2)) ) then
       buffer(ix, iy) = grid(ix, iy)
       grid(ix, iy) = dcmplx(zero, zero)
     else
       buffer(ix, iy) = dcmplx(zero,zero)
     end if
     if (include_Ey) then
       if ( (inv == 1) .neqv. &
          ( (abs(xaxis(ix)-xoff) > xd2) .or. (abs(yaxis(iy)-yoff) > yd2)) ) then
         buffery(ix, iy) = gridy(ix, iy)
         gridy(ix, iy) = dcmplx(zero, zero)
       else
         buffery(ix, iy) = dcmplx(zero,zero)
       end if
     end if
   end do ! ix
 end do ! iy

 Mxbuf = Mxtot
 Mybuf = Mytot
      
end ! subroutine square



subroutine opld(xoff, yoff)
! ==================================================================
! When is=1, reads an external file with optical path length 
! differences for each grid point. It then applies the optical path
! length difference to each gridpoint (is>=1)
!
! xoff   = offset in x-direction of the opld-grid
! yoff   = offset in y-direction of the opld-grid
!
! opld is made zero for the region where the opld-grid does not
! overlap the grid
! ------------------------------------------------------------------

 use opc_globals
 use opc_interface
 implicit none  
  
 real (kind=rk16), intent(IN) :: xoff, yoff
 integer                      :: ix, iy, ixoff, iyoff, ixbegin, ixend, iybegin, iyend
 integer                      :: ngrid_cur, igrid
 real (kind=rk16)             :: angle
 complex (kind=rk16)          :: ztemp
 
 ngrid_cur=0
 if (associated(ogrid_name)) then
   ! find the grid to apply
   do igrid=1,ubound(ogrid_name,1)
     if ( ogrid_name(igrid) == opldfile ) ngrid_cur=igrid
   end do
 end if
 if ( .NOT.(associated(ogrid_name)) .OR. (ngrid_cur == 0) ) then
   if ( associated(ogrid_name) ) ngrid_cur=nogrid
   call add_mask(ngrid_cur, opldfile)    ! ngrid_cur is updates in subroutine add_mask
 end if 
 
 ixoff = floor(xoff/mesh_x + 0.5)
 iyoff = floor(yoff/mesh_y + 0.5)
 if (ixoff < 0) then ! adjust counter for xoffset
   ixbegin = 1
   ixend = Nx + ixoff
 else
   ixbegin = abs(ixoff) + 1
   ixend = Nx
 end if
 if (iyoff < 0) then ! adjust counter for yoffset
   iybegin = 1
   iyend = Ny + iyoff
 else
   iybegin = abs(iyoff) + 1
   iyend = Ny
 end if
 do iy=iybegin,iyend
   do ix=ixbegin,ixend
     angle = k_wave * ogrid(ngrid_cur) % r(ix-ixoff, iy-iyoff)
     ztemp = dcmplx(cos(angle), sin(angle))
     grid(ix,iy) = grid(ix,iy) * ztemp
     if (include_Ey) gridy(ix,iy) = gridy(ix,iy) * ztemp
   end do ! ix
 end do ! iy

end ! subroutine opld



subroutine add_mask(ngrid, filename)
! ================================================================== 
! Adds a mask to memory. Memory is dynamically extended.
! number of masks (nogrid) is updated at end of routine
! ------------------------------------------------------------------

 use opc_globals
 use opc_interface
 use opc_lib
#ifdef MPI
 use mpi
#endif
 implicit none ! be strict

 integer, intent(INOUT)                      :: ngrid
 character(len=100), intent(IN)              :: filename
 integer                                     :: ierr, length, tNx, tNy, igrid, iy
 type(opld_grid), dimension(:), pointer      :: togrid      ! temporary memory for mask data
 character(len=100), dimension(:), pointer   :: togrid_name ! temporary memory for mask names
 real(kind=rk16), dimension(:,:),allocatable :: dogrid      ! temporary memory for MPI data transfer
#ifdef MPI
 integer                                     :: mpi_err
#endif
 
 ! update data
 if (ngrid == 0) then ! first time a mask is requested, allocate memory
   allocate(ogrid(1), stat=ierr)           ! mask data
   if (ierr /=0) call opc_abort("Opc_optics.f90::add_mask", "Not enough memory to allocate ogrid")
   allocate(ogrid_name(1), stat=ierr)      ! mask name
   if (ierr /=0) call opc_abort("Opc_optics.f90::add_mask", "Not enough memory to allocate ogrid_name")
 else
   ! several masks are in use, new one is requested
   ! allocate temporary memory, copy data to temporary memory, release memory
   ! memory for ogrid and allocate increased memory to contain all masks
   ! copy old data back to new memory and release temporary memory
   allocate(togrid(ngrid+1), Stat=ierr)                 ! allocate new memory for mask data
   if (ierr /=0) call opc_abort("Opc_optics.f90::add_mask", "Not enough memory to allocate togrid")
   do igrid=1,ngrid
     allocate(togrid(igrid) % r(Nx,Ny), Stat=ierr)      ! second step for memory allocation 
     if (ierr /=0) call opc_abort("Opc_optics.f90::add_mask", "Not enough memory to allocate togrid")
     do iy=1,Ny
       togrid(igrid) % r(:,iy) = ogrid(igrid) % r(:,iy) ! copy mask data to new memory
     end do
     deallocate(ogrid(igrid) % r, Stat=ierr)            ! free old memory after being copied
     if (ierr /=0) call opc_abort("Opc_optics.f90::add_mask", "Error when deallocating memory associated to ogrid")
   end do
   deallocate(ogrid, Stat=ierr)                         ! second step of freeing old memory
   if (ierr /=0) call opc_abort("Opc_optics.f90::add_mask", "Error when deallocating memory associated to ogrid")
   ogrid => togrid                                      ! associate ogrid with new enlarged memory
   nullify(togrid)                                      ! remove association togrid with enlarged memory

   ! now do the names
   allocate(togrid_name(ngrid+1), Stat=ierr)         ! new memory for mask names
   if (ierr /=0) call opc_abort("Opc_optics.f90::opld", "Not enough memory to allocate togrid_name")
   togrid_name=''    
   togrid_name(1:ngrid) = ogrid_name(1:ngrid)        ! copy mask names to new memory
   deallocate(ogrid_name, Stat=ierr)                 ! release old memory for mask names
   if (ierr /=0) call opc_abort("Opc_optics.f90::opld", "Error when deallocating memory associated to ogrid_name")
   ogrid_name => togrid_name                         ! associate ogrid_name with new memory
   nullify(togrid_name)                              ! disassociate togrid_name with new memory
 end if 

 ! old masks and names have been copied back, now read in new mask and distribute
 ngrid = ngrid+1
 allocate(ogrid(ngrid) % r(Nx,Ny), stat=ierr)           ! allocate space for new mask
 ogrid(ngrid) %r = zero
 if (ierr /=0) call opc_abort("Opc_optics.f90::add_mask", "Not enough memory to allocate ogrid")
 ogrid(ngrid) % r = zero
 call read_opld(filename, ngrid, ogrid) ! read data for new mask
 ogrid_name(ngrid) = filename
 nogrid = ngrid
 	
end ! subroutine add_mask
 
 
 	
subroutine lens(xoff, yoff, f)
! ================================================================== 
! Simulate a lens with focussing distance f
! ------------------------------------------------------------------

 use opc_globals
 implicit none

 real (kind=rk16), INTENT(IN) :: xoff, yoff, f
 integer                      :: ix, iy
 real (kind=rk16)             :: const1, angle
 complex (kind=rk16)          :: ztemp
      
 const1 = -half * (k_wave/f)

 do iy=1,Ny
   do ix=1,Nx
      angle= const1 * ( (xaxis(ix)-xoff)**2 + (yaxis(iy)-yoff)**2 )
      ztemp = dcmplx(cos(angle), sin(angle))
      grid(ix,iy) = grid(ix,iy) * ztemp
      if (include_Ey) gridy(ix,iy) = gridy(ix,iy) * ztemp
   end do
 end do
     
end ! subroutine lens



subroutine tilt(xoff, yoff, xr, yr)
! ================================================================== 
! Simulate a tilt in the beam angles are given in radians.
! ------------------------------------------------------------------

 use opc_globals
 implicit none

 real (kind=rk16), INTENT(IN) :: xoff, yoff, xr, yr
 integer                      :: ix, iy
 real (kind=rk16)             :: angle, xrt, yrt
 complex (kind=rk16)          :: ztemp

 xrt = 1D-3 * xr ! milliradian => radian
 yrt = 1D-3 * yr

 do iy=1,Ny
   do ix=1,Nx
     angle = k_wave * ( xrt*(xaxis(ix)-xoff) + yrt*(yaxis(iy)-yoff) )
     ztemp = dcmplx(cos(angle), -sin(angle))
     grid(ix, iy) = grid(ix, iy) * ztemp
     if (include_Ey) gridy(ix,iy) = gridy(ix,iy) * ztemp
   end do
 end do

end ! subroutine tilt



subroutine waveguide(is, wgtype, Nm, Mm, r, dx, dy, xoffset, yoffset, L)
! ================================================================== 
! Decompose the total field into empty waveguide modes, propagate the
! modes over a length L and generates the field file at the other side
! of the waveguide
! wgtype  = 'cyl' or 'rect' for cylindrical and rectangular waveguides
! r       = waveguide inner radius (type = 'cyl')
! dx (dy) = width (height) of waveguide (type - 'rect')
! xoffset = offset waveguide in x-direction
! yoffset = offset waveguide in y-direction
! L       = length of waveguide
! ------------------------------------------------------------------
 use opc_globals
 use opc_lib
 use opc_math
#ifdef MPI
 use mpi
#endif

 implicit none
 real (kind=rk16), intent(IN)   :: r, dx, dy, xoffset, yoffset, L
 integer, intent(IN)            :: is, Nm, Mm            ! Nm and Mm are the maximum mode numbers
 character (len=100), INTENT (IN) :: wgtype
 integer                        :: ierr
 integer                        :: im, in                ! counters for modes
 real (kind=rk16), allocatable  :: rootsBJP(:,:)         ! zeros of dJn/dx
 real (kind=rk16), allocatable  :: coeff(:,:)            ! some mode dependent coefficients, calculate once
 real (kind=rk16), allocatable  :: kappa(:,:)            ! transverse wave numbers of the modes
 complex (kind=rk16), allocatable :: Bnm (:,:), Cnm(:,:) ! Complex mode amplitudes one for each polarisation direction
 complex (kind=rk16)            :: Ex1, Ex2, Ey1, Ey2    ! Calculated fields at x,y for mode in, im
 real (kind=rk16), allocatable  :: roots(:), zerror(:) 
 real (kind=rk16)               :: k0, beta, k0Z0, k0Z02, area, const, fin, power
 integer                        :: i, j, imin, imax, jmin, jmax ! counters and boundary for grid
 integer                        :: it, jt, im2, jm2
 logical                        :: exists                ! variable used to check if file exists.
  
 ! find total power in input field
 power = zero
 do i = 1,Nx ! Ex field
   do j = 1,Ny
     power = power + abs(grid(i,j))**2
   end do
 end do
 if (include_Ey) then
   do i = 1,Nx
     do j = 1,Ny
       power = power + abs(gridy(i,j))**2
     end do
   end do
 end if ! include Ey field
 k0 = twopi/lambda
 k0Z0 = k0*VacImp
 k0Z02 = k0Z0**2
 if (init_waveguide) then
   if (trim(wgtype) == 'cyl') then
     allocate(rootsBJP(0:Nm,Mm), stat=ierr)   ! Index in runs from 0 to Nm, index im from 1 to Mm
     if (ierr /=0) call opc_abort("opc_optics.f90::waveguide", "Not enough memory to allocate rootsBJP")
     allocate(coeff(0:Nm,Mm), stat=ierr)
     if (ierr /=0) call opc_abort("opc_optics.f90::waveguide", "Not enough memory to allocate coeff")
     allocate(kappa(0:Nm,Mm), stat=ierr)
     if (ierr /=0) call opc_abort("opc_optics.f90::waveguide", "Not enough memory to allocate kappa")
     allocate(Bnm(0:Nm,Mm), stat=ierr)
     if (ierr /=0) call opc_abort("opc_optics.f90::waveguide", "Not enough memory to allocate Bnm")
     allocate(Cnm(0:Nm,Mm), stat=ierr)
     if (ierr /=0) call opc_abort("opc_optics.f90::waveguide", "Not enough memory to allocate Cnm")
     allocate(roots(Mm), stat=ierr)
     if (ierr /=0) call opc_abort("opc_optics.f90::waveguide", "Not enough memory to allocate roots")
     allocate(zerror(Mm), stat=ierr)
     if (ierr /=0) call opc_abort("opc_optics.f90::waveguide", "Not enough memory to allocate zerror")  
     do in=0,Nm
       fin=dfloat(in)
       call rootJP(in,Mm,roots,zerror)
       rootsBJP(in,1:Mm) = roots(1:Mm) ! zeros of dJn(x)/dx
       do im=1,Mm
         kappa(in,im) = rootsBJP(in,im)/r   ! transverse wavenumbers
         coeff(in,im) = TwoPi*k0Z02*(1-(fin/rootsBJP(in,im))**2)
         coeff(in,im) = coeff(in,im)*(bessel_JN(in,rootsBJP(in,im))**2)*half*rootsBJP(in,im)**2
         if (in > 0) coeff(in,im) = half*coeff(in,im)
       end do ! loop im
     end do ! find roots of dJn/dx and calculate coefficients once 
   end if ! waveguide is cylindrical
 end if ! initialise various variables only once    
 ! Ex is stored in grid, Ey is stored in gridt
 ! calculate the mode coefficients
 !   field should be zero outside the aperture give by the waveguide. We therefore do not need
 !   to integrate over the whole grid, only the part that contains the waveguide.
 !   imin, imax bounds the domain in x-direction, likewise jmin and jmax for y-direction
 imin = 1
 imax = Nx
 jmin = 1
 jmax = Ny
 do i = 1, Nx  ! range for x-integration
   if ( (xaxis(i) - xoffset + r) < 0) imin=i
   if ( (xaxis(Nx-i+1) - xoffset - r) > 0) imax=Nx-i+1
 end do
 do j = 1, Ny  ! range for y-integration
   if ( (yaxis(j) - yoffset + r) < 0) jmin=j
   if ( (yaxis(Nx-j+1) - yoffset - r) > 0) jmax=Ny-j+1
 end do
 ! make sure there are an odd number of grid points in each direction
 if (2*((imax-imin)/2) /= (imax-imin)) imax=imax+1  ! use integer division n/2 to see if difference is even or odd
 if (2*((jmax-jmin)/2) /= (jmax-jmin)) jmax=jmax+1
 
 ! apply Simpsons' rule for 2D integration
 im2 = (imax-imin)/2
 jm2 = (jmax-jmin)/2
 area = mesh_x*mesh_y       ! field in dfl file is stored as amplitude times sqrt(area)
 const = sqrt(two*area/(cl*eps0))/nine ! constant used in integration to get mode coefficients
                            ! values in field file squared and summed over grid gives power in slice    
                            ! complex amplitude in dfl file = sqrt(c*eps0/2)sqrt(area) times
                            ! field amplitude. A factor area/nine is from 2D Simpson's integration rule.
  
 ! Simpson's rule requires multiplying with area, as the values from the dfl file already
 ! contains a factor of sqrt(area), we only need to multiply by sqrt(area). This is taken
 ! into account in the factor const, defined above.
 
 ! open files for output to PERL script
 inquire(file='opc_waveguide_energy.txt',EXIST=exists)
 if (exists) then
   open(FHOE,file='opc_waveguide_energy.txt',position='APPEND', status='OLD')
 else 
   open(FHOE,file='opc_waveguide_energy.txt',status='NEW')
 end if
 inquire(file='opc_waveguide_modes.txt',EXIST=exists)
 if (exists) then
   open(FHOW,file='opc_waveguide_modes.txt',position='APPEND', status='OLD')
 else 
   open(FHOW,file='opc_waveguide_modes.txt',status='NEW')
 end if
 
 write(FHOW,'(2(I4,X))') Nm,Mm    ! maximum mode numbers
 write(FHOE,'(2(I4,X))') Nm,Mm
 
 do in=0,Nm
   do im=1,Mm
     call Enm(in, im, xaxis(imin), yaxis(jmin), kappa(in,im), Ex1, Ey1, Ex2, Ey2)
     Bnm(in,im) = grid(imin,jmin)*conjg(Ex1) 
     if (include_Ey)  Bnm(in,im) = Bnm(in,im) + gridy(imin,jmin)*conjg(Ey1)
     call Enm(in, im, xaxis(imin), yaxis(jmax), kappa(in,im), Ex1, Ey1, Ex2, Ey2)
     Bnm(in,im) = Bnm(in,im) + grid(imin,jmax)*conjg(Ex1)
     if (include_Ey)  Bnm(in,im) = Bnm(in,im) + gridy(imin,jmax)*conjg(Ey1)
     call Enm(in, im, xaxis(imax), yaxis(jmin), kappa(in,im), Ex1, Ey1, Ex2, Ey2)
     Bnm(in,im) = Bnm(in,im) + grid(imax,jmin)*conjg(Ex1)
     if (include_Ey)  Bnm(in,im) = Bnm(in,im) + gridy(imax,jmin)*conjg(Ey1)
     call Enm(in, im, xaxis(imin), yaxis(jmax), kappa(in,im), Ex1, Ey1, Ex2, Ey2)
     Bnm(in,im) = Bnm(in,im) + grid(imax,jmax)*conjg(Ex1)
     call Enm(in, im, xaxis(imin), yaxis(jmax), kappa(in,im), Ex1, Ey1, Ex2, Ey2)
     do j=1,jm2
       jt=jmin+2*j-1
       call Enm(in, im, xaxis(imin), yaxis(jt), kappa(in,im), Ex1, Ey1, Ex2, Ey2)
       Bnm(in,im) = Bnm(in,im) + four*grid(imin,jt)*conjg(Ex1)
       if (include_Ey)  Bnm(in,im) = Bnm(in,im) + four*gridy(imin,jt)*conjg(Ey1)
       call Enm(in, im, xaxis(imax), yaxis(jt), kappa(in,im), Ex1, Ey1, Ex2, Ey2)
       Bnm(in,im) = Bnm(in,im) + four*grid(imax,jt)*conjg(Ex1)
       if (include_Ey)  Bnm(in,im) = Bnm(in,im) + four*gridy(imax,jt)*conjg(Ey1)
     end do
     do j=1,jm2-1
       jt = jmin+2*j
       call Enm(in, im, xaxis(imin), yaxis(jt), kappa(in,im), Ex1, Ey1, Ex2, Ey2)
       Bnm(in,im) = Bnm(in,im) + two*grid(imin,jt)*conjg(Ex1)
       if (include_Ey)  Bnm(in,im) = Bnm(in,im) + two*gridy(imin,jt)*conjg(Ey1)
       call Enm(in, im, xaxis(imax), yaxis(jt), kappa(in,im), Ex1, Ey1, Ex2, Ey2)
       Bnm(in,im) = Bnm(in,im) + two*grid(imax,jt)*conjg(Ex1)
       if (include_Ey)  Bnm(in,im) = Bnm(in,im) + two*gridy(imax,jt)*conjg(Ey1)
     end do
     do i=1,im2
       it = imin+2*i-1
       call Enm(in, im, xaxis(it), yaxis(jmin), kappa(in,im), Ex1, Ey1, Ex2, Ey2)
       Bnm(in,im) = Bnm(in,im) + four*grid(it-1,jmin)*conjg(Ex1)
       if (include_Ey)  Bnm(in,im) = Bnm(in,im) + four*gridy(it,jmin)*conjg(Ey1)
       call Enm(in, im, xaxis(it), yaxis(jmax), kappa(in,im), Ex1, Ey1, Ex2, Ey2)
       Bnm(in,im) = Bnm(in,im) + four*grid(it-1,jmax)*conjg(Ex1)
       if (include_Ey)  Bnm(in,im) = Bnm(in,im) + four*gridy(it,jmax)*conjg(Ey1)
     end do
     do i=1,im2-1
       it=imin+2*i
       call Enm(in, im, xaxis(it), yaxis(jmin), kappa(in,im), Ex1, Ey1, Ex2, Ey2)
       Bnm(in,im) = Bnm(in,im) + two*grid(it,jmin)*conjg(Ex1)
       if (include_Ey)  Bnm(in,im) = Bnm(in,im) + two*gridy(it,jmin)*conjg(Ey1)
       call Enm(in, im, xaxis(it), yaxis(jmax), kappa(in,im), Ex1, Ey1, Ex2, Ey2)
       Bnm(in,im) = Bnm(in,im) + two*grid(it,jmax)*conjg(Ex1)
       if (include_Ey)  Bnm(in,im) = Bnm(in,im) + two*gridy(it,jmax)*conjg(Ey1)
     end do 
     do j=1,jm2
       jt=jmin+2*j-1
       do i=1,im2
          it=imin+2*i-1
          call Enm(in, im, xaxis(it), yaxis(jt), kappa(in,im), Ex1, Ey1, Ex2, Ey2)
          Bnm(in,im) = Bnm(in,im) + sixteen*grid(it,jt)*conjg(Ex1)
          if (include_Ey)  Bnm(in,im) = Bnm(in,im) + sixteen*gridy(it,jt)*conjg(Ey1)
       end do
     end do
     do j=1,jm2-1
       jt=jmin+2*j
       do i=1,im2
          it=imin+2*i-1
          call Enm(in, im, xaxis(it), yaxis(jt), kappa(in,im), Ex1, Ey1, Ex2, Ey2)
          Bnm(in,im) = Bnm(in,im) + eight*grid(it,jt)*conjg(Ex1)
          if (include_Ey)  Bnm(in,im) = Bnm(in,im) + eight*gridy(it,jt)*conjg(Ey1)
       end do
     end do
     do j=1,jm2
       jt=jmin+2*j-1
       do i=1,im2-1
          it=imin+2*i
          call Enm(in, im, xaxis(it), yaxis(jt), kappa(in,im), Ex1, Ey1, Ex2, Ey2)
          Bnm(in,im) = Bnm(in,im) + eight*grid(it,jt)*conjg(Ex1)
          if (include_Ey)  Bnm(in,im) = Bnm(in,im) + eight*gridy(it,jt)*conjg(Ey1)
         end do
     end do
     do j=1,jm2-1
       jt=jmin+2*j-1
       do i=1,im2
          it=imin+2*i
          call Enm(in, im, xaxis(it), yaxis(jt), kappa(in,im), Ex1, Ey1, Ex2, Ey2)
          Bnm(in,im) = Bnm(in,im) + four*grid(it,jt)*conjg(Ex1)
          if (include_Ey)  Bnm(in,im) = Bnm(in,im) + four*gridy(it,jt)*conjg(Ey1)
       end do
     end do
! integration over grid done, apply appropriate constants
     Bnm(in,im) = const*Bnm(in,im)/coeff(in,im)
     power = TE_Power(in,im,Bnm(in,im),rootsBJP(in,im),r)
     write(FHOE,'(2(I4,X),E15.8)') in,im,power
     write(FHOW,'(2(I4,X),2(E15.8,1X))') in,im,REAL(Bnm(in,im)),AIMAG(Bnm(in,im))
! now propagate mode to other side of waveguide
     if (kappa(in,im)**2 > k0**2) then
! evanescent mode
       beta=sqrt(kappa(in,im)**2 - k0**2)
       Bnm(in,im)=Bnm(in,im)*exp(-beta*L)  ! attenuation of mode
     else 
! propagating mode
       beta=sqrt(k0**2 - kappa(in,im)**2)
       Bnm(in,im)=Bnm(in,im)*exp(cmplx(zero,beta*L)) ! propagation of mode
     end if ! propagating or evanescent mode
   end do
 end do
! close files
 close(FHOW)
 close(FHOE) 
 ! now upgrade the grid to write the data to FILE
 grid = cmplx(zero,zero)
 gridy = cmplx(zero,zero)
 const = sqrt(area*cl*eps0/two)
 include_Ey = .TRUE.
 do i = imin, imax
   do j = jmin, jmax
     if ( (xaxis(i)**2 + yaxis(j)**2) > r**2 ) cycle
     do in=0,Nm
       do im=1,Mm
          call Enm(in, im, xaxis(i), yaxis(j), kappa(in,im), Ex1, Ey1, Ex2, Ey2)
          Ex1 = const*Bnm(in,im)*Ex1
          Ey1 = const*Bnm(in,im)*Ey1
          grid(i,j)  = grid(i,j)  + Ex1
          gridy(i,j) = gridy(i,j) + Ey1
       end do ! loop over mode im
     end do !loop over mode in
   end do ! loop over y-coordinates
 end do  ! loop over x-coordinates
 return	
end subroutine waveguide



subroutine xmirror(rt_mode, d, Theta, E_H, chi00, chi0H, chiH0, sqrt0HH0)
! ==================================================================
! Calculate reflection (rt_mode='r') of or transmission (rt_mode='t') 
! through a flat x-ray Bragg miror.
! d        = thickness of crystal
! Theta    = Angle of incidence with respect to normal of  
!            crystal planes in [deg]. We assume crystal planes are 
!            parallel to surface of mirror
! E_B      = Design energy for symmetric Bragg reflection [keV]
! E_H      = h*c/2d_h [keV], energy corresponding to separation 
!            crystal planes
! chiXX    = Fourier compenents 00(0), OH (+1) and H0 (-1) of 
!            susceptibility (calculated externally)
!
! This subroutine assumes progation in the frequency domain
!
! see Phys. Rev. ST Accel. Beams 010701 (2011)
! ------------------------------------------------------------------
 use opc_globals
 use opc_lib
 use fftw3
#ifdef MPI
 use mpi
#endif
 implicit none
 
 real (kind=rk16), INTENT(IN)                 :: d, E_H, Theta
 complex (kind=rk16), INTENT(IN)              :: chi00, chi0H, chiH0, sqrt0HH0
 character, INTENT(IN)                        :: rt_mode 
! local variables
 complex (kind=rk16)                          :: rt, R1, R2, chi1, chi2, temp1, temp2, temp3 
 real (kind=rk16)                             :: Lx, Ly, k0x, dkx, dky, kx2, ky2, kp2
 real (kind=rk16)                             :: cs, E, EH, rk, alpha
 integer                                      :: ix, iy, Nx2, Ny2, nzx, nzy
#ifdef MPI
 integer                                      :: mpi_err
#endif
 
 ! spatial Fourier transformation
 call fftw_execute_dft ( plan_forward_spectral_x, grid, grid ) 
 if (include_Ey) call fftw_execute_dft ( plan_forward_spectral_y, gridy, gridy) 
 call shift_fft(nzx,nzy) ! centers zero spatial frequency for both Ex and Ey
  
 ! action of the mirror, we now have plane waves propagating at angles defined
 ! by the transverse k-vectors
 Lx=(Nx-1)*(Mxtot*mesh_x) ! total grid size in x-direction
 Ly=(Ny-1)*(Mytot*mesh_y) ! total grid size in y-direction
 
 ! determine transverse component of wave vector incident at the specified angle of incidence.
 ! we assume that this transverse component is alligned with the x-axis of the grid
 k0x = k_wave * sin(Theta*Pi/180.0d0)
 dkx = TwoPi/Lx
 dky = TwoPi/Ly    
 
 ! calculate ratio of E/EH
 E  = h_planck*cl*k_wave/TwoPi   ! physical & numeric constants defined in opc_globals.f90
 rk = E_H * keV / E
 do iy=1, Ny
 	 ky2 = ( (iy-nzy)*dky )**2
 	 do ix=1, Nx
 	 	 kx2 = ( k0x + (ix-nzx)*dkx )**2
 	 	 kp2 = kx2 + ky2
 	 	 cs = cos(atan2(sqrt(kp2), sqrt(k_wave**2-kp2)))
 	 	 alpha = four*rk*(rk - cs) 
 	 	 temp1 = (alpha - two*chi00)
 	 	 temp2 = sqrt(temp1**2 - four*chi0H*chiH0)
 	 	 if ( aimag(temp2) < 0) temp2 = - temp2
 	 	 temp3 = half*k_wave / cs
 	 	 R1 = half*(temp1 + temp2)/chi0H
 	 	 R2 = half*(temp1 - temp2)/chi0H
 	 	 chi1 = temp3 * (chi0H*R1 + chi00)
 	 	 chi2 = temp3 * (chi0H*R2 + chi00)	 	 
 	 	 temp1 = exp(dcmplx(zero, d)*(chi1-chi2))
 	 	 temp2 = R2 - R1*temp1
 	 	 if (rt_mode == 'r') then
  	   rt = R1*R2 * (one-temp1) / temp2
 	 	 else
 	 	   rt = exp(dcmplx(zero,d)*chi1) * (R2-R1) / temp2
 	   end if
 	   grid(ix,iy) = rt * grid(ix,iy)
 	   if (include_Ey) gridy(ix,iy) = rt * gridy(ix,iy)
 	 end do
 end do
 
 ! inverse Fourier transform  
 call inv_shift_fft(nzx,nzy)
 call fftw_execute_dft ( plan_backward_spectral_x, grid, grid )
 grid = grid / dble (Nx*Ny)
 if (include_Ey) then
 	 call fftw_execute_dft ( plan_backward_spectral_y, gridy, gridy )
   gridy = gridy / dble (Nx*Ny)
 end if
 
end ! subroutine xmirror



subroutine spectral(z)
! ================================================================== 
! Propagate the field over a distance z by spectral transformation
! ------------------------------------------------------------------

 use opc_globals
 use opc_lib
 use fftw3
#ifdef MPI
 use mpi
#endif
 implicit none

 real (kind=rk16), INTENT(IN)                :: z
 integer                                     :: iy, ix, ii, nzx, nzy, ierr
 real (kind=rk16)                            :: angle, Lx, Ly
 complex (kind=rk16)                         :: ztemp
 real (kind=rk16), dimension(:), allocatable :: theta_x, theta_y
#ifdef MPI
 integer                                     :: mpi_err
#endif

! Fourier transformation
 call fftw_execute_dft ( plan_forward_spectral_x, grid, grid )
 if (include_Ey) call fftw_execute_dft ( plan_forward_spectral_y, gridy, gridy)
 call shift_fft(nzx,nzy) ! shifts zero spatial frequency to center of grid for even of odd grid sizes (Nx, Ny)

! Propagation in k-space
 Lx=(Nx-1)*(Mxtot*mesh_x) ! total grid size in x-direction
 Ly=(Ny-1)*(Mytot*mesh_y) ! total grid size in y-direction
 allocate(theta_x(Nx), Stat=ierr)
 if (ierr /= 0) call opc_abort ("Opc_optics.f90::spectral", "Error, not enough memory to allocate theta_x")
 allocate(theta_y(Ny), Stat=ierr)
 if (ierr /= 0) call opc_abort ("Opc_optics.f90::spectral", "Error, not enough memory to allocate theta_y")
 do ii=1, Nx
   theta_x(ii) = (float(ii)-nzx)*TwoPi/Lx   
 end do
 do ii=1, Ny  
   theta_y(ii) = (float(ii)-nzy)*TwoPi/Ly
 end do
      
 do iy=1,Ny
   do ix=1,Nx
     angle = (theta_y(iy)**2 + theta_x(ix)**2) * (half * z/k_wave)
     ztemp = dcmplx( cos(angle), -sin(angle) )
     grid(ix,iy) = grid(ix,iy) * ztemp
     if (include_Ey) gridy(ix,iy) = gridy(ix,iy) * ztemp
   end do
 end do
 
! Inverse Fourier transform
 call inv_shift_fft(nzx,nzy) ! put spatial frequencies in right order for inverse fft
 call fftw_execute_dft ( plan_backward_spectral_x, grid, grid )
 grid = grid / dble (Nx*Ny)
 if (include_Ey) then
   call fftw_execute_dft ( plan_backward_spectral_y, gridy, gridy )
   gridy = gridy / dble (Nx*Ny)
 end if
  
end ! subroutine spectral



subroutine fresnel(z)
! ================================================================== 
! Propagate the field over a distance z by fresnel integration
! Integrates X and Y dimensions independently
! ------------------------------------------------------------------

 use opc_globals
 use fftw3
#ifdef MPI
 use mpi
#endif
 implicit none

 real (kind=rk16), INTENT(IN)     :: z
 integer                          :: iy, ix, ierr, N
 real (kind=rk16)                 :: alpha
#ifdef MPI
 integer                          :: mpi_err
#endif

 if (.NOT. c_associated( plan_forward_fresnel_ux )) then
  ! allocate memory for the arrays needed the calculate the convolution integral
   p_fresnel_ux = fftw_alloc_complex(int(2*Nx, C_SIZE_T))
   call c_f_pointer(p_fresnel_ux, fresnel_ux, [2*Nx])        
   fresnel_ux(1:2*Nx) = dcmplx(zero, zero)
   
   p_fresnel_uy = fftw_alloc_complex(int(2*Ny, C_SIZE_T))
   call c_f_pointer(p_fresnel_uy, fresnel_uy, 2*[Ny])        
   fresnel_uy(1:2*Ny) = dcmplx(zero, zero)
   
   p_fresnel_vx = fftw_alloc_complex(int(2*Nx, C_SIZE_T))
   call c_f_pointer(p_fresnel_vx, fresnel_vx, [2*Nx])
   fresnel_vx(1:2*Nx) = dcmplx(zero, zero)
   
   p_fresnel_vy = fftw_alloc_complex(int(2*Ny, C_SIZE_T))
   call c_f_pointer(p_fresnel_vy, fresnel_vy, [2*Ny])
   fresnel_vy(1:2*Ny) = dcmplx(zero, zero)   
   
   N = MAX(Nx, Ny)
   p_fresnel_w = fftw_alloc_complex(int(2*N, C_SIZE_T))
   call c_f_pointer(p_fresnel_w, fresnel_w, [2*N])
   fresnel_w(1:2*N) = dcmplx(zero, zero)
 end if

 alpha = (Mytot * mesh_y) * dsqrt(k_wave/Pi/z)

 do ix=1, Nx
   fresnel_uy(1:Ny) = grid(ix,:)
   call fresnel1d(alpha, Ny, 'y')
   grid(ix,:) = fresnel_uy(1:Ny)
 end do
 if (include_Ey) then
   do ix=1, Nx
     fresnel_uy(1:Ny) = gridy(ix,:)
     call fresnel1d(alpha, Ny, 'y')
     gridy(ix,:) = fresnel_uy(1:Ny)
   end do
 end if
 
 alpha = (Mxtot * mesh_x) * dsqrt(k_wave/Pi/z)
      
 do iy=1, Ny
   fresnel_ux(1:Nx) = grid(:,iy)
   call fresnel1d(alpha, Nx, 'x')
   grid(:,iy) = fresnel_ux(1:Nx)
 end do
 if (include_Ey) then
   do iy=1, Ny
     fresnel_ux(1:Nx) = gridy(:,iy)
     call fresnel1d(alpha, Nx, 'x')
     gridy(:,iy) = fresnel_ux(1:Nx)
   end do
 end if

end ! subroutine fresnel



subroutine fresnelm(Ax,Bx,Cx,Dx, Ay,By,Cy,Dy, MMx,MMy)
! ================================================================== 
! Propagate the field over a distance z by fresnel integration
! Using an ABCD matrix to simulate lenses etc.
! Integrates X and Y dimensions independently
! ------------------------------------------------------------------

 use opc_globals
 use opc_lib
 use fftw3
#ifdef MPI
 use mpi
#endif
 implicit none

 real (kind=rk16), INTENT(IN)     :: Ax,Bx,Cx,Dx, Ay,By,Cy,Dy, MMx,MMy
 real (kind=rk16)                 :: alpha, q, Cxt, Cyt, const1, const2
 real (kind=rk16), allocatable    :: xaxisz(:), yaxisz(:)
 integer                          :: iy, ix, ierr, N
#ifdef MPI
 integer                          :: mpi_err
#endif

 if (.NOT. c_associated( plan_forward_fresnel_ux )) then
  ! allocate memory for the arrays needed the calculate the convolution integral
   p_fresnel_ux = fftw_alloc_complex(int(2*Nx, C_SIZE_T))
   call c_f_pointer(p_fresnel_ux, fresnel_ux, [2*Nx])        
   fresnel_ux(1:2*Nx) = dcmplx(zero, zero)
   
   p_fresnel_uy = fftw_alloc_complex(int(2*Ny, C_SIZE_T))
   call c_f_pointer(p_fresnel_uy, fresnel_uy, 2*[Ny])        
   fresnel_uy(1:2*Ny) = dcmplx(zero, zero)
   
   p_fresnel_vx = fftw_alloc_complex(int(2*Nx, C_SIZE_T))
   call c_f_pointer(p_fresnel_vx, fresnel_vx, [2*Nx])
   fresnel_vx(1:2*Nx) = dcmplx(zero, zero)
   
   p_fresnel_vy = fftw_alloc_complex(int(2*Ny, C_SIZE_T))
   call c_f_pointer(p_fresnel_vy, fresnel_vy, [2*Ny])
   fresnel_vy(1:2*Ny) = dcmplx(zero, zero)   
   
   N = MAX(Nx, Ny)
   p_fresnel_w = fftw_alloc_complex(int(2*N, C_SIZE_T))
   call c_f_pointer(p_fresnel_w, fresnel_w, [2*N])
   fresnel_w(1:2*N) = dcmplx(zero, zero)
 end if

 Cxt=Cx ! No "variable unused" warning
 Cyt=Cy ! No "variable unused" warning
  
 allocate(xaxisz(Nx), stat=ierr)
 if (ierr /= 0) call opc_abort ("Opc_optics.f90::fresnelm", "Error, not enough memory to allocate xaxisz")
  allocate(yaxisz(Ny), stat=ierr)
 if (ierr /= 0) call opc_abort ("Opc_optics.f90::fresnelm", "Error, not enough memory to allocate yaxisz") 
  
! Calculate the axis at the output plane
 xaxisz = MMx * xaxis
 yaxisz = MMy * yaxis     
! Y direction transform
 alpha = (Mytot * mesh_y) * dsqrt( abs(MMy * k_wave/Pi/By) )
 const1 = k_wave*half*(Ay-MMy)/By
 const2 = k_wave*half*(Dy-1/MMy)/By     
 do ix=1, Nx
   do iy=1, Ny
     q = const1 * yaxis(iy)**2
     fresnel_uy(iy) = grid(ix,iy) * dcmplx(cos(q), -sin(q))
   end do
   call fresnel1d(alpha, Ny, 'y')
   do iy=1, Ny
     q = const2 * yaxisz(iy)**2
     grid(ix,iy) = fresnel_uy(iy) / dcmplx(cos(q), sin(q))
   end do
 end do
 if (include_Ey) then
   do ix=1, Nx
     do iy=1, Ny
       q = const1 * yaxis(iy)**2
       fresnel_uy(iy) = gridy(ix,iy) * dcmplx(cos(q), -sin(q))
     end do
     call fresnel1d(alpha, Ny, 'y')
     do iy=1, Ny
       q = const2 * yaxisz(iy)**2
       gridy(ix,iy) = fresnel_uy(iy) / dcmplx(cos(q), sin(q))
     end do
   end do
 end if
      
! X direction transform
 alpha = (Mxtot * mesh_x) * dsqrt( abs(MMx * k_wave/Pi/Bx) )
 const1 = k_wave*half*(Ax-MMx)/Bx
 const2 = k_wave*half*(Dx-1/MMx)/Bx    
 do iy=1, Ny
   do ix=1, Nx
     q = const1 * xaxis(ix)**2
     fresnel_ux(ix) = grid(ix,iy) * dcmplx(cos(q), -sin(q))
   end do
   call fresnel1d(alpha, Nx, 'x')
   do ix=1, Nx
     q = const2 * xaxisz(ix)**2
     grid(ix,iy) = fresnel_ux(ix) / dcmplx(cos(q), sin(q))
   end do
 end do
 if (include_Ey) then
   do iy=1, Ny
     do ix=1, Nx
       q = const1 * xaxis(ix)**2
       fresnel_ux(ix) = gridy(ix,iy) * dcmplx(cos(q), -sin(q))
     end do
     call fresnel1d(alpha, Nx, 'x')
     do ix=1, Nx
       q = const2 * xaxisz(ix)**2
       gridy(ix,iy) = fresnel_ux(ix) / dcmplx(cos(q), sin(q))
     end do
   end do 
 end if 
      
! Set the new axis
 Mxtot = Mxtot * MMx
 Mytot = Mytot * MMy
 call fillaxis()

 deallocate(xaxisz, stat=ierr)
 if (ierr /= 0) call opc_abort ("Opc_optics.f90::fresnelm", "Error deallocating memory associated with xaxisz")
 deallocate(yaxisz, stat=ierr)
 if (ierr /= 0) call opc_abort ("Opc_optics.f90::fresnelm", "Error deallocating memory associated with yaxisz")
end ! subroutine fresnelm



SUBROUTINE fresnel1d(alpha, N, direction)
! u is the data set
! N is the length of the data set
! direction is used to select the correct FFTW plan
! alpha is the factor for the fresnel integrals
! v and w are temporary buffers
 
 use opc_globals
 use opc_math
 implicit none

 real(kind=rk16), INTENT(INOUT) :: alpha
 integer, INTENT(IN) :: N
 character, INTENT(IN) :: direction
 integer i, Nsym, Ndiff
 real(kind=rk16)  a, c, s

 fresnel_vx = dcmplx(zero,zero)
 fresnel_vy = dcmplx(zero,zero)
 fresnel_w = dcmplx(zero,zero)
 
! Calculate the fresnel Kernel (K_j as a function of j)
! This is a symetric function
! For even number of grid points, N/2 is zero axis
! while for odd number of grid points (N-1)/2 is the zero axis.
! The kernel needs to be symetric around N+1 (even) of N (odd)
! to make everything work as expected. 

!if ( mod(N,2) == 0 ) then ! even
!	Nsym = N+1
!	Ndiff = 2
!else ! odd
!	Nsym = N
!	Ndiff = 1
!end if
      
! Calculate the fresnel numbers
 Nsym = N
 do i=1,Nsym+1
   a = alpha * (i-half)
   call FCS(a, c, s) ! get fresnel sin and cos integrals
   fresnel_w(i) = dcmplx(c,s)*dcmplx(half, -half) ! factor (1 - i)/2
 end do
!print *,'alpha: ',alpha,'w(1): ',w(1)
      

 if (direction == 'x') then
  do i=1,Nsym
    fresnel_vx(i) = fresnel_w(Nsym+2-i) - fresnel_w(Nsym+1-i)
  end do
  do i=1,Nsym-1
    fresnel_vx(Nsym+1+i) = fresnel_vx(Nsym+1-i)
  end do
  fresnel_vx(Nsym+1) = two*fresnel_w(1) ! Around zero we do not have the difference of
                                      ! two integrals, but the sum
 else                                   
  do i=1,Nsym
    fresnel_vy(i) = fresnel_w(Nsym+2-i) - fresnel_w(Nsym+1-i)
  end do
  do i=1,Nsym-1
    fresnel_vy(Nsym+1+i) = fresnel_vy(Nsym+1-i)
  end do
  fresnel_vy(Nsym+1) = two*fresnel_w(1) ! Around zero we do not have the difference of
                                    ! two integrals, but the sum
 end if
! convolute the data with the kernel
 call conv(N, direction)
     
end ! subroutine fresnel1d



subroutine conv(N, direction)
! ==================================================================
! Convolution of a complex array u(x,y)(N) with a complex array 
! v(x,y)(2Nu) Both arrays should have 2*N allocated space.
! ------------------------------------------------------------------
 use, intrinsic :: iso_c_binding
 use opc_globals
 use opc_lib
 use fftw3
#ifdef MPI
 use mpi
#endif
 implicit none
 
 interface
   integer(C_INT) function strlen(s) bind(C, name='strlen')
     import
     type(C_PTR), value :: s
   end function strlen
   subroutine free(p) bind(C, name='free')
     import
     type(C_PTR), value :: p
   end subroutine free
 end interface 
 
 integer, INTENT(IN)     :: N
 character, INTENT(IN)   :: direction
 integer                 :: l, i, isuccess, mpi_rank, mpi_size, ierr, slen_f
 complex(kind=rk16)      :: tmp
 character (len=100)     :: opc_wisdom_filename
 logical                 :: opc_wisdom_exist
 integer(C_INT)          :: ret    ! for FFTW wisdom import/export
 character(C_CHAR), pointer :: s_c(:)
 character,dimension(:),allocatable  :: s_f
 integer(C_SIZE_T)       :: slen
 type (C_PTR)            :: p
 
#ifdef MPI
 integer                 :: mpi_err
#endif
#ifdef MPI
 call MPI_COMM_RANK(MPI_COMM_WORLD, mpi_rank, mpi_err) ! rank and number of processes
 call MPI_COMM_SIZE(MPI_COMM_WORLD, mpi_size, mpi_err)
#else
 mpi_rank=0 ! default value for NON-MPI execution on a single node
 mpi_size=1 ! default value for NON-MPI execution on a single node
#endif 
 if (.NOT. c_associated( plan_forward_fresnel_ux )) then
 	! plan the FFT required for the fresnel propagation using in place replacement
 	! check if wisdom already has been created before 
  if ((trim(export_wisdom) == 'all').OR.(mpi_size == 1)) then
   write(opc_wisdom_filename,FMT='(A18,I3.3,"_",I5.5,"x",I5.5)') 'opc_wisdom_fresnel',mpi_rank,2*Nx,2*Ny
   inquire(FILE=trim(opc_wisdom_filename),EXIST=opc_wisdom_exist)
   if (opc_wisdom_exist) then
     ! restore plans from previous runs using the same grid size
     ret = fftw_import_wisdom_from_filename(C_CHAR_''//trim( opc_wisdom_filename) // C_NULL_CHAR)
     if (ret == 0) call  opc_abort('opc_optics.f90::main', 'error importing wisdom from file')
   end if
  else if ((trim(export_wisdom) == 'single').AND.(mpi_size>1)) then
 	 if (mpi_rank == 0) then  ! read wisdom file and broadcast to other subprocesses
 	 	write(opc_wisdom_filename,FMT='(A18,I3.3,"_",I5.5,"x",I5.5)') 'opc_wisdom_fresnel',mpi_rank,2*Nx,2*Ny
    inquire(FILE=trim(opc_wisdom_filename),EXIST=opc_wisdom_exist)
    if (opc_wisdom_exist) then
     ! restore plans from previous runs using the same grid size
     ret = fftw_import_wisdom_from_filename(C_CHAR_''//trim( opc_wisdom_filename) // C_NULL_CHAR)
     if (ret == 0) call  opc_abort('opc_optics.f90::conv', 'error importing wisdom from file')
    end if
    p = fftw_export_wisdom_to_string()
    if (.not. c_associated(p)) call opc_abort('opc_optics.f90::conv', 'error converting wisdom to string')
    slen = strlen(p)+1
    slen_f = slen
    call c_f_pointer(p, s_c, [slen])
    allocate(s_f(slen_f),stat=ierr)
    s_f=s_c
#ifdef MPI
    call mpi_bcast(slen_f,1,mpi_integer,0,mpi_comm_world,mpi_err)
    call mpi_bcast(s_f,slen_f,mpi_character,0,mpi_comm_world,mpi_err) 
    call free(p)     
#endif       
   end if
#ifdef MPI
   if (mpi_rank > 0) then  ! receive wisdom broadcasted by subprocess with rank 0
   	call mpi_bcast(slen_f,1,mpi_integer,0,mpi_comm_world,mpi_err)
   	allocate(s_f(slen_f),stat=ierr)
   	call mpi_bcast(s_f, slen_f, mpi_character,0,mpi_comm_world,mpi_err)
   	ret = fftw_import_wisdom_from_string(C_CHAR_'' // s_f)
   	deallocate(s_f)
   end if
#endif
  end if 
  ! plan FFT's using existing wisdom if available
  plan_forward_fresnel_ux  = fftw_plan_dft_1d ( 2*Nx, fresnel_ux, fresnel_ux, FFTW_FORWARD, FFTW_MEASURE )
  plan_backward_fresnel_ux = fftw_plan_dft_1d ( 2*Nx, fresnel_ux, fresnel_ux, FFTW_BACKWARD, FFTW_MEASURE )
  plan_forward_fresnel_uy  = fftw_plan_dft_1d ( 2*Ny, fresnel_uy, fresnel_uy, FFTW_FORWARD, FFTW_MEASURE )
  plan_backward_fresnel_uy = fftw_plan_dft_1d ( 2*Ny, fresnel_uy, fresnel_uy, FFTW_BACKWARD, FFTW_MEASURE )
  plan_forward_fresnel_vx  = fftw_plan_dft_1d ( 2*Nx, fresnel_vx, fresnel_vx, FFTW_FORWARD, FFTW_MEASURE )
  plan_forward_fresnel_vy  = fftw_plan_dft_1d ( 2*Ny, fresnel_vy, fresnel_vy, FFTW_FORWARD, FFTW_MEASURE )
  if (.NOT.opc_wisdom_exist) then
  ! no previous plans exists for this grid size, check if we need to store plan to file for later use
   if (trim(export_wisdom) == 'single') then
   	if (mpi_rank == 0) then
      ret = fftw_export_wisdom_to_filename(C_CHAR_'' // trim(opc_wisdom_filename) // C_NULL_CHAR)
      if (ret == 0) call opc_abort('opc_optics.f90::main', 'error exporting wisdom to file')
    end if
   end if
   if (trim(export_wisdom) == 'all') then
 	  ret = fftw_export_wisdom_to_filename(C_CHAR_'' // trim(opc_wisdom_filename) // C_NULL_CHAR)
    if (ret == 0) call opc_abort('opc_optics.f90::main', 'error exporting wisdom to file')
   end if   
  end if  
 end if

 l = 2*N
      
 if (direction == 'x') then
! zero padding for u (padding should be length(v)/2)
    fresnel_ux(N+1:l) = dcmplx(zero, zero)
! set v in "wrap around" order
    do i=1,N
      tmp = fresnel_vx(i)
      fresnel_vx(i) = fresnel_vx(N+i)
      fresnel_vx(N+i) = tmp
    end do
! transform to frequency domain
    call fftw_execute_dft ( plan_forward_fresnel_ux, fresnel_ux, fresnel_ux )
    call fftw_execute_dft ( plan_forward_fresnel_vx, fresnel_vx, fresnel_vx )
! multiply in frequency domain
    do i=1,l
      fresnel_ux(i) = fresnel_ux(i) * fresnel_vx(i) / l
    end do
     
! transform back
    call fftw_execute_dft( plan_backward_fresnel_ux, fresnel_ux, fresnel_ux)
  else ! y direction
! zero padding for u (padding should be length(v)/2)
    fresnel_uy(N+1:l) = dcmplx(zero, zero)
! set v in "wrap around" order
    do i=1,N
      tmp = fresnel_vy(i)
      fresnel_vy(i) = fresnel_vy(N+i)
      fresnel_vy(N+i) = tmp
    end do
! transform to frequency domain
    call fftw_execute_dft ( plan_forward_fresnel_uy, fresnel_uy, fresnel_uy )
    call fftw_execute_dft ( plan_forward_fresnel_vy, fresnel_vy, fresnel_vy )
! multiply in frequency domain
    do i=1,l
      fresnel_uy(i) = fresnel_uy(i) * fresnel_vy(i) / l
    end do
! transform back
    call fftw_execute_dft( plan_backward_fresnel_uy, fresnel_uy, fresnel_uy )
  end if

 end ! subroutine conv 
         
