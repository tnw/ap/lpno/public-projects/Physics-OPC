#!/usr/bin/perl

use lib './lib';
use Physics::OPC qw/field/;

@files = sort {
	$a =~ /(\d+)\D*$/;
	$num_a = $1;
	$b =~ /(\d+)\D*$/;
	$num_b = $1;
	$num_a <=> $num_b;
} @ARGV;
#warn "@files\n";

open CROSS, '>totals.cross.txt' or die $!;
open TOTAL, '>totals.total.txt' or die $!;

# time between two frames is zsep * lambda / c
# for FELIX = 2 * 4E-5 / 3E+8 = 2.6667e-13
my $time;
for (@files) {
	print "Processing $_\n";
	my $f = field file => $_;
	$time = get_time($f) unless defined $time; # extract for first file
	my @cross = $f->total();
	my $total = 0;
	$total += $time * $_ for @cross;
	print CROSS map("$_\n", @cross), "\n";
	print TOTAL "$total\n";
}

close CROSS;
close TOTAL;

exit;

sub get_time {
	my $f = shift;
	my $c = 299_792_458; # =~ 3E+8
	my ($zsep, $lambda) = $f->get_params('zsep', 'lambda');
	return $zsep * $lambda / $c;
}


