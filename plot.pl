#!/usr/bin/perl

use File::Spec;

# Find libs from arbitrary directory
my $lib = $0;
$lib =~ s![^/\\]*$!lib/!;
unshift @INC, $lib if -d $lib;
eval "use Physics::OPC::Plot;";
die $@ if $@;

$VERBOSE = 0;

sub display_info {
	print  "\n\nplot.pl - plotting script for creating plots from datafiles produced by OPC\n\n".
         "Command: ./plot.pl option=value option=value\n\n".
         "Allowed options are:\n".
         "-help        --help        : display this information, no value is required.\n".
         "-plottype    --plottype    : type of plot\n". 
         "                             value = 'field', 'total', 'fluence', 'cross_slice' or 'cross_roundtrip'.\n".
         "-graph       --graph       : type of graph, value = 'contour' for contour plot, when not specified a '3D' plot is made.\n".
         "-file_in     --file_in     : value is the name of either an ASCII (.txt or .dat) or a binary (.dfl) file.\n".
         "-file_out    --file_out    : value is the name of file that will contain the scaled data to be plotted.\n".
         "-slice       --slice       : make plot for certain slice number, value is a numeric, defaults to centre slice.\n".
         "-first_slice --first_slice : first slice to plot, value is numeric, defaults to 1.\n".
         "-last_slice  --last_slice  : last slice to plot, value is numeric, defaults to last slice in file.\n".
         "-roundtrip   --roundtrip   : make plot for certain roundtrip, value is numeric, defaults to 1.\n".
         "-first_roundtrip --first_roundtrip : first roundtrip to plot, value is numeric, defaults to 1.\n".
         "-last_roundtrip  --last_roundtrip  : last roundtrip to plot, value is numeric, defaults to first_roundtrip.\n".
         "-term        --term        : a valid gnuplot terminal name.\n".
         "-plotfile    --plotfile    : name of the plotfile. If set, redirects output of GNUPLOT to this file.\n".
         "-autoscale   --autoscale   : use autoscaling, value is text (n,x,y,z,xy,xz,yz,xyz), default is xy (2D) or xyz (3D).\n".
         "                             n means no autoscaling, x autoscaling for x-axis, etc.\n".
         "-xmin        --xmin        : start point of x-axis (similar parameter for y- and z-axis\n".
         "-xmax        --xmax        : end point of x-axis (similar parameter for y- and z-axis\n".
         "-xincr       --xincr       : incr along x-axis (similar paramer for y- and z-axis\n\n".
         "Note, not all combinations of options result in a valid command.\n";
}

my %param;
# get command line arguments
if (@ARGV == 0) {	# display info if no options are specified
	display_info;
	exit;
}
for (@ARGV) {
	s/^--?//;
	if (/^help/) {
		display_info;
		exit;
	}	
	my ($key, $val) = split /\s*=\s*/, $_, 2;
	$param{$key} = $val;
}

$param{file_out} ||= "opc_tmp_plotdata.txt"; # assign default filename for file containing data to plot
$param{title} ||= 'File: '.$param{file_in}.'\n';
die "$0: type of plot is not specified. Add 'plottype=....' to commandline\n" unless defined $param{plottype};

my @allowed_plots = qw/cross_roundtrip cross_slice field total phase fluence/;	
my %list_of_plots;
@list_of_plots{@allowed_plots} = ();

die "$0: unknown plottype, use 'plot.pl -help' to display info on allowed plot types.\n" 
	unless exists $list_of_plots{$param{plottype}};

# extract data to be plotted from file and apply appropriate scaling

%param = extract_cross_vs_roundtrip(%param) if $param{plottype} eq 'cross_roundtrip';
%param = extract_cross_vs_slice(%param)     if $param{plottype} eq 'cross_slice';
%param = extract_field(%param)              if $param{plottype} eq 'field';
%param = extract_phase(%param)              if $param{plottype} eq 'phase';
%param = extract_total(%param)              if $param{plottype} eq 'total';
%param = extract_fluence(%param)            if $param{plottype} eq 'fluence';


# create the scriptfile for gnuplot and plot the data

# temporary files
my $tmpscriptfile = File::Spec->catfile( File::Spec->tmpdir, 'opc_tmp.script.txt' );
open SCRIPT, ">$tmpscriptfile" or die "Could not write $tmpscriptfile\n";
if ($param{plotfile}) {                 # redirect gnuplot output to file
  print SCRIPT << "EOT";
set terminal $param{term} enhanced
set output "$param{plotfile}"
EOT
}

if ($param{plotstyle} eq "2D") {        # 2-dimensional plot
	# check on autoscaling
	$param{autoscale} ||= 'xy';
	if ($param{autoscale} =~ /\bn\b|\by\b/i) {
		die "No autoscaling for x-axis, parameters for axis not specified\n" unless
			defined $param{xmin} && defined $param{xmax};
	} elsif ($param{autoscale} =~ /\bn\b|\bx\b/i) {
		die "No autoscaling for y-axis, parameters for axis not specified\n" unless
			defined $param{ymin} && defined $param{ymax};
	} else {
		die "Unknown value for parameter 'autoscale' : $param{autoscale}\n" unless
			$param{autoscale} =~ /\bxy\b/i;
	}	
	print SCRIPT << "EOT";
set mouse
set style data lines
set title "$param{title}"
set xlabel "$param{xtitle}"
set ylabel "$param{ztitle}"
EOT
	if ($param{autoscale} =~ /\bn\b|\by\b/i) {
		print SCRIPT << "EOT";
set xrange [$param{xmin}:$param{xmax}]
EOT
		if (defined $param{xincr}) {
			print SCRIPT << "EOT";
set xtics $param{xmin},$param{xincr},$param{xmax}
EOT
		}
	}
	if ($param{autoscale} =~ /\bn\b|\bx\b/i) {
		print SCRIPT << "EOT";
set yrange [$param{ymin}:$param{ymax}]
EOT
		if (defined $param{yincr}) {
			print SCRIPT << "EOT";
set ytics $param{ymin},$param{yincr},$param{ymax}
EOT
		}
	}
	my $zscale = "1";
	$zscale = sprintf("%.5e", 10 ** (3*$param{zscale})) if defined $param{zscale}; # force floating point	
	print SCRIPT << "EOT";	
set ticslevel 0
set key off
#set terminal png
#set output "$output"
plot "$param{file_out}" using 1:(\$3/$zscale)
EOT
	close SCRIPT;
}
else {                                  # 3-dimensional plot
	$param{autoscale} ||= 'xyz';
	if ($param{autoscale} =~ /\bn\b|\by\b|\bz\b|\byz\b/i) {
		die "No autoscaling for x-axis, parameters for axis not specified\n" unless
			defined $param{xmin} && defined $param{xmax};
	} elsif ($param{autoscale} =~ /\bn\b|\bx\b|\bz\b|\bxz\b/i) {
		die "No autoscaling for y-axis, parameters for axis not specified\n" unless
			defined $param{ymin} && defined $param{ymax};
	} elsif ($param{autoscale} =~ /\bn\b|\bx\b|\by\b|\bxy\b/i) {
		die "No autoscaling for z-axis, parameters for axis not specified\n" unless
			defined $param{zmin} && defined $param{zmax};
	} else {
		die "Unknown value for parameter 'autoscale' : $param{autoscale}\n" unless
			$param{autoscale} =~ /\bxyz\b/i;
	}	
	print SCRIPT << "EOT";
set mouse
set style data lines
set title "$param{title}"
set xlabel "$param{xtitle}"
set ylabel "$param{ytitle}"
EOT
  if (($param{plottype} eq 'phase') || ($param{graph} eq 'contour')) {
  	print SCRIPT << "EOT";
set pm3d map  	
set cblabel "$param{ztitle}" offset graph -0.10, 0.65 rotate by 0
EOT
	} else {
		print SCRIPT << "EOT";
set zlabel "$param{ztitle}" offset graph 0.2, 0, 0.65
EOT
	}
	if ($param{autoscale} =~ /\bn\b|\bx\b|\bz\b|\bxz\b/i) {
		print SCRIPT << "EOT";
set yrange [$param{ymin}:$param{ymax}]
EOT
		if (defined $param{yincr}) {
			print SCRIPT << "EOT";
set ytics $param{ymin},$param{yincr},$param{ymax}
EOT
		}
	}
	if ($param{autoscale} =~ /\bn\b|\by\b|\bz\b|\byz\b/i) {
		print SCRIPT << "EOT";
set xrange [$param{xmin}:$param{xmax}]
EOT
		if (defined $param{xincr}) {
			print SCRIPT << "EOT";
set xtics $param{xmin},$param{xincr},$param{ymax}
EOT
		}
	}	
	if ($param{autoscale} =~ /\bn\b|\bx\b|\by\b|\bxy\b/i) {
		print SCRIPT << "EOT";
set zrange [$param{zmin}:$param{zmax}]
EOT
		if (defined $param{zincr}) {
			print SCRIPT << "EOT";
set ztics $param{zmin},$param{zincr},$param{zmax}
EOT
		}
	}	
	print SCRIPT << "EOT";
set ticslevel 0
set key off
EOT
	my $zscale = "1";
	$zscale = sprintf("%.5e", 10 ** (3*$param{zscale})) if defined $param{zscale}; # force floating point
	print SCRIPT << "EOT";
#set terminal png
#set output "$output"
splot "$param{file_out}" using 1:2:(\$3/$zscale) every 1:1
EOT
	close SCRIPT;
}

# Run gnuplot

my $gnuplot = ($^O eq 'MSWin32') ? 'wgnuplot' : 'gnuplot' ;
exec $gnuplot, $tmpscriptfile, '-';

die "$0: Could not exec: $gnuplot $tmpscriptfile -\n";
	# exec should have exited this script
