#!/usr/bin/perl

die "usage: $0 FILE XMIN:XSTEP YMIN:YSTEP\n"
	unless @ARGV == 3;

($file, $xrange, $yrange) = @ARGV;

@x = split /:/, $xrange, 2;
@y = split /:/, $yrange, 2;

open IN, $file or die $!;

my ($i, $j) = (0,0);
while (<IN>) {
	if (/^\s*#/) {
		print $_;
		next;
	}
	elsif (/\S/) {
		print $x[0]+$i*$x[1], "\t", $y[0]+$j*$y[1], "\t", $_;
		$i++;
	}
	else {
		print $_;
		$i = 0;
		$j++;
	}
}

