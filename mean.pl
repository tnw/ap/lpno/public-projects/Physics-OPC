#!/usr/bin/perl

open FILE, shift @ARGV;

($total, $n) = (0, 0);

while (<FILE>) {
	/(\d+[\dE\+\-\.]+)/;
	$total += $1;
	$n++;
}

print $total/$n, "\n";
