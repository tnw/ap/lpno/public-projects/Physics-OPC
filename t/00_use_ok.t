#!/usr/bin/perl

use Test::More tests => 6;

use_ok('Physics::OPC::Namelist'); # included by rest
use_ok('Physics::OPC::FieldFile'); 
use_ok('Physics::OPC::Optics');
use_ok('Physics::OPC'); # includes previous 2
use_ok('Physics::OPC::Genesis');
use_ok('Physics::OPC::Medusa');

