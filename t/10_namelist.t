use Test::Simple tests => 3;
use Physics::OPC::Namelist;

my $file = './t/test.in~';

unlink $file;

my $nl = Physics::OPC::Namelist->new('optics',$file);

$nl->set_param(lambda => 4E-5);

ok cat($file) eq
' $optics
 lambda = 4e-05
 $end
', 'new namelist';

$nl->set_param(lambda => 5E-5, debug => 'true');

ok cat($file) eq
' $optics
 lambda = 5e-05
 debug = '."'true'".'
 $end
', 'update namelist';

ok $nl->get_param('DeBuG') eq 'true', 'read namelist';

sub cat {
	my $file = shift;
	open IN, $file or die $!;
	my $text = join '', <IN>;
	close IN;
	return $text;
}
