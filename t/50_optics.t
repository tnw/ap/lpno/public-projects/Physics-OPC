#!/usr/bin/perl

use Test::More tests => 2;
use Physics::OPC;

#$VERBOSE = 1;

my $conf = "
	spectral z = 10
	mirror f=15 R=10%;
	spectral z=23
#	Comment here
	mirror f =15, R=0.1 ; fresnel z=3
	hole r=0.1 (
		fresnel z=10 M=5
		dump var=far
	) # comment
	mirror R=50% (
		spectral z=1
	); fresnel z=1 M = 3;
	diaphragm r=3 ( dump var=out ); spectral z=5
	mirror R=.5
		( dump var=test )
";
my @data = (
	['spectral', z => 10],
	['mirror',   f => 15, R => 0.1],
	['spectral', z => 23],
	['mirror',   f => 15, R => 0.1],
	['fresnel', z => 3],
	['hole', fork => 1, r => 0.1 ],
	['fresnel', z => 10, M => 5],
	['dump', var => 'far'],
	['end_fork'],
	['mirror', fork => 1, R => 0.5],
	['spectral', z => 1],
	['end_fork'],
	['fresnel', z => 1, M => 3],
	['diaphragm', fork => 1, r => 3],
	['dump', var => 'out'],
	['end_fork'],
	['spectral', z => 5],
	['mirror', R => 0.5, fork => 1],
	['dump', var => 'test'],
	['end_fork'],
);

my $optics = optics $conf;
ok(ref($optics));
$optics->write_script('t/optics.script~');

my @conf = $optics->parse_config($conf);
#use Data::Dumper; print STDERR Dumper \@conf;

is_deeply(\@conf, \@data);
