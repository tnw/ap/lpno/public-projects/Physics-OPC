package Physics::OPC;

use strict;
use vars qw/$AUTOLOAD/;

use Cwd 'cwd';				# used to query current working directory
use Env '$PWD', '@PATH';	# used to import environment variables
use Carp;					# used for formatting errors
use Exporter;				# used to export to script namespace
use POSIX qw/strftime/;		# used to format timestamps
use File::Spec;				# used to manipulate file names
use File::Copy qw/cp mv/;	# used to copy and move files

use Physics::OPC::Optics;
use Physics::OPC::FieldFile;

# Define Export interface
our @ISA = qw/Exporter/;
our @EXPORT = qw/
	$field $VERBOSE %USE_MPI $LOG $OPC_VERSION %optics_data $include_Ey
	genesis optics medusa hermes minerva mercury field message copy move remove mask
	AUTOLOAD
	/; # these can be used directly from the script

# Defined global variables
our $field;       # Default input/output buffer Ex component
our %field;       # lookup table for non-default input/output buffers
our $VERBOSE = 0; # Verbose output
our $OPC_VERSION = 'Physics-OPC-0.8.0'; # OPC version (equals root of directory tree of OPC)
our %USE_MPI;     # MPI commando
our $LOG;         # Log file
our $logfh;       # File handle for log file - private
our %EXEC;        # Cache with paths for executables
our %optics_data; # variable to make data from optics run available for user
our $include_Ey;  # variable that determines of a second (perpendicular) polarisation is required

# Search for directories to add to the PATH
$PWD = cwd();
my $updir = File::Spec->updir;
push @PATH, $PWD,
	grep {-d $_}
	map File::Spec->catdir(@$_),
	map {
		[$PWD, $_],                 # ./dir/
		[$PWD, $updir, $_],         # ../dir/
		[$PWD, $updir, $updir, $_], # ../../dir/
		[$ENV{HOME}, $_],           # $HOME/dir/
	}
	qw/optics genesis medusa minerva/;
	#use Data::Dumper; warn Dumper \@PATH;

=head1 NAME

Physics::OPC - An optical simulation toolkit

=head1 DESCRIPTION

This module and it''s sub-modules provide a high-level interface
to a set of Fortran programs for an Optical Propagation Code.

See the OPC manual for usage instructions.

=head1 EXPORT

The following methods are exported:
C<genesis()>, C<medusa()>, C<hermes()>, C<optics()>,
 C<field()>, C<message()>, C<copy>, C<move>, C<remove>
and C<mask>.

An C<AUTOLOAD> method is exported to convert function
calls to object calls for non-OO style scripts.

The following variables are exported:
C<$field>, C<$VERBOSE>, C<%USE_MPI>, C<$LOG > C<$include_Ey>

=head1 METHODS

=over 4

=cut

sub AUTOLOAD {
	$AUTOLOAD =~ s/.*:://;
	return if $AUTOLOAD eq 'DESTROY';
	my $obj = shift;
	croak "No such method: $AUTOLOAD"
		unless ref $obj and $old->UNIVERSAL::can($AUTOLOAD);
	#warn "AUTOLOADED $obj->$AUTOLOAD(@_)\n";
	$obj->$AUTOLOAD(@_);
}

=item C<genesis(FILE)>

Returns an object of the type L<Physics::OPC::Genesis>.

=cut

sub genesis {
	message "New genesis object @_\n" if $VERBOSE;
	unshift @_, 'Physics::OPC::Genesis';
	eval 'use Physics::OPC::Genesis;';
	die $@ if $@;
	goto \&Physics::OPC::Genesis::new;
}

=item C<medusa(FILE)>

Returns an object of the type L<Physics::OPC::Medusa>.

=cut

sub medusa {
	message "New medusa object @_\n" if $VERBOSE;
	unshift @_, 'Physics::OPC::Medusa';
	eval 'use Physics::OPC::Medusa;';
	die $@ if $@;
	goto \&Physics::OPC::Medusa::new;
}

=item C<minerva(FILE)>

Returns an object of the type L<Physics::OPC::Minerva>.

=cut

sub minerva {
	message "New Minerva object @_\n" if $VERBOSE;
	unshift @_, 'Physics::OPC::Minerva';
	eval 'use Physics::OPC::Minerva;';
	die $@ if $@;
	goto \&Physics::OPC::Minerva::new;
}

=item C<hermes(FILE)>

Returns an object of the type L<Physics::OPC::Hermes>.

=cut

sub hermes {
	message "New Hermes object @_\n" if $VERBOSE;
	unshift @_, 'Physics::OPC::Hermes';
	eval 'use Physics::OPC::Hermes;';
	die $@ if $@;
	goto \&Physics::OPC::Hermes::new;
}

=item C<mercury(FILE)>

Returns an object of the type L<Physics::OPC::Mercury>.

=cut

sub mercury {
	message "New Mercury object @_\n" if $VERBOSE;
	unshift @_, 'Physics::OPC::Mercury';
	eval 'use Physics::OPC::Mercury;';
	die $@ if $@;
	goto \&Physics::OPC::Mercury::new;
}


=item C<optics(CONFIG)>

Returns an object of the type L<Physics::OPC::Optics>.

=cut

sub optics {
	message join "\n", '# New optics  object <<', @_, "...\n" if $VERBOSE;
	unshift @_, 'Physics::OPC::Optics';
	goto \&Physics::OPC::Optics::new;
}

=item C<field(%PARAM)>

Initialize a field variable.

=cut

sub field {
	my %param = @_;

	if (defined $param{file}) {
		croak "To many arguments" if keys(%param) != 1;
		return Physics::OPC::FieldFile->new($param{file});
	}
#	print "$_ : $param{$_}\n" for keys %param;
	print "field: no file defined, initialising field\n" if $VERBOSE;
	my ($w0, $z0, $p0, $mx, $my, $trms, $tprofile, $rad_offset, $mode_type, $npoints, $mesh, $M, $init, $filename, $Rwg, $n, $m) =
	     delete @param{qw/w0 z0 p0 mx my trms tprofile rad_offset mode_type npoints mesh M init filename Rwg n m/};
	#to be compatible with files produced by older releases, check if 'npoints', 'mesh' and M are present
	if ($mesh) {
		$param{$_} = $mesh for qw/mesh_x mesh_y/;
	}
	if ($npoints) {
	  $param{$_} = $npoints for qw/npoints_x npoints_y/;
	}
	if ($M) {
	  $param{$_} = $M for qw/Mx My/;
	}
	if ($init) {
		$mode_type = $init; # to be compatible with older versions script files
	}
  # if mode_type equals "cylindrical, we set the field to be that of a TEnm mode in a cylindrical waveguide
  # we reuse some of the variables for a Gaussian field: n => mx, m => my, Rwg => w0
    if (lc($mode_type) eq 'cylindrical') {
      print "cylindrical waveguide mode\n" if $VERBOSE;
      $mx = $n;
      $my = $m;
      $w0 = $Rwg;
      $include_Ey = 1;  # set flag to include second (perpendicular) polarisation
      $param{'field_next'} = 'gaussian_y.dfl';
    }
  # set default values for certain parameters
	$w0 ||= 1.0e-3;         # waist size
	$z0 ||= 0.0e0;          # position of waist
	$p0 = 1.0e0 unless defined ($p0); # Peak intensity, $p0 may be zero and ||= would result in setting p0 to 1
	$mx ||= 0;              # first (x,r) mode number
	$my ||= 0;              # second (y.theta) mode number
	$trms ||= '1e-12';      # rms temporal duration of pulse
	$tprofile ||= 'gaussian';     # type of temporal profile, 'gaussian' or 'parabolic'
	$rad_offset ||= 0.0;          # offset of center of pulse with respect to center of time window
	$mode_type ||= 'plane';       # type of mode to generate
	$filename ||= 'gaussian.dfl'; # default name of the .dfl file containing the field

	$param{'nslices'} ||= 1;          # default number of slices
	$param{'zsep'}    ||= 1;          # default separation between slices
	$include_Ey ||=0;                 # default is to not include second (perpendicularular) polarisation
	$param{'field_next'} ||= 'none' ; # points to associated file (harmonic or second polarisation component)
	                                  # if <> 'none' points to base name of associated file

	my $field =  Physics::OPC::FieldFile->new($filename);
	$field->set_param(%param);
	if (lc($mode_type) eq 'cylindrical') {
	  my $f2 = $field->{field};
	  my $f1 = $field->{file};
      $f2 =~ s/(\.)(\w+\s*)?$/_y.$2/;
      my $field_y=Physics::OPC::FieldFile->new($f2);
      $field_y->copyfrom($f1);
      $field_y->set_param(field_next => 'none');
	}
	my $exec = Physics::OPC->executable('dfl_gauss');
	die "Could not find executable 'dfl_gauss'\n"
		unless defined $exec;
	$exec .= sprintf(" $field->{file} $field->{field} %13.6e %13.6e %13.6e %d %d %13.6e $tprofile %13.6e $mode_type",
	                                  $w0, $z0, $p0, $mx, $my, $trms, $rad_offset);
	print "$exec\n" if $VERBOSE>1;
	open GAUS, "| $exec\n" or die $!;
	close GAUS;
	return $field;
}

=item C<mask(%param)>

Calculates a mask. The mask can either be an intensity or phase mask and
specifies for each grid point the change in intensity or phase.
To make the phase change independent of the wavelength, the phase change
is implemented as an optical path length difference. Currently implemented is

=over 4

=item zernike

phase (dz) aberration for each grid point

=back

Use the opld command in the optics script to read in and apply a phase mask.

=cut

sub mask {
	eval 'use Physics::OPC::Mask;';
	die $@ if $@;
	my $mask = Physics::OPC::Mask->new(@_);
	$mask->prepare() unless defined wantarray;
		# if called in void context (wantarray = undef) the object
		# is not put into a variable, thus the file needs to
		# exist already
	return $mask;
}

=item C<message(STRING)>

Will echo STRING to the logfile (if C<$LOG> is set) and to the terminal.
A timestamp is prefixed.

=cut

sub message {
	my $string = join "\t", @_;
	chomp $string;
	$string = '['.strftime('%d/%m/%Y %H:%M:%S', localtime)."] $string\n";
	warn $string;
	if (defined $LOG) {
		open $logfh, ">>$LOG" or die "Could not write to $LOG\n"
			unless defined $logfh;
		print {$logfh} $string;
	}
}

=item C<executable(NAME)>

Returns the location of a executable.

For exmaple if NAME is "genesis", it checks C<./genesis>,
C<./genesis/genesis>, C<../genesis/genesis> and
looks for 'genesis' in the PATH directories.

=cut

sub executable {
	my ($class, $name) = @_;

	$name .= '.exe' if $^O eq 'MSWin32';

	return $EXEC{$name} if defined $EXEC{$name}; # result from cache

	# loop through candidates from path and environment
	my $file = File::Spec->catfile(File::Spec->catdir($ENV{OPC_HOME},'optics'), $name);
	if (-f $file and -x $file) {   # first check if environment variable is set
	  $EXEC{$name} = $file;
	} else {
	  for my $path (@PATH) {   # next check path
		my $file = File::Spec->catfile($path, $name);
		message "! checking: $file\n" if $VERBOSE > 1;
		if (-f $file and -x $file) {         # file exists and is executable
		  $EXEC{$name} = $file;
		  last;
		} else {
		  $file = File::Spec->catfile($path, 'optics', $name);
		  message "! checking: $file\n" if $VERBOSE > 1;
		  next unless -f $file and -x $file; # file exists and is executable
		  $EXEC{$name} = $file;
		  last;
		}
	  }
	}
	# if not found search along current path
	unless (defined $EXEC{$name}) {
      my @mydirs =  File::Spec->splitdir( Cwd->cwd() );
      while (@mydirs) {
         my $path = File::Spec->catdir (@mydirs, $OPC_VERSION, 'optics');
         my $file = File::Spec->catfile($path, $name);
         if (-f $file and -x $file) {
           $EXEC{$name}=$file;
           last;
         }
         pop @mydirs;
	   }
	}
	# enclose in quotes for windows in case of spaces in path
        $EXEC{$name} = '"'.$EXEC{$name}.'"' if $^O eq 'MSWin32';
	message "Executable $name => $EXEC{$name}\n" if $VERBOSE;
	return $EXEC{$name};
}

=item C<abs_path(FILE)>

Returns the absolute file name for FILE.

=cut

sub abs_path {
	my $file = pop;
	$file =~ s/^([><]*)//;
	my $prefix = $1;
	$file = File::Spec->rel2abs($file, $PWD);
	return $prefix.$file;
}

=item C<check_dir(FILE)>

Returns cleaned filename when FILE is in the current dir.
Returns undef when we need to change directory first.

=cut

sub check_dir {
	my $file = pop;
	$file =~ s/^([><]*)//;
	my $prefix = $1;
	my @part = File::Spec->splitdir($file);
	$file = $prefix . pop @part;
	return $file if @part == 0;
	return $file if @part == 1 and $part[0] = '.'; # ./file
	return undef;
}

=item C<export(VAR => VALUE)>

Export a variable to the script namespace.
Used to export additional field variables.

=cut

sub export {
	my ($class, $var, $value)  = @_;
	$var =~ s/^\$//;
	message "! Export \$$var => $value\n" if $VERBOSE > 1;
	$field{$var} = $value;
	no strict 'refs';
	*{"main::$var"} = \$value;
}

=item C<var(VAR)>

Opposite of C<export()> - C<import()> is a reserved name in Perl.
Imports a variable from the script namespace.

=cut

sub var {
	my ($class, $var) = @_;
	$var =~ s/^\$//;
	no strict 'refs';
	return ${*{"main::$var"}{SCALAR}};
}

=item C<remove(FILE)>

Delete a file.

=item C<copy(FILE, FILE)>

Copy a file.

=item C<move(FILE, FILE)>

Move a file.

=cut

sub remove {
	my $file = shift;
	croak "Not a file: $file" if ref $file;
	unlink $file;
}

sub copy {
	my ($old, $new) = @_;
	if (ref $old) {
		return $old->copy($new) if $old->UNIVERSAL::can('copy');
		croak "Not a file: $old";
	}
	croak "Not a file $new" if ref $new;
	cp($old, $new);
}

sub move {
	my ($old, $new) = @_;
	if (ref $old) {
		return $old->move($new) if $old->UNIVERSAL::can('move');
		croak "Not a file: $old";
	}
	croak "Not a file $new" if ref $new;
	mv($old, $new);
}

=back

=head1 COPYRIGHT

Copyright (C) 2006 J.G. Karssenberg, P.J.M. van der Slot, I.V.Volokhine

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; version 2 of the
License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

=cut

1;

