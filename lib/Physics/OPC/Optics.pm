package Physics::OPC::Optics;

use strict;
use vars qw/$field %field $VERBOSE %USE_MPI %optics_data $include_Ey/;
use Carp;
use Physics::OPC::Namelist;
use Scalar::Util qw/looks_like_number/;
use Data::Dumper;

*field =   \$Physics::OPC::field;   # import $field
*VERBOSE = \$Physics::OPC::VERBOSE; # import $VERBOSE
*USE_MPI = \%Physics::OPC::USE_MPI; # import %USE_MPI
*message = \&Physics::OPC::message; # import message() routine
*optics_data = \%Physics::OPC::optics_data; # import %optics_data  
*include_Ey = \$Physics::OPC::include_Ey;   # import $include_Ey 

# global variables in this module

our ($nxcross, $nycross, $nstat, $ntotal, $nfluence, $nspectrum);


=head1 NAME

Physics::OPC::Optics

=head1 DESCRIPTION

Wrapper for the Fortran program for optical propagation.
Conceptually this object maps to the optics I<scripts>, the I<namelist> 
is asociated with the object for the fieldfile used as input.

=head1 METHODS

=over 4

=item C<new(CONFIG)>

Simple constructor. Takes a multiline string describing the optical
elements. (See C<parse_config()>.)

=cut

sub new {
	my $class = shift;
	my $config = join "\n",  @_;
	croak "Optics.pm::new -> Missing optics config" unless $config =~ /\S/;
	($nxcross, $nycross, $nstat, $ntotal, $nfluence, $nspectrum) = (0, 0, 0, 0, 0, 0);
	
	my $self = bless {
		script => '',
		dumps => 0,
    Mx => 1,
    My => 1,
		Mxbuf => 1,
		Mybuf => 1,
		fork => 0,
		dumpfiles => [],
		pre_run => [],
	}, $class;
	my @conf = $self->parse_config($config);
	$self->add_config(@conf);
	return $self;
}

=item C<parse_config(CONFIG)>

Method that parses config into a data structure.

For example:

        "spectral z=10
         mirror f=15 R=10%
         spectral z=23"

Is parsed to:

		    ['spectral', z => 10],
        ['mirror',   f => 15, R => 0.1],
        ['spectral', z => 23],

Notice the significance of the "%" sign in the config.

=cut

sub parse_config {
	my ($self, $conf) = @_;
	# if ($conf =~ /^</) { # XML config
	# }
	# else { # plain text
	my @lines = split /[\r\n]+/, $conf;	  # split physical lines
	@lines = map {split /;/, $_} @lines;	# split logical lines
	@lines = map {s/#.*//; $_} @lines;  	# remove comments
	my @conf;
	while (@lines) {
		my $line = shift @lines;
		# warn ">> $line\n";
		next unless $line =~ /\S/; # skip empty lines
		my ($comp, @args, @out);
		
		# parse fork
		if ($line =~ s/^\s*\(//) {
			die "Optics.pm::parse_config -> Syntax error near: >>$line<<\n" unless ref $conf[-1];
			push @{$conf[-1]}, 'fork' => 1; # push to args of previous statement
		}
		if ($line =~ s/^\s*\)//) {
			die "Optics.pm::parse_config -> Syntax error near: >>$1<<\n" if $line =~ /\S/;
			push @conf, ['end_fork'];
			next;
		}
		if ($line =~ s/\((.*)$//) {
			unshift @lines, $1;
			push @args, 'fork' => 1;
		}
		if ($line =~ s/(\).*)$//) {
			unshift @lines, $1;
		}

		# parse component name
		$line =~ s/^\s+|\s+$//g; # remove whitespace
		$line =~ s/^(\w+)\s*// or die "Optics.pm::parse_config -> Syntax error near: >>$line<<\n";
		$comp = $1;

		# parse attributes
		while ($line =~ s/(\w+)\s*=\s*(\S+)\s*//) {
			my ($key, $val) = ($1, $2);
			$val =~ s/,$//;
			$val =~ s/^\./0\./;
			$val *= 0.01 if $val =~ s/\%$//;
			push @args, $key => $val;
		}
		
		die "Optics.pm::parse_config -> Syntax error near: >>$line<<\n" if $line =~ /\S/;
		push @conf, [$comp, @args];
	}
	# }

	return @conf;
}

=item C<add_config(@CONF)>

Add a config data struct to the current simulation. The output from C<parse_config()> can be used here.

All component structures in the list will be processed in order to add to correct low-level elements to the script.

If the list contains plain strings these will be added to the script without further processing.

=cut

sub add_config {
	my ($self, @elements) = @_;
	for (@elements) {
		if (ref $_) {
			my ($element, @args) = @$_;
			my $sub = 'add_'.$element;
			croak "Optics.pm::add_config -> No such optical element: $element\n"
				unless $self->can($sub);
			$self->$sub(@args); # dispatch to element specific subroutine
		}
		else { $self->{script} .= $_."\n" }
	}
}


sub no_fork {
	 "Optics.pm::no_fork -> ERROR: You can not use the \"$_[0]\" component within a forked path.\n";
}

sub add_spectral {
	my ($self, %args) = @_;
	$self->{script} .= "spectral\n$args{z}\n";
}

sub add_fresnel {
	my ($self, %args) = @_;
	
	if (defined $args{z}) { # catch z + M
		die "Optics.pm::add_fresnel -> Specify 'z' or 'A,B,C,D' for fresnel propagation, not both.\n"
	  		if grep defined($args{$_}), map { ($_, $_.'x', $_.'y') } qw/A B C D/;
	  if (defined $args{M}) { # same multiplification
		  $args{Mx} = $args{M};
		  $args{My} = $args{M};
		}
		if (defined $args{Mx}) {
			@args{qw/Ax Bx Cx Dx/} = (1, -$args{z}, 0 ,1);
			@args{qw/Ay By Cy Dy/} = (1, -$args{z}, 0 ,1);
			delete $args{z};
		}
	}
	else { # catch A B C D without direction specified
		for (grep defined($args{$_}), qw/A B C D/) {
			$args{$_.'x'} = $args{$_};
			$args{$_.'y'} = $args{$_};
			delete $args{$_};
		}
	}
	
	if (defined $args{z}) { # simpel version of the algolrithm
		$self->{script} .= "fresnel\n$args{z}\n";
	}
	else { # matrix version with de-coupled matrices
		my @f = qw/Ax Bx Cx Dx Ay By Cy Dy Mx My/;
		$args{$_} ||= '0' for @f;
		$args{Mx}  ||= '1';
		$args{My}  ||= '1';
		$self->{script} .= "fresnelm\n@args{@f}\n";
		$self->{Mx} *= $args{Mx};
		$self->{My} *= $args{My};
	}
}

sub add_lens {
	my ($self, %args) = @_;
	$self->add_tilt(%args) if grep defined($args{$_}), qw/xr yr/;
	$args{$_} ||= '0' for qw/xoff yoff/;
	$self->{script} .= "lens\n@args{'xoff','yoff','f'}\n";
}

sub add_tilt {
	my ($self, %args) = @_;
	$args{$_} ||= '0' for qw/xr yr xoff yoff/;
	$self->{script} .= "tilt\n@args{'xoff','yoff','xr','yr'}\n";
}

sub add_power {
	my ($self, %args) = @_;
	return if $args{c} == 1;
	print "Optics.pm::add_power -> Warning: Power factor larger than 1 !\nMaybe you need to add a '%' sign?\n" if ($args{c} > 1) && ($VERBOSE>0);
	my $factor = sqrt $args{c}; # power is |E|**2
	$self->{script} .= "multiply\n$factor 0\n";
}

sub add_multiply {
  my ($self, %args) = @_;
	return if ($args{cr} == 1) && ($args{ci} == 0);    # nothing to do
	print "Optics.pm::add_multiply -> Warning: Power factor larger than 1 !\nMaybe you need to add a '%' sign?\n" if (($args{cr}**2 + $args{ci}**2)> 1) && ($VERBOSE>0);
	$self->{script} .= "multiply\n$args{cr}  $args{ci}\n";
}

sub add_diaphragm {
	my ($self, %args) = @_;
	die no_fork('diaphragm') if $self->{_in_fork};
	$self->{_in_fork} = $args{fork} || 0;
	$args{$_} ||= '0' for qw/xoff yoff dr n/;
	$args{type} ||= 'straight';
	$args{fork} ||= '0';
	$self->{Mxbuf} = $self->{Mx};
	$self->{Mybuf} = $self->{My};
	$self->{script} .= "diaphragm\n@args{'xoff','yoff','r','fork','type','dr','n'}\n";
}

sub add_slit {
	my ($self, %args) = @_;
	die no_fork('slit') if $self->{_in_fork};
	$self->{_in_fork} = $args{fork} || 0;
	$args{$_} ||= '0' for qw/xoff yoff/;
	$args{fork} ||= '0';
	$self->{Mxbuf} = $self->{Mx};
	$self->{Mybuf} = $self->{My};
	$self->{script} .= "rectangle\n@args{'xoff','yoff','d'} 0 $args{fork}\n";
}

sub add_square {
	my ($self, %args) = @_;
	die no_fork('square') if $self->{_in_fork};
	$self->{_in_fork} = $args{fork} || 0;
	$args{$_} ||= '0' for qw/xoff yoff/;
	$args{fork} ||= '0';
	$self->{Mxbuf} = $self->{Mx};
	$self->{Mybuf} = $self->{My};
	$self->{script} .= "rectangle\n@args{'xoff','yoff','d','d','fork'}\n";
}

sub add_rectangle {
	my ($self, %args) = @_;
	die no_fork('rectangle') if $self->{_in_fork};
	$self->{_in_fork} = $args{fork} || 0;
	$args{$_} ||= '0' for qw/dx dy xoff yoff/;
	$args{fork} ||= '0';
	$self->{Mxbuf} = $self->{Mx};
	$self->{Mybuf} = $self->{My};
	$self->{script} .= "rectangle\n@args{'xoff','yoff','dx','dy','fork'}\n";
}

sub add_hole {
	my ($self, %args) = @_;
	die no_fork('hole') if $self->{_in_fork};
	$self->{_in_fork} = $args{fork} || 0;
	$args{$_} ||= '0' for qw/xoff yoff dr n/;
	$args{type} ||= 'straight';
	$args{fork} ||= '0';
	$self->{Mxbuf} = $self->{Mx};
	$self->{Mybuf} = $self->{My};
	$args{fork} = $args{fork} ? '0' : '1'; # inverse logic
	$self->{script} .= "diaphragm\n@args{'xoff','yoff','r','fork','type','dr','n'}\n";
}

sub add_opld {
	my ($self, %args) = @_;
	
	if ($args{var}) { # we have an object
		# set the file already
		# have the object hooked up to create the file just before
		# we run the optics script
		$args{var} =~ s/^\$//;
		my $mask = Physics::OPC->var($args{var})
			|| die "Optics.pm::add_opld -> ERROR: No such mask defined: $args{var}\n(never define masks with 'my', use 'our')\n";
		$args{file} = $args{var}.'.opd';
    my $Mx = $self->{Mx};
    my $My = $self->{My};
		my $hook = sub {
			$mask->prepare( field => $field, file => $args{file}, Mx => $Mx, My => $My );
			1;
		};
		push @{$self->{pre_run}}, $hook;
	}
	elsif ($args{file}) { # we have just a file
		die "Optics.pm::add_opld -> ERROR: no valid file specified for opld element, extension must be \'.opd\'\n" 
                         unless $args{file} =~ /\.opd\s*$/;
	}
	else { die "Optics.pm::add_opld -> ERROR: opld element misses var or file\n" }

	$args{$_} ||= '0' for qw/xoff yoff/;
	$self->{script} .= "opld\n@args{'file','xoff','yoff'}\n";
}

sub add_mirror {
	my ($self, %args) = @_;
	die no_fork('mirror') if $self->{_in_fork} and $args{fork};
	
	(@args{'R','T','A'}) = three_way( @args{'R','T','A'} );
	
	for (qw/xr yr/) { # beam tilt aplied twice for mirror
		$args{$_} *= 2 if defined $args{$_};
	}
	# Set mirror parts	
	my @parts = ( ['power', c => $args{R}] );
	if (defined $args{r}) {
		$args{f} = $args{r}/2; # f = r/2
		push @parts, ['lens', %args];
	}

	# Check fork
	if ($args{fork}) { # outcoupling
		$self->{continue} = \@parts;
		$self->add_fork();
		$self->add_power(c => $args{T});
	}
	else {
		$self->add_config(@parts);
	}
}

sub add_xmirror {
	my ($self, %args) = @_;
	my $ec = 1.60217662e-19;    # [C], electron charge
	my $cvac = 299792458.0;     # [m/s], speed of light in vacuum
	my $h = 6.62607004e-34;     # [m^2 kg/s], Planck's constant
	my $pi = 3.141592653589793; # number Pi
  my $keV = 1000.0*$ec;       # keV
	die no_fork('xmirror') if $self->{in_fork} and $args{fork};
	for (qw/chi00_im chi0H_im chiH0_im/) { # set imaginary part to zero if not defined
		$args{$_} ||= 0.0;
	}
	for (qw/d E_H Theta/) {
		die "Optics.pm::add_xmirror -> Error: required input not specified : $_\n" unless $args{$_};
	}
	
	$args{action} ||= 'r';      # default action is to calculate the reflectivity. In case of a fork we need also to calculate the transmission
	$args{Theta}  ||= 0.0;      # set angle of incidence equal to zero if not specified
	# Check fork
	if ($args{fork}) { #outcoupling
		# store command to execure after ending fork
		$args{fork}=0;
		my @parts = ( ['xmirror', %args] ); 
		$self->{continue} = \@parts;
		$self->add_fork();
		$args{action} = $args{action} eq 'r' ? 't' : 'r';
	} 
	$self->{script}.="xmirror\n@args{'action','d','Theta','E_H','chi00_re','chi00_im','chi0H_re','chi0H_im','chiH0_re','chiH0_im'}\n";
}

sub three_way { # we only use R and T
	my ($R, $T, $A) = @_;
	if (! defined $R) {
		$A ||= 0;
		$T ||= 0;
		$R = 1 - $A - $T;
	}
	elsif (! defined $T) {
		$A ||= 0;
		$T = 1 - $R - $A;
	}
	elsif (! defined $A) {
		$A = 0;
	}
	elsif (defined $A) {
		my $total = $R + $T + $A;
		warn "WARNING: $R + $T + $A != 100%\n"
			unless 0.9 < $total and $total < 1 ;
	}
	return ($R, $T, $A);
}

sub add_fork {
	my $self = shift;
	die no_fork('fork') if $self->{_in_fork};
	$self->{_in_fork} = 1;
	$self->{Mxbuf} = $self->{Mx};
	$self->{Mybuf} = $self->{My};
	$self->{script} .= "fork\n1\n";
}

sub add_end_fork {
	my $self = shift;
	$self->{script} .= "end_fork\n1\n";
	$self->{Mx} = $self->{Mxbuf};
	$self->{My} = $self->{Mybuf};
	$self->add_config(@{delete $self->{continue}})
		if defined $self->{continue};
	$self->{_in_fork} = 0;
}

sub add_dump {
	my ($self, %args) = @_;
	$args{var} =~ s/^\$//;
	my $i = ++$self->{dumps};
	$self->{script} .= "dump\n$i\n";
	my $name = sprintf 'optics.dump%02u.dfl', $i;
	my $file = Physics::OPC::FieldFile->new($name);
	Physics::OPC->export($args{var} => $file);
	$file->{optics} = {
		Mx => $self->{Mx},
		My => $self->{My},
		name => $name
	};
	push @{$self->{dumpfiles}}, $file;
	if ($include_Ey) {
	  # need to dump Ey component as well
	  $args{var_y} = $args{var}.'_y';
	  $name = sprintf 'optics.dump%02u_y.dfl', $i;
	  $file = Physics::OPC::FieldFile->new($name);
	  Physics::OPC->export($args{var_y} => $file);
    $file->{optics} = {
		  Mx => $self->{Mx},
		  My => $self->{My},
		  name => $name
	  };
	  push @{$self->{dumpfiles}}, $file;
	}; 
}

sub add_cross{
	my($self, %args) = @_;
    $args{var} ||= 'no_perl_output';
    $args{offset} ||= 0;
	$args{filename} ||= 'no_output_file';
	$args{dir} ||= 'x'; 
	my $command = "$args{dir}cross";
	$args{dir} eq 'x' ?  ($nxcross += 1) :  ($nycross += 1);		 
	$self->{script} .= "$command\n@args{'var','offset','filename'}\n";
}

sub add_stat{
	my($self, %args) = @_;
	$args{var} ||= 'no_perl_output';
    $args{offset} ||= 0;
	$args{filename} ||= 'no_output_file';
	$nstat += 1;		
	$self->{script} .= "stat\n@args{'var','filename'}\n";
}

sub add_total{
	my($self, %args) = @_;
	$args{var} ||= 'no_perl_output';
    $args{filename} ||= 'no_output_file';
	$ntotal += 1;		
	$self->{script} .= "total\n@args{'var','filename'}\n";
}

sub add_fluence{
	my($self, %args) = @_;
	$args{var} ||= 'no_perl_output';
    $args{filename} ||= 'no_output_file';
	$nfluence += 1;		
	$self->{script} .= "fluence\n@args{'var','filename'}\n";
}

sub add_spectrum{
	my($self, %args) = @_;
	$args{var} ||= 'no_perl_output';
  $args{filename} ||= 'no_output_file';
  $args{x} ||= '-';
  $args{y} ||= '-';
	$nspectrum += 1;		
	$self->{script} .= "spectrum\n@args{'var','filename','x','y'}\n";
}

sub add_waveguide {
    my ($self, %args) = @_;
	$self->{_waveguide} += 1;
    die "Optics.pm::add_waveguide -> Length of waveguide needs to be defined\n" unless $args{'L'};
    $args{$_} ||= '0' for qw/xoff yoff r dx dy/;
    $args{$_} ||= 1 for qw/n_max m_max/;
	$args{type} ||= 'cyl';    # possible values are cyl and rect
	die "Optics.pm::add_waveguide -> r needs to be > 0 for type='cyl'\n" if ($args{'type'} eq 'cyl') &&  ($args{'r'}<=0);
	die "Optics.pm::add_waveguide -> dx,dy need to be > 0 for type='rect'\n" if ($args{'type'} eq 'rect')  
	    and (($args{'dx'}<=0) or ($args{'dy'}<=0)); 
	$self->{script} .= "waveguide\n@args{'type','n_max','m_max','r','dx','dy','xoff','yoff','L'}\n";
}
	
=item C<write_script(FILE)>

Write a script corresponding to the current config to FILE.

=cut

sub write_script {
	my ($self, $file) = @_;
	open SCRIPT, ">$file" or croak "Optics.pm::write_script -> Could not write to $file";
	print SCRIPT $self->{script};
	close SCRIPT or croak "Optics.pm::write_script -> Could not write to $file";
}

=item C<run()>

Run the optical simulation.

=cut

sub run {
	my $self = shift;
	croak "Optics.pm::run -> No input file for optics" unless defined $field;
	my $exe = defined $USE_MPI{optics} ? 'opc_optics_mpi' : 'opc_optics';
	my $optics = Physics::OPC->executable($exe);
	croak "Optics.pm::run -> Could not find $exe executable" unless defined $optics;

	# pre-run hooks
	# Each of these hooks is an anonymous subroutine
	# When they return "1" we remove them from the list,
	# else they will be called again next time we run
	for (@{$self->{pre_run}}) {
		my $re = $_->();
		$_ = undef if $re;
	}
	@{$self->{pre_run}} = grep defined($_), @{$self->{pre_run}};

  # get extension of input file (maybe .dfl or .sfl)
	
	$field->filename =~ /.*\.(\w*)$/;
	my $ext = $1;
  
  my $lfile = Physics::OPC->check_dir( $field->filename );
  my $llist = Physics::OPC->check_dir( $field->namelist );
	{ # some checks
		croak "Optics.pm::run -> Can not run Optics on file in other directory" unless defined $lfile;
		croak "Optics.pm::run -> No such file: '$lfile'" unless -f $lfile;
		croak "Optics.pm::run -> Parameters for fieldfile are not initialized\n" unless -f $llist;
  }
	
	# Create namelist
	my $namelist = Physics::OPC::Namelist->new('optics', 'optics.tmp.in');
	$namelist->copyfrom( $field->namelist );	
	$namelist->set_param(
		scriptfile => 'optics.tmp.script',
		infile     => $lfile,
		outfile    => "optics.tmp.$ext",
		Mx         => 1,
		My         => 1,
    nxcross     => $nxcross,
		nycross     => $nycross,
		ntotal      => $ntotal,
		nstat     	=> $nstat,
		nfluence    => $nfluence,
		nspectrum   => $nspectrum	
	);
  # check if input and output file are the same (e.g. input is output of a previous call to "run $optics")
  if ($lfile eq "optics.tmp.$ext") {
  	$lfile = "optics.tmp.previous.$ext";
		Physics::OPC::FieldFile->new("optics.tmp.$ext")->move($lfile);
		$namelist->set_param(infile=>$lfile);
	} else {
    unlink "optics.tmp.$ext";
	}
	# write script file
	$self->write_script('optics.tmp.script');

	# set parameters for dumpfiles
	my  ($mesh_x, $mesh_y) = $namelist->get_param('mesh_x', 'mesh_y');
	for my $dumpfile (@{$self->{dumpfiles}}) {
		$dumpfile->set_file($dumpfile->{optics}{name}); # reset file names
		$dumpfile->copyfrom('optics.tmp.in');           # copy namelist
		my $Mx = $dumpfile->{optics}{Mx};
		my $My = $dumpfile->{optics}{My};
		$dumpfile->set_param(mesh_x => $mesh_x * $Mx, Mx => $Mx,
		                     mesh_y => $mesh_y * $My, My => $My,
		                     field_next => 'none');
		my $fld_file =$dumpfile->{field}; # check if we need to modify field_next in the .param file
		unless ($fld_file =~ /_y\.\w+\s*?$/) { 
		  $fld_file  =~ s/(\.)(\w+\s*)?$/_y.$2/;  # look for partner file (y-polarisation)
		  $dumpfile->set_param(field_next => $fld_file) if $include_Ey;
		}
	}
	
	# Run command
	# dump files are written by direct access methods. This may lead to corrupted
  # files if number of slices or number of grid points changes. Therefore unlink
	# dump files before starting optics run
    for my $dumpfile (@{$self->{dumpfiles}}) {
	    unlink $dumpfile->filename;
	}
	
	# remove tempory files for data exchange between fortran codes and perl script
	unlink 'for2perl.tmp' if -e 'for2perl.tmp';
	
	my $cmd = defined($USE_MPI{optics}) ? $USE_MPI{optics}.' '.$optics : $optics;
	message("Running `$cmd` with input file 'optics.tmp.in'\n") if $VERBOSE;
	open OPT, '| '.$cmd.' optics.tmp.in' or croak "Optics.pm::run -> Starting `$cmd` failed";
	close OPT or croak "Optics.pm::run -> Executing `$cmd` failed";

	# Set output fieldfile
	$field = Physics::OPC::FieldFile->new("optics.tmp.$ext");
	$field->copyfrom('optics.tmp.in'); # copy namelist
	$field->set_param(
		mesh_x => $mesh_x * $self->{Mx},
		Mx => $self->{Mx},
		mesh_y => $mesh_y * $self->{My},
		My => $self->{My}  );
	if ($include_Ey) {
	 if ($field->get_param('field_next') ne 'none') {
       my $tfield = Physics::OPC::FieldFile->new("optics.tmp_y.$ext");	
       $tfield->copyfrom($field->{file});
       # set correct input and output file names
       (my $infile, my $outfile) = $tfield->get_param('infile', 'outfile');
       $infile =~ s/(\.\w+\s*)?$/_y.dfl/;
       $outfile =~ s/(\.\w+\s*)?$/_y.dfl/;
       $tfield->set_param(
         field_next=>'none',
         infile => $infile,
         outfile => $outfile );
     }
    }	
  # Read data from for2perl.tmp to make it available for the user  
    if ( -e 'for2perl.tmp') {
  	  open FIN, 'for2perl.tmp'; 
      # set counters
  	  my ($iline, @data, $cmd, $name, $nlines, $nx, $ny, $mesh_x, $mesh_y);
  	  # process one line at a time
   	  while (<FIN>) {
   		chomp;
   		s/^\s*//; # remove spaces at begin of line
  		# if not a number, line contains type of variable, name, nlines, nx, ny, mesh_x and mesh_y
  		unless (/^\s*-?\d/) {
  		  ($cmd, $name, $nlines, $nx, $ny, $mesh_x, $mesh_y) = split(/ +/, $_);
        $nlines = 1 if $nlines == 0;  # set default to one line of data if not specified
  		  @data=();
  		  $iline=0; # reset dataline counter
  	    }
  	    # if starting with a number, line contains data
  	    if (/^\s*-?\d/) {
  	  	  $iline++;
  	  	  push @data, [ split(/ +/, $_) ];
  	    }
  	    if ($iline == $nlines) {
  	  	  # all slices are read, store data in variable %optics_data
  	  	  if ($cmd eq 'stat') {
  	  		my %temp;
  	  		@temp{qw/tot xc yc sx sy sr w lambda_avg bw/} = @{$data[0]};  # create hash, stat command returns only one line of data
  	  	    $optics_data{$cmd.'_'.$name} = {cmd=>$cmd, name=>$name, nslices => $nlines, nx => $nx, 
  	  	    	                    ny => $ny, mesh_x =>$mesh_x, mesh_y => $mesh_y, data => { %temp } };
  	  	  } else { 	
  		      $optics_data{$cmd.'_'.$name} = {cmd=>$cmd, name=>$name, nslices => $nlines, nx => $nx, 
  		      	                    ny => $ny, mesh_x =>$mesh_x, mesh_y => $mesh_y, data => [ @data ] }; 		
  	      }  
  	    }
  	  }
  	  close FIN;
  	  	
  	  unlink 'for2perl.tmp';  # remove temporay file for exchanging data between optics.exe and perl script
    }
}

=back

=head1 COPYRIGHT

Copyright (C) 2006 J.G. Karssenberg, P.J.M. van der Slot, I.V.Volokhine

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; version 2 of the
License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

=cut

1;
