package Physics::OPC::Hermes;

use strict;
use vars qw/@PATH $field $VERBOSE %USE_MPI/;
use Carp;
use Physics::OPC::Namelist;

our @ISA = qw/Physics::OPC::Namelist/; # inherit from the namelist class

*field =   \$Physics::OPC::field;   # import $field
*VERBOSE = \$Physics::OPC::VERBOSE; # import $VERBOSE
*USE_MPI = \%Physics::OPC::USE_MPI; # import %USE_MPI
*message = \&Physics::OPC::message; # import message() routine

=head1 NAME

Physics::OPC::Hermes

=head1 DESCRIPTION

This module is a wrapper for the Hermes code that is required
to run the FEL simulation code Medusa writen by H.P. Freund.

This class inherits functions from L<Physics::OPC::Namelist>.
See the documentation with Medusa/Hermes for a parameter 
listing.

=head1 METHODS

=over 4

=item C<new(CONFIG_FILE)>

Simple constructor.

=cut

sub new {
	croak 'Hermes.pm::new -> usage: new(class, config)' unless @_ == 2;
	my ($class, $config) = @_;
	croak "Hermes.pm::new -> Missing hermes config file" unless defined $config;

	my @parts = File::Spec->splitdir($config);
	my $file = pop @parts; # remove directory part of file name
	my $self = bless {name => 'optics', file => $config, i=>1}, $class;

  # The file HermesConfig.in will be created at run time by Medusa. 
	# set to default unless already defined in input file
	if (-f $config) {
	  my ($hermesdfl, $Config_Hermes) = 
	  $self->get_param('optics', 1, 'hermesdflfile', 'Config_Hermes');
	  $self->set_param('optics', 1, hermesdflfile => 'HermesInput.dfl') unless defined $hermesdfl;
	  $self->set_param('optics', 1, Config_Hermes => $config) unless defined $Config_Hermes;
	}
	return $self;
}

=item C<run()>

Run the program with the given config on the global C<$field>.

=cut

sub run {
	my $self = shift;
    my $exe = defined($USE_MPI{hermes}) ? 'hermes_mpi' : 'hermes';
	my $hermes = Physics::OPC->executable($exe);
	# locating mpi version fails if it does not end on _mpi. Retry assuming it has the same name as
	# the non-mpi version
	$hermes = Physics::OPC->executable('hermes') unless defined $hermes;
	croak "Hermes.pm::run -> Could not find the Hermes executable" unless defined $hermes;
	
	if (defined $field) {
  	  my $lfile = Physics::OPC->check_dir($field->filename);
  	  print "Hermes -> filename = $lfile\n";
	  croak "Hermes.pm::run -> Can not run Hermes on a file in other directory" unless defined $lfile;
	  $self->set_param('optics', 1, hermesdflfile => $lfile);
	  my $m = $field->get_param('M');
	  # Carp "WARNING: M = $m for input field for Hermes" unless defined $m and $m == 1;
	}
	# Execute the binary
	
	my $cmd = defined($USE_MPI{hermes}) ?  $USE_MPI{hermes}.' '.$hermes : $hermes;
	my $Config_Hermes = $self->{file}; 
	message("Running `$cmd` with input file '$Config_Hermes'") if $VERBOSE;
	open HER, '|'.$cmd or croak "Hermes.pm::run -> Executing `$cmd` failed";
	close HER or croak "Hermes.pm::run -> Executing `$cmd` failed";

}

=item C<set_param(NAME, I, KEY => VALUE, ...)>

Set param in the I'th occurance of namelist NAME .

=cut

sub set_param {
		my $self = shift;
		my $name = shift;
		my $i = shift;
		die "Hermes.pm::set_param -> Error file '".$self->{file}."'to modify does not exist\n" 
			unless -f $self->{file};
		$self->{name} = $name;
		$self->{i} = $i;
		return $self->SUPER::set_param(@_); # dispatch to parent class
}

=item C<get_param(NAME, I, KEY, ...)>

Get param from the I'th occurance of namelist NAME .

=cut

sub get_param {
		my $self = shift;
		my $name = shift;
		my $i = shift;
		die "Hermes.pm::get_param -> Error file '".$self->{file}."'to read from does not exist\n" 
			unless -f $self->{file};

		$self->{name} = $name;
		$self->{i} = $i;
		return $self->SUPER::get_param(@_); # dispatch to parent class
}

=back

=head1 COPYRIGHT

Copyright (C) 2006 J.G. Karssenberg, P.J.M. van der Slot, I.V.Volokhine

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; version 2 of the
License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

=cut

1;
