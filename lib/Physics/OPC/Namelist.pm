package Physics::OPC::Namelist;

use strict;
use File::Copy qw/cp/;

=head1 NAME

Physics::OPC::Namelist

=head1 DESCRIPTION

Object to handle files containing  fortran namelists.

=head1 METHODS

=over 4

=item C<new(NAME, FILE, I)>

Constructor. NAME is the name of the namelist, this should be 'newrun'
for Genesis 1.3 config files or 'optics' for optics config files.
FILE is the file name of the namelist.

The variable I is intended for cases where you have multiple namelists
in the same file that have the same name. The value of I gives a counter
that tells which one we want. This value defaults to '1'.

=cut

sub new {
	my ($class, $name, $file, $i) = @_;
	$i ||= 1;
	bless {name => $name, file => $file, i => $i}, $class;
}

=item C<copyfrom(FILE)>

Make our file a copy of FILE.

=cut

sub copyfrom { 
 die "Namelist.pm::copyfrom -> Can not copy file, specified input file does not exists\n" 
         unless defined $_[1] and -f $_[1]; 
 open FIN, $_[1] or die "Namelist.pm::copyfrom -> Could not read file: $_[1]\n";
 my $input = join ('', <FIN>);
 close FIN;
 my @lines = split /[\r\n]/, $input;
 foreach (@lines) {
	# add comma behind namelist identifier unless identifier is alone on a line
	s/^(\s*[\$\&]\w*)/$1,/i unless /^\s*[\$\&]\w*\s*$/;
	# add comma before end of namelist indentifier unless identifier is alone on line
	s/([\$\&]end|\/)\s*$/, $1/i unless /^\s*([\$\&]end|\/)\s*$/;
	# remove comma at end of line
	s/,\s*$//;
 }
 my @output= map {split /,/, $_} @lines;   # split at comma
 open FOUT, ">$_[0]->{file}" or die "Namelist.pm::copyfrom -> Could not open file: ".$_[0]->{file}."\n";
 foreach (@output) {
	 chomp;
         s/^\s*//;                         # remove spaces at beginning of line
	 s/^(.)/  $1/ unless /^\s*[\$\&\/]/; # indent lines with parameters
	 # substitute double quote by single quote to delimited character variables
         s/"/'/g;       #'"
         # put single quotes around character string if not present
         s/=\s*([a-zA-Z_]+)/='$1'/;
         # use / as end of namelist block indentifier
	 s/[\$\&]end/\//;
	 print FOUT "$_\n";
 }
 close FOUT;
}

=item C<delete()>

Clean up any previous file by the same name.

=cut

sub delete { unlink $_[0]->{file} }

=item C<set_param(KEY => VALUE, ...)>

=cut

# This function reads the complete namelist into memory and modifies it
# then the complete file is written again.

sub set_param {
	my ($self, @data) = @_;
	my ($file, $name) = @$self{'file','name'};
  	$$self{i} ||= 1;
	my ($head, $body, $foot);
	my ($state, $i) = (0, 0);
	if (-f $file) {
		open IN, $file or die "Namelist.pm::set_param -> Could not read file: $file\n";
		while (<IN>) {
			if ($state == 0) {
				if (/^\s*[\$\&]$name\b/i) { # start reading after "$name"
					$i++;
					$state=1 if $i == $$self{i};
				}
				if ($state == 1) { $body .= $_ }
				else             { $head .= $_ }
 			}
			elsif ($state == 1) {
				$state = 2 if /^\s*([\$\&]end|\/)\s*$/i; # stop after "$end"
				$body .= $_;
			}
			else { $foot .= $_ }
		}
		close IN;
	}

	$body = " \$$name\n \$end\n" unless defined $body;
	$head ||= '';
	$foot ||= '';
	while (@data) {
		my ($key, $value) = splice @data, 0, 2;
		$value =~ s/\s*,\s*$//;		# remove comma at end if present
		$value =~ s/"/'/g;		# replace double quotes with single quotes if present
		$value = "\'$value\'"
			unless $value =~ /^(['"]).+\1$/                      # allready quoted
			    or $value =~ /[+-]?\d+(\.\d+)?([eEdD][+-]\d+)?$/ # number
                            or $value =~ /.true.|.false./i;                  # logical
		unless ($body =~ s/^(\s*(?i:$key)\s*=\s*).*$/$1$value/m) {
			# search and replace, else add to end
			$body =~ s/^(\s*[\$\&]end|\s*\/)/ $key = $value\n$1/im;
		}
	}

	open OUT, '>',$file or die "Namelist.pm::set_param -> Could not write file: $file\n";
	print OUT $head,$body,$foot;
	close OUT;
}

=item C<get_param(KEY, ...)>

Returns list of values.

=cut

# Read in file line by line till end of namelist

sub get_param {
	my $self = shift;
	my ($file, $name) = @$self{'file','name'};
 	 $$self{i} ||= 1;
	return () unless -f $file;
	
	my ($state, $i) = (0, 0);
	my %values;
	my @keys = map lc($_), @_;
	my $regex = join '|', map quotemeta($_), @keys; # compile pattern
	open IN, "< $file" or die "Namelist.pm::get_param -> Could not read file: $file\n";
	while (<IN>) {
		if ($state == 0) {
			next unless /^\s*[\$\&]$name\b/i; # start reading after "$name"
			$i++;
			$state=1 if $i == $$self{i};
		}
		else { # state == 1
			/^\s*([\$\&]end|\/)\s*$/i and last; # return after "$end"
			/^\s*($regex)\s*=\s*(.+)/i or next; # match keys we are looking for
			my ($key, $val) = ($1, $2);
			$val =~ s/\s*,?\s*$//; # remove trailing "," and whitespace
			$val =~ s/([+-]?\d+)(\.\d+)?[dD]([+-]\d+)?$/$1$2e$3/; # change [dD] into [e] to make it a number
			$val =~ s/^(['"])\s*(.*?)\s*\1$/$2/; # "' remove quotes if any and whitespace
			$values{lc($key)} = $val;
		}
	}
        close IN;
	return @values{@keys};
}

=back

=head1 LIMITATIONS

This modules assumes that each variable in a namelist is defined on a separate line.
Namelists need to END with an explicit "end" statement, not with a '/'.

=head1 COPYRIGHT

Copyright (C) 2006 J.G. Karssenberg, P.J.M. van der Slot, I.V.Volokhine

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; version 2 of the
License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

=cut

1;
