package Physics::OPC::Genesis;

use strict;
use vars qw/@PATH $field $VERBOSE %USE_MPI/;
use Carp;
use Physics::OPC::Namelist;

our @ISA = qw/Physics::OPC::Namelist/; # inherit from the namelist class

*field =   \$Physics::OPC::field;   # import $field
*VERBOSE = \$Physics::OPC::VERBOSE; # import $VERBOSE
*USE_MPI = \%Physics::OPC::USE_MPI; # import %USE_MPI
*message = \&Physics::OPC::message; # import message() routine

=head1 NAME

Physics::OPC::Genesis

=head1 DESCRIPTION

This module is a wrapper for the Genesis 1.3 program,
see L<http://corona.physics.ucla.edu/~reiche/>.

This class inherits functions from L<Physics::OPC::Namelist>.
See the Genesis 1.3 manual for a parameter listing.

=head1 METHODS

=over 4

=item C<new(CONFIG_FILE)>

Simple constructor.

=cut

sub new {
	croak 'Genesis.pm::new -> usage: new(class, config)' unless @_ == 2;
	my ($class, $config) = @_;
	croak "Genesis.pm::new -> Missing genesis config file" unless defined $config and -f $config;

	my @parts = File::Spec->splitdir($config);
	my $file = pop @parts; # remove directory part of file name
	my $self = bless {name => 'newrun', file => "$file.tmp", i => 1}, $class;

	$self->copyfrom($config);
	$self->set_param(outputfile => 'genesis.tmp.out', idmpfld => 1);
	
	return $self;
}

=item C<run()>

Run the program with the given config on the global C<$field>.

=cut

sub run {
	my $self = shift;
	
	my $exe = defined $USE_MPI{genesis} ? 'genesis_mpi' : 'genesis';
	my $genesis = Physics::OPC->executable($exe);
	croak "Genesis.pm::run -> Could not find the $exe executable\n" unless defined $genesis;

	# Check input field
	if (defined $field) {
		my $lfile = Physics::OPC->check_dir($field->filename);
		croak "Genesis.pm::run -> Can not run Genesis on file in other directory"
		      unless defined $lfile;
		if ($lfile =~ "genesis.tmp.out.dfl") {
			#genesis has been run immediately before another call to genesis
			#genesis opens first genesis.tmp.out.dfl for output before it reads
			#the fieldfile. This produces an error by genesis 1.3. Hence, copy
			#the associate field to a different name
			$lfile = "genesis.tmp.2.out.dfl";
			$field->move($lfile);
		}	
		$self->set_param(fieldfile => $lfile);
		my ($m, $mx, $my) = $field->get_param('M','Mx', 'My');
		carp "Genesis.pm::run -> WARNING: M = $m for input field for Genesis"
			if defined $m and abs($m-1) > 1e-5;
		carp "Genesis.pm::run -> WARNING: Mx = $mx for input field for Genesis"
			if defined $mx and abs($mx-1) > 1e-5;
        carp "Genesis.pm::run -> WARNING: My = $my for input field for Genesis"
			if defined $my and abs($my-1) > 1e-5;
	}
	# Execute the binary
	unless (defined $field && ($field->filename =~ "genesis.tmp.out.dfl")) { 
		#propagation between successive calls to run genesis, so it is save 
		#to delete the "genesis.tmp.out.dfl" file.
		unlink "genesis.tmp.out.dfl";		# just to be sure, due to direct access 
		unlink "genesis.tmp.out.param";		# file can become corrupted if current 
											# run contains a smaller set of slices.
	}										# or different grid size
	my $cmd = defined($USE_MPI{genesis}) ? $USE_MPI{genesis}.' '.$genesis : $genesis ;
#	open GEN, ">genesis.tmp.stdin" or croak "Genesis.pm::run -> cannot open genesis.tmp.std.in for writing";
#	print GEN "$self->{file}\n";
#	close GEN;
	message("Running `$cmd` with input file '$self->{file}'") if $VERBOSE;
	open GEN, '| '.$cmd or croak "Genesis.pm::run -> Starting `$cmd` failed";
	print GEN "$self->{file}\n";
	close GEN or croak "Genesis.pm::run -> Executing `$cmd` failed";
  # Set output field and update field param
  my ($outfile) = $self->get_param('outputfile');
	$field = Physics::OPC::FieldFile->new("$outfile.dfl");
	my ($itdp, $xlamd, $zsep, $ncar) = $self->get_param(qw/ITDP XLAMDS ZSEP NCAR/);
	my $mesh = $self->_get_mesh($outfile);
	my %param = (
	    # starting from version 0.7.5, variables npoints_x, npoints_y, mesh_x and mesh_y are 
	    # introduced. Here we also set npoints and mesh to be compatible with earlier releases.
		mesh => $mesh,
		mesh_x => $mesh,
		mesh_y => $mesh,
		lambda => $xlamd,
		zsep => $zsep,
		npoints => $ncar,
		npoints_x => $ncar,
		npoints_y => $ncar
	);
	if ($itdp) { $field->set_nslices($ncar, $ncar)}
	else       { $param{nslices} = 1       }
	$field->set_param(%param);
}

sub _get_mesh { # Get meshsize from .out file
	my ($self, $file) = @_;
	my $mesh;
	open OUT, $file or die "Genesis.pm::get_mesh -> Could not open file: $file\n";
	while (<OUT>) {
		/^\s*(\S+)\s+meshsize\s*$/i or next;
		$mesh = $1;
		last
	}
	close OUT;
	die "Genesis.pm::get_mesh -> Could not get meshsize from: $file\n" unless defined $mesh;
	return $mesh;
}

=back

=head1 COPYRIGHT

Copyright (C) 2006 J.G. Karssenberg, P.J.M. van der Slot, I.V.Volokhine

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; version 2 of the
License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

=cut

1;
