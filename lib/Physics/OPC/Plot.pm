package Physics::OPC::Plot;

use strict;
use Env '$PWD';
use Carp;
use File::Copy qw/cp mv/;
use Physics::OPC qw/field/;
use Physics::OPC::FieldFile;

# Define Export interface
our @ISA = qw/Exporter/;
our @EXPORT = qw/
  $VERBOSE
	get_header extract_cross_vs_slice extract_cross_vs_roundtrip
	extract_field extract_total extract_phase extract_fluence
/; # these can be used directly from the script

our $VERBOSE = 0;	# Global, for extra debug info

=head1 NAME

Physics::OPC::Plot

=head1 DESCRIPTION

Module that contains routines to extract data from binary (.dfl, electric field 
distribution) or ASCII (.txt or .dat, intensity or power distribution) and plot it 
using GNUPlot.

This module is not object orientated and the public routines contained in here are 
exported to the script.

=head1 METHODS

=over 4

=item C<get_header(FILE, %PARAM)>

Retrieves the header from FILE and returns it in the hash %PARAM
In case of a binary file, the header is retrieved from the associated
.param file.

=cut

sub get_header {
	my ($datafile, %param) = @_;
	if ($datafile =~ /\.(dfl|fld|sfl)$/) {		# binary datafile get info from .param
		my $f = field (file => $datafile);
		my @par = qw/npoints_x npoints_y nslices lambda mesh_x mesh_y zsep mesh npoints/;
		@param{@par} = $f->get_param(@par);
		$param{mesh_x} = $param{mesh} unless defined $param{mesh_x};  # to be compatible with earlier releases
		$param{mesh_y} = $param{mesh} unless defined $param{mesh_y};
		$param{npoints_x} = $param{npoints} unless defined $param{npoints_x};
		$param{npoints_y} = $param{npoints} unless defined $param{npoints_y};
		return %param;
	}
	else {						# ASCII datafile get info from header in file
		open IN, $datafile or die "Plot.pm::get_header -> Can not open file $datafile, $!\n";
		while (<IN>) {
			next unless /\S/; # skip empty lines
			last unless /^#/;	# no comment, data found, exit loop
			s/[\r\n]+$//;			# remove CR and LF on both unix and windows
			s/^#\s*//;
			my ($key, $val) = split /\s*:\s*/, $_, 2;
		 	$param{$key} = $val;
		}
		$param{mesh_x} = $param{mesh} unless defined $param{mesh_x};  # to be compatible with earlier releases
		$param{mesh_y} = $param{mesh} unless defined $param{mesh_y};
		$param{npoints_x} = $param{npoints} unless defined $param{npoints_x};
		$param{npoints_y} = $param{npoints} unless defined $param{npoints_y};
		close IN;
		return %param;
		
	}
}

=item C<extract_cross_vs_slice( )>

Extracts data to plot from either a .dfl or .(txt|dat) file. Parameters are passed through a hash

The hash PARAM contains the following parameters that control the type of plot

=over 4

=item plottype    => Must by "cross_slice" for this function

=item first_slice => First slice to plot, defaults to 1

=item last_slice  => Last slice to plot, defaults to the last slice

=item roundtrip   => Plot data for ROUNDTRIP. If not defined, plot the first roundtrip

=item file_in	  => Name of input file

=item file_out    => Name of output file

=back

on return the hash contains also the header of the datafile as well as parameters for plotting the data

=cut

sub extract_cross_vs_slice {
	# routine to extract cross version slice number for a specific roundtrip
	# if roundtrip is not specified it defaults to the first
	
	my %param = @_;

	die "Plot.pm::extract_cross_vs_slice -> Can not extract plot data from file, no input file specified\n" 
		unless defined $param{file_in};

	$param{direction} ||= 'x';
	my ($file_in, $file_out) = @param{'file_in', 'file_out'};

	# temporary files
	my $tmpdatafile = File::Spec->catfile(File::Spec->tmpdir, "$file_out.tmp");
  if ($file_in =~ /\.(dfl|fld|sfl)$/) { # binary input file, convert first to ascii
		my $f = field ( file => $file_in );
		$f->cross($param{direction}, ">$tmpdatafile");
  } else {
		$tmpdatafile = $file_in;
	}

	# get header of datafile
	%param = (get_header($tmpdatafile), %param);
 		
	if ($VERBOSE == 1) { # print extra data to screen for debugging
		print "Header from $tmpdatafile\n";
		print "$_: $param{$_}\n" for sort keys %param;
	}
	die "Plot.pm::extract_cross_vs_slice -> Datafile $file_in does not support plotting the cross section for a range of slices, $!\n"
		unless $param{type} =~ /[cross|phase_1D]/;

	open IN, $tmpdatafile;
	$file_out =~ s/^>*//;
	open OUT, ">$file_out" or die "Plot.pm::extract_cross_vs_slice -> Can not open output file $file_out for plot data, $!\n";

	my $nslices = $param{nslices};	# number of slices in datafile
	my $npoints = ($param{direction} eq 'x') ? $param{npoints_x} : $param{npoints_y};	# number of grid points
	my $dgrid = ($param{direction} eq 'x') ? $param{mesh_x} : $param{mesh_y};	# distance between grid points
	my $nhalf = int ($npoints % 2) ? ($npoints+1) / 2 : $npoints / 2;
	my $ds = $param{zsep} * $param{lambda};	# distance between two slices

	my $roundtrip = (defined $param{roundtrip}) ? $param{roundtrip} : 1;
	my $first_slice; my $last_slice;
	if (defined $param{slice}) {
		$first_slice = $last_slice = $param{slice}
	}
	else {
		$first_slice = $param{first_slice} || 1;
		$last_slice =  $param{last_slice}  || $nslices;
		$last_slice =  $nslices if $last_slice > $nslices;  # check if specified last slice is not larger than nslices
	}

	my $i_max = 0.0;		 # used to track maximum intensity for scaling axis
	my ($state, $ncurves, $dpoints) = (0, 0, 0);
	my ($islice, $iroundtrip, $ipoint) = (1, 1, 0);
	while (<IN>) {
		if ($state == 0) { # start of file, skip header, comments or empty lines
			next unless /^\s*-?\d/;
			$state = 1;
		}
		if ($state == 1) { # read data and determine if it needs to be plotted
			if (!/(^\s*-?\d)/) { # end of column reached, adjust counters
				$islice += 1;
				if ($islice == $nslices +1) {
					$iroundtrip++;
					$islice = 1;
				}
				$state = 0;
				next;
			}
			$state = 2 if ($islice >= $first_slice) and ($islice <= $last_slice);
			$state = 1 unless $iroundtrip == $roundtrip; #skip data if not the right roundtrip
		}
		if ($state == 2) { # data to be plotted, write to file_out
			if (!/^\s*-?\d/) { # reached end of column
				$islice++;
				$ipoint = 0;
				if ($islice == $nslices+1) {
					$iroundtrip++; 	 
					$islice = 1;
				}
				$state = 0;	# skipt comments or empty lines that separate datasets
				$ncurves +=1;
				print OUT "\n";
			}
			else {
				$dpoints++;
				$ipoint++;
				my $x = ($ipoint -$nhalf) * $dgrid * 1000;
				my $y = ($islice - 1) * $ds * 1000000;
				chomp;
				my $p = $param{type} eq 'phase_1D' ? $_ : $_*1e-4;	# convert from W/m² to W/cm² if intensity
				$i_max = $p if $i_max<$p;				            # check for maximum intensity								
				print OUT "$x\t$y\t$p\n";
			}
		}
	}
	$param{zscale} = nice_scale($i_max) unless $param{type} eq 'phase_1D';   # $i_max is in W/cm²	
	close IN;
	close OUT;

	print "Plot.pm::extract_cross_vs_slice -> Warning, number of datapoints extracted from $file_in for plotting is $dpoints.\n".
		"Expected ".$npoints * ($last_slice - $first_slice + 1)." points to plot.\n" 
		unless $dpoints == $npoints * ($last_slice - $first_slice + 1);
	
	print "Plot.pm::extract_cross_vs_slice -> Extracted $dpoints datapoints from file $file_in for plotting\n" if $VERBOSE == 1;
	
	# set parameters for plotting
	if ($first_slice == $last_slice) {
		$param{title} .= "$param{direction}-cross section for roundtrip $roundtrip and slice $first_slice";
	}
	else {
		$param{title}  .= "$param{direction}-cross section versus s for roundtrip $roundtrip";
	}
	$param{xtitle} = "$param{direction} (mm)";
	$param{ytitle} = "s (micron)";
	$param{ztitle} = "I (".prefactor($param{zscale})."W/cm^2)";
	$param{ztitle} = "Phase (rad)" if $param{type} eq 'phase_1D';
	$param{plotstyle} = ($ncurves > 1) ? "3D" : "2D";
	return %param;
}

=item C<extract_fluence( )>

Extracts data to plot from a .(txt|dat) file. Parameters are passed through a hash

The hash PARAM contains the following parameters that control the type of plot

=over 4

=item plottype    => Must by "fluence" for this function

=item file_in	    => Name of input file

=item file_out    => Name of output file

=back

on return the hash contains also the header of the datafile as well as parameters for plotting the data

=cut

sub extract_fluence {
	# routine to extract cross version slice number for a specific roundtrip
	# if roundtrip is not specified it defaults to the first
	
	my %param = @_;

	die "Plot.pm::extract_fluence -> Can not extract plot data from file, no input file specified\n" 
		unless defined $param{file_in};

	my ($file_in, $file_out) = @param{'file_in', 'file_out'};

	# temporary files
	my $tmpdatafile = File::Spec->catfile(File::Spec->tmpdir, "$file_out.tmp");
  if ($file_in =~ /\.(dfl|fld|sfl)$/) { # binary input file, convert first to ascii
		my $f = field ( file => $file_in );
		$f->fluence(">$tmpdatafile");
  } else {
		$tmpdatafile = $file_in;
	}

	# get header of datafile
	%param = (get_header($tmpdatafile), %param);
 		
	if ($VERBOSE == 1) { # print extra data to screen for debugging
		print "Header from $tmpdatafile\n";
		print "$_: $param{$_}\n" for sort keys %param;
	}
	die "Plot.pm::extract_fluence -> Datafile $file_in does not support plotting the intensity summed over slices, $!\n"
		unless $param{type} =~ /fluence/;

	open IN, $tmpdatafile;
	$file_out =~ s/^>*//;
	open OUT, ">$file_out" or die "Plot.pm::extract_fluence -> Can not open output file $file_out for plot data, $!\n";

	my $nslices = 1;	# number of slices in datafile for this type of plot
	my ($npoints_x, $npoints_y) = ($param{npoints_x}, $param{npoints_y}); # number of grid points
	my ($dgrid_x, $dgrid_y)     = ($param{mesh_x},    $param{mesh_y});	  # distance between grid points
	my $nhalf_x = int ($npoints_x % 2) ? ($npoints_x+1) / 2 : $npoints_x / 2;
	my $nhalf_y = int ($npoints_y % 2) ? ($npoints_y+1) / 2 : $npoints_y / 2;
#	my $cvac = 299792458.0;  # speed of light in vacuum
#	my $conv = $param{nslices} * $param{zsep} * $param{lambda} / $cvac;	# conversion factor to E/m^2
	
	my $roundtrip = (defined $param{roundtrip}) ? $param{roundtrip} : 1;

	my $i_max = 0.0;		 # used to track maximum intensity for scaling axis
	my ($state, $ncurves, $dpoints) = (0, 0, 0);
	my ($iroundtrip, $ix, $iy) = (1, 0, 1);
	while (<IN>) {
		if ($state == 0) { # start of file, skip header, comments or empty lines
			next unless /^\s*-?\d/;
			$state = 1;
		}
		if ($state == 1) { # read data and determine if it needs to be plotted
			if (!/(^\s*-?\d)/) { # end of column reached, adjust counters
				$iy++;
				if ($iy == $npoints_y +1) {
					$iroundtrip++;
					$iy = 1;
				}
				$ix = 0;
				$state = 0;
				next;
			}
			$state = 2; 
			$state = 1 unless $iroundtrip == $roundtrip; #skip data if not the right roundtrip
		}
		if ($state == 2) { # data to be plotted, write to file_out
			if (!/^\s*-?\d/) { # reached end of column
			  if ($ix == $npoints_x) {
					$iy++; 	 
					$ix = 0;
				}
				if ($iy == $npoints_y+1) { 
			  	$iroundtrip++;
			  	$iy = 1;
			  }	
				$state = 0;	# skipt comments or empty lines that separate datasets
				$ncurves +=1;
				print OUT "\n";
			}
			else {
				$dpoints++;
				$ix++;
				my $x = ($ix -$nhalf_x) * $dgrid_x * 1000;
				my $y = ($iy -$nhalf_y) * $dgrid_y * 1000;
				chomp;
				my $p = $_*1e-4;	               # convert from J/m² to J/cm² if intensity
				$i_max = $p if $i_max<$p;				 # check for maximum intensity								
				print OUT "$x\t$y\t$p\n";
			}
		}
	}
	$iroundtrip--;                         # counter is one too high due to logic of loop reading data file
	$param{zscale} = nice_scale($i_max);   # $i_max is in J/cm²	
	close IN;
	close OUT;

	print "Plot.pm::extract_fluence -> Warning, number of datapoints extracted from $file_in for plotting is $dpoints.\n".
		"Expected ".$npoints_x*$npoints_y*$iroundtrip." points to plot.\n" 
		unless $dpoints == $npoints_x*$npoints_y*$iroundtrip;
	
	print "Plot.pm::extract_fluence -> Extracted $dpoints datapoints from file $file_in for plotting\n" if $VERBOSE == 1;
	
	# set parameters for plotting
	$param{title} .= "Fluence (intensity integrated over time)";
	$param{xtitle} = "x (mm)";
	$param{ytitle} = "y (mm)";
	$param{ztitle} = "F (".prefactor($param{zscale})."J/cm^2)";
	$param{plotstyle} = "3D";
	return %param;
}

		
=item C<extract_cross_vs_roundtrip( )>

Extracts data to plot from either a .dfl or .(txt|dat) file. Parameters are passed through a hash

The hash PARAM contains the following parameters that control the type of plot

=over 4

=item plottype        => Must by "cross_roundtrip" for this function

=item first_roundtrip => First roundtrip to plot, defaults to 1

=item last_roundtrip  => Last roundtrip to plot, defaults to all roundtrips

=item slice           => Plot data for slice SLICE. If not defined, plot the centre slice

=item file_in         => Name of input file

=item file_out        => Name of output file

=back

on return the hash contains also the header of the datafile as well as parameters for plotting the data

=cut

sub extract_cross_vs_roundtrip {
	# subroutine to extract data from ASCII files to plot a particular slice number versus roundtrip
	# if no slice number is specified it extracts the data for the centre slice 
	# .dfl files do not contain field data for different roundtrips. Therefore this function only
	# accepts .txt or .dat files as input.
	
	my %param = @_;

	my ($file_in, $file_out) = @param{'file_in', 'file_out'};

	die "Plot.pm::extract_cross_vs_roundtrip -> Can not extract plot data from file, no input file specified\n" unless defined $param{file_in};
	die "Plot.pm::extract_cross_vs_roundtrip -> Plottype = cross_roundtrip: .dfl files do not support this type of plotting data\n" 
		unless $param{file_in} =~ /\.(txt|dat)$/;

	# get header of datafile
	%param = (get_header($file_in), %param);
 	
	if ($VERBOSE == 1) { # print extra data to screen for debugging
		print "Header from $file_in\n";
		print "$_: $param{$_}\n" for sort keys %param;
	}

	die "Plot.pm::extract_cross_vs_roundtrip -> Datafile $file_in does not support plotting the cross section for a range of roundtrips, $!\n"
		unless $param{type} =~ /[cross|phase_1D]/;

	open IN, $file_in;
	$file_out =~ s/^>*//;
	open OUT, ">$file_out" or die "Plot.pm::extract_cross_vs_roundtrip -> Can not open output file $file_out for plot data, $!\n";

	my $nslices = $param{nslices};	# number of slices in datafile
	my $npoints = ($param{direction} eq 'x') ? $param{npoints_x} : $param{npoints_y};	# number of grid points
	my $dgrid = ($param{direction} eq 'x') ? $param{mesh_x} : $param{mesh_y};	# distance between grid points
	my $nhalf = int ($npoints % 2) ? ($npoints+1) / 2 : $npoints / 2;

    my ($first_roundtrip, $last_roundtrip);
    if ($param{roundtrip}) {
      $first_roundtrip = $param{roundtrip};
      $last_roundtrip = $first_roundtrip;
    } else {
	  $first_roundtrip = $param{first_roundtrip} || 1;
	  $last_roundtrip =  $param{last_roundtrip}  || 0;
	}
	my $slice = $param{slice} || int ( ($nslices+1) / 2 ); 

	my $i_max = 0.0;		 # used to track maximum intensity for scaling axis
	my ($state, $ncurves, $dpoints) = (0, 0, 0);
	my ($islice, $iroundtrip, $ipoint) = (1, 1, 0);
	while (<IN>) {
		if ($state == 0) { # start of file, skip header or skip empty lines
			next unless /^\s*-?\d/;
			$state = 1;
		}
		if ($state == 1) { # read data and determine if it needs to be plotted
			if (!/(^\s*-?\d)/) { # end of column reached, adjust counters
				$islice += 1;
				if ($islice == $nslices +1) {
					$iroundtrip++;
					$islice = 1;
				}
				$state = 0;
				next;
			}
			$state = 2 if ($iroundtrip >= $first_roundtrip) and 
				( ($iroundtrip <= $last_roundtrip) or ($last_roundtrip == 0) );
			$state = 1 unless $islice == $slice; # skip data unless for the right slice
		}
		if ($state == 2) { # data to be plotted, write to file_out
			if (!/^\s*-?\d/) { # reached end of column
				$islice++;
				$ipoint = 0;
				if ($islice == $nslices+1) {
					$iroundtrip++; 	 
					$islice = 1;
				}
				$state = 0;	# skip any empty lines or comments between data
				$ncurves +=1;
				print OUT "\n";
			}
			else {
				$dpoints++;
				$ipoint++;
				my $x = ($ipoint -$nhalf) * $dgrid * 1000;
				my $y = $iroundtrip;
				chomp;
				my $p = $param{type} eq 'phase_1D' ? $_ : $_*1e-4;	# convert from W/m² to W/cm² if intensity
				$i_max = $p if $i_max<$p;				# check for maximum intensity				
				print OUT "$x\t$y\t$p\n";				
			}
		}
	}
	$param{zscale} = nice_scale($i_max) unless $param{type} eq 'phase_1D';   # $i_max is in W/cm²	
#	print "Maximum value = $i_max\n";
#	print "scale factor = $param{zscale}\n";
	close IN;
	close OUT;

	print "Plot.pm::extract_cross_vs_roundtrip -> Warning, number of datapoints extracted from $file_in for plotting is $dpoints.\n".
		"Expected ".$npoints * ($last_roundtrip - $first_roundtrip + 1)." points to plot.\n" 
		unless ( $dpoints == $npoints * ($last_roundtrip - $first_roundtrip + 1) ) or ($last_roundtrip == 0);
	
	print "Plot.pm::extract_cross_vs_roundtrip -> Extracted $dpoints datapoints from file $file_in for plotting\n" if $VERBOSE == 1;
	
	# set parameters for plotting
	if ($first_roundtrip == $last_roundtrip) {
		$param{title} .= "$param{direction}-cross section for slice $slice and roundtrip $first_roundtrip";
	}
	else {
		$param{title} .= "$param{direction}-cross section versus roundtrip for slice $slice";
	}
	$param{xtitle} = "$param{direction} (mm)";
	$param{ytitle} = "roundtrip";
	$param{ztitle} = "I (".prefactor($param{zscale})."W/cm^2)";
	$param{ztitle} = "Phase (rad)" if $param{type} eq 'phase_1D';
	$param{plotstyle} = ($ncurves > 1) ? "3D" : "2D";
	return %param;
}

=item C<extract_field( )>

Extracts data for plotting a two dimensional field distribution from a .dfl file. 
Parameters are passed through a hash.

The hash PARAM contains the following parameters that control the type of plot

=over 4

=item plottype        => Must by "field" for this function

=item slice           => Plot data for slice SLICE. If not defined, plot the centre slice

=item file_in	        => Name of input file

=item file_out        => Name of output file containing the data to plot

=back

on return the hash contains also the header of the datafile as well as parameters for plotting the data

=cut

sub extract_field {
	# subroutine to extract data from .dfl files to plot the two dimensional field distribution
	# for a particular slice number. If no slice number is specified it extracts the data for the centre slice 
	# Only .dfl files are accepted as input file.
		
	my %param = @_;

	my ($file_in, $file_out) = @param{'file_in', 'file_out'};

	die "Plot.pm::extract_field -> Can not extract plot data from file, no input file specified\n" unless defined $param{file_in};
	die "Plot.pm::extract_field -> plottype = field: warning, only .d(s)fl files support this type of plotting\n" unless $param{file_in} =~ /\.(dfl|fld|sfl)$/;
  
  $param{datatype} = 'temporal';
  $param{datatype} = 'spectral' if $param{file_in} =~ /\.sfl$/;

	# get header of datafile
	%param = (get_header($file_in), %param);
	
	my $nslices = $param{nslices};	# number of slices in datafile
	my $npoints_x = $param{npoints_x};	# number of datapoints along X direction
	my $npoints_y = $param{npoints_y};	# number of datapoints along Y direction
	my $nhalf_x = int ($npoints_x % 2) ? ($npoints_x+1) / 2 : $npoints_x / 2;
  my $nhalf_y = int ($npoints_y % 2) ? ($npoints_y+1) / 2 : $npoints_y / 2;
  my $dgrid_x = $param{mesh_x};
  my $dgrid_y = $param{mesh_y};
  
	my $slice = $param{slice} || int ( ($nslices+1) / 2); 
	
	my $cvac = 299792458.0;  # speed of light in vacuum
	my $dfreq = $cvac/($param{lambda}*$param{zsep}*$param{nslices});
	my $fc = $cvac/$param{lambda};
	my $freq;
	if ($nslices == 1) {
	  $freq = $fc;
  } else { 
	  $freq = $slice < ($nslices+1)/2 ? $fc + ($slice-1)*$dfreq : $fc -($nslices-$slice+1)*$dfreq;
	}
	my $lambda = 1.0e9*$cvac/$freq;  # wavelength in nm

	# temporary files
	my $tmpdatafile = File::Spec->catfile(
		File::Spec->tmpdir, "$file_out.tmp");

	my $f = field ( file => $file_in );
	$f->txt(">$tmpdatafile", $slice);  # only the right slice is extracted and converted to ascii data
        
 	if ($VERBOSE > 0) { # print extra data to screen for debugging
		print "Header from $file_in\n";
		print "$_: $param{$_}\n" for sort keys %param;
	}

	open IN, $tmpdatafile;
	$file_out =~ s/^>*//;
	open OUT, ">$file_out" or die "Plot.pm::extract_field -> Can not open output file $file_out for plot data, $!\n";

	my $i_max = 0.0;		 # used to track maximum intensity for scaling axis
	my ($state, $dpoints) = (0, 0);
	my ($islice, $iy, $ix) = (1, 1, 0);
	while (<IN>) {
		chomp;
		if ($state == 0) { # start of file, skip header or skip empty lines
			next unless /^\s*\d/;
			$state = 1;
		}
		if ($state == 1) { # data to be plotted, write to file_out
			if (!/^\s*\d/) { # reached end of column
				$iy++;
				$ix = 0;
				if ($iy == $npoints_y+1) {
					$islice++; 	 
					$iy = 1;
				}
				$state = 0;
				print OUT "\n";
			}
			else {
				$dpoints++;
				$ix++;
				my $x = ($ix - $nhalf_x) * $dgrid_x * 1000;
				my $y = ($iy - $nhalf_y) * $dgrid_y * 1000;
				chomp;
				my $p = $_*1e-4;								# convert from W/m² to W/cm²
				$p *= $cvac*1e9/($lambda*$lambda) if $param{datatype} eq 'spectral'; # convert to J/nm cm^2
				$i_max = $p if $i_max<$p;				# check for maximum intensity
				print OUT "$x\t$y\t$p\n";
			}
		}
	}
	$param{zscale} = nice_scale($i_max);   # $i_max is in W/cm²
#	print "Maximum value = $i_max\n";
#	print "scale factor = $param{zscale}\n";
	close IN;
	close OUT;

	print "Plot.pm::extract_field -> Warning, number of datapoints extracted from $file_in for plotting is $dpoints.\n".
		"Expected ". $npoints_x*$npoints_y ." points to plot.\n" 
		unless $dpoints == $npoints_x*$npoints_y ;
	
	print "Plot.pm::extract_field -> Extracted $dpoints datapoints from file $file_in for plotting\n" if $VERBOSE == 1;
	
	# set parameters for plotting
	if ($file_in =~ /\.sfl$/) {
		# file is in spectral domain, calculate wavelength of slice
		
	  $param{title}  .= "Field distribution for wavelength = ".sprintf("%.2f",$lambda)." nm";
	  $param{ztitle} = "Is (".prefactor($param{zscale})."J/nm cm^2)";
	} else {
    $param{title}  .= "Field distribution for slice $slice";
    $param{ztitle} = "I (".prefactor($param{zscale})."W/cm^2)";
  }
	$param{xtitle} = "x (mm)";
	$param{ytitle} = "y (mm)";
	$param{plotstyle} = "3D";
	return %param;
}

=item C<extract_phase( )>

Extracts data for plotting the phase of a two dimensional field distribution from a .dfl file. 
Parameters are passed through a hash.

The hash PARAM contains the following parameters that control the type of plot

=over 4

=item plottype        => Must by "phase" for this function

=item slice           => Plot data for slice SLICE. If not defined, plot the centre slice

=item file_in	        => Name of input file

=item file_out        => Name of output file containing the data to plot

=back

on return the hash contains also the header of the datafile as well as parameters for plotting the data

=cut

sub extract_phase {
	# subroutine to extract data from .dfl files to plot the two dimensional field distribution
	# for a particular slice number. If no slice number is specified it extracts the data for the centre slice 
	# Only .dfl files are accepted as input file.
		
	my %param = @_;

	my ($file_in, $file_out) = @param{'file_in', 'file_out'};

	die "Plot.pm::extract_phase -> Can not extract plot data from file, no input file specified\n" unless defined $param{file_in};
	die "Plot.pm::extract_phase -> plottype = field: warning, only .dfl files support this type of plotting\n" unless $param{file_in} =~ /\.(dfl|fld|sfl)$/;

	# get header of datafile
	%param = (get_header($file_in), %param);
	
	my $nslices = $param{nslices};	# number of slices in datafile
	my $npoints_x = $param{npoints_x};	# number of datapoints along X direction
	my $npoints_y = $param{npoints_y};	# number of datapoints along Y direction
	my $nhalf_x = int ($npoints_x % 2) ? ($npoints_x+1) / 2 : $npoints_x / 2;
    my $nhalf_y = int ($npoints_y % 2) ? ($npoints_y+1) / 2 : $npoints_y / 2;
    my $dgrid_x = $param{mesh_x};
    my $dgrid_y = $param{mesh_y};
  
	my $slice = $param{slice} || int ( ($nslices+1) / 2); 

	# temporary files
	my $tmpdatafile = File::Spec->catfile(
		File::Spec->tmpdir, "$file_out.tmp");

	my $f = field ( file => $file_in );
	$f->txt(">$tmpdatafile", $slice, "p");  # only the right slice is extracted and converted to ascii data
        
 	if ($VERBOSE == 1) { # print extra data to screen for debugging
		print "Header from $file_in\n";
		print "$_: $param{$_}\n" for sort keys %param;
	}

	open IN, $tmpdatafile;
	$file_out =~ s/^>*//;
	open OUT, ">$file_out" or die "Plot.pm::extract_field -> Can not open output file $file_out for plot data, $!\n";

	my ($state, $dpoints) = (0, 0);
	my ($islice, $iy, $ix) = (1, 1, 0);
	while (<IN>) {
		if ($state == 0) { # start of file, skip header or skip empty lines
			next unless /^\s*-?\d/;   #allow for negative numbers
			$state = 1;
		}
		if ($state == 1) { # data to be plotted, write to file_out
			if (!/^\s*-?\d/) { # reached end of column
				$iy++;
				$ix = 0;
				if ($iy == $npoints_y+1) {
					$islice++; 	 
					$iy = 1;
				}
				$state = 0;
				print OUT "\n";
			}
			else {
				$dpoints++;
				$ix++;
				my $x = ($ix - $nhalf_x) * $dgrid_x * 1000;
				my $y = ($iy - $nhalf_y) * $dgrid_y * 1000;
				chomp;
				print OUT "$x\t$y\t$_\n";
			}
		}
	}
	close IN;
	close OUT;

	print "Plot.pm::extract_field -> Warning, number of datapoints extracted from $file_in for plotting is $dpoints.\n".
		"Expected ". $npoints_x*$npoints_y ." points to plot.\n" 
		unless $dpoints == $npoints_x*$npoints_y ;
	
	print "Plot.pm::extract_field -> Extracted $dpoints datapoints from file $file_in for plotting\n" if $VERBOSE == 1;
	
	# set parameters for plotting
	$param{title}  .= "Phase distribution for slice $slice";
	$param{xtitle} = "x (mm)";
	$param{ytitle} = "y (mm)";
	$param{ztitle} = 'Phase\n(radians)';
	$param{plotstyle} = "3D";
	return %param;
}


=item C<extract_total( )>

Extracts data to plot from either a .dfl or .(txt|dat) file. Parameters are passed through a hash

The hash PARAM contains the following parameters that control the type of plot

=over 4

=item plottype        => Must by "total_roundtrip" for this function

=item roundtrip       => If specified plot for specific roundtrip, otherwise from FIRST_ROUNDTRIP to LAST_ROUNDTRIP

=item first_roundtrip => First roundtrip to plot, defaults to 1

=item last_roundtrip  => Last roundtrip to plot, defaults to all roundtrips

=item slice           => If specified plot for specific slice, otherwise from FIRST_SLICE to LAST_SLICE

=item first_slice     => First slice to plot, defaults to 1

=item last_slice      => Last slice to plot, defaults to last slice

=item file_in         => Name of input file

=item file_out        => Name of output file

=back

on return the hash contains also the header of the datafile as well as parameters for plotting the data

=cut

sub extract_total {
	# subroutine to extract and plot data from files produces by OPC
	# - .dfl input file: calculates the total for each slices and plots it versus the distance s within simulation window 
	# - .sfl input file: calculates the total for each wavelength and plots it versus the wavelength within the simulation window
	# - .txt input file: creates a 2D aor 3D plot of the total power versus distance s within the simulation window and
	#   versus the roundtrip number (or another parameter that is incremented with a constant value. A particular roundtrip 
	#   number can be specified to produce a 2D plot.
		
	my %param = @_;

	my ($file_in, $file_out) = @param{'file_in', 'file_out'};

	die "Plot.pm::extract_total -> Can not extract plot data from file, no input file specified\n" unless defined $param{file_in};
	die "Plot.pm::extract_total -> File type is not supported\n" unless $param{file_in} =~ /\.(dfl|fld|sfl|txt|dat)$/;

  # set the data type to temporal (sampled at time intervals) or spectral (samples at frequencies)
  $param{datatype} = 'temporal';
  $param{datatype} = 'spectral' if $param{file_in} =~ /\.sfl$/;

	# temporary files
	my $tmpdatafile = File::Spec->catfile(
		File::Spec->tmpdir, "$file_out.tmp");

	if ($file_in =~ /\.(dfl|fld|sfl)$/) {  # binary file, retrieve total power for each slice
		my $f = field ( file => $file_in );
		$f->total($tmpdatafile, 0) if $param{datatype} eq 'temporal';
		$f->spectrum($tmpdatafile, 0, undef) if $param{datatype} eq 'spectral';
	}
	else {
		$tmpdatafile = $file_in;
	}

	# get header of datafile
	%param = (get_header($tmpdatafile), %param);
	
	$param{datatype} = 'spectral' if $param{type} eq 'spectrum';  # header of text file indicates spectral power density data
 	
	if ($VERBOSE >= 1) { # print extra data to screen for debugging
		print "Header from $file_in\n";
		print "$_: $param{$_}\n" for sort keys %param;
	}
	my $nslices = $param{nslices};	# number of datapoints in simulation window (number of slices)
	$param{slice} = 1 unless ($param{nslices} > 1);  # set slice to 1 in %param to force 2D plot
	                                                 # if only 1 slice is present in the file
	my ($dx, $xstart);  # define variables for start and increment of horizontal axis	 
	my $cvac = 299792458.0;		              # speed of light in vacuum
	if ( $param{datatype} eq 'temporal' ) {
    $dx = $param{zsep} * $param{lambda} * 1e6;	# distance in micron between two slices in temporal domain
    $xstart = 0.0;
  } else {
    $dx = $cvac /( $param{zsep} *$param{lambda} * $nslices);  # distance between to slices in spectral domain
    $xstart = $cvac / $param{lambda} - $dx*(int(($nslices)/2));
  }
	
	my $i_max = 0;  # variable used to track maximum intensity
	my $first_roundtrip; my $last_roundtrip;
        if (defined $param{roundtrip}) {
		$first_roundtrip = $last_roundtrip = $param{roundtrip};
	}
	else {
		$first_roundtrip = $param{first_roundtrip} || 1;
	        $last_roundtrip  = $param{last_roundtrip}  || 0; # last_roundtrip = 0 means all roundtrips
	}

	my $first_slice; my $last_slice;
	if (defined $param{slice}) {
		$first_slice = $last_slice = $param{slice};
	}
	else {
		$first_slice = $param{first_slice} || 1;
		$last_slice  = $param{last_slice}  || $nslices;
	}
	
  die "Plot.pm::extract_total -> plottype = total: can't plot data for both a single slice (=$first_slice) and a single roundtrip (=$first_roundtrip)\n"
	if (defined $param{slice}) and (defined $param{roundtrip});

 	open IN, $tmpdatafile;
	$file_out =~ s/^>*//;
	open OUT, ">$file_out" or die "Plot.pm::extract_total -> Can not open output file $file_out for plot data, $!\n";

	my ($state, $ncurves, $dpoints) = (0, 0, 0);
	my ($islice, $iroundtrip) = (0, 1);
	while (<IN>) {
		chomp;
		if ($state == 0) { # start of file, skip header or skip empty lines
			next unless /^\s*\d/;
			$state = 1;
		}
		if ($state == 1) { # read data and determine if it needs to be plotted
			if (!/(^\s*\d)/) { # end of column reached, adjust counters
				$iroundtrip++;
				$islice = 0;
				$state = 0;
				next;
			}
			$state = 2 if ($iroundtrip >= $first_roundtrip) 
					and ( ($iroundtrip <= $last_roundtrip) or ($last_roundtrip == 0) );
		}
		if ($state == 2) { # data to be plotted, write to file_out
			if (!/^\s*\d/) { # reached end of column
				$iroundtrip++; 
				$ncurves++ unless defined $param{slice}; # if only one slice the result is a single curve
				$islice = 0;
				$state = 0;	# skip any empty lines or comments between data
				print OUT "\n" unless defined $param{slice};
			}
			else {
				$dpoints++;
				$islice++;
				my $x = $xstart + ($islice - 1) * $dx;    # position within pulse in microns or in frequency
				$x = $cvac*1e9/$x if $param{datatype} eq 'spectral'; # wavelength in nm
				my $y = $iroundtrip;
				$_ *= $cvac*1e9/($x*$x) if $param{datatype} eq 'spectral'; # convert to J/nm
				$i_max = $_ if $i_max < $_;	# check for maximum intensity
				if (defined $param{slice}) {
					print OUT "$y\t$x\t$_\n" if ($islice >= $first_slice) and ($islice <= $last_slice);
				}
				else {
					print OUT "$x\t$y\t$_\n" if ($islice >= $first_slice) and ($islice <= $last_slice);
				}
			}
		}
	}
	$ncurves++ if $ncurves == 0; # in case of a single slice, $ncurves is not increased.
	$param{zscale} = nice_scale($i_max);   # set scaling for intensity
	close IN;
	close OUT;

	print "Plot.pm::extract_total -> Warning, number of datapoints extracted from $file_in for plotting is $dpoints.\n".
		"Expected ".$nslices * ($last_roundtrip - $first_roundtrip + 1)." points to plot.\n" 
		unless ( $dpoints == $nslices * ($last_roundtrip - $first_roundtrip + 1) ) or ($last_roundtrip == 0);
	
	print "Plot.pm::extract_total -> Extracted $dpoints datapoints from file $file_in for plotting\n" if $VERBOSE == 1;
	
	# set parameters for plotting
  if ( defined $param{roundtrip} ) {
		$param{title}  .= "Total power versus s for roundtrip $first_roundtrip";
	}
  elsif ( defined $param{slice} ) {
		$param{title}  .= "Total power versus roundtrip for slice $first_slice";
	}
  else {
		$param{title}  .= "Power P(s)" unless $param{datatype} eq 'spectral';
		$param{title}  .= "Spectral energy density Es"if $param{datatype} eq 'spectral'
	}
	$param{xtitle} = "s (micron)";
	$param{xtitle} = "lambda (nm)" if $param{datatype} eq 'spectral';
	$param{xtitle} = "roundtrip" if defined $param{slice};
	$param{ytitle} = "roundtrip";
	$param{ztitle} = "Power (".prefactor($param{zscale})."W)";
	$param{ztitle} = "Spectral Energy density (".prefactor($param{zscale})."J/nm)" if $param{datatype} eq 'spectral';
#	$param{plotstyle} = (defined $param{slice} or defined $param{roundtrip}) ? "2D" : "3D";
  $param{plotstyle} = ($ncurves == 1) ? "2D" : "3D";
	return %param;
}

   sub log10 {
   # calculates the logaritm base 10
     my $x = shift;
     return log($x)/log(10);
   }

   sub nice_scale {
   # calculates the scaling factor 10^s where s is a multiple of 3
     use POSIX qw/floor/;
     my $x = shift;
     my $s = $x > 0 ? log10 ($x) : 0;
     $s /= 3.0;
     return floor($s);
	}
	 
   sub prefactor {
   # set the right prefactor depending on the scaling
     my $x = shift;
     my %factors = (
     -8 => "y",
     -7 => "z",
     -6 => "a",
     -5 => "f",
     -4 => "p",
     -3 => "n",
	   -2 => "{/Symbol m}",
	   -1 => "m",
	    0 => "",
	    1 => "k",
	    2 => "M",
	    3 => "G",
	    4 => "T",
	    5 => "P"
     );
     return $factors{$x};
   }

=back

=head1 COPYRIGHT

Copyright (C) 2006 J.G. Karssenberg, P.J.M. van der Slot

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; version 2 of the
License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

=cut

1;
