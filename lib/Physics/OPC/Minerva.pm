package Physics::OPC::Minerva;

use strict;
use vars qw/@PATH $field $VERBOSE %USE_MPI/;
use Carp;
use Physics::OPC::Namelist;

our @ISA = qw/Physics::OPC::Namelist/; # inherit from the namelist class

*field =   \$Physics::OPC::field;   # import $field
*mercury = \$Physics::OPC::mercury; # import $mercury
*VERBOSE = \$Physics::OPC::VERBOSE; # import $VERBOSE
*USE_MPI = \%Physics::OPC::USE_MPI; # import %USE_MPI
*message = \&Physics::OPC::message; # import message() routine

sub round {
  # local subroutine to round towards nearest integer
  my $var = shift;
  my $tmpRounded = int( abs($var) + 0.5 );  
  return $var >= 0 ? 0 + $tmpRounded : 0 - $tmpRounded;
}

=head1 NAME

Physics::OPC::Minerva

=head1 DESCRIPTION

This module is a wrapper for the FEL simulation code
Minerva writen by H.P. Freund.

This class inherits functions from L<Physics::OPC::Namelist>.
See the documentation with Minerva for a parameter listing.

=head1 METHODS

=over 4

=item C<new(CONFIG_FILE)>

Simple constructor.

=cut

sub new {
	croak 'Minerva.pm::new -> usage: new(class, config)' unless @_ == 2;
	my ($class, $config) = @_;
	croak "Minerva.pm::new -> Missing Minerva config file" unless defined $config and -f $config;

	my @parts = File::Spec->splitdir($config);
	my $file = pop @parts; # remove directory part of file name
	my $self = bless {name => 'inp1', file => "MinervaConfig.in", i=>1}, $class;

	$self->copyfrom($config);
	
	# set to default unless already defined in input file
	my ($minervadfl, $Config_Minerva, $mercurydfl, $Config_Mercury) = 
	 	$self->get_param('optics', 1, 'minervadflfile', 'Config_Minerva',
	 	                              'mercurydflfile', 'Config_Mercury');
	 	                              
	$self->set_param('optics', 1, minervadflfile => 'OpticsInput.dfl')  unless defined $minervadfl;
	$self->set_param('optics', 1, Config_Minerva => 'MinervaConfig.in') unless defined $Config_Minerva;
	$self->set_param('optics', 1, mercurydflfile => 'MercuryInput.dfl') unless defined $mercurydfl;
	$self->set_param('optics', 1, Config_Mercury => 'MercuryConfig.in') unless defined $Config_Mercury;
	
	return $self;
}

=item C<run()>

Run the program with the given config on the global C<$field>.

=cut

sub run {
	my $self = shift;
	
	my $exe = defined($USE_MPI{minerva}) ? 'minerva_mpi' : 'minerva';
	my $minerva = Physics::OPC->executable($exe);
	# if mpi version is not ending on _mpi, executable will not be found. Retry without _mpi
	$minerva = Physics::OPC->executable('minerva') unless defined $minerva;
	croak "Minerva.pm::run -> Could not find the $exe executable\n" unless defined $minerva;
	
	# Check input field
	if (defined $field) {
		# field exists, Mercury has created a namelist input file for Minerva 
		my $lfile = Physics::OPC->check_dir($field->filename);
		croak "Minerva.pm::run -> Can not run Minerva on a file in other directory" 
			unless defined $lfile;
		my $dflfile = $self->get_param('optics', 1, 'mercurydflfile');
		croak "Minerva.pm::run -> Required input files are not available.\n" 
			unless -f $dflfile;
		# $self->set_param('inp1', 1, opc => "on"); # just to be sure
		my ($m, $mx, $my) = $field->get_param('M','Mx','My');
		carp "Minerva.pm::run -> WARNING: M = $m for input field for Minerva" 
			if defined $m and abs($m-1) > 1e-5;
		carp "Minerva.pm::run -> WARNING: Mx = $mx for input field for Minerva" 
			if defined $mx and abs($mx-1) > 1e-5;
		carp "Minerva.pm::run -> WARNING: My = $my for input field for Minerva" 
			if defined $my and abs($my-1) > 1e-5;
	}
	
	# Execute the binary
	
	my $cmd = defined($USE_MPI{minerva}) ? $USE_MPI{minerva}.' '.$minerva : $minerva ;
	message("Running `$cmd` with input file '$self->{file}'") if $VERBOSE;
#	open MEDIN, $self->{file} or croak "Could not open: $self->{file}";
#	my @minerva_in = join('', <MEDIN>);
#	close MEDIN;
    croak "Minerva::pm::run - >Could not access file $self->{file}" unless -f $self->{file};
    $cmd .= " < $self->{file}";
    print "$cmd\n";
	open MED, '| '.$cmd or croak "Minerva.pm::run -> Executing `$cmd` failed";
#	print MED @minerva_in;
	close MED or croak "Minerva.pm::run -> Executing `$cmd` failed";
	# See IPC::Open2 if you need both STDIN and STDOUT filehandles for the Minerva process

	# Set output field and update field param
	my $dflfile = $self->get_param('optics', 1, 'minervadflfile');
	$dflfile =~ s/\s*&//;	# remove trailing whitespaces
	$dflfile =~ s/^\s*//;   # remove leading whitespaces
	$field = Physics::OPC::FieldFile->new($dflfile);       # set first harmonic by default

	# Note, starting from version 0.7.5, the .param file is written by OPC
  my $slippage = $self->get_param('inp1', 1,'slippage');
  my $wavelength = $self->get_param('inp4', 1,'wavelnth');
  my ($ndfl, $ndflx, $ndfly) = $self->get_param('optics', 1,'ndfl','ndflx','ndfly');
  # Note, Minerva writes a MercuryConfig file that contains the gridsize. So read it from that file
  my $mercury = Physics::OPC::Namelist->new('optics' ,'MercuryConfig.in',1);
  my $gridsize = $mercury->get_param('optics',1,'gridsize');
  $ndfl ||= 0;                   # set default to zero if not defined
  $ndflx = $ndfl if $ndfl != 0;  # ndfl takes precedence if both ndfl and ndflx/y are present
  $ndfly = $ndfl if $ndfl != 0; 
  carp "Minerva.pm::run -> Warning, no ndfl or ndflx defined in optics namelist" unless defined $ndflx;
  carp "Minerva.pm::run -> Warning, no ndfl or ndfly defined in optics namelist" unless defined $ndfly;
  carp "Minerva.pm::run -> Warning, no nsigma or gridsize defined in optics namelist" unless defined $gridsize;
  # calculate mesh size
  $gridsize *= 2.0*0.01 ;                # gridsize read from Minerva is half width in cm
  my $mesh_x = $gridsize/($ndflx-1);
  my $mesh_y = $gridsize/($ndfly-1);
  my $mesh = 0;
  $mesh = $gridsize/($ndfl-1) if $ndfl != 0;
  my $zsep = 1;    # single slice, if multiple slices ('slippage'='on' then recalculate)   
  my $nslices = 1; # single slice, if multiple slices ('slippage'='on' then recalculate) 
	my %param = (
	  # starting from version 0.7.5, variables npoints_x, npoints_y, mesh_x and mesh_y are 
	  # introduced. 
	  mesh_x => $mesh_x,
	  mesh_y => $mesh_y,
	  lambda => $wavelength,
	  npoints_x => $ndflx,
	  npoints_y => $ndfly
	);
	my $tslices;
	if ($slippage =~ /\bon\b/i) { 
	    # determine ZSEP
	    ($tslices, $nslices) = $self->get_param('slip', 1, 'tslices', 'nslices');
	    my $c =299792458;		#speed of light in vacuum (m/s)
	    my $tperiod = $wavelength/$c;
	    $zsep = $tslices/(($nslices-1)*$tperiod);
	} 
	$param{nslices} = $nslices;
	$param{zsep} = $zsep; 
	unlink $field->{file} if -e $field->{file};
	#foreach my $key (keys %param) {
 	# print "$key = $param{$key}\n";
  #}
	$field->set_param(%param);
}

=item C<set_param(NAME, I, KEY => VALUE, ...)>

Set param in the I''th occurance of namelist NAME .

=cut

sub set_param {
		my $self = shift;
		my $name = shift;
		my $i = shift;
		$self->{name} = $name;
		$self->{i} = $i;
		return $self->SUPER::set_param(@_); # dispatch to parent class
}

=item C<get_param(NAME, I, KEY, ...)>

Get param from the I''th occurance of namelist NAME .

=cut

sub get_param {
		my $self = shift;
		my $name = shift;
		my $i = shift;
		$self->{name} = $name;
		$self->{i} = $i;
		return $self->SUPER::get_param(@_); # dispatch to parent class
}

=back

=head1 COPYRIGHT

Copyright (C) 2006 J.G. Karssenberg, P.J.M. van der Slot, I.V.Volokhine

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; version 2 of the
License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

=cut

1;
