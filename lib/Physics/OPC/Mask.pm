package Physics::OPC::Mask;

use vars qw/$VERBOSE/;
use strict;
use Carp;

*message = \&Physics::OPC::message; # import message() routine
*VERBOSE = \$Physics::OPC::VERBOSE; # import $VERBOSE

our @Types = qw/zernike/;

=head1 NAME

Physics::OPC::FieldFile

=head1 DESCRIPTION

Object which maps to a ".opd" mask file.

=head1 METHODS

=over 4

=item C<new(\%PARAM)>

=item C<new(\%PARAM, \%PARAM, ...)>

Simple constructor.

=cut

sub new {
	my $class = shift;

	# store mask parameters
	my $self = bless {}, $class;
	if (ref($_[0]) eq 'HASH') { # array of hash references
		$self->{masks} = [@_];
	}
	else { # just one set of params
		$self->{masks} = [ {@_} ];
	}
	
	# disentangle object param from mask param
	for (qw/file field/) {
		$self->{$_} = delete $self->{masks}[0]{$_};
	}
	return $self;
}

=item C<prepare(%PARAM)>

Creates the binary mask file.

=cut

sub prepare {
	my $self = shift;
	my %args = @_;
	return if $self->{is_prepared}++;
	$self->{file} ||= $args{file} ;
	$self->{file} =~ s/^\>+//; # just to be sure
	die "Mask.pm::prepare -> ERROR: no file name given to write mask\n"
		unless defined $$self{file};
	die "Mask.pm::prepare -> ERROR: 'file' must have extension '.opd' for mask type zernike\n"
		unless $$self{file} =~ /\.opd$/;
	if (-e $self->{file}) { # clean up old file
		unlink $self->{file} or die "Mask.pm::prepare -> ERROR: Could not delete file: $self->{file}\n";
	}
	
	$self->{field} ||= $args{field} ;
	$self->{M} ||= $args{M};
	$self->{Mx} ||= $args{Mx};
	$self->{My} ||= $args{My};

	for ( @{$$self{masks}} ) {
		$self->_add_mask($_);
	}
}

sub _add_mask {
		my ($self, $param) = @_;
		my %param = %$param;

		die "Mask.pm::add_mask -> ERROR: unknown mask type: $param{type}\n"      
			unless grep {$param{type} =~ /$_/} @Types;
	
		# if field is defined, take mesh, npoints, wavelength from field,
		# if not these need to be specified explicitly in param
	
                my $mask_param = $$self{file};
                $mask_param =~ s/(\.\w+\s*)?$/.param/;
		my $config = Physics::OPC::Namelist->new('optics', $mask_param);
		
		if (defined $$self{field}) { 
		    print "Mask: using parameters associated with optical field\n" if $VERBOSE > 1;
			my ($npoints_x, $npoints_y, $mesh_x, $mesh_y, $lambda) = $$self{field}->get_param('npoints_x', 'npoints_y', 'mesh_x', 'mesh_y', 'lambda');
            $mesh_x *= $$self{Mx}; # adjust mesh with magnification applied during propagation up to point
            $mesh_y *= $$self{My}; # where mask is applied
			$config->set_param(
				npoints_x => $npoints_x, 
				npoints_y => $npoints_y, 
				mesh_x    => $mesh_x,
				mesh_y    => $mesh_y,
				lambda  => $lambda
			);
		} else {
		    print "Mask.pm: using parameters passed in mask command\n" if $VERBOSE > 1;
  		  	die "Mask.pm::add_mask -> ERROR: mask needs to know 'npoints_(x/y)', 'mesh_(x/y)' and 'lambda' when no field is defined\n"
			if grep {! defined $param{$_}} qw/npoints_x npoints_y mesh_x mesh+_y lambda/;
			unlink 'mask.param' or die "Mask.pm::add_mask -> ERROR: could not delete file: mask.param\n" if -e 'mask.param';
			$config->set_param(
				  npoints_x => $param{npoints_x}, 
				  npoints_y => $param{npoints_y}, 
				  mesh_x    => $param{mesh_x},
				  mesh_y    => $param{mesh_y}, 
				  lambda    => $param{lambda}
			);
		}

		# Mark type zernike
		if ($param{type} =~ /zernike/i) {
	  		# check if required zernike specific input parameters are specified
			die "Mask.pm::add_mask -> ERROR: mask type zernike needs arguments 'n', 'm', 'R' and 'A'\n" 
				if grep {! defined $param{$_}}  qw/n m R A/;
			# set file name
			$param{file} = $$self{file};
			$param{file} = '+'.$param{file} if -e $param{file};
			# default values for xoff and yoff if not specified
			$param{$_} ||= '0' for qw/xoff yoff/;
    
	   	# execute fortran code
	   	message("Adding to mask: $param{file}") if ($param{file} =~ /^\+/) && ($VERBOSE);
			message("Preparing mask: $param{file}") if ($param{file} =~ /^\w/) && ($VERBOSE);
			my $exec = Physics::OPC->executable('opc_utils');
	   		die "Mask.pm::add_mask -> ERROR: could not find executable 'opc_utils'\n" unless defined $exec;
	   		# add parameters to command line
	   		$exec .= sprintf(" $config->{file} zernike %i %i %13.6e %13.6e $param{file} %13.6e %13.6e",
	                                 $param{n}, $param{m}, $param{R}, $param{A}, $param{xoff}, $param{yoff});
			print "$exec\n" if $VERBOSE > 99;
	                open UTILS, "| $exec" or die $!;
			close UTILS;
		}
	
}

=back

=head1 COPYRIGHT

Copyright (C) 2007 J.G. Karssenberg, P.J.M. van der Slot, I.V.Volokhine

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; version 2 of the
License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

=cut

1;


