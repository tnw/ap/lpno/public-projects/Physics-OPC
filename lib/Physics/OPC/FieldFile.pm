package Physics::OPC::FieldFile;

use strict;
use Env '$PWD';
use Carp;
use File::Copy qw/cp mv/;
use IPC::Open2; # needed to open programs read/write at the same time
use Physics::OPC::Namelist;
use vars qw/%USE_MPI $VERBOSE/;

*USE_MPI = \%Physics::OPC::USE_MPI; # import %USE_MPI
*VERBOSE = \$Physics::OPC::VERBOSE; # import $VERBOSE

our @ISA = qw/Physics::OPC::Namelist/; # inherit from the namelist class

=head1 NAME

Physics::OPC::FieldFile

=head1 DESCRIPTION

Object to correspond with a binary file containing the complex electric field.
This module uses Fortran programs to get data from these binary files or to convert them
to plain text data.

Paramters about the electric field are stored in a namelist file of the same name as the
binary but using the '.param' extension.

This class inherits functions from  L<Physics::OPC::Namelist>.
Valid parameters are:

=over 4

=item lambda

Radiation wavelength in meters, used for propagation algorithms.

=item mesh

Meshsize, the distance in meter between two grid points of the square grid. Keep to be
compatible with older versions.

=item mesh_x,

Meshsize, the distance in meters between two grid points in x-directon.

=item mesh_y,

Meshsize, the distance in meters between two grid points in y-directon.


=item zsep

Separation distance between two time slices given in number of wavelengths.

=item npoints

Number of grid points in both x- and y-direction (square grid). Keep for compatibility
with older releases.

=item npoints_x

Number of grid points in x-direction

=item npoints_y

Number of grid points in y-direction

=item nslices

Number of time slices (in z).

=item Mx

Scaling factor in x-direction. Only used for checks, should already be incorporated in the meshsize.

=item My

Scaling factor in y-direction. Only used for checks, should already be incorporated in the meshsize.

=back

=head1 METHODS

=over 4

=item C<new(FILE)>

Simple constructor. FILE is the name of the binary file.

=cut

# Attribute 'file' already used by Namelist.pm therefor
# using attribute 'field' for the binary file name

sub new {
	my ($class, $file) = @_;
	my $self = bless {name => 'optics', i => 1}, $class;
	$self->set_file($file);
	return $self;
}

=item C<set_file(FILE)>

Make object point to FILE.

=cut

sub set_file {
	my ($self, $file) = @_;
	my $lfile = Physics::OPC->check_dir($file);
	$file = $lfile if defined $lfile;

	my $namelist = $file;
	$namelist =~ s/(\.\w+\s*)?$/.param/;
	$self->{field} = $file;
	$self->{file} = $namelist;
	
	if (defined $lfile) {
		@$self{qw/_field _file/} = @$self{qw/field file/};
		$self->{_dir} = undef;
	}
	else { # file not in current dir
		my @part = File::Spec->splitdir($file);
		$self->{_field} = pop @part;
		$self->{_file} = $self->{_field};
		$self->{_file} =~ s/(\.\w+\s*)?$/.param/;
		$self->{_dir} = File::Spec->catdir(@part);
	}
	#use Data::Dumper; warn Dumper $self;
}

=item C<filename()>

Returns the filename.

=cut

sub filename { return $_[0]->{field} }

=item C<namelist()>

Returns the name of the param file.

=cut

sub namelist { return $_[0]->{file} }

=item C<set_nslices()>

Sets the "nslices" param based on the file size and a given "npoints".
Assumes the field consists of 16 byte numbers.

=cut

sub set_nslices {
	my ($self, $npoints_x, $npoints_y) = @_; # npoints is private arg
	my $size = (stat $self->{field})[7]
		or croak "Fieldfile.pm::set_nslices -> Could not stat $self->{field}";
	$npoints_x ||= $self->get_param('npoints_x');
	$npoints_y ||= $self->get_param('npoints_y');
    if (!defined ($npoints_x)) { # npoints_x is not defined, look for $npoints
      $npoints_x = $self->get_param('npoints');
      $npoints_y = $npoints_x;
    }
	croak "Fieldfile.pm::set_nslices -> npoints not set" unless $npoints_x;
	my $nslices = $size / (16 * $npoints_x*$npoints_y);
	$self->set_param(nslices => $nslices);
}

=item C<copy(FILE)>

Save a copy of the fieldfile to FILE.

=cut

sub copy {
	my ($self, $f2)  = @_;
	$f2 =~ s/^>+//;
	my ($f1y, $f2y);
  my $f1 = $self->{field}; # copy of original name
  ($f1y = $f1) =~ s/(\.)(\w+\s*)?$/_y.$2/; # check if y_polarisation is already present
  if (-e $f1y) { # y polarisation is present, so copy this as well
    ($f2y = $f2) =~ s/(\.)(\w+\s*)?$/_y.$2/;
    cp $f1y, $f2y;
    $f1y =~ s/(\.\w+\s*)?$/.param/;
    $f2y =~ s/(\.\w+\s*)?$/.param/;
    cp $f1y, $f2y;
  }
	cp $self->{field}, $f2;  # copy binary
	$f2 =~ s/(\.\w+\s*)?$/.param/;
	cp $self->{file}, $f2; # copy namelist
}

=item C<move(FILE)>

Rename to FILE. More efficient then copy when possible.
Updates object to point to the new name.

=cut

sub move {
	my ($self, $f2) = @_;
	$f2 =~ s/^>+//;
	my ($f1y, $f2y);
  my $f1 = $self->{field};  # copy of original file name
  ($f1y = $f1) =~ s/(\.)(\w+\s*)?$/_y.$2/; # check if y_polarisation is already present
  if (-e $f1y) {
      ($f2y = $f2) =~ s/(\.)(\w+\s*)?$/_y.$2/;
      mv $f1y, $f2y;
      $f1y =~ s/(\.\w+\s*)?$/.param/;
      $f2y =~ s/(\.\w+\s*)?$/.param/;
      mv $f1y, $f2y;
    }
	mv $self->{field}, $f2;                    # move binary
	my $param;
	($param = $f2) =~ s/(\.\w+\s*)?$/.param/;  # set name for .param file
	mv $self->{file}, $param;                  # move namelist
	$self->set_file($f2);
	    
}

=item C<fourier()>

Time-dependent fourier transform.

The extension of the file is used ("dfl" or "sfl")
to determine the direction.

=cut

sub fourier {
	my ($self, $in_memory) = @_;
	$in_memory = 1 unless defined $in_memory;
	$self->{_field} =~ /\.(\w+)/;
	my $sign = ($1 eq 'dfl') ? '-1' : '1';
  my $ext = ($sign < 0) ? 'sfl' : 'dfl' ;
	my $outfile = $self->{_field};
	$outfile =~ s/\.\w+$/.$ext/; # change extension on outfile
	die "Fieldfile.pm::fourier -> Can't tranform to same file name\n" if $outfile eq $$self{_field};
	my $memory = $in_memory ? 'true' : 'false';
	$self->run_fortran('dfl_utils', undef,"$self->{_file} $self->{_field} 0 fourier $memory");
	$self->set_file( defined($self->{_dir})
		? File::Spec->cat_file($self->{_dir}, $outfile)
		: $outfile
	);
	# set spacing dfreq for the slices (spacing is equidistant in frequency, not in wavelength)
	my ($zsep, $lambda, $nslices) = $self->get_param('zsep','lambda','nslices');
	my $dfreq = ($sign < 0) ? 299792458.0/( $zsep * $lambda * $nslices ) : 0;
	$self->set_param(dfreq => $dfreq) if $dfreq > 0; 
}

=item C<cross(DIRECTION, FILE)>

Writes a plain text crossection of the field to FILE.

DIRECTION can be either 'x' or 'y', and defaults to 'x' when it is not defined.

Prefix the file name with '>>' to have the crossection appended instead of
overwriting the file.

=cut

sub cross {
	my ($self, $direction, $file) = (@_ == 3) ? (@_) : ($_[0], 'x', $_[1]);
	$file =~ s/^>?/>/ if defined $file;
	croak "Fieldfile.pm::cross -> invalid direction for cross section: $direction"
		unless $direction eq 'x' or $direction eq 'y';
	my $command = $direction.'cross';

	croak "Fieldfile.pm::cross -> No such file: $self->{field}" unless -f $self->{field};
	croak "Fieldfile.pm::cross -> Parameters for fieldfile are not initialized" unless -f $self->{file};
	
	$file = Physics::OPC->abs_path($file) if defined $file;

	if (defined $file) {
		my ($iomode, $filename) = ($file =~ /^(>*)(.*)$/);
		my $write_header = !((-e $filename) and ($iomode =~/^>>/));
    if ($write_header) {
			open OUT, $iomode.$filename or die "Fieldfile.pm::cross -> Could not write to $filename\n";
			print OUT $self->_header(type=>"cross", direction=>$direction);
			close OUT;
			$file=">>".$filename;
		}
	}

#	warn "'dfl_utils', $file, $self->{_file}, $self->{_field}, ' 0 ',$command, $file\n";
	$self->run_fortran('dfl_utils', $file, "$self->{_file} $self->{_field} 0 $command");
}

=item C<tot_cross(DIRECTION, FILE1, FILE2)>

Write the total power (i.e intensity integrated over the cross section) to FILE1
and writes a plain text crossection of the field to FILE2. Command is usefull
when the gridsize is large and consequently the binary FIELD file. This command
reduces io overhead as extracting total power and cross section only accesses 
the FIELD file only once.

DIRECTION can be either 'x' or 'y', and defaults to 'x' when it is not defined.

Prefix the file name with '>>' to have the crossection appended instead of
overwriting the file.

=cut


sub tot_cross {
	my ($self, $direction, $file1, $file2) = (@_ == 4) ? (@_) : ($_[0], 'x', $_[1], $_[2]);

        if (defined $file1) {
		$file1 =~ s/^>?/>/;
	} else {
		 croak "Fieldfile.pm::tot_cross -> filename for 'total' is missing";
	}
	if (defined $file2) {
		$file2 =~ s/^>?/>/;
	} else {
		 croak "Fieldfile.pm::tot_cross -> filename for 'cross' is missing";
	}
	croak "Fieldfile.pm::tot_cross -> invalid direction for cross section: $direction"
		unless $direction eq 'x' or $direction eq 'y';
	my $command = 'tot_'.$direction.'cross';

	croak "Fieldfile.pm::tot_cross -> No such file: $self->{field}" unless -f $self->{field};
	croak "Fieldfile.pm::tot_cross -> Parameters for fieldfile are not initialized" unless -f $self->{file};
	
	my ($iomode, $filename) = ($file2 =~ /^(>*)(.*)$/);
	my $write_header = !((-e $filename) and ($iomode =~/^>>/));
	if ($write_header) {
		open OUT, $iomode.$filename or die "Fieldfile.pm::tot_cross -> Could not write to $filename\n";
		print OUT $self->_header(type=>"cross", direction=>$direction);
		close OUT;
		$file2 = "+".$filename;
	}
	$file2="+".$filename if ($iomode =~/^>>/) and !($filename=~/^\+/); # use "+" instead of ">>" for command line argument passing in combination with runmpi

	($iomode, $filename) = ($file1 =~ /^(>*)(.*)$/);
	$write_header = !((-e $filename) and ($iomode =~/^>>/));
	if ($write_header) {
		open OUT, $iomode.$filename or die "Fieldfile.pm::total -> Could not write to $filename\n";
		print OUT $self->_header(type => 'total', direction => '') if $write_header;
		print OUT "\n";  # empty line to separate from next dataset in file
		close OUT;
		$file1 = "+".$filename;
	}
	$file1="+".$filename if ($iomode =~/^>>/) and !($filename=~/^\+/); # use "+" instead of ">>" for command line argument passing in combination with runmpi

	my $file = undef; # so run_fortran does not have to capture output and write it to file

	$self->run_fortran('dfl_utils', $file, "$self->{_file} $self->{_field} 0 $command $file1 $file2");
}


=item C<zshift(ZSHIFT, NADD)>

First adds NADD slices to the dfl file and then shifts the field by an amount
of SHIFT with respect to the time window

NADD > 0 adds zero field slices to the head of the pulse
NADD < 0 adds zero field slices to the tail of the pulse

ZSHIFT > 0 shifts towards the head to the pulse 
ZSHIFT < 0 shifts towards the tail of the pulse

ZSHIFT is specified in meters

=cut

sub zshift {
	my ($self, $zshift, $nadd) = @_;
	print "dfl_utils::zshift -> zshift = $zshift, nadd = $nadd\n" if $VERBOSE > 1;
	$nadd   ||= 0;	# set to zero if not specified
	$zshift ||= 0.0; # set to zero if not specified
	my $command = "shift";
	croak "Fieldfile.pm::shift -> No such file: $self->{field}" unless -f $self->{field};
	croak "Fieldfile.pm::shift -> Parameters for fieldfile are not initialized" unless -f $self->{file};
	
#	warn "'dfl_utils', $file, $self->{_file}, $self->{_field}.' 0 '.$command\n";
	my $command_arg = "$self->{file} $self->{field} 0 $command".sprintf(" %15.6e %i",$zshift, $nadd);
	$self->run_fortran('dfl_utils', undef, $command_arg);
}

=item C<statistics(FILE)>

Writes statistical data for the pulse (or slice in steady state) to FILE.

Information written to file is
	tot	total power/energy
	xc	x-coordinate of centre of optical pulse
	yc	y-coordinate of centre of optical pulse
	sx      standard deviation in x
	sy	standard deviation in y
	sr	standard deviation in r
	w   half width of the optical beam assuming a Gaussian distribution
	lambda_avg  intensity weighted average wavelength of the optical distribution
	bw          rms bandwidth (standard deviation) of the optical distribution

The values returned are in the form of a hash with the above names as keys

Prefix the file name with '>>' to have the data appended instead of
overwriting the file.

=cut

sub statistics {
	my ($self, $file) = @_;
	$file =~ s/^>?/>/ if defined $file;
  my $slice = 0;

	croak "Fieldfile.pm::statistics -> No such file: $self->{field}" unless -f $self->{field};
	croak "Fieldfile.pm::statistics -> Parameters for fieldfile are not initialized" unless -f $self->{file};
	
	$file = Physics::OPC->abs_path($file) if defined $file;
	
	# Fetch data
	my $stats = $self->run_fortran('dfl_utils', undef,
		"$self->{_file} $self->{_field} $slice stat" );
	
	# strip white space etc. from output
	my @stats = ($stats =~ /([\d+-][\d\.E+-]+)/g);
	if ($stats =~ /error|NaN/i or ! @stats) {
        # error detected
		die "Fieldfile.pm::statistics -> Error, could not get statistics for $self->{field}\n".
                    "                            No results returned.\n" if ! @stats;
		# something is returned, print output to screen
		print "Fieldfile.pm::statistics -> ERROR, could not get statistics for $self->{field}\n";
		print "                            Output returned is:\n";
		print "$_\n" for @stats;
		die;
	}

	if (defined $file) {
		my ($iomode, $filename) = ($file =~ /^(>*)(.*)$/);
		my $write_header = !((-e $filename) and ($iomode =~/^>>/));
		open OUT, $iomode.$filename or die "Fieldfile.pm::statistics -> Could not write to $filename\n";
		if ($write_header) {
			print OUT $self->_header(type => 'statistics');
			print OUT "#columns: tot xc yc sx sy sr w lambda_avd bw\n";
		}
		print OUT map "$_  ", @stats;
		print OUT "\n";  
		close OUT;
	}
	
	my %hash;
	@hash{qw/tot xc yc sx sy sr w lambda_avg bw/} = @stats;
	
	return %hash;
}


=item C<bandwidth(FILE)>

Writes the bandwidth of the optical pulse clipped at radial position r
(with respect to the center of the optical pulse) to FILE.

Information written to file is
 r      mm    the radial position from the pulse centre at which the 
              field is clipped 
 bw(r)  nm    rmw bandwidth calculated using the optical field clipped
              at radius r.
	
The values returned are in the form of a hash with r as key

Prefix the file name with '>>' to have the data appended instead of
overwriting the file.

=cut

sub bandwidth {
	my ($self, $file) = @_;
	$file =~ s/^>?/>/ if defined $file;
  my $slice = 0;

	croak "Fieldfile.pm::bandwidth -> No such file: $self->{field}" unless -f $self->{field};
	croak "Fieldfile.pm::bandwidth -> Parameters for fieldfile are not initialized" unless -f $self->{file};
	
	$file = Physics::OPC->abs_path($file) if defined $file;
	
	# Fetch data
	my $data = $self->run_fortran('dfl_utils', undef,
		"$self->{_file} $self->{_field} $slice bandwidth" );
	
	# strip white space etc. from output
	my @data = ($data =~ /([\d+-][\d\.E+-]+)/g);
	if ($data =~ /error|NaN/i or ! @data) {
        # error detected
		die "Fieldfile.pm::bandwidth -> Error, could not get bandwidth for $self->{field}\n".
        "                           No results returned.\n" if ! @data;
		# something is returned, print output to screen
		print "Fieldfile.pm::statistics -> ERROR, could not get statistics for $self->{field}\n";
		print "                            Output returned is:\n";
		print "$_\n" for @data;
		die;
	}
	# @data contains three arrays interleaved, r, lambda_c(r) and bw(r)
	# Currently we only use r and bw(r).
  my %hash;
  my @lambda_c;
  my $size = $#data;
	for (my $i=0; $i <= $size; $i+=3) {
		 $hash{$data[$i]} = $data[$i+2];  # store r ab
		 push @lambda_c, $data[$i+1];     # store center wavelength for later use
	}
	if (defined $file) {
		my ($iomode, $filename) = ($file =~ /^(>*)(.*)$/);
		my $write_header = !((-e $filename) and ($iomode =~/^>>/));
		open OUT, $iomode.$filename or die "Fieldfile.pm::bandwidth -> Could not write to $filename\n";
		if ($write_header) {
			print OUT $self->_header(type => 'bandwidth');
			print OUT "#columns: r (mm)  bw (nm)\n";
		}
		for my $key (sort {$a <=> $b} keys %hash) {
		  print OUT sprintf("%12.3e \t%12.3e\n",$key*1e3,$hash{$key}*1e9); 
		}
		print OUT "\n";
		close OUT;
	}
	
	return %hash;
}



=item C<total(FILE, SLICE)>

Outputs total power (= Intensity integratted over cross section) in the field. 
If SLICE is 0 total power for each slice is outputed.

=cut

sub total {
	my ($self, $file, $slice) = @_;
	$file =~ s/^>?/>/ if defined $file;
	$slice ||= '0';

	croak "Fieldfile.pm::total -> No such file: $self->{field}" unless -f $self->{field};
	croak "Fieldfile.pm::total -> Parameters for fieldfile are not initialized" unless -f $self->{file};
	
	# Fetch data
	# warn ">>$self->{field} $slice total<<\n";
	my $total = $self->run_fortran('dfl_utils', undef,
		"$self->{_file} $self->{_field} $slice total" );

	# strip white space etc. from output
	my @total = ($total =~ /([\d+-][\d\.E+-]+)/g);
	die "Fieldfile.pm::total -> ERROR, could not get total power in field for $self->{field}\n"
		if $total =~ /error/i or ! @total;

	if (defined $file) {
		my ($iomode, $filename) = ($file =~ /^(>*)(.*)$/);
		my $write_header = !((-e $filename) and ($iomode =~/^>>/));
		open OUT, $iomode.$filename or die "Fieldfile.pm::total -> Could not write to $filename\n";
		print OUT $self->_header(type => 'total', direction => '') if $write_header;
		print OUT map "$_\n", @total;
		print OUT "\n";  # empty line to separate from next dataset in file
		close OUT;
	}

	return @total;
}

=item C<spectrum(FILE, SLICE, X, Y)>

Outputs spectral power density (= Intensity integratted over cross section for each wavelength) in the field. 
If SLICE is 0 spectral power density is output for all wavelengths. X and Y are optional arguments and 
determine the location in the grid where the spectrum must be taken.

=cut

sub spectrum {
	my ($self, $file, $slice, $x, $y) = @_;
	$file =~ s/^>?/>/ if defined $file;
	$slice = '0' unless defined $slice;
	my $sx = defined $x ? sprintf("%13.6e", $x) : '-' ;
	my $sy = defined $y ? sprintf("%13.6e", $y) : '-' ;

	croak "Fieldfile.pm::spectrum -> No such file: $self->{field}" unless -f $self->{field};
	croak "Fieldfile.pm::spectrum -> Parameters for fieldfile are not initialized" unless -f $self->{file};
	croak "Fieldfile.pm::spectrum -> Cannot retrieve spectral power density, input is in temporal domain" unless $self->{field} =~/\.sfl$/i;
		
	# Fetch data
	# warn ">>$self->{field} $slice total<<\n";
	my $spectrum = $self->run_fortran('dfl_utils', undef,
		"$self->{_file} $self->{_field} $slice spectrum undef $sx $sy" );

	# strip white space etc. from output
	my @spectrum = ($spectrum =~ /([\d+-][\d\.E+-]+)/g);
	die "Fieldfile.pm::total -> ERROR, could not get spectral power density in field for $self->{field}\n"
		if $spectrum =~ /error/i or ! @spectrum;

	if (defined $file) {
		my ($iomode, $filename) = ($file =~ /^(>*)(.*)$/);
		my $write_header = !((-e $filename) and ($iomode =~/^>>/));
		open OUT, $iomode.$filename or die "Fieldfile.pm::total -> Could not write to $filename\n";
		print OUT $self->_header(type => 'spectrum', direction => '') if $write_header;
		print OUT map "$_\n", @spectrum;
		print OUT "\n";  # empty line to separate from next dataset in file
		close OUT;
	}

	return @spectrum;
}

=item C<fluence(FILE)>

Outputs Energy density (= Intensity integrated over time). 

=cut

sub fluence {
	my ($self, $file) = @_;
	$file =~ s/^>?/>/ if defined $file;

	croak "Fieldfile.pm::fluence -> No such file: $self->{field}" unless -f $self->{field};
	croak "Fieldfile.pm::fluence -> Parameters for fieldfile are not initialized" unless -f $self->{file};
	croak "Fieldfile.pm::fluence -> No output file specified to receive data of command fluence" unless defined $file;
	
	# Fetch data
	my $total = $self->run_fortran('dfl_utils', undef, "$self->{_file} $self->{_field} 0 fluence '".$file."'" );
}

=item C<Stokes(FILE1, FILE2)>

Outputs Stokes vector calculated from two perpendicular linear polarizations. 

=cut

sub Stokes {
	my ($self, $file1, $file2, $nslice, $dn) = @_;
  
	croak "Fieldfile.pm::Stokes -> No such file: $self->{field}" unless -f $self->{field};
	croak "Fieldfile.pm::Stokes -> Parameters for fieldfile are not initialized" unless -f $self->{file};
	croak "Fieldfile.pm::Stokes -> Second linear polarization not specified" unless defined $file1;
	croak "Fieldfile.pm::Stokes -> No output file specified to receive data of command Stokes" unless defined $file2;
	
	$nslice ||= 0; #set default to zero (average over all slices)
	$dn ||= 1;     #set default to one (only one slice if nslice <> 0)
	
	# Fetch data
	my $total = $self->run_fortran('dfl_utils', undef, "$self->{_file} $self->{_field} $nslice stokes $dn '".$file1."' '".$file2."'");
}


sub _header {
	my ($self, %param) = @_; 
	my @par = qw/npoints_x npoints_y nslices lambda mesh_x mesh_y zsep/;
	my @val = $self->get_param(@par);
	$param{$par[$_]} = $val[$_] for 0 .. $#par;
	my $text = join "", map "# $_: $param{$_}\n", sort keys %param;
	return $text;
}	


=item C<txt(FILE, SLICE)>

Write plain text output of the field. If SLICE is 0 all slices are outputed.

=cut

sub txt {
	my ($self, $file, $slice, $mode) = @_;
	$file =~ s/^>?/>/ if defined $file;
	$slice ||= '0';

	croak "Fieldfile.pm::txt -> No such file: $self->{field}" unless -f $self->{field};
	croak "Fieldfile.pm::txt -> Parameters for fieldfile are not initialized" unless -f $self->{file};

	unless (-e $file and $file =~ /^>>/) {
		#write headers
		open IN, $file or die "Fieldfile.pm::txt -> Can not open for writing: $file, $!";
		print IN $self->_header(type=>'field',direction=>'',slice=>$slice);
		close IN;
		$file =~ s/^>+/>>/;	# open next time to append
	}

	# Fetch data
	$self->run_fortran('dfl2txt', $file, "$self->{_file} $self->{_field} $slice $mode");
}

sub run_fortran {
	# run a fortran program catching output
	# output can be a file name or undef, if undef output is returned
	# each input argument is a line
	my ($self, $program, $output, $input) = @_;
	
	$program = defined($USE_MPI{optics}) ? $program.'_mpi' : $program if $program == 'dfl_utils';
	my $exec = Physics::OPC->executable($program);
	croak "Fieldfile.pm::run_fortran -> Could not find executable: $program"
		unless defined $exec;
  # run program using mpi if requested by user, currently only dfl_utils run using MPI
  $exec = $USE_MPI{optics}.' '.$exec if $program == 'dfl_utils_mpi';
	chdir $self->{_dir} if defined $self->{_dir};
  $exec .= " $input\n";   # input parameters are specified as commandline arguments
	print "$exec\n" if $VERBOSE > 9;
	my ($read, $write, $pid, $out);
	if ($input =~ /.*fourier.*/) { # do not capture output, command writes informative data to screen
	  open UTIL, '| '.$exec or croak "Fieldfile.pm::run -> Starting `$exec` failed";
	  close UTIL or croak "Fieldfile.pm::run -> Executing `$exec` failed";
	} else { # capture output
	  my $pid = open2($read, $write, $exec);
	  close $write;
	  if (defined $output) {
	  	my $fh;
	  	if (ref $output) { $fh = $output } # output is a filehandle allready
	  	else { # output is a file name
	  		open $fh, $output or die "Fieldfile.pm::run_fortran -> Could not open for writing: $output\n";
	  	}
	  	while (<$read>) { # Skip prompt(s) for input data
	  		next if /^\s*Enter/;
	  		s/[\n\r]+//; # on win32 to many \r chars !?
	  		print {$fh} $_, "\n";
	  		last;
	  	}
	  	while (<$read>) {
	  		s/[\n\r]+//; # on win32 to many \r chars !?
	  		print {$fh} $_, "\n";
	  	}
	  	close $fh unless ref $output;
	  }
	  else { $out = join '', <$read> }
	  close $read;
    
	  waitpid $pid, 0 if defined $pid; 
	}
	
	chdir $PWD if defined $self->{_dir};
   
	return $out;
}

=back

=head1 COPYRIGHT

Copyright (C) 2006 J.G. Karssenberg, P.J.M. van der Slot, I.V.Volokhine

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; version 2 of the
License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

=cut

1;
