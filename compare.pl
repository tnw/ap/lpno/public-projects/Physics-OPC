#!/usr/bin/perl

open FILE1, shift @ARGV;
open FILE2, shift @ARGV;

while (<FILE1>) {
	chomp;
	s/^\s*|\s*$//g;
	$total = $_;
	$_ = <FILE2>;
	chomp;
	s/^\s*|\s*$//g;
	$fraction = $_;

	print $fraction/$total * 100, "\n";
}

close FILE1;
close FILE2;

